NOTE: generate code to make that scope from *run.Scope (make sure to have the proper order (e.g. x = 1, y = x not y = x, x = 1)) and when running pure, save scope for each scope (program, block)

NOTE: pure blocks can have "compile-time" errors (i.e. errors that happen when running purely)

# [Go Coa](https://coalang.gitlab.io/go-coa)

- [Input Source]
- pkg/parser.Parse
  - Parses the source.
- pkg/parser.RootBlock
  - The AST that the parser generates. *This AST is not preferred; please use `pkg/ast.Program` instead.*
- pkg/convert.ToProgram
  - Converts `pkg/parser.RootBlock` to `pkg/ast.Program`.
- pkg/ast.Program
  - The preferred AST that is tailored for compiling, running and exporting.
- pkg/compile
  - "Compiles" (optimizes) the AST.
- pkg/ast.Program
  - *ditto*
- pkg/run
  - Traverses and runs the AST.
- [Output]

## Notes

- Common bugs
    - `*.Clone` doesn't work (not updated after change)
    - `pkg/encoding.Encode*` doesn't work (not updated after change) 
- Getting refs must not run non-pure code
- `pkg/convert` should convert compound labels
  (e.g. `x\rat.ℤ fib _: {x - 1 fib _ + (x - 2 fib _)}`)
  to non-compound labels
  (e.g. `fib: [x\rat.ℤ]{x - 1 fib _ + (x - 2 fib _)}`)

## Filenames
- `<name>.coa` source file for a pod named `<name>`
- `<name>.coa.json` source in JSON format
- `<name>.c.coa` compiled source (verified and ran purely)
- `<name>.c.coa.json` compiled source in JSON format
