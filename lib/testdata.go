// Package lib provides an embed.FS of the standard library and index, sample gob and JSON files for testing.
package lib

import (
	"embed"
	"fmt"
	"io/fs"
)

//go:embed base core index
var StaticFS embed.FS

//go:embed *.coa.gob
var gobs embed.FS
var Gobs = map[string][]byte{}

//go:embed *.coa.json
var jsons embed.FS
var JSONs = map[string][]byte{}

func init() {
	matches, err := fs.Glob(gobs, "*.coa.gob")
	if err != nil {
		panic(err)
	}
	for _, match := range matches {
		if match[0] == '.' {
			continue
		}
		b, err := gobs.ReadFile(match)
		if err != nil {
			panic(fmt.Errorf("failed to open file: %w", err))
		}
		Gobs[match] = b
	}

	matches, err = fs.Glob(jsons, "*.coa.json")
	if err != nil {
		panic(err)
	}
	for _, match := range matches {
		if match[0] == '.' {
			continue
		}
		b, err := jsons.ReadFile(match)
		if err != nil {
			panic(fmt.Errorf("failed to open file: %w", err))
		}
		JSONs[match] = b
	}
}
