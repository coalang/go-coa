package main

import (
	"fmt"
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils/encoding"
	scope2 "gitlab.com/coalang/go-coa/pkg/utils/scope"
	"os"
)

const src = `b: _ ..io.inln _ int _, p: 5 * b - 400, level: b match [{. > 100}: "-1", {. < 100}: "1", "0"], "(p)\n(level)\n" ..io.out _`

func readProgram() (ast.Program, error) {
	var program ast.Program
	var err error
	program, err = encoding.BytesDecodeProgram("ccc21j1.coa", []byte(src))
	if err != nil {
		return ast.Program{}, err
	}
	return program, nil
}

func newScope(pure ast.Pureness) (*eval.Scope, error) {
	return scope2.NewScopeWithStd(ast.CtxFromCaller(2), lib.StaticFS, pure, [][]ast.ID{})
}

func evalProgram(program ast.Program) (ast.Program, error) {
	scope, err := newScope(ast.Impure)
	if err != nil {
		return ast.Program{}, err
	}
	program, err = scope.Program(program)
	if err != nil {
		return ast.Program{}, err
	}
	return program, nil
}

func main2() error {
	program, err := readProgram()
	if err != nil {
		return err
	}
	_, err = evalProgram(program)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	err := main2()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
