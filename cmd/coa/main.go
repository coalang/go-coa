package main

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	encoding2 "gitlab.com/coalang/go-coa/pkg/encoding"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils/encoding"
	"gitlab.com/coalang/go-coa/pkg/utils/path"
	scope2 "gitlab.com/coalang/go-coa/pkg/utils/scope"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
)

func args() (string, string, fs.FileMode, bool, error) {
	flag.Usage = func() {
		_, _ = fmt.Fprintf(
			flag.CommandLine.Output(),
			"%s IN [-o OUT [-op OUT_PERM]] [-p PURE]\n",
			os.Args[0],
		)
		flag.PrintDefaults()
	}

	var (
		inPath  string
		outPath string
		outPerm uint
		pure    bool
	)
	flag.StringVar(&outPath, "o", "",
		"Write the resulting program to OUT instead of stdout. This overwrites the file. "+
			"If ending with .*, writes using all available output formats.")
	flag.UintVar(&outPerm, "op", 0644,
		"Set the file permission bits to OUT_PERM instead of 0644 (rw-r--r--).")
	flag.BoolVar(&pure, "p", false,
		"Run the program using only pure evaluation.")
	flag.Parse()

	// get inPath
	switch flag.NArg() {
	case 0:
		return "", "", 0, false, errors.New("IN is required")
	case 1:
		inPath = flag.Arg(0)
	default:
		return "", "", 0, false, errors.New("can only run one file")
	}

	// verify outPerm
	if outPath == "" && outPerm != 0644 {
		log.Fatalf("OUT_PATH must be set to use OUT_PERM.")
	}
	mode := fs.FileMode(outPerm)
	if mode.IsDir() {
		return "", "", 0, false, errors.New("OUT_PERM must not be directory")
	} else if !mode.IsRegular() {
		return "", "", 0, false, errors.New("OUT_PERM must not be an irregular file")
	}

	if pure && path.GetStatus(outPath) != path.ImpureOnly {
		return "", "", 0, false, fmt.Errorf("OUT is a source file (%s), but PURE is true", outPath)
	}

	return inPath, outPath, fs.FileMode(outPerm), pure, nil
}

func hint(inPath, outPath string, pure ast.Pureness) string {
	arrow := fmt.Sprintf("→ (%s)", ast.Pure)
	if pure == ast.Impure {
		arrow = fmt.Sprintf("→ (%s) → (%s)", ast.Pure, ast.Impure)
	}
	return fmt.Sprintf(
		"%s %s %s",
		path.GetType(inPath),
		arrow,
		path.GetType(outPath),
	)
}

func readProgram(inPath string) (ast.Program, error) {
	var program ast.Program
	var data []byte
	var err error

	if inPath == "-" {
		return ast.Program{}, errors.New("reading from stdin is unsupported")
	}
	data, err = os.ReadFile(inPath)
	if err != nil {
		return ast.Program{}, err
	}

	program, err = encoding.BytesDecodeProgram(inPath, data)
	if err != nil {
		return ast.Program{}, err
	}
	return program, nil
}

func newScope(pure ast.Pureness) (*eval.Scope, error) {
	return scope2.NewScopeWithStd(ast.CtxFromCaller(2), lib.StaticFS, pure, [][]ast.ID{})
}

func evalProgram(status path.Status, program ast.Program, pure bool) (ast.Program, error) {
	//{
	//	switch status {
	//	case path.Any, path.Unknown:
	//		scope, err := newScope(ast.Pure)
	//		if err != nil {
	//			return ast.Program{}, err
	//		}
	//		program, err = scope.Program(program)
	//		if err != nil {
	//			return ast.Program{}, err
	//		}
	//	case path.ImpureOnly:
	//	default:
	//		return ast.Program{}, fmt.Errorf("%w status %s", errs.ErrInvalid, status)
	//	}
	//}

	if !pure {
		scope, err := newScope(ast.Impure)
		if err != nil {
			return ast.Program{}, err
		}
		program, err = scope.Program(program)
		if err != nil {
			return ast.Program{}, err
		}
	}
	return program, nil
}

func writeProgram(outPath string, outPerm fs.FileMode, program ast.Program) error {
	if outPath == "" {
		return nil
	}

	var data []byte
	var err error
	if strings.HasSuffix(outPath, ".*") {
		outPath = strings.TrimSuffix(outPath, ".*")
		data, err = encoding.BytesEncodeProgram(path.JSON, program)
		if err != nil {
			return err
		}
		if outPath == "-" {
			_, err := os.Stdout.Write(data)
			if err != nil {
				return err
			}
		} else {
			err = ioutil.WriteFile(outPath+encoding2.ExtJSON, data, outPerm)
			if err != nil {
				return err
			}
		}
		data, err = encoding.BytesEncodeProgram(path.Gob, program)
		if err != nil {
			return err
		}
		if outPath == "-" {
			_, err := os.Stdout.Write(data)
			if err != nil {
				return err
			}
		} else {
			err = ioutil.WriteFile(outPath+encoding2.ExtGob, data, outPerm)
			if err != nil {
				return err
			}
		}
	}

	data, err = encoding.BytesEncodeProgram(path.GetEnc(outPath), program)
	if err != nil {
		return err
	}
	if outPath == "-" {
		_, err := os.Stdout.Write(data)
		if err != nil {
			return err
		}
	} else {
		err = ioutil.WriteFile(outPath, data, outPerm)
		if err != nil {
			return err
		}
	}
	return nil
}

func main2() error {
	inPath, outPath, outPerm, pure, err := args()
	if err != nil {
		flag.Usage()
		return err
	}
	//_, _ = fmt.Fprintln(os.Stderr, hint(inPath, outPath, ast.Pureness(pure)))
	program, err := readProgram(inPath)
	if err != nil {
		return err
	}
	//fmt.Println(program.C, inPath)
	program, err = evalProgram(path.GetStatus(inPath), program, pure)
	if err != nil {
		return err
	}
	err = writeProgram(outPath, outPerm, program)
	if err != nil {
		return err
	}
	return nil
}

func Stack() []byte {
	buf := make([]byte, 1024)
	for {
		n := runtime.Stack(buf, true)
		if n < len(buf) {
			return buf[:n]
		}
		buf = make([]byte, 2*len(buf))
	}
}

func main() {
	go func() {
		//for range time.Tick(1 * time.Second) {
		//	log.Printf("\n\n\n")
		//	log.Printf(string(Stack()))
		//}
	}()

	err := main2()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
