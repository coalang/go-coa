package main

import (
	"bytes"
	_ "embed"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/convert"
	lib22 "gitlab.com/coalang/go-coa/pkg/astproto/lib"
	"gitlab.com/coalang/go-coa/pkg/astproto/lib/builtin"
	"gitlab.com/coalang/go-coa/pkg/astproto/render"
	"gitlab.com/coalang/go-coa/pkg/astproto/run"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/parser"
)

//go:embed src.coa
var src []byte

func main() {
	parse, err := parser.Parse("src.coa", bytes.NewBuffer(src))
	if err != nil {
		panic(err)
	}
	program, err := convert.RootBlockToProgram(*parse)
	if err != nil {
		panic(err)
	}
	r, err := run.NewRunner(lib22.StaticFS, false)
	if err != nil {
		panic(err)
	}
	builtin.Apply(r)
	core, err := r.Pod(&astproto.Pod{Path: run.CorePath})
	if err != nil {
		panic(err)
	}
	r.Set(ast.IDCore, core)
	index, err := r.Pod(&astproto.Pod{Path: run.IndexPath})
	if err != nil {
		panic(err)
	}
	r.Set(ast.IDIndex, index)

	program, err = r.Program(program)
	if err != nil {
		panic(err)
	}
	fmt.Println(render.Program(program))
}
