package main

import (
	"bytes"
	_ "embed"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/lib"
	"gitlab.com/coalang/go-coa/pkg/astnext/lib/builtin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/parser"
	"log"
)

//go:embed src.coa
var src []byte

func init() {
	log.SetFlags(0)
}

func main() {
	parse, err := parser.Parse("src.coa", bytes.NewBuffer(src))
	if err != nil {
		panic(err)
	}
	program, err := astnext.ConvertRootBlockToPriority(*parse)
	if err != nil {
		panic(err)
	}
	runner, err := astnext.NewRunnerWithBase(lib.StaticFS, false, builtin.Apply)
	if err != nil {
		panic(err)
	}

	eval, err := program.Eval(runner)
	if err != nil {
		panic(err)
	}
	fmt.Println(eval.CoaString())
}
