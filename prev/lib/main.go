package lib

import (
	"gitlab.com/coalang/go-coa/prev/ast"
	"gitlab.com/coalang/go-coa/prev/runtime"
)

func Connect(src, dst runtime.Pipe) error {
	var received ast.Node
	var err error
	for !src.Closed() && !dst.Closed() {
		received, err = src.Receive()
		if err != nil {
			return err
		}
		err := dst.Send(received)
		if err != nil {
			return err
		}
	}
	if !src.Closed() {
		err = src.Close()
		if err != nil {
			return err
		}
	}
	if !dst.Closed() {
		err = dst.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
