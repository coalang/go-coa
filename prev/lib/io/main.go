package io

import (
	"gitlab.com/coalang/go-coa/prev/runtime"
	"os"
)

var (
	Stdin  = runtime.NewReadWriteCloserWrapper(os.Stdin)
	Stdout = runtime.NewReadWriteCloserWrapper(os.Stdout)
	Stderr = runtime.NewReadWriteCloserWrapper(os.Stderr)
)
