package runtime

import "gitlab.com/coalang/go-coa/prev/ast"

type Pipe interface {
	ast.Node
	Send(ast.Node) error
	Receive() (ast.Node, error)
	Close() error
	Closed() bool
}

