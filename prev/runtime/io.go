package runtime

import (
	"fmt"
	"gitlab.com/coalang/go-coa/prev/ast"
	"io"
	"math/big"
	"sync"
)

type ReadWriteCloserWrapper struct {
	rwc    io.ReadWriteCloser
	closed bool
	lock   sync.RWMutex
}

func NewReadWriteCloserWrapper(rwc io.ReadWriteCloser) *ReadWriteCloserWrapper {
	return &ReadWriteCloserWrapper{rwc: rwc}
}

var _ Pipe = &ReadWriteCloserWrapper{}

func (w *ReadWriteCloserWrapper) CoaString() string {
	return fmt.Sprintf("<ReadWriteCloserWrapper for %p>", w.rwc)
}

func (w *ReadWriteCloserWrapper) Type() ast.Type {
	return ast.Nothing
}

func (w *ReadWriteCloserWrapper) Eval() (ast.Node, error) {
	return w, nil
}

func (w *ReadWriteCloserWrapper) Send(node ast.Node) error {
	w.lock.Lock()
	defer w.lock.Unlock()
	_, err := w.rwc.Write([]byte{byte(node.(ast.Int).Int.Int64())})
	return err
}

func (w *ReadWriteCloserWrapper) Receive() (ast.Node, error) {
	w.lock.Lock()
	defer w.lock.Unlock()
	b := []byte{0}
	_, err := w.rwc.Read(b)
	if err != nil {
		return nil, err
	}
	return ast.Int{Int: new(big.Int).SetInt64(int64(b[0]))}, nil
}

func (w *ReadWriteCloserWrapper) Close() error {
	w.lock.Lock()
	defer w.lock.Unlock()
	err := w.rwc.Close()
	if err != nil {
		return err
	}
	w.closed = true
	return nil
}

func (w *ReadWriteCloserWrapper) Closed() bool {
	w.lock.RLock()
	defer w.lock.RUnlock()
	return w.closed
}
