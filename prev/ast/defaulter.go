package ast

var Defaulter defaulter

type defaulter struct{}

func (d defaulter) CoaString() string {
	return "_"
}

func (d defaulter) Type() Type {
	return Nothing
}

func (d defaulter) Eval() (Node, error) {
	return d, nil
}

var _ Node = defaulter{}
