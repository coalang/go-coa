package ast

type PipeType struct {
	In  Type
	Out Type
}
