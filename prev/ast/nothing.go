package ast

import "gitlab.com/coalang/go-coa/pkg/errs"

var Nothing nothing

type nothing struct{}

var _ Type = nothing{}

func (n nothing) CoaString() string {
	return "__"
}

func (n nothing) Type() Type {
	return n
}

func (n nothing) Eval() (Node, error) {
	return n, nil
}

func (n nothing) Coerce(node Node) (Node, error) {
	_, ok := node.(nothing)
	if !ok {
		return nil, errs.ErrTypeMismatch
	}
	return n, nil
}
