package ast

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"reflect"
)

func Pure(i interface{}) bool {
	if p, ok := i.(astnext.PureCheckable); ok {
		return p.Pure()
	} else {
		return true
	}
}

func Origin(i interface{}) origin.Origin {
	if o, ok := i.(origin.HasOrigin); ok {
		return o.Origin()
	} else {
		return origin.Origin{}
	}
}

func Clone(n Node) Node {
	if c, ok := n.(Cloneable); ok {
		return c.Clone()
	} else {
		return n
	}
}

func Hash(d Digestable) hash.Hash {
	return hash.Hash{
		Type:   reflect.TypeOf(d).String(),
		Digest: d.Digest(),
	}
}

type Cloneable interface {
	Clone() Node
}

type Digestable interface {
	Digest() [64]byte
}

type Node interface {
	astnext.CoaStringer
	Type() Type
	Eval() (Node, error)
}

type Type interface {
	Node
	Coerce(Node) (Node, error)
}

//type Number struct {
//	Rat *big.Rat
//}
//
//var _ Node = Number{}

//func (n Number) MarshalJSON() ([]byte, error) {
//	rat, err := n.Rat.MarshalText()
//	if err != nil {
//		return nil, err
//	}
//	return json.Marshal([2]interface{}{
//		n.Origin,
//		rat,
//	})
//}
//
//func (n Number) UnmarshalJSON(bytes []byte) error {
//	data := [2]interface{}{}
//	err := json.Unmarshal(bytes, &data)
//	if err != nil {
//		return err
//	}
//	{
//		o, ok := data[0].(origin.Origin)
//		if !ok {
//			return fmt.Errorf("expected %T, not %T", n.Origin, data[0])
//		}
//		n.Origin = o
//	}
//	{
//		data, ok := data[1].([]byte)
//		if !ok {
//			return fmt.Errorf("expected %T, not %T", data, data[1])
//		}
//		err = n.Rat.UnmarshalText(data)
//		if err != nil {
//			return err
//		}
//	}
//	return nil
//}
