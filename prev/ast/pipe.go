package ast

type Pipe struct {
	Type PipeType
}

var _ Node = &Pipe{}
