package ast

import "math/big"

type Int struct {
	Int *big.Int
}

var _ Node = Int{}

func (i Int) CoaString() string {
	return i.Int.String()
}

func (i Int) Type() Type {
	return Nothing
}

func (i Int) Eval() (Node, error) {
	return i, nil
}
