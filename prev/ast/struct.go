package ast

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"reflect"
)

type Struct struct {
	origin.Origin
	Fields []StructField
}

func (s Struct) ApplyToValue(value reflect.Value) (reflect.Value, error) {
	fields := make([]string, value.NumField())
	for i := 0; i < value.NumField(); i++ {
		fields[i] = value.Type().Field(i).Tag.Get("coa-name")
	}
	{
		already := map[string]int{}
		var ok bool
		var j int
		for i, field := range fields {
			if j, ok = already[field]; ok {
				return reflect.Value{}, fmt.Errorf("field %d and %d have equal coa-names", i, j)
			}
			already[field] = i
		}
	}
	panic("not implemented")
}

type StructField struct {
	origin.Origin
	Key     string
	Value   Type
	Default Node
}
