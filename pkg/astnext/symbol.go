package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"strings"
)

type Symbol struct {
	O  origin.Origin
	ID ID
}

var _ Node = Symbol{}

func (s Symbol) Origin() origin.Origin      { return s.O }
func (s Symbol) Hash() hash.Hash            { return hash.Hash{Type: "Symbol", Digest: hash.Digest(s.ID)} }
func (s Symbol) Pure() bool                 { return true }
func (s Symbol) Valid() bool                { return strings.ContainsAny(s.ID, lexer.SymbolDisallowedChars) }
func (s Symbol) Name() string               { return " " }
func (s Symbol) CoaString() string          { return "#" + s.ID }
func (s Symbol) Clone() Node                { return s }
func (s Symbol) Type() Type                 { return SymbolType }
func (s Symbol) Eval(*Runner) (Node, error) { return s, nil }
