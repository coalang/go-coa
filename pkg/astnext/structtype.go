package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

type StructType struct {
	O        origin.Origin
	Generics GenericsType
	Pairs    []StructTypePair
}

var _ Type = StructType{}

func (s StructType) Origin() origin.Origin { return s.O }

func (s StructType) Hash() hash.Hash {
	pairs := ""
	for _, pair := range s.Pairs {
		pairs += pair.Key + hash.DigestToString(pair.Value.Hash().DigestAll()) + hash.DigestToString(pair.Default.Hash().DigestAll())
	}
	return hash.Hash{
		Type:   "StructType",
		Digest: hash.Digest(hash.DigestToString(s.Generics.Hash().DigestAll()) + pairs),
	}
}

func (s StructType) Pure() bool {
	if !s.Generics.Pure() {
		return false
	}
	for _, pair := range s.Pairs {
		if !(pair.Value.Pure() && pair.Default.Pure()) {
			return false
		}
	}
	return true
}

func (s StructType) Valid() bool {
	if !s.Generics.Valid() {
		return false
	}
	for _, pair := range s.Pairs {
		if !(pair.Value.Valid() && pair.Default.Valid()) {
			return false
		}
	}
	return true
}

func (s StructType) Name() string { return "" }

func (s StructType) CoaString() string {
	content := make([]Node, len(s.Pairs))
	for i, pair := range s.Pairs {
		content[i] = Label{
			Er: Cast{
				Ee: rendered(pair.Key),
				Er: pair.Value,
			}.CoaString(),
			Ee: pair.Default,
		}
	}
	return coaStringGroupWithGenerics("[", lexer.SepStructTypeSrc, "]", CoaString(s.Generics), content)
}

func (s StructType) Clone() Node {
	s2 := StructType{
		O:        s.O,
		Generics: s.Generics.Clone(),
		Pairs:    make([]StructTypePair, len(s.Pairs)),
	}
	for i, pair := range s.Pairs {
		s2.Pairs[i] = StructTypePair{
			Key:     pair.Key,
			Value:   CloneType(pair.Value),
			Default: Clone(pair.Default),
		}
	}
	return s2
}

func (s StructType) Type() Type { return RootType }

func (s StructType) Eval(runner *Runner) (Node, error) {
	// FIXME: generics must not be evaluated in
	var err error
	s.Generics, err = s.Generics.Eval(runner)
	if err != nil {
		return nil, err
	}
	inner := runner.inherit(Frame{
		O:      s.O,
		Reason: "structtype",
	})
	s.Generics.Apply(inner)

	for i, pair := range s.Pairs {
		s.Pairs[i].Default, err = inner.Eval(pair.Default)
		if err != nil {
			return nil, err
		}
		s.Pairs[i].Value, err = inner.EvalType(pair.Value)
		if err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (s StructType) Coercible(runner *Runner, from Type) (bool, error) {
	s2, ok := from.(StructType)
	if !ok {
		return false, nil
	}
	generics, err := s.Generics.Coercible(runner, s2.Generics)
	if err != nil {
		return false, err
	}
	if !generics {
		return false, nil
	}
	for i, pair := range s.Pairs{
		if pair.Key != s.Pairs[i].Key {
			return false, nil
		}
		ok, err = pair.Default.Type().Coercible(runner, s2.Pairs[i].Default.Type())
		if err != nil {
			return false, err
		}
		if !ok {
			return false, nil
		}
		ok, err = pair.Value.Type().Coercible(runner, s2.Pairs[i].Value.Type())
		if err != nil {
			return false, err
		}
		if !ok {
			return false, nil
		}
	}
	return true, nil
}

func (s StructType) Coerce(runner *Runner, from Node) (Node, error) {
	s2 := from.(StructType)
	var err error
	s.Generics, err = s.Generics.Coerce(runner, s2.Generics)
	if err != nil {
		return nil, err
	}
	var value Node
	for i, pair := range s.Pairs {
		s.Pairs[i].Default, err = pair.Default.Type().Coerce(runner, s2.Pairs[i].Default)
		if err != nil {
			return nil, err
		}
		value, err = pair.Value.Type().Coerce(runner, s2.Pairs[i].Value)
		if err != nil {
			return nil, err
		}
		s.Pairs[i].Value = value.(Type)
	}
	return s2, nil
}

func (s StructType) Get(key string) int {
	for i, pair := range s.Pairs {
		if pair.Key == key {
			return i
		}
	}
	return -1
}

type StructTypePair struct {
	Key     string
	Value   Type
	Default Node
}
