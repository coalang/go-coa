package astnext

import (
	"github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/ast"
	lexer3 "gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"log"
	"strconv"
	"strings"
	"unicode"
)

func convertToOrigin(pos, endPos lexer.Position) origin.Origin {
	return origin.Origin{
		Filename: pos.Filename,
		Start:    [3]int32{int32(pos.Line), int32(pos.Column), int32(pos.Offset)},
		End:      [3]int32{int32(endPos.Line), int32(endPos.Column), int32(endPos.Offset)},
	}
}

func ConvertRootBlockToPriority(block ast.RootBlock) (Generator, error) {
	o := convertToOrigin(block.Pos, block.EndPos)
	if block.Exprs == nil {
		return Generator{
			O: o,
		}, nil
	}
	nodes, err := convertExprsToNodes(block.Exprs)
	if err != nil {
		return Generator{}, err
	}
	return Generator{
		O:       o,
		Content: nodes,
		Mode:    ModeProgram,
	}, nil
}

func convertExprsToNodesBracked(exprs *ast.Exprs) ([]Node, []Node, error) {
	if exprs == nil {
		return nil, nil, nil
	}
	keys := make([]Node, 0, 1+len(exprs.AfterExprs))
	values := make([]Node, 0, 1+len(exprs.AfterExprs))
	var expr *ast.Expr
	var node Node
	var err error
	isKeyed := exprs.FirstExpr.Colon != ""
	for i := 0; i < len(exprs.AfterExprs)+1; i++ {
		if i == 0 {
			expr = exprs.FirstExpr
		} else {
			expr = exprs.AfterExprs[i-1].Expr
		}

		if (isKeyed && expr.Colon == "") || (!isKeyed && expr.Colon != "") {
			return nil, nil, convertToOrigin(expr.Pos, expr.EndPos).Errorf("cannot mix keyed and unkeyed nodes")
		}

		if isKeyed {
			node, err = convertExprNoColonToNode(expr.First)
			if err != nil {
				return nil, nil, err
			}
			if node != nil {
				keys = append(keys, node)
			}
			node, err = convertExprNoColonToNode(expr.Second)
			if err != nil {
				return nil, nil, err
			}
			if node != nil {
				values = append(values, node)
			}
		} else {
			node, err = convertExprToNode(expr)
			if err != nil {
				return nil, nil, err
			}
			if node != nil {
				keys = append(keys, node)
			}
		}
	}
	if isKeyed {
		return nil, values, nil
	} else {
		return keys, values, nil
	}
}

func convertExprsToNodes(exprs *ast.Exprs) ([]Node, error) {
	if exprs == nil {
		return nil, nil
	}
	nodes := make([]Node, 0, 1+len(exprs.AfterExprs))
	var expr *ast.Expr
	var node Node
	var err error
	for i := 0; i < len(exprs.AfterExprs)+1; i++ {
		if i == 0 {
			expr = exprs.FirstExpr
		} else {
			expr = exprs.AfterExprs[i-1].Expr
		}

		node, err = convertExprToNode(expr)
		if err != nil {
			return nil, err
		}
		if node != nil {
			nodes = append(nodes, node)
		}
	}
	return nodes, nil
}

func convertExprToNode(expr *ast.Expr) (Node, error) {
	if expr == nil {
		return nil, nil
	}

	o := convertToOrigin(expr.Pos, expr.EndPos)
	if expr.First == nil {
		return nil, o.Errorf("expr.First is blank")
	}
	if expr.Second != nil && expr.Colon == "" {
		return nil, o.Errorf("expr.Second is not blank but expr.Colon is blank")
	}
	if expr.Colon == "" {
		// expr
		return convertExprNoColonToNode(expr.First)
	}

	// labeler: labelee
	labeler, err := convertExprNoColonToNode(expr.First)
	if err != nil {
		return nil, err
	}
	labelerRef, ok := labeler.(Ref)
	if !ok {
		return nil, labeler.Origin().Errorf("%w: labeler must be Ref", errs.ErrTypeMismatch)
	}
	if len(labelerRef.IDs) != 1 {
		return nil, labeler.Origin().Errorf("length of labeler ref must be 1, not %d", len(labelerRef.IDs))
	}

	labelee, err := convertExprNoColonToNode(expr.Second)
	if err != nil {
		return nil, err
	}

	return Label{
		O:  o,
		Er: labelerRef.IDs[0],
		Ee: labelee,
	}, nil

}

func convertExprNoColonToNode(exprNC *ast.ExprNoColon) (Node, error) {
	if exprNC == nil {
		return nil, nil
	}
	node, err := convertObjToNode(&exprNC.Obj)
	if err != nil {
		return nil, err
	}
	if len(exprNC.Suffixes) == 0 {
		return node, nil
	}
	var callee, arg1 Node
	for _, suffix := range exprNC.Suffixes {
		callee, err = convertObjToNode(&suffix.Callee)
		if err != nil {
			return nil, err
		}
		arg1, err = convertObjToNode(&suffix.Arg1)
		if err != nil {
			return nil, err
		}
		node = Call{
			O:    convertToOrigin(suffix.Pos, suffix.EndPos),
			Arg1: node,
			Arg2: arg1,
			Ee:   callee,
		}
	}
	return node, nil
}

func convertObjToNode(obj *ast.Obj) (Node, error) {
	if obj == nil {
		return nil, nil
	}
	o := convertToOrigin(obj.Pos, obj.EndPos)
	if obj.Second != nil && obj.BSlash == "" {
		return nil, o.Errorf("obj.Second is not blank but obj.BSlash is blank")
	}
	first, err := convertObjRawToNode(obj.First)
	if err != nil {
		return nil, err
	}
	if obj.BSlash == "" {
		return first, nil
	}
	second, err := convertObjRawToNode(obj.Second)
	if err != nil {
		return nil, err
	}
	second_, ok := second.(Type)
	if !ok {
		return nil, second.Origin().Errorf("%w: obj.Second must be type", errs.ErrInvalid)
	}
	return Cast{
		O:  o,
		Ee: first,
		Er: second_,
	}, nil
}

func convertObjRawToNode(objRaw *ast.ObjRaw) (Node, error) {
	if objRaw == nil {
		return nil, nil
	}
	o := convertToOrigin(objRaw.Pos, objRaw.EndPos)
	switch {
	case objRaw.Text != "":
		unquote, err := strconv.Unquote(objRaw.Text)
		if err != nil {
			return nil, o.Errorf("unquote: %w", err)
		}
		return ListFromString(o, unquote), nil
	case objRaw.Symbol != "":
		if objRaw.Symbol[1] == '#' {
			id, err := utils.UUIDFrom(objRaw.Symbol[2:])
			if err != nil {
				return nil, err
			}
			return External{
				O:  o,
				ID: id,
			}, nil
		}
		return Symbol{
			O:  o,
			ID: objRaw.Symbol[1:],
		}, nil
	case objRaw.Rune != "":
		unquote, err := strconv.Unquote(objRaw.Rune)
		if err != nil {
			return nil, o.Errorf("unquote: %w", err)
		}
		var first rune
		for _, r := range unquote {
			if r != 0 {
				return nil, o.Errorf("2+ runes in rune literal")
			}
			first = r
		}
		return NumFromRune(o, first), nil
	case objRaw.Ref != "":
		if convertIsNum(objRaw.Ref) {
			if convertIsNumType(objRaw.Ref) {
				return NumType{
					O: o,
					Unit: Symbol{
						O:  o,
						ID: objRaw.Ref[2:],
					},
				}, nil
			}
			return Num{
				O:       o,
				Content: objRaw.Ref,
				Flag:    NumFlagNormal,
			}, nil
		}
		if strings.ContainsAny(objRaw.Ref, lexer3.RefDisallowedChars) {
			return nil, o.Errorf("%w: ref contains disallowed chars", errs.ErrInvalid)
		}
		return Ref{
			O:   o,
			IDs: strings.Split(objRaw.Ref, lexer3.DotSrc),
		}, nil
	case objRaw.Bracked != nil:
		return convertBrackedToNode(*objRaw.Bracked)
	case objRaw.Braced != nil:
		return convertExprsToFunc(o, objRaw.Braced.Content)
	case objRaw.Parened != nil:
		nodes, err := convertExprsToNodes(objRaw.Parened.Content)
		if err != nil {
			return nil, err
		}
		return Generator{
			O:       o,
			Content: nodes,
		}, nil
	default:
		return nil, o.Errorf("objRaw is blank for all")
	}
}

func convertBrackedToNode(bracked ast.Bracked) (Node, error) {
	o := convertToOrigin(bracked.Pos, bracked.EndPos)
	var gn GenericsNode
	nodes, err := convertExprsToNodes(bracked.First)
	if err != nil {
		return nil, err
	}
	if bracked.Sep != "" {
		// not a list or map (list or map has no generics)
		gn, err = convertNodesToGenericsNode(nodes)
		if err != nil {
			return nil, err
		}
		nodes, err = convertExprsToNodes(bracked.Second)
		if err != nil {
			return nil, err
		}
	}
	log.Println("convert bracked", o, bracked.Sep, gn)

	node := Generator{
		O:        o,
		Generics: gn,
		Content:  nodes,
	}

	switch bracked.Sep {
	case lexer3.SepStructSrc:
		node.Mode = ModeStruct
	case lexer3.SepStructTypeSrc:
		node.Mode = ModeStructType
	case lexer3.SepEnumSrc:
		node.Mode = ModeEnum
	case lexer3.SepInterSrc:
		node.Mode = ModeInter
	case lexer3.SepCallTypeSrc:
		node.Mode = ModeCallType
	case lexer3.SepListOrMapSrc:
		node.Mode = ModeListOrMap
	default:
		return nil, o.Errorf("%w: Bracked.Sep: %s", errs.ErrInvalid, bracked.Sep)
	}
	return node, nil
}

//func convertExprsToNodesWithKeys(exprs *ast.Exprs) (keys []Node, values []Node, err error) {
//	if exprs == nil {
//		return nil, nil, nil
//	}
//	keys, values = make([]Node, exprs.Len()), make([]Node, exprs.Len())
//	var expr ast.Expr
//	for i := 0; i < exprs.Len(); i++ {
//		expr = exprs.Index(i)
//		if !expr.IsLabel() {
//			return nil, nil, convertToOrigin(expr.Pos, expr.EndPos).Errorf("must be a label")
//		}
//		keys[i], err = convertExprNoColonToNode(expr.First)
//		if err != nil {
//			return nil, nil, err
//		}
//		values[i], err = convertExprNoColonToNode(expr.Second)
//		if err != nil {
//			return nil, nil, err
//		}
//	}
//	return
//}
//
//func convertExprsToNodesWithoutKeys(exprs *ast.Exprs) (values []Node, err error) {
//	if exprs == nil {
//		return nil, nil
//	}
//	values = make([]Node, exprs.Len())
//	var expr ast.Expr
//	for i := 0; i < exprs.Len(); i++ {
//		expr = exprs.Index(i)
//		if expr.IsLabel() {
//			return nil, convertToOrigin(expr.Pos, expr.EndPos).Errorf("must not be a label")
//		}
//		values[i], err = convertExprNoColonToNode(expr.First)
//		if err != nil {
//			return nil, err
//		}
//	}
//	return
//}
//
//func convertExprsToNodesWithOrWithoutKeys(exprs *ast.Exprs) (keys []Node, values []Node, err error) {
//	var withKeys bool
//	var withKeysSet bool
//	for i := 0; i < exprs.Len(); i++ {
//		expr := exprs.Index(i)
//		if !withKeysSet {
//			withKeys = expr.IsLabel()
//		} else {
//			if withKeys != expr.IsLabel() {
//				return nil, nil, convertToOrigin(expr.Pos, expr.EndPos).Errorf("must either be with or without keys")
//			}
//		}
//	}
//
//	if withKeys {
//		return convertExprsToNodesWithKeys(exprs)
//	} else {
//		values, err = convertExprsToNodesWithoutKeys(exprs)
//		if err != nil {
//			return nil, nil, err
//		}
//		return
//	}
//}

//func convertToStruct(gn GenericsNode, bracked ast.Bracked) (Struct, error) {
//	o := convertToOrigin(bracked.Pos, bracked.EndPos)
//	keys, values, err := convertExprsToNodesWithOrWithoutKeys(bracked.Second)
//	if err != nil {
//		return Struct{}, err
//	}
//	if keys == nil {
//		// positional
//		return Struct{
//			O:        o,
//			Generics: gn,
//			Values:   values,
//		}, nil
//	} else {
//		// keyed
//		return Struct{}, o.Errorf("%w: keyed structs", errs.ErrNotImplemented)
//	}
//}
//
//func convertToStructType(gt GenericsType, bracked ast.Bracked) (StructType, error) {
//	o := convertToOrigin(bracked.Pos, bracked.EndPos)
//}
//
//func convertToCallType(gt GenericsType, bracked ast.Bracked) (CallType, error) {
//	panic("not implemented")
//}
//
//func convertToList(o origin.Origin, bracked ast.Bracked) (List, error) {
//	nodes, err := convertExprsToNodes(bracked.Second)
//	if err != nil {
//		return List{}, err
//	}
//	return List{
//		O:       o,
//		Content: nodes,
//	}, nil
//}

func convertExprsToFunc(o origin.Origin, exprs *ast.Exprs) (Node, error) {
	nodes, err := convertExprsToNodes(exprs)
	if err != nil {
		return nil, err
	}
	return convertNodesToFunc(o, nodes), nil
}

func convertNodesToGenericsNode(nodes []Node) (GenericsNode, error) {
	if nodes == nil {
		return nil, nil
	}
	gn := GenericsNode{}
	for _, node := range nodes {
		if label, ok := node.(Label); ok {
			gn[label.Er] = label.Ee
		} else {
			return nil, node.Origin().Errorf("%w: must be a type (generics convert)", errs.ErrInvalid)
		}
	}
	return gn, nil
}

func convertGenericsNodeToGenericsType(gn GenericsNode) (GenericsType, error) {
	if gn == nil {
		return nil, nil
	}
	gt := GenericsType{}
	var t Type
	var ok bool
	var cast Cast
	for key, value := range gn {
		t, ok = value.(Type)
		if !ok {
			if cast, ok = value.(Cast); ok {
				gt[key] = cast.Er
				continue
			}
			return nil, value.Origin().Errorf("must be a type (generics): %T", value)
		}
		gt[key] = t
	}
	return gt, nil
}

func convertNodesToFunc(o origin.Origin, nodes []Node) Func {
	return Func{
		O:       o,
		Content: nodes,
	}
}

func convertIsNum(ref string) bool {
	var first rune
	for _, r := range ref {
		first = r
		break
	}
	return unicode.IsDigit(first)
}

func convertIsNumType(ref string) bool {
	return len(ref) > 2 && ref[0] == '0' && ref[1] == '#'
}
