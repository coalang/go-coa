package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"strings"
)

// ID represents a valid Coa identifier.
type ID = string

func idValid(id ID) bool {
	return strings.ContainsAny(id, lexer.RefDisallowedChars)
}
