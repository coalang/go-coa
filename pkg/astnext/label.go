package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"strings"
)

type Label struct {
	O  origin.Origin
	Er string
	Ee Node
}

var _ Node = Label{}

func (l Label) Origin() origin.Origin { return l.O }

func (l Label) Hash() hash.Hash {
	return hash.Hash{
		Type:   "Label",
		Digest: hash.Digest(hash.DigestToString(hash.Digest(l.Er)) + hash.DigestToString(l.Ee.Hash().DigestAll())),
	}
}

func (l Label) Pure() bool { return l.Ee.Pure() }

func (l Label) Valid() bool {
	if strings.ContainsAny(l.Er, lexer.RefDisallowedChars) {
		return false
	}
	return l.Ee.Valid()
}

func (l Label) Name() string { return "" }

func (l Label) CoaString() string {
	if l.Er == "" {
		return CoaString(l.Ee)
	}
	return l.Er + ": " + CoaString(l.Ee)
}

func (l Label) Clone() Node { return Label{O: l.O, Er: l.Er, Ee: Clone(l.Ee)} }
func (l Label) Type() Type  { return Zero }

func (l Label) Eval(runner *Runner) (Node, error) {
	var err error
	l.Ee, err = l.Ee.Eval(runner)
	if err != nil {
		return nil, err
	}
	runner.Set(l.Er, l.Ee)
	return nil, nil
}
