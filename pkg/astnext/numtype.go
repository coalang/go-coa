package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

var NumTypeRune = NumType{
	O: origin.Base("rune"),
	Unit: Symbol{
		O:  origin.Base("rune"),
		ID: "rune",
	},
}

func IsNumTypeRune(p Type) bool {
	if p, ok := p.(NumType); ok {
		return p.Unit.ID == "rune"
	}
	return false
}

type NumType struct {
	O    origin.Origin
	Unit Symbol
}

var _ Type = NumType{}

func (n NumType) Origin() origin.Origin { return n.O }
func (n NumType) Hash() hash.Hash       { return hash.Hash{Type: "NumType", Digest: n.Unit.Hash().Digest} }
func (n NumType) Pure() bool            { return true }
func (n NumType) Valid() bool           { return n.Unit.Valid() }
func (n NumType) Name() string          { return "" }
func (n NumType) CoaString() string     { return "0#" + n.Unit.ID }
func (n NumType) Clone() Node           { return NumType{O: n.O, Unit: n.Unit.Clone().(Symbol)} }
func (n NumType) Type() Type            { return RootType }

func (n NumType) Eval(runner *Runner) (Node, error) {
	unit, err := n.Unit.Eval(runner)
	if err != nil {
		return nil, err
	}
	unit_, ok := unit.(Symbol)
	if !ok {
		return nil, n.O.Errorf("num unit is an external symbol")
	}
	n.Unit = unit_
	return n, nil
}

func (n NumType) Coercible(_ *Runner, from Type) (bool, error) {
	n2, ok := from.(NumType)
	if !ok {
		return false, nil
	}
	return n.Unit.ID == "" || n2.Unit.ID == "" || n.Unit.ID == n2.Unit.ID, nil
}

func (n NumType) Coerce(_ *Runner, from Node) (Node, error) {
	coercible, err := n.Coercible(nil, from.Type())
	if err != nil {
		return nil, err
	}
	if !coercible {
		return nil, nil
	}
	n2 := from.(Num)
	n2.T = n.Clone().(NumType)
	return n2, nil
}
