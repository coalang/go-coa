package astnext

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"strings"
)

const (
	IDCore  = ",c"
	IDIndex = ",i"
	IDSelf  = ",s"
	IDArg   = ",a"
)

type Ref struct {
	O   origin.Origin
	IDs []ID
}

var _ Type = Ref{}

func (r Ref) Origin() origin.Origin { return r.O }

func (r Ref) Hash() hash.Hash {
	return hash.Hash{Type: "Ref", Digest: hash.Digest(strings.Join(r.IDs, "."))}
}

func (r Ref) Pure() bool { return false }

func (r Ref) Valid() bool {
	for _, id := range r.IDs {
		if strings.ContainsAny(id, lexer.RefDisallowedChars) {
			return false
		}
	}
	return true
}

func (r Ref) Name() string      { return "" }
func (r Ref) CoaString() string { return strings.Join(r.IDs, ".") }

func (r Ref) Clone() Node {
	r2 := Ref{
		O:   r.O,
		IDs: make([]ID, len(r.IDs)),
	}
	copy(r2.IDs, r.IDs)
	return r2
}

func (r Ref) Type() Type { return Zero }

func (r Ref) Eval(runner *Runner) (Node, error) {
	if len(r.IDs) == 0 {
		return nil, r.O.Errorf("%w: ref is blank", errs.ErrASTInvalid)
	}
	if len(r.IDs) == 1 && r.IDs[0] == "_" {
		return Zero, nil
	}

	eval, err := r.evalScope(runner)
	if err != nil {
		return nil, err
	}
	if eval == nil {
		return nil, r.O.Errorf("%w: nil: %s", errs.ErrNotFound, r.CoaString())
	}
	return runner.Eval(eval)
}

func (r Ref) toComma() {
	id := strings.Join(r.IDs, ".")
	if strings.HasPrefix(id, "...") {
		id = ast.IDIndex + id[2:]
	}
	if strings.HasPrefix(id, "..") {
		id = ast.IDCore + id[1:]
	}
	r.IDs = strings.Split(id, ".")
}

func (r Ref) evalScope(runner *Runner) (Node, error) {
	r.toComma()
	node, ok := runner.Get(r.IDs[0])
	if !ok {
		return nil, r.O.Errorf("%w in scope (ref): %s of %s", errs.ErrNotFound, r.IDs[0], r.CoaString())
	}
	if len(r.IDs) == 1 {
		return node, nil
	}

	re, err := Ref{
		O:   r.O,
		IDs: r.IDs[1:],
	}.evalNode(runner, node)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", r.CoaString(), err)
	}
	return re, nil
}

func (r Ref) evalNode(runner *Runner, node Node) (Node, error) {
	node, err := node.Eval(runner)
	if err != nil {
		return nil, err
	}
	if len(r.IDs) == 0 {
		return nil, r.O.Errorf("%w: ref is blank", errs.ErrASTInvalid)
	}

	switch node := node.(type) {
	case Struct:
		i := node.T.Get(r.IDs[0])
		if i == -1 {
			return nil, fmt.Errorf("%w: %s in struct: %s", errs.ErrNotFound, r.IDs[0], r.CoaString())
		}
		if len(r.IDs) == 1 {
			return node.Values[i], nil
		}
		return Ref{
			O:   r.O,
			IDs: r.IDs[1:],
		}.evalNode(runner, node.Values[i])
	case Enum:
		i := node.Get(r.IDs[0])
		if i == -1 {
			return nil, fmt.Errorf("%w: %s in structtype: %s", errs.ErrNotFound, r.IDs[0], r.CoaString())
		}
		if len(r.IDs) == 1 {
			return node.Pairs[i].Default, nil
		}
		return Ref{
			O:   r.O,
			IDs: r.IDs[1:],
		}.evalNode(runner, node.Pairs[i].Default)
	case StructType:
		i := node.Get(r.IDs[0])
		if i == -1 {
			return nil, fmt.Errorf("%w: %s in structtype: %s", errs.ErrNotFound, r.IDs[0], r.CoaString())
		}
		if len(r.IDs) == 1 {
			return node.Pairs[i].Value, nil
		}
		return Ref{
			O:   r.O,
			IDs: r.IDs[1:],
		}.evalNode(runner, node.Pairs[i].Value)
	default:
		return nil, fmt.Errorf("%T which is invalid in evalNode", node)
	}
}

func (r Ref) Coercible(runner *Runner, from Type) (bool, error) {
	t, err := r.Eval(runner)
	if err != nil {
		return false, err
	}
	return t.(Type).Coercible(runner, from)
}

func (r Ref) Coerce(runner *Runner, from Node) (Node, error) {
	t, err := r.Eval(runner)
	if err != nil {
		return nil, err
	}
	return t.(Type).Coerce(runner, from)
}
