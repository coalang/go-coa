package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

type MapType struct {
	// TODO: add generics to MapType, Map, ListType, List

	O     origin.Origin
	Key   Type
	Value Type
}

var _ Type = MapType{}

func (m MapType) Origin() origin.Origin { return m.O }

func (m MapType) Hash() hash.Hash {
	return hash.Hash{
		Type:   "MapType",
		Digest: hash.Digest(hash.DigestToString(m.Key.Hash().DigestAll()) + hash.DigestToString(m.Value.Hash().DigestAll())),
	}
}

func (m MapType) Pure() bool { return m.Key.Pure() && m.Value.Pure() }

func (m MapType) Valid() bool { return m.Key.Valid() && m.Value.Valid() }

func (m MapType) Name() string { return "" }

func (m MapType) CoaString() string {
	return coaStringGroupWithGenerics("[", lexer.SepListOrMapTypeSrc, "]", "", []Node{
		m.Key,
		m.Value,
	})
}

func (m MapType) Clone() Node {
	return MapType{
		O:     m.O,
		Key:   CloneType(m.Key),
		Value: CloneType(m.Value),
	}
}

func (m MapType) Type() Type { return RootType }

func (m MapType) Eval(runner *Runner) (Node, error) {
	var err error
	m.Value, err = runner.EvalType(m.Value)
	if err != nil {
		return nil, err
	}
	m.Key, err = runner.EvalType(m.Key)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func (m MapType) Coercible(runner *Runner, from Type) (bool, error) {
	m2, ok := from.(MapType)
	if !ok {
		return false, nil
	}
	ok, err := m.Key.Coercible(runner, m2.Key)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	ok, err = m.Value.Coercible(runner, m2.Value)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	return true, nil
}

func (m MapType) Coerce(runner *Runner, from Node) (Node, error) {
	m2 := from.(MapType)
	key, err := m.Key.Coerce(runner, m2.Key)
	if err != nil {
		return nil, err
	}
	value, err := m.Value.Coerce(runner, m2.Value)
	if err != nil {
		return nil, err
	}
	m.Key = key.(Type)
	m.Value = value.(Type)
	return m, nil
}
