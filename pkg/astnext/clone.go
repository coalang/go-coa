package astnext

func cloneNodes(nodes []Node) []Node {
	nodes2 := make([]Node, len(nodes))
	for i, node := range nodes {
		nodes2[i] = Clone(node)
	}
	return nodes2
}
