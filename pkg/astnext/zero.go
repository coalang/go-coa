package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

var Zero zero

type zero struct{}

var _ Type = zero{}

func (z zero) Origin() origin.Origin      { return origin.Origin{} }
func (z zero) Hash() hash.Hash            { return hash.Hash{Type: "zero"} }
func (z zero) Pure() bool                 { return true }
func (z zero) Valid() bool                { return true }
func (z zero) Name() string               { return "zero" }
func (z zero) CoaString() string          { return "_" }
func (z zero) Clone() Node                { return z }
func (z zero) Type() Type                 { return z }
func (z zero) Eval(*Runner) (Node, error) { return z, nil }

func (z zero) Coercible(_ *Runner, from Type) (bool, error) {
	_, ok := from.(zero)
	return ok || from == nil, nil
}

func (z zero) Coerce(runner *Runner, from Node) (Node, error) {
	return z, nil
}
