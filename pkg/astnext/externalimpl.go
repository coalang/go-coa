package astnext

import (
	"github.com/google/uuid"
	"sync"
)

type ExternalImpl interface {
	Callable
	Type
}

type ExternalImpls struct {
	lock  sync.RWMutex
	m     map[uuid.UUID]ExternalImpl
	outer *ExternalImpls // outer *cannot* be itself (it may cause a deadlock)
}

func (n *ExternalImpls) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m == nil {
		n.m = map[uuid.UUID]ExternalImpl{}
	}
}

func (n *ExternalImpls) Inherit() *ExternalImpls {
	return &ExternalImpls{
		outer: n,
	}
}

func (n *ExternalImpls) Keys() []uuid.UUID {
	n.lock.RLock()
	defer n.lock.RUnlock()
	keys := make([]uuid.UUID, 0, len(n.m))
	for key := range n.m {
		keys = append(keys, key)
	}
	if n.outer != nil {
		keys = append(keys, n.outer.Keys()...)
	}
	return keys
}

func (n *ExternalImpls) Get(key uuid.UUID) (ExternalImpl, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val, ok := n.m[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val, ok
}

func (n *ExternalImpls) Set(key uuid.UUID, val ExternalImpl) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m[key] = val
}
