package astnext

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"math/big"
	"strconv"
)

var (
	Bool = NumType{
		O: origin.Base("bool"),
		Unit: Symbol{
			O:  origin.Base("bool"),
			ID: "bool",
		},
	}
	BoolFalse = Num{
		O:       origin.Base("bool"),
		T:       Bool,
		Content: "0",
		Flag:    NumFlagNormal,
	}
	BoolTrue = Num{
		O:       origin.Base("bool"),
		T:       Bool,
		Content: "1",
		Flag:    NumFlagNormal,
	}
)

func NumFromRune(o origin.Origin, r rune) Num {
	return Num{
		O:       o,
		T:       NumTypeRune,
		Content: strconv.FormatInt(int64(r), 10),
		Flag:    NumFlagNormal,
	}
}

type Num struct {
	O       origin.Origin
	T       NumType
	Content string
	Flag    NumFlag
}

var _ Node = Num{}

func (n Num) Origin() origin.Origin { return n.O }

func (n Num) Hash() hash.Hash {
	return hash.Hash{
		Type:   "Num",
		Digest: hash.Digest(hash.DigestToString(n.T.Hash().DigestAll()) + string(n.Flag.Rune()) + n.Content),
	}
}

func (n Num) Pure() bool   { return true }
func (n Num) Valid() bool  { panic("not implemented") }
func (n Num) Name() string { return "" }

func (n Num) CoaString() string {
	switch n.Flag {
	case NumFlagNormal:
		return n.Content
	case NumFlagPInf:
		return "+inf"
	case NumFlagNInf:
		return "-inf"
	default:
		panic("Num.Flag invalid")
	}
}

func (n Num) Rune() rune {
	if n.Flag != NumFlagNormal {
		panic("Num not normal")
	}
	value, ok := big.NewRat(1, 1).SetString(n.Content)
	if !ok {
		panic("Num.Rune: SetString failed")
	}
	if !value.IsInt() {
		panic("Num.Rune: not int")
	}
	return rune(value.Num().Int64())
}

func (n Num) Rat() (*big.Rat, error) {
	if n.Flag != NumFlagNormal {
		return nil, errors.New("Num.Flag not normal")
	}
	r, ok :=new(big.Rat).SetString(n.Content)
	if !ok {
		return nil, errors.New("Nim.Content invalid")
	}
	return r, nil
}

func (n Num) Clone() Node {
	return Num{
		O:       n.O,
		T:       n.T.Clone().(NumType),
		Content: n.Content,
		Flag:    n.Flag,
	}
}

func (n Num) Type() Type { return n.T }

func (n Num) Eval(*Runner) (Node, error) {
	switch n.Flag {
	case NumFlagNormal:
		if n.Content == "" {
			n.Content = "0"
		}
		value, ok := big.NewRat(1, 1).SetString(n.Content)
		if !ok {
			return nil, n.O.Errorf("%w: Num.Content: %s", errs.ErrASTInvalid, n.Content)
		}
		n.Content = value.RatString()
		return n, nil
	case NumFlagNaN, NumFlagPInf, NumFlagNInf:
		if n.Content != "" {
			n.Content = ""
		}
		return n, nil
	default:
		return nil, n.O.Errorf("%w: Num.Flag: %d", errs.ErrASTInvalid, n.Flag)
	}
}

type NumFlag uint8

const (
	NumFlagNormal NumFlag = iota
	NumFlagNaN
	NumFlagPInf
	NumFlagNInf
)

func (n NumFlag) Rune() rune {
	switch n {
	case NumFlagNormal:
		return 'n'
	case NumFlagNaN:
		return 'o'
	case NumFlagPInf:
		return 'p'
	case NumFlagNInf:
		return 'n'
	default:
		return 0
	}
}
