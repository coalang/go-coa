package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"io/fs"
	"sync"
)

type Runner struct {
	scope *Scope
	root  *Scope
	outer *Runner
	Frame
	fs fs.FS

	ExternalImpls *ExternalImpls

	lock sync.RWMutex
}

func NewRunnerWithBase(fs fs.FS, pure bool, applyExternals func(runner *Runner)) (*Runner, error) {
	r := NewRunnerNoRoot(fs, pure)
	applyExternals(r)
	// apply base
	err := r.fileInPlace(BasePath)
	if err != nil {
		return nil, err
	}
	// (r.root == nil at this point)
	// since base writes to r.scope, swap r.scope to r.root
	// → r.root will contain base scope, and r.scope would be blank
	r.scope, r.root = newScope(), r.scope
	return r, nil
}

func NewRunnerNoRoot(fs fs.FS, pure bool) *Runner {
	return &Runner{
		scope: newScope(),
		Frame: Frame{
			O:      origin.FromCaller(1),
			Reason: "root",
			Pure:   pure,
		},
		fs:            fs,
		ExternalImpls: &ExternalImpls{},
	}
}

func (r *Runner) EvalType(node Node) (Type, error) {
	if node == nil {
		return nil, nil
	}
	eval, err := node.Eval(r)
	if err != nil {
		return nil, err
	}
	if eval == nil {
		return nil, nil
	}
	return eval.(Type), nil
}

func (r *Runner) Eval(node Node) (Node, error) {
	if node == nil {
		return nil, nil
	}
	return node.Eval(r)
}

func (r *Runner) Nodes(nodes []Node) ([]Node, error) {
	var err error
	for i, node := range nodes {
		nodes[i], err = r.Eval(node)
		if err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

func (r *Runner) inherit(frame Frame) *Runner {
	return &Runner{
		scope: newScope(),
		root:  r.root,
		outer: r,
		Frame: Frame{
			O:      frame.O,
			Len:    r.Len + 1,
			Reason: frame.Reason,
			Call:   frame.Call,
			Lone:   frame.Lone,
			Pure:   r.Pure,
		},
		fs:            r.fs,
		ExternalImpls: r.ExternalImpls.Inherit(),
	}
}

func (r *Runner) Keys() []string {
	return r.scope.Keys()
}

func (r *Runner) Get(key string) (Node, bool) {
	if r == nil {
		return nil, false
	}
	node, ok := r.scope.Get(key)
	if ok {
		return node, true
	}
	if r.root != nil {
		if node, ok = r.root.Get(key); ok {
			return node, true
		}
	}
	if r.outer != nil {
		return r.outer.Get(key)
	}
	return nil, false
}

func (r *Runner) Set(key string, value Node) {
	r.scope.Set(key, value)
}
