package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type MapPair struct {
	Key   Node
	Value Node
}

type Map struct {
	O     origin.Origin
	T     MapType
	Pairs []MapPair
}

var _ Node = Map{}

func (m Map) Origin() origin.Origin { return m.O }

func (m Map) Hash() hash.Hash {
	content := ""
	for _, pair := range m.Pairs {
		content += hash.DigestToString(pair.Key.Hash().DigestAll()) + hash.DigestToString(pair.Value.Hash().DigestAll())
	}
	return hash.Hash{
		Type:   "Map",
		Digest: hash.Digest(hash.DigestToString(m.T.Hash().DigestAll()) + content),
	}
}

func (m Map) Pure() bool {
	if !m.T.Pure() {
		return false
	}
	for _, pair := range m.Pairs {
		if !pair.Key.Pure() || !pair.Value.Pure() {
			return false
		}
	}
	return true
}

func (m Map) Valid() bool {
	if !m.T.Pure() {
		return false
	}
	for _, pair := range m.Pairs {
		if !pair.Key.Pure() || !pair.Value.Pure() {
			return false
		}
	}
	return true
}

func (m Map) Name() string { return "" }

func (m Map) CoaString() string {
	content := make([]Node, len(m.Pairs))
	for i, pair := range m.Pairs {
		content[i] = Label{
			Er: pair.Key.CoaString(),
			Ee: pair.Value,
		}
	}
	return coaStringGroup("[", "]", content)
}

func (m Map) Clone() Node {
	pairs := make([]MapPair, len(m.Pairs))
	for i, pair := range m.Pairs {
		pairs[i] = MapPair{
			Key:   pair.Key.Clone(),
			Value: pair.Value.Clone(),
		}
	}
	return Map{
		O:     m.O,
		T:     m.T.Clone().(MapType),
		Pairs: pairs,
	}
}

func (m Map) Type() Type { return m.T }

func (m Map) Eval(runner *Runner) (Node, error) {
	t, err := m.T.Eval(runner)
	if err != nil {
		return nil, err
	}
	m.T = t.(MapType)
	for i, pair := range m.Pairs {
		m.Pairs[i].Key, err = runner.EvalType(pair.Key)
		if err != nil {
			return nil, err
		}
		m.Pairs[i].Value, err = runner.EvalType(pair.Value)
		if err != nil {
			return nil, err
		}
	}
	return m, nil
}
