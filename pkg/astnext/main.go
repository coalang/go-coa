package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

// Node is an AST node, and may or may not be an object.
type Node interface {
	origin.HasOrigin
	hash.Hashable
	PureCheckable
	Validatable
	Nameable
	CoaStringer
	Clone() Node // must return same as its own type
	Type() Type
	Eval(*Runner) (Node, error)
}

type Callable interface {
	Node
	Call(*Runner, Call) (Node, error)
}

// Type is an AST node of a Coa type.
// if Type.Type() is not rootType, type matching changes to value matching
type Type interface {
	Node
	Coercible(runner *Runner, from Type) (bool, error)
	Coerce(runner *Runner, from Node) (Node, error)
}

type CoaStringer interface {
	CoaString() string
}

func CoaString(stringer CoaStringer) string {
	if stringer == nil {
		return "_"
	}
	return stringer.CoaString()
}

type PureCheckable interface {
	Pure() bool
}

type Validatable interface {
	Valid() bool
}

type Nameable interface {
	Name() string
}
