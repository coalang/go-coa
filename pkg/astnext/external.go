package astnext

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type External struct {
	O  origin.Origin
	ID uuid.UUID
}

var _ Callable = External{}

func (e External) Origin() origin.Origin { return e.O }

func (e External) Hash() hash.Hash {
	return hash.Hash{Type: "External", Digest: hash.Digest(string(e.ID[:]))}
}

func (e External) Pure() bool        { return false }
func (e External) Valid() bool       { return true }
func (e External) Name() string      { return "" }
func (e External) CoaString() string { return "##" + e.ID.String() }
func (e External) Clone() Node       { return e }
func (e External) Type() Type        { return Zero }

func (e External) Eval(runner *Runner) (Node, error) {
	impl, ok := runner.ExternalImpls.Get(e.ID)
	if !ok {
		return nil, origin.Errorf(e.O, "%w: %s (eval)", errs.ErrExternalNotFound, e.ID)
	}
	return impl.Eval(runner)
}

func (e External) Call(runner *Runner, call Call) (Node, error) {
	impl, ok := runner.ExternalImpls.Get(e.ID)
	if !ok {
		return nil, origin.Errorf(e.O, "%w: %s (call)", errs.ErrExternalNotFound, e.ID)
	}
	return impl.Call(runner, call)
}
