package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"strconv"
)

func ListFromString(o origin.Origin, s string) List {
	l := List{
		O: o,
		T: ListType{
			O:       o,
			Content: NumTypeRune,
		},
		Content: make([]Node, 0, len(s)),
	}
	for _, r := range s {
		l.Content = append(l.Content, NumFromRune(o, r))
	}
	return l
}

type List struct {
	O       origin.Origin
	T       ListType
	Content []Node
}

func (l List) Origin() origin.Origin { return l.O }

func (l List) Hash() hash.Hash {
	content := ""
	for _, node := range l.Content {
		content += hash.DigestToString(node.Hash().DigestAll())
	}
	return hash.Hash{Type: "List", Digest: hash.Digest(hash.DigestToString(l.T.Hash().DigestAll()) + content)}
}

func (l List) Pure() bool {
	if !l.T.Pure() {
		return false
	}
	for _, node := range l.Content {
		if !node.Pure() {
			return false
		}
	}
	return true
}

func (l List) Valid() bool {
	if !l.T.Valid() {
		return false
	}
	for _, node := range l.Content {
		if !node.Valid() {
			return false
		}
	}
	return true
}

func (l List) Name() string { return "" }

func (l List) CoaString() string {
	if IsNumTypeRune(l.T.Content) {
		s := make([]rune, len(l.Content))
		for i, node := range l.Content {
			s[i] = node.(Num).Rune()
		}
		return strconv.Quote(string(s))
	}
	return Cast{
		Ee: rendered(coaStringGroup("[", "]", l.Content)),
		Er: l.T,
	}.CoaString()
}

func (l List) Clone() Node {
	return List{
		O:       l.O,
		T:       l.T.Clone().(ListType),
		Content: cloneNodes(l.Content),
	}
}

func (l List) Type() Type { return l.T }

func (l List) Eval(runner *Runner) (Node, error) {
	t, err := l.T.Eval(runner)
	if err != nil {
		return nil, err
	}
	l.T = t.(ListType)
	for i, node := range l.Content {
		l.Content[i], err = node.Eval(runner)
		if err != nil {
			return nil, err
		}
	}
	return l, nil
}

var _ Node = List{}
