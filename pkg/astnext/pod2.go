package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"io/fs"
	"log"
	"path/filepath"
	"strings"
)

const (
	BasePath  = `base.coa`
	CorePath  = `core`
	IndexPath = `index`
)

func (r *Runner) Pod(pod Pod) (Node, error) {
	return r.load(pod.Path)
}

func (r *Runner) load(p string) (Node, error) {
	node, ok := podMemoGet(p)
	if ok {
		return node, nil
	}

	node = nil
	stat, err := fs.Stat(r.fs, p)
	if err != nil {
		return nil, err
	}
	if !stat.IsDir() {
		node, err = r.file(p)
	} else {
		node, err = r.dir(p)
	}
	if err != nil {
		return nil, err
	}
	podMemoSet(p, node)
	return node, nil
}

func (r *Runner) dir(p string) (Node, error) {
	log.Printf("pod dir\t%s", p)
	var err error
	var entries []fs.DirEntry
	entries, err = fs.ReadDir(r.fs, p)
	if err != nil {
		return nil, err
	}
	s := Struct{
		O: origin.Origin{
			Filename: p,
		},
		T: StructType{
			Pairs: make([]StructTypePair, len(entries)),
		},
		Values: make([]Node, len(entries)),
	}
	log.Printf("pod dir a\t%v", entries)
	for i, entry := range entries {
		name := entry.Name()
		s.Values[i], err = r.load(filepath.Join(p, name))
		if err != nil {
			return nil, err
		}
		s.T.Pairs[i] = StructTypePair{
			Key:     strings.TrimSuffix(name, filepath.Ext(name)),
			Default: s.Values[i],
		}
	}
	return s, nil
}

func (r *Runner) fileInPlace(p string) error {
	log.Printf("load file\t%s", p)
	data, err := fs.ReadFile(r.fs, p)
	if err != nil {
		return err
	}
	program, err := Decode(p, data)
	if err != nil {
		return err
	}
	_, err = program.Eval(r)
	if err != nil {
		return err
	}
	return nil
}

func (r *Runner) file(p string) (Node, error) {
	if filepath.Ext(p) != ".coa" {
		return nil, nil
	}
	inner := r.inherit(Frame{
		O: origin.Origin{
			Filename: p,
		},
		Lone: true,
	})
	err := inner.fileInPlace(p)
	if err != nil {
		return nil, err
	}
	exported, ok := inner.Get("export")
	if !ok {
		return nil, inner.O.Errorf("%w", errs.ErrNothingExported)
	}
	return exported, nil
}
