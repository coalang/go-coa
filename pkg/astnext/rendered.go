package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

const ThresholdNoIndent = 50

type rendered string

func (r rendered) Coercible(*Runner, Type) (bool, error) {
	panic("implement me")
}

func (r rendered) Coerce(*Runner, Node) (Node, error) {
	panic("implement me")
}

var _ Node = rendered("")

func (r rendered) Origin() origin.Origin {
	panic("implement me")
}

func (r rendered) Hash() hash.Hash {
	panic("implement me")
}

func (r rendered) Pure() bool {
	panic("implement me")
}

func (r rendered) Valid() bool {
	panic("implement me")
}

func (r rendered) Name() string {
	panic("implement me")
}

func (r rendered) CoaString() string {
	return string(r)
}

func (r rendered) Clone() Node {
	panic("implement me")
}

func (r rendered) Type() Type {
	panic("implement me")
}

func (r rendered) Eval(*Runner) (Node, error) {
	panic("implement me")
}

func coaStringGroupWithGenerics(open, genericsSep, close, genericsRendered string, nodes []Node) string {
	re := open + genericsRendered + genericsSep
	for i, node := range nodes {
		re += CoaString(node)
		if i != len(nodes)-1 {
			re += ", "
		}
	}
	re += close
	if len(re) >= ThresholdNoIndent {
		re = open + "\n"
		for _, node := range nodes {
			re += utils.Indent(CoaString(node)) + ",\n"
		}
		re += close
	}
	return re
}
func coaStringGroup(open, close string, nodes []Node) string {
	return coaStringGroupWithGenerics(open, "", close, "", nodes)
}
