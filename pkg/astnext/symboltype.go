package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

var SymbolType symbolType

type symbolType struct{}

var _ Type = symbolType{}

func (s symbolType) Origin() origin.Origin      { return origin.Base("symbol type") }
func (s symbolType) Hash() hash.Hash            { return hash.Hash{Type: "symbolType"} }
func (s symbolType) Pure() bool                 { return true }
func (s symbolType) Valid() bool                { return true }
func (s symbolType) Name() string               { return "" }
func (s symbolType) CoaString() string          { return "symbol" }
func (s symbolType) Clone() Node                { return s }
func (s symbolType) Type() Type                 { return RootType }
func (s symbolType) Eval(*Runner) (Node, error) { return s, nil }

func (s symbolType) Coercible(_ *Runner, from Type) (bool, error) {
	_, ok := from.(symbolType)
	return ok, nil
}

func (s symbolType) Coerce(_ *Runner, from Node) (Node, error) {
	return s, nil
}
