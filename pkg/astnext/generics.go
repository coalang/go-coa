package astnext

import "gitlab.com/coalang/go-coa/pkg/astnext/hash"

type GenericsNode map[string]Node
type GenericsType map[string]Type

func (g GenericsType) Coercible(runner *Runner, from GenericsType) (bool, error) {
	panic("not implemented")
}

func (g GenericsType) Coerce(runner *Runner, from GenericsType) (GenericsType, error) {
	panic("not implemented")
}

func (g GenericsNode) Coercible(runner *Runner, from GenericsNode) (bool, error) {
	panic("not implemented")
}

func (g GenericsNode) Coerce(runner *Runner, from GenericsNode) (GenericsNode, bool, error) {
	panic("not implemented")
}

func (g GenericsType) Apply(runner *Runner) {
	for key, value := range g {
		runner.Set(key, value)
	}
}

func (g GenericsNode) Apply(runner *Runner) {
	for key, value := range g {
		runner.Set(key, value)
	}
}

func (g GenericsType) Eval(runner *Runner) (GenericsType, error) {
	var node Node
	var err error
	for key, value := range g {
		node, err = value.Eval(runner)
		if err != nil {
			return nil, err
		}
		g[key] = node.(Type)
	}
	return g, nil
}

func (g GenericsNode) Eval(runner *Runner) (GenericsNode, error) {
	var node Node
	var err error
	for key, value := range g {
		node, err = value.Eval(runner)
		if err != nil {
			return nil, err
		}
		g[key] = node.(Type)
	}
	return g, nil
}

func (g GenericsNode) Pure() bool {
	for _, value := range g {
		if !value.Pure() {
			return false
		}
	}
	return true
}

func (g GenericsType) Pure() bool {
	for _, value := range g {
		if !value.Pure() {
			return false
		}
	}
	return true
}

func (g GenericsNode) Hash() hash.Hash {
	re := ""
	for key, value := range g {
		re += hash.DigestToString(value.Hash().DigestAll()) + key + "\n"
	}
	return hash.Hash{Type: "genericsNode", Digest: hash.Digest(re)}
}

func (g GenericsType) Hash() hash.Hash {
	re := ""
	for key, value := range g {
		re += hash.DigestToString(value.Hash().DigestAll()) + key + "\n"
	}
	return hash.Hash{Type: "genericsType", Digest: hash.Digest(re)}
}

func (g GenericsNode) CoaString() string {
	nodes := make([]Node, 0, len(g))
	for key, value := range g {
		nodes = append(nodes, Label{
			Er: key,
			Ee: value,
		})
	}
	return coaStringGroup("", "", nodes)
}

func (g GenericsType) CoaString() string {
	nodes := make([]Node, 0, len(g))
	for key, value := range g {
		nodes = append(nodes, Label{
			Er: key,
			Ee: value,
		})
	}
	return coaStringGroup("", "", nodes)
}

func (g GenericsNode) Clone() GenericsNode {
	m2 := map[string]Node{}
	for key, value := range g {
		m2[key] = Clone(value)
	}
	return m2
}

func (g GenericsType) Clone() GenericsType {
	m2 := map[string]Type{}
	for key, value := range g {
		m2[key] = CloneType(value)
	}
	return m2
}

func (g GenericsNode) Valid() bool {
	for _, value := range g {
		if !value.Valid() {
			return false
		}
	}
	return true
}

func (g GenericsType) Valid() bool {
	for _, value := range g {
		if !value.Valid() {
			return false
		}
	}
	return true
}
