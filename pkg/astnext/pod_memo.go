package astnext

import (
	"sync"
)

var memo = map[string]Node{}
var memoLock sync.RWMutex

func podMemoGet(p string) (Node, bool) {
	memoLock.RLock()
	defer memoLock.RUnlock()
	node, ok := memo[p]
	return node, ok
}

func podMemoSet(p string, node Node) {
	memoLock.Lock()
	defer memoLock.Unlock()
	memo[p] = node
}
