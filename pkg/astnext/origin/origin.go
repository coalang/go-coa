package origin

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"runtime"
	"strconv"
)

type HasOrigin interface {
	Origin() Origin
}

type Origin struct {
	Filename string
	Start    [3]int32 // line, column, offset
	End      [3]int32
}

func Errorf(o Origin, format string, v ...interface{}) error {
	return fmt.Errorf(o.String()+" "+format, v...)
}

func Wrap(o Origin, err error) error {
	return fmt.Errorf(o.String()+" %w", err)
}

func FromCaller(skip int) (o Origin) {
	_, filepath, line, ok := runtime.Caller(skip + 1)
	if !ok {
		if skip != 1 {
			return FromCaller(1)
		}
	}
	o.Filename = filepath
	o.Start[0] = int32(line)
	return
}

func ImplementationSpecific(name string) Origin {
	return Origin{
		Filename: "base/" + name,
	}
}

func Base(name string) Origin {
	return Origin{
		Filename: "base/" + name,
	}
}

func (o Origin) Hash() hash.Hash {
	return hash.MakeHash("Origin", string([]byte{
		byte(int8(o.Start[0])),
		byte(int8(o.Start[0] >> 8)),
		byte(int8(o.Start[0] >> 16)),
		byte(int8(o.Start[0] >> 24)),
		byte(int8(o.Start[1])),
		byte(int8(o.Start[1] >> 8)),
		byte(int8(o.Start[1] >> 16)),
		byte(int8(o.Start[1] >> 24)),
		byte(int8(o.Start[2])),
		byte(int8(o.Start[2] >> 8)),
		byte(int8(o.Start[2] >> 16)),
		byte(int8(o.Start[2] >> 24)),
		byte(int8(o.End[0])),
		byte(int8(o.End[0] >> 8)),
		byte(int8(o.End[0] >> 16)),
		byte(int8(o.End[0] >> 24)),
		byte(int8(o.End[1])),
		byte(int8(o.End[1] >> 8)),
		byte(int8(o.End[1] >> 16)),
		byte(int8(o.End[1] >> 24)),
		byte(int8(o.End[2])),
		byte(int8(o.End[2] >> 8)),
		byte(int8(o.End[2] >> 16)),
		byte(int8(o.End[2] >> 24)),
	})+o.Filename)
}

func (o Origin) Errorf(format string, v ...interface{}) error {
	return fmt.Errorf(o.String()+":\n"+format, v...)
}

func formatInt32(i int32) string {
	return strconv.FormatInt(int64(i), 10)
}

func (o Origin) String() string {
	re := o.Filename
	if o.Start != [3]int32{} {
		re += ":" + formatInt32(o.Start[0]) + ":" + formatInt32(o.Start[1])
	}
	if o.End != [3]int32{} {
		re += "→" + formatInt32(o.End[0]) + ":" + formatInt32(o.End[1])
	}
	return re
}
