package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

type Enum struct {
	StructType
}

var _ Type = Enum{}

func (e Enum) Origin() origin.Origin {
	return e.StructType.O
}

func (e Enum) Hash() hash.Hash {
	return hash.Hash{Type: "Enum", Digest: e.StructType.Hash().Digest}
}

func (e Enum) CoaString() string {
	content := make([]Node, len(e.StructType.Pairs))
	for i, pair := range e.StructType.Pairs {
		content[i] = Label{
			Er: Cast{
				Ee: rendered(pair.Key),
				Er: pair.Value,
			}.CoaString(),
			Ee: pair.Default,
		}
	}
	return coaStringGroupWithGenerics("[", lexer.SepEnumSrc, "]", CoaString(e.StructType.Generics), content)
}

func (e Enum) Clone() Node {
	return Enum{StructType: e.StructType.Clone().(StructType)}
}

func (e Enum) Eval(runner *Runner) (Node, error) {
	s, err := e.StructType.Eval(runner)
	if err != nil {
		return nil, err
	}
	return Enum{StructType: s.(StructType)}, nil
}

func (e Enum) Coercible(runner *Runner, from Type) (bool, error) {
	panic("implement me")
}

func (e Enum) Coerce(runner *Runner, from Node) (Node, error) {
	panic("implement me")
}
