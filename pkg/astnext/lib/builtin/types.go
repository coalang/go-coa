package builtin

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var numID = utils.UUIDFromMust("a2d8e271-4fa7-460c-8bac-b1c817e9ba81")
var numImpl = astnext.ExternalImplNodeOnly{
	N: astnext.NumType{},
	P: true,
}
