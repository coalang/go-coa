package builtin

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/lib/builtin/arithmetic"
	"gitlab.com/coalang/go-coa/pkg/astnext/lib/builtin/op"
)

func Apply(r *astnext.Runner) {
	arithmetic.Apply(r)
	op.Apply(r)
	r.ExternalImpls.Set(numID, numImpl)
}
