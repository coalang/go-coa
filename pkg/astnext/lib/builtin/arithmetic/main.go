package arithmetic

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"math"
	"math/big"
)

var (
	AddNumID  = utils.UUIDFromMust("4f92eb67-cd73-4726-9a68-dd564f7b9c4e")
	SubNumID  = utils.UUIDFromMust("d998c33f-93ae-4eed-894b-9024689c3895")
	MulNumID  = utils.UUIDFromMust("5a867598-f67d-4d86-9b02-832074d87118")
	DivNumID  = utils.UUIDFromMust("a5f028f8-5f95-4b8b-b195-6879fafed4b7")
	ModNumID  = utils.UUIDFromMust("cb5dc99e-887b-4a60-95be-d5d462cfc0d3")
	PowNumID  = utils.UUIDFromMust("2ce4c662-e679-47dc-b92b-2eb13004a3d7")
	RootNumID = utils.UUIDFromMust("e78161c6-941d-4193-8537-47b1e90d0811")
)

func Apply(r *astnext.Runner) {
	r.ExternalImpls.Set(AddNumID, astnext.ExternalImplCallOnly{F: addNumImpl, P: true})
	r.ExternalImpls.Set(SubNumID, astnext.ExternalImplCallOnly{F: subNumImpl, P: true})
	r.ExternalImpls.Set(MulNumID, astnext.ExternalImplCallOnly{F: mulNumImpl, P: true})
	r.ExternalImpls.Set(DivNumID, astnext.ExternalImplCallOnly{F: divNumImpl, P: true})
	r.ExternalImpls.Set(ModNumID, astnext.ExternalImplCallOnly{F: modNumImpl, P: true})
	r.ExternalImpls.Set(PowNumID, astnext.ExternalImplCallOnly{F: powNumImpl, P: true})
	r.ExternalImpls.Set(RootNumID, astnext.ExternalImplCallOnly{F: rootNumImpl, P: true})
}

var (
	addNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return a.Add(a, b), false, nil
	})
	subNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return a.Sub(a, b), false, nil
	})
	mulNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return a.Mul(a, b), false, nil
	})
	divNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		if b.Cmp(big.NewRat(0, 1)) == 0 {
			return nil, true, nil
		} else {
			return a.Mul(a, b.Inv(b)), false, nil
		}
	})
	modNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		if !a.IsInt() || !b.IsInt() {
			return nil, true, nil
		}
		{
			a, b := a.Num(), b.Num()
			r := new(big.Int)
			a.QuoRem(a, b, r)
			// FIXME: use QuoRem or DivMod?
			return new(big.Rat).SetInt(r), false, nil
		}
	})
	powNumImpl = makeOperators(func(x *big.Rat, y *big.Rat) (*big.Rat, bool, error) {
		// x to the exponent of y
		{
			x, _ := x.Float64()
			y, _ := y.Float64()
			return new(big.Rat).SetFloat64(math.Pow(x, y)), false, nil
		}
	})
	rootNumImpl = makeOperators(func(x *big.Rat, n *big.Rat) (*big.Rat, bool, error) {
		// nth root of x
		return nil, false, errs.NewErrNotImplemented("root")
	})
)

func makeOperators(opFunc func(*big.Rat, *big.Rat) (*big.Rat, bool, error)) func(*astnext.Runner, astnext.Call) (astnext.Node, error) {
	return func(r *astnext.Runner, call astnext.Call) (astnext.Node, error) {
		a := call.Arg1.(astnext.Num)
		b := call.Arg2.(astnext.Num)

		coercible, err := a.T.Coercible(r, b.T)
		if err != nil {
			return nil, err
		}
		if !coercible {
			return nil, call.O.Errorf("%w: not coercible: %s and %s", errs.ErrTypeMismatch, a.CoaString(), b.CoaString())
		}

		switch a.Flag {
		case astnext.NumFlagNormal:
		case astnext.NumFlagNaN:
			return a, nil
		case astnext.NumFlagPInf, astnext.NumFlagNInf:
			panic("inf unsupported")
		default:
			panic("num.Content invalid")
		}
		switch b.Flag {
		case astnext.NumFlagNormal:
		case astnext.NumFlagNaN:
			return b, nil
		case astnext.NumFlagPInf, astnext.NumFlagNInf:
			panic("inf unsupported")
		default:
			panic("num.Content invalid")
		}

		aR, err := numToRat(a)
		if err != nil {
			return nil, err
		}
		bR, err := numToRat(b)
		if err != nil {
			return nil, err
		}
		cR, nan, err := opFunc(aR, bR)
		if err != nil {
			return nil, err
		}
		if nan {
			return astnext.Num{
				O:    call.O,
				T:    a.T,
				Flag: astnext.NumFlagNaN,
			}, nil
		}
		return astnext.Num{
			O:       call.O,
			T:       a.T,
			Content: cR.RatString(),
			Flag:    astnext.NumFlagNormal,
		}, nil
	}
}

func numToRat(num astnext.Num) (*big.Rat, error) {
	real_, ok := new(big.Rat).SetString(num.Content)
	if !ok {
		return nil, errors.New("num invalid")
	}
	return real_, nil
}
