package op

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var arrow = astnext.ExternalImplCallOnly{
	O:  origin.Base("arrow"),
	P:  true,
	ID: utils.UUIDFromMust("22dc755f-5256-4e4d-9574-ea173b95c8c1"),
	T: astnext.CallType{
		O:      origin.Base("arrow"),
		Arg1:   astnext.Ref{IDs: []astnext.ID{"stream-source"}},
		Return: astnext.Ref{IDs: []astnext.ID{"stream-source"}},
		Arg2:   astnext.Ref{IDs: []astnext.ID{"stream-sink"}},
	},
	F: arrowFunc,
}

func arrowFunc(runner *astnext.Runner, call astnext.Call) (astnext.Node, error) {
	receive_, ok := runner.Get("receive")
	if !ok {
		return nil, errors.New("receive not found")
	}
	send_, ok := runner.Get("send")
	if !ok {
		return nil, errors.New("send not found")
	}
	receive := receive_.(astnext.Callable)
	send := send_.(astnext.Callable)
	for {
		received, err := receive.Call(runner, astnext.Call{
			O:    call.O,
			Arg1: receive,
		})
		if err != nil {
			return nil, err
		}
		_, err = send.Call(runner, astnext.Call{
			O:    call.O,
			Arg1: send,
			Arg2: received,
		})
		if err != nil {
			return nil, err
		}
	}
}
