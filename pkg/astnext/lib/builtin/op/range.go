package op

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"math/big"
)

var range_ = astnext.ExternalImplCallOnly{
	O:  origin.Base("range_"),
	P:  true,
	ID: utils.UUIDFromMust("2f7b976e-c866-47d4-a8f4-a97bdb17a189"),
	T: astnext.CallType{
		O:      origin.Base("range_"),
		Arg1:   astnext.NumType{},
		Return: astnext.ListType{Content: astnext.NumType{}},
		Arg2:   astnext.NumType{},
	},
	F: rangeFunc,
}

func rangeFunc(_ *astnext.Runner, call astnext.Call) (astnext.Node, error) {
	a1, err := call.Arg1.(astnext.Num).Rat()
	if err != nil {
		return nil, err
	}
	a2, err := call.Arg2.(astnext.Num).Rat()
	if err != nil {
		return nil, err
	}
	l := astnext.List{
		O: call.O,
		T: astnext.ListType{
			O:       call.O,
			Content: astnext.NumType{},
		},
		Content: make([]astnext.Node, 0),
	}
	for i := a1; i.Cmp(a2) == -1; i.Add(i, big.NewRat(1, 1)) {
		l.Content = append(l.Content, astnext.Num{
			Content: i.RatString(),
		})
	}
	return l, nil
}
