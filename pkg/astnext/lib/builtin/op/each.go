package op

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var each = astnext.ExternalImplCallOnly{
	O:  origin.Base("each"),
	P:  true,
	ID: utils.UUIDFromMust("c2b689af-2ae0-4eb0-98f8-e78cd2cc6f71"),
	T: astnext.CallType{
		O:      origin.Base("each"),
		Arg1:   astnext.ListType{},
		Return: astnext.ListType{},
		Arg2: astnext.CallType{
			O:      origin.Base("each"),
			Arg1:   astnext.Inter{},
			Return: astnext.Inter{},
		},
	},
	F: eachFunc,
}

func eachFunc(runner *astnext.Runner, call astnext.Call) (astnext.Node, error) {
	re := astnext.List{
		O:       call.O,
		Content: []astnext.Node{},
	}
	for _, node := range call.Arg1.(astnext.List).Content {
		result, err := call.Arg2.(astnext.Callable).Call(runner, astnext.Call{
			O:    call.O,
			Arg1: node,
		})
		if err != nil {
			return nil, err
		}
		re.Content = append(re.Content, result)
	}
	return re, nil
}
