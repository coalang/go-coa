package op

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var or = astnext.ExternalImplCallOnly{
	O:  origin.Base("or"),
	P:  true,
	ID: utils.UUIDFromMust("1cabe4c0-57f9-4cec-b545-891643c691c2"),
	T: astnext.CallType{
		O:      origin.Base("or"),
		Arg1:   astnext.Bool,
		Return: astnext.Bool,
		Arg2:   astnext.Bool,
	},
	F: orFunc,
}

func orFunc(_ *astnext.Runner, call astnext.Call) (astnext.Node, error) {
	if call.Arg1.(astnext.Num).Content == astnext.BoolTrue.Content || call.Arg2.(astnext.Num).Content == astnext.BoolTrue.Content {
		return astnext.BoolTrue, nil
	} else {
		return astnext.BoolFalse, nil
	}
}
