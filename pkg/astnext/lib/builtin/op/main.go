package op

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
)

func Apply(r *astnext.Runner) {
	r.ExternalImpls.Set(eq.ID, eq)
	r.ExternalImpls.Set(or.ID, or)
	r.ExternalImpls.Set(and.ID, and)

	r.ExternalImpls.Set(arrow.ID, arrow)
	r.ExternalImpls.Set(each.ID, each)
	r.ExternalImpls.Set(range_.ID, range_)
}
