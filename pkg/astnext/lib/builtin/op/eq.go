package op

import (
	"gitlab.com/coalang/go-coa/pkg/astnext"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var eq = astnext.ExternalImplCallOnly{
	O:  origin.Base("eq"),
	P:  true,
	ID: utils.UUIDFromMust("9ae44c09-9096-4ee6-99cc-cd4c03ab0962"),
	T: astnext.CallType{
		O:      origin.Base("eq"),
		Arg1:   astnext.Inter{},
		Return: astnext.Bool,
		Arg2:   astnext.Inter{},
	},
	F: eqFunc,
}

func eqFunc(_ *astnext.Runner, call astnext.Call) (astnext.Node, error) {
	if hash.Of(call.Arg1) == hash.Of(call.Arg2) {
		return astnext.BoolTrue, nil
	} else {
		return astnext.BoolFalse, nil
	}
}
