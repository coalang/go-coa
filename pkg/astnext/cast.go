package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type Cast struct {
	O  origin.Origin
	Ee Node
	Er Type
}

var _ Node = Cast{}

func (c Cast) Origin() origin.Origin { return c.O }

func (c Cast) Hash() hash.Hash {
	return hash.Hash{Type: "Cast", Digest: hash.Digest(
		hash.DigestToString(c.Ee.Hash().DigestAll()) +
			hash.DigestToString(c.Er.Hash().DigestAll()),
	)}
}

func (c Cast) Pure() bool        { return c.Ee.Pure() && c.Er.Pure() }
func (c Cast) Valid() bool       { return c.Ee.Valid() && c.Er.Valid() }
func (c Cast) Name() string      { return "" }
func (c Cast) CoaString() string { return CoaString(c.Ee) + `\` + CoaString(c.Er) }

func (c Cast) Clone() Node {
	return Cast{
		O:  c.O,
		Ee: Clone(c.Ee),
		Er: CloneType(c.Er),
	}
}

func (c Cast) Type() Type                 { return c.Er }

func (c Cast) Eval(runner *Runner) (Node, error) {
	coerce, err := c.Er.Coerce(runner, c.Ee)
	if err != nil {
		return nil, err
	}
	return coerce, nil
}
