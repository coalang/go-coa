package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type Pod struct {
	O origin.Origin
	Path string
}

//var _ Node = Pod{}
