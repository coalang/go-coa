package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

var _ Type = ListType{}

type ListType struct {
	O       origin.Origin
	Content Type
}

func (l ListType) Origin() origin.Origin { return l.O }

func (l ListType) Hash() hash.Hash {
	return hash.Hash{Type: "ListType", Digest: l.Content.Hash().DigestAll()}
}

func (l ListType) Pure() bool   { return l.Content.Pure() }
func (l ListType) Valid() bool  { return l.Content.Valid() }
func (l ListType) Name() string { return "" }

func (l ListType) CoaString() string {
	return "[" + lexer.SepListOrMapTypeSrc + CoaString(l.Content) + "]"
}

func (l ListType) Clone() Node { return ListType{O: l.O, Content: CloneType(l.Content)} }
func (l ListType) Type() Type  { return RootType }

func (l ListType) Eval(runner *Runner) (Node, error) {
	content, err := l.Content.Eval(runner)
	if err != nil {
		return nil, err
	}
	l.Content = content.(Type)
	return l, nil
}

func (l ListType) Coercible(runner *Runner, from Type) (bool, error) {
	l2, ok := from.(ListType)
	if !ok {
		return false, nil
	}
	return l.Content.Coercible(runner, l2.Content)
}

func (l ListType) Coerce(runner *Runner, from Node) (Node, error) {
	l2, ok := from.(ListType)
	if !ok {
		return nil, nil
	}
	t, err := l.Content.Coerce(runner, l2.Content)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}
	l2.Content = t.(Type)
	return l2, nil
}
