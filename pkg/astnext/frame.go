package astnext

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type Frame struct {
	O      origin.Origin
	Len    uint64
	Reason string
	Call   Call
	Lone   bool
	Pure   bool
}

var _ fmt.Stringer = Frame{}

func (f Frame) String() string {
	lone := ""
	if f.Lone {
		lone = "lone"
	}
	call := "(no call)"
	if f.Call.Hash() != (Call{}).Hash() {
		call = f.Call.CoaString()
	}
	return fmt.Sprintf("%d\t%s\n%s\t%s\n\t%s", f.Len, f.O, lone, f.Reason,call)
}
