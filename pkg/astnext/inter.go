package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

type Inter struct {
	StructType
}

var _ Type = Inter{}

func (i Inter) Origin() origin.Origin {
	return i.StructType.O
}

func (i Inter) Hash() hash.Hash {
	return hash.Hash{Type: "Inter", Digest: i.StructType.Hash().Digest}
}

func (i Inter) CoaString() string {
	content := make([]Node, len(i.StructType.Pairs))
	for i, pair := range i.StructType.Pairs {
		content[i] = Label{
			Er: Cast{
				Ee: rendered(pair.Key),
				Er: pair.Value,
			}.CoaString(),
			Ee: pair.Default,
		}
	}
	return coaStringGroupWithGenerics("[", lexer.SepInterSrc, "]", CoaString(i.StructType.Generics), content)
}

func (i Inter) Clone() Node {
	return Inter{StructType: i.StructType.Clone().(StructType)}
}

func (i Inter) Eval(runner *Runner) (Node, error) {
	s, err := i.StructType.Eval(runner)
	if err != nil {
		return nil, err
	}
	return Inter{StructType: s.(StructType)}, nil
}

func (i Inter) Coercible(runner *Runner, from Type) (bool, error) {
	i2, ok := from.(Inter)
	if !ok {
		var node Node
		var err error
		for _, pair := range i.Pairs {
			node, ok = runner.Get(pair.Key)
			if !ok {
				return false, nil
			}
			ok, err = node.Type().Coercible(runner, pair.Value)
			if err != nil {
				return false, err
			}
			if !ok {
				return false, nil
			}
		}
		return true, nil
	}
	return i2.StructType.Coercible(runner, i2.StructType)
}

func (i Inter) Coerce(runner *Runner, from Node) (Node, error) {
	i2, ok := from.(Inter)
	if !ok {
		return i, nil
	}
	return i2.StructType.Coerce(runner, i2.StructType)
}
