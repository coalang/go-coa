package hash

import (
	"fmt"
	"golang.org/x/crypto/sha3"
	"sync"
)

var toHashMemo = map[string][64]byte{}
var toHashMemoLock sync.RWMutex

// Hash represents a hash from Node.Hash()
type Hash struct {
	Type   string
	Digest [64]byte
}

func (h Hash) String() string {
	return h.Type + fmt.Sprintf("%x", h.Digest)
}

func (h Hash) DigestAll() [64]byte {
	return Digest(string(h.Digest[:]) + h.Type)
}

func DigestToString(digest [64]byte) string {
	return string(digest[:])
}

type Hashable interface {
	Hash() Hash // must be SHA3_512 hash (must not take its own Ctx into account when calculating hash)
}

func Of(hashable Hashable) Hash {
	if hashable == nil {
		return Hash{}
	}
	return hashable.Hash()
}

func Digest(src string) [64]byte {
	digest, ok := toHashMemoGet(src)
	if !ok {
		digest = sha3.Sum512([]byte(src))
		toHashMemoSet(src, digest)
	}
	return digest
}

func MakeHash(typ, src string) Hash {
	return Hash{
		Type:   typ,
		Digest: Digest(src),
	}
}

func toHashMemoGet(src string) ([64]byte, bool) {
	toHashMemoLock.RLock()
	defer toHashMemoLock.RUnlock()
	hash, ok := toHashMemo[src]
	return hash, ok
}

func toHashMemoSet(src string, digest [64]byte) {
	toHashMemoLock.Lock()
	defer toHashMemoLock.Unlock()
	toHashMemo[src] = digest
}
