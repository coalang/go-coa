package astnext

func lastNonNilNode(nodes []Node) Node {
	for i := len(nodes)-1; i > 0; i --{
		if nodes[i] != nil {
			return nodes[i]
		}
	}
	return nil
}
