package astnext

import (
	"log"
	"sort"
	"sync"
)

type Scope struct {
	m    map[string]Node
	lock sync.RWMutex
}

func newScope() *Scope {
	return &Scope{m: map[string]Node{}}
}

func (s *Scope) Keys() []string {
	s.lock.RLock()
	defer s.lock.RUnlock()
	keys := make([]string, 0, len(s.m))
	for key := range s.m {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

func (s *Scope) Get(key string) (Node, bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	value, ok := s.m[key]
	return value, ok
}

func (s *Scope) Set(key string, value Node) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.m[key] = value
	log.Printf("%p set %s\tto %s", s, key, value.CoaString())
}
