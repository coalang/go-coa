package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

var RootType rootType

// rootType is the type of types.
type rootType struct{}

var _ Type = rootType{}

func (r rootType) Origin() origin.Origin      { return origin.Base("type") }

func (r rootType) Hash() hash.Hash            { return hash.Hash{Type: "rootType"} }
func (r rootType) Pure() bool                 { return true }
func (r rootType) Valid() bool                { return true }
func (r rootType) Name() string               { return "" }
func (r rootType) CoaString() string          { return "type" }
func (r rootType) Clone() Node                { return r }
func (r rootType) Type() Type                 { return r }
func (r rootType) Eval(*Runner) (Node, error) { return r, nil }

func (r rootType) Coercible(_ *Runner, from Type) (bool, error) {
	_, ok := from.(rootType)
	return ok, nil
}

func (r rootType) Coerce(_ *Runner, from Node) (Node, error) {
	 ok, _ := r.Coercible(nil, from.Type())
	 if !ok {
	 	return nil, nil
	 }
	 return r, nil
}
