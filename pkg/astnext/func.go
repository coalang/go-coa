package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type Func struct {
	O       origin.Origin
	T       CallType
	N       string
	Content []Node
}

var _ Callable = Func{}
var _ Type = Func{}

func (f Func) Origin() origin.Origin { return f.O }

func (f Func) Hash() hash.Hash {
	re := ""
	t := f.T.Hash().DigestAll()
	return hash.Hash{
		Type:   "Value",
		Digest: hash.Digest(string(t[:]) + re),
	}
}

func (f Func) Pure() bool {
	if !f.T.Pure() {
		return false
	}
	for _, node := range f.Content {
		if !node.Pure() {
			return false
		}
	}
	return true
}

func (f Func) Valid() bool {
	if !f.T.Valid() {
		return false
	}
	for _, node := range f.Content {
		if !node.Pure() {
			return false
		}
	}
	return true
}

func (f Func) Name() string { return f.N }

func (f Func) CoaString() string {
	return Cast{Ee: rendered(coaStringGroup("{", "}", f.Content)), Er: f.T}.CoaString()
}

func (f Func) Clone() Node {
	return Func{
		O:       f.O,
		T:       f.T.Clone().(CallType),
		N:       f.N,
		Content: cloneNodes(f.Content),
	}
}

func (f Func) Type() Type { return f.T }

func (f Func) Eval(runner *Runner) (Node, error) {
	t, err := f.T.Eval(runner)
	if err != nil {
		return nil, err
	}
	f.T = t.(CallType)
	return f, nil
}

func (f Func) Call(runner *Runner, call Call) (Node, error) {
	inner := runner.inherit(Frame{
		O:      f.O,
		Reason: "function call",
		Call:   call.Clone().(Call),
		Lone:   true,
	})
	var err error

	t, err := f.T.Eval(runner)
	if err != nil {
		return nil, err
	}
	f.T = t.(CallType)

	if call.Arg1 == nil {
		call.Arg1 = Zero
	}
	if call.Arg2 == nil {
		call.Arg2 = Zero
	}

	arg1Ok, err := f.T.Arg1.Coercible(runner, TypeOf(call.Arg1))
	if err != nil {
		return nil, err
	}
	if !arg1Ok {
		return nil, origin.Errorf(call.O, "arg1: %w", errs.NewErrTypeMismatch(f.T.Arg1, TypeOf(call.Arg1)))
	}
	arg2Ok, err := f.T.Arg2.Coercible(runner, TypeOf(call.Arg2))
	if err != nil {
		return nil, err
	}
	if !arg2Ok {
		return nil, origin.Errorf(call.O, "arg2: %w", errs.NewErrTypeMismatch(f.T.Arg2, TypeOf(call.Arg2)))
	}
	inner.Set(f.T.Arg1ID, call.Arg1)
	inner.Set(f.T.Arg2ID, call.Arg2)

	f.Content, err = inner.Nodes(f.Content)
	if err != nil {
		return nil, err
	}

	re := lastNonNilNode(f.Content)
	reOk, err := f.T.Return.Coercible(runner, TypeOf(re))
	if err != nil {
		return nil, err
	}
	if !reOk {
		return nil, origin.Errorf(call.O, "return: %w", errs.NewErrTypeMismatch(f.T.Return, TypeOf(re)))
	}
	return re, nil
}

func (f Func) Coercible(runner *Runner, from Type) (bool, error) {
	c := Call{
		O:    f.O,
		T:    f.T,
		Arg1: from,
		Arg2: Zero,
		Ee:   f,
	}
	returned, err := f.Call(runner, c)
	if err != nil {
		return false, err
	}
	if sym, ok := returned.(Symbol); ok {
		if sym.ID == "true" {
			return true, nil
		}
	}
	return false, nil
}

func (f Func) Coerce(runner *Runner, from Node) (Node, error) {
	ok, err := f.Coercible(runner, from.Type())
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, f.O.Errorf("not coercible")
	}
	return f, nil
}
