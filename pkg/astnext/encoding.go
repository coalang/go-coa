package astnext

import (
	"bytes"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/parser"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/utils/path"
)

func Decode(p string, data []byte) (Generator, error) {
	o := origin.Origin{Filename: p}
	enc := path.GetEnc(p)
	switch enc {
	case path.Source:
		parse, err := parser.Parse(p, bytes.NewBuffer(data))
		if err != nil {
			return Generator{}, err
		}
		return ConvertRootBlockToPriority(*parse)
	default:
		return Generator{}, o.Errorf("%w: %s", errs.ErrFormatUnsupported, enc)
	}
}

func Encode(enc path.Enc, data Generator) ([]byte, error) {
	switch enc {
	case path.Source:
		return []byte(data.CoaString()), nil
	default:
		return nil, fmt.Errorf("%w: %s", errs.ErrFormatUnsupported, enc)
	}
}
