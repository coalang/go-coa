package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

// CallType represents the type of a Func.
type CallType struct {
	O        origin.Origin
	Generics GenericsType
	Arg1     Type
	Arg1ID   ID
	Return   Type
	Arg2     Type
	Arg2ID   ID
}

var _ Type = CallType{}

func (c CallType) Origin() origin.Origin { return c.O }

func (c CallType) Hash() hash.Hash {
	generics := ""
	var keyDigest, valueDigest [64]byte
	for key, value := range c.Generics {
		keyDigest = hash.Digest(key)
		valueDigest = value.Hash().DigestAll()
		generics += string(keyDigest[:]) + string(valueDigest[:])
	}
	arg1Digest := hash.Of(c.Arg1).DigestAll()
	arg2Digest := hash.Of(c.Arg2).DigestAll()
	returnDigest := hash.Of(c.Return).DigestAll()
	return hash.Hash{
		Type: "CallType",
		Digest: hash.Digest(
			generics +
				string(returnDigest[:]) +
				string(arg1Digest[:]) +
				string(arg2Digest[:]) +
				c.Arg1ID + " " +
				c.Arg2ID,
		),
	}
}

func (c CallType) Pure() bool {
	for _, value := range c.Generics {
		if !value.Pure() {
			return false
		}
	}
	return c.Arg1.Pure() && c.Arg2.Pure() && c.Return.Pure()
}

func (c CallType) Valid() bool {
	for key := range c.Generics {
		if !idValid(key) {
			return false
		}
	}
	if !idValid(c.Arg1ID) || !idValid(c.Arg2ID) {
		return false
	}
	return c.Arg1.Valid() && c.Arg2.Valid() && c.Return.Valid()
}

func (c CallType) Name() string { return "" }

func (c CallType) CoaString() string {
	generics := CoaString(c.Generics)
	return coaStringGroupWithGenerics("[", lexer.SepCallTypeSrc, "]", generics, []Node{
		Label{Er: c.Arg1ID, Ee: c.Arg1},
		Label{Er: c.Arg2ID, Ee: c.Arg2},
		c.Return,
	})
}

func (c CallType) Clone() Node {
	return CallType{
		O:        c.O,
		Generics: c.Generics.Clone(),
		Arg1:     CloneType(c.Arg1),
		Arg1ID:   c.Arg1ID,
		Return:   CloneType(c.Return),
		Arg2:     CloneType(c.Arg2),
		Arg2ID:   c.Arg2ID,
	}
}

func (c CallType) Type() Type { return RootType }

func (c CallType) Eval(runner *Runner) (Node, error) {
	if c.Arg1 == nil {
		c.Arg1 = Zero
	}
	if c.Arg2 == nil {
		c.Arg2 = Zero
	}
	if c.Return == nil {
		c.Return = Zero
	}

	var err error
	c.Generics, err = c.Generics.Eval(runner)
	if err != nil {
		return nil, err
	}
	inner := runner.inherit(Frame{
		O:      c.O,
		Reason: "calltype",
	})
	c.Generics.Apply(inner)

	c.Arg1, err = inner.EvalType(c.Arg1)
	if err != nil {
		return nil, err
	}
	c.Arg2, err = inner.EvalType(c.Arg2)
	if err != nil {
		return nil, err
	}
	c.Return, err = inner.EvalType(c.Return)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c CallType) Coercible(runner *Runner, from Type) (bool, error) {
	c2, ok := from.(CallType)
	if !ok {
		return false, nil
	}
	ok, err := c.Arg1.Coercible(runner, c2.Arg1)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	ok, err = c.Arg2.Coercible(runner, c2.Arg2)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	ok, err = c.Return.Coercible(runner, c2.Return)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	ok, err = c.Generics.Coercible(runner, c2.Generics)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, nil
	}
	return true, nil
}

func (c CallType) Coerce(runner *Runner, from Node) (Node, error) {
	c2, ok := from.(CallType)
	if !ok {
		return nil, nil
	}
	arg1, err := c.Arg1.Coerce(runner, c2.Arg1)
	if err != nil {
		return nil, err
	}
	arg2, err := c.Arg2.Coerce(runner, c2.Arg2)
	if err != nil {
		return nil, err
	}
	re, err := c.Return.Coerce(runner, c2.Return)
	if err != nil {
		return nil, err
	}
	gn, err := c.Generics.Coerce(runner, c2.Generics)
	if err != nil {
		return nil, err
	}
	c.Generics = gn
	c.Arg1 = TypeOf(arg1)
	c.Return = TypeOf(re)
	c.Arg2 = TypeOf(arg2)
	return c, nil
}
