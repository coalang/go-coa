package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type Dispatcher struct {
	O     origin.Origin
	Pairs []DispatchPair
}

var _ Node = Dispatcher{}

func (d Dispatcher) Origin() origin.Origin { return d.O }

func (d Dispatcher) Hash() hash.Hash {
	re := ""
	for _, pair := range d.Pairs {
		re += hash.DigestToString(pair.Hash().DigestAll())
	}
	return hash.Hash{Type: "Dispatcher", Digest: hash.Digest(re)}
}

func (d Dispatcher) Pure() bool {
	for _, pair := range d.Pairs {
		if !pair.Key.Pure() || !pair.Value.Pure() {
			return false
		}
	}
	return true
}

func (d Dispatcher) Valid() bool {
	for _, pair := range d.Pairs {
		if !pair.Key.Valid() || !pair.Value.Pure() {
			return false
		}
	}
	return true
}

func (d Dispatcher) Name() string { return "" }

func (d Dispatcher) CoaString() string {
	nodes := make([]Node, len(d.Pairs))
	for i, pair := range d.Pairs {
		nodes[i] = Clone(pair.Value)
	}
	return Cast{
		Ee: List{
			Content: nodes,
		},
		Er: rendered("dispatcher"),
	}.CoaString()
}

func (d Dispatcher) Clone() Node {
	pairs := make([]DispatchPair, len(d.Pairs))
	for i, pair := range d.Pairs {
		pairs[i] = DispatchPair{
			Key:   pair.Key.Clone().(CallType),
			Value: pair.Value.Clone().(Callable),
		}
	}
	return Dispatcher{
		O:     d.O,
		Pairs: pairs,
	}
}

func (d Dispatcher) Type() Type { return Zero }

func (d Dispatcher) Eval(runner *Runner) (Node, error) {
	var err error
	var key, value Node
	for i, pair := range d.Pairs {
		key, err = pair.Key.Eval(runner)
		if err != nil {
			return nil, err
		}
		d.Pairs[i].Key = key.(CallType)
		value, err = pair.Value.Eval(runner)
		if err != nil {
			return nil, err
		}
		d.Pairs[i].Value = value.(Callable)

	}
	return d, nil
}

type DispatchPair struct {
	Key   CallType
	Value Callable
}

func (d DispatchPair) Hash() hash.Hash {
	return hash.Hash{
		Type:   "DispatchPair",
		Digest: hash.Digest(hash.DigestToString(d.Key.Hash().Digest) + hash.DigestToString(d.Value.Hash().Digest)),
	}
}
