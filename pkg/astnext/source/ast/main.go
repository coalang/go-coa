package ast

import (
	"github.com/alecthomas/participle/v2/lexer"
)

// RootBlock is the root block
type RootBlock struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Exprs  *Exprs `parser:"@@?"`
}

type Exprs struct {
	FirstExpr  *Expr       `parser:"@@"`
	AfterExprs []AfterExpr `parser:"@@*"`
}

func (e Exprs) Len() int {
	count := len(e.AfterExprs)
	if e.FirstExpr != nil {
		count += 1
	}
	return count
}

func (e Exprs) Index(i int) Expr {
	if i == 0 {
		return *e.FirstExpr
	}
	return *e.AfterExprs[i-1].Expr
}

type AfterExpr struct {
	Sep  string `parser:"@Sep"`
	Expr *Expr  `parser:"@@?"`
}

type Expr struct {
	Pos    lexer.Position
	EndPos lexer.Position
	First  *ExprNoColon `parser:"@@"`
	Colon  string       `parser:"(@Colon"`
	Second *ExprNoColon `parser:" @@)?"`
}

func (e Expr) IsLabel() bool {
	return e.Colon != ""
}

type ExprNoColon struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	Obj      Obj          `parser:"@@"`
	Suffixes []ExprSuffix `parser:"@@*"`
}

type Obj struct {
	Pos    lexer.Position
	EndPos lexer.Position
	First  *ObjRaw `parser:"@@"`
	BSlash string  `parser:"(@BSlash"`
	Second *ObjRaw `parser:" @@?)?"`
}

type ObjRaw struct {
	Pos     lexer.Position
	EndPos  lexer.Position
	Text    string   `parser:" @Text"`
	Symbol  string   `parser:"|@Symbol"`
	Rune    string   `parser:"|@Rune"`
	Ref     string   `parser:"|@Ref"`
	Bracked *Bracked `parser:"|@@"`
	Braced  *Braced  `parser:"|@@"`
	Parened *Parened `parser:"|@@"`
}

type ExprSuffix struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Callee Obj `parser:"@@"`
	Arg1   Obj `parser:"@@"`
}

type Bracked struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string `parser:"@OBrack"`
	First    *Exprs `parser:"@@?"`
	Sep      string `parser:"(@Backtick|@Tilde)*"`
	// struct ~
	// enum ~`
	// structtype ~~
	// enumtype ~~`
	// calltype `

	Second   *Exprs `parser:"@@?"`
	ContentC string `parser:"@CBrack"`
}

type Braced struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string `parser:"@OBrace"`
	Content  *Exprs `parser:"@@?"`
	ContentC string `parser:"@CBrace"`
}

type Parened struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string `parser:"@OParen"`
	Content  *Exprs `parser:"@@?"`
	ContentC string `parser:"@CParen"`
}
