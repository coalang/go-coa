package source

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/parser"
	"gitlab.com/coalang/go-coa/pkg/convert"
	"io"
)

func ParseToProgram(filename string, src io.Reader) (program ast.Program, err error) {
	parsed, err := parser.Parse(filename, src)
	if err != nil {
		return ast.Program{}, err
	}
	return convert.FromRootBlock(*parsed)
}
