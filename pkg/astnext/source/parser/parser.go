package parser

import (
	"bytes"
	"github.com/alecthomas/participle/v2"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/ast"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"io"
	"io/ioutil"
)

var parser = participle.MustBuild(
	&ast.RootBlock{},
	participle.Lexer(lexer.Definition),
	participle.Elide("Space"), // ← elide comments for now (put in ast later)
	participle.UseLookahead(1),
)

// Parse is a function to parse an io.Reader and convert it into an *RootBlock and an error.
func Parse(filename string, srcReader io.Reader) (*ast.RootBlock, error) {
	src, err := ioutil.ReadAll(srcReader)
	if err != nil {
		return nil, err
	}
	root := ast.RootBlock{}
	err = parser.Parse(filename, bytes.NewBuffer(src), &root)

	if err != nil {
		if e, ok := err.(participle.Error); ok {
			return nil, NewParseError(
				e,
				string(src),
			)
		} else {
			return nil, err
		}
	}

	return &root, nil
}
