package parser

import (
	"fmt"
	lexer2 "github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"gitlab.com/coalang/go-coa/pkg/convert"
	"strings"

	"github.com/alecthomas/participle/v2"
)

const (
	columnNumLimit = 64 // why not 64? (笑) マイクラもそうだし
	offset         = 3
)

type ParseError struct {
	wrapped  participle.Error
	srcLines []string
	src      string
}

func NewParseError(err participle.Error, src string) ParseError {
	return ParseError{
		wrapped:  err,
		srcLines: strings.Split(src, "\n"),
		src:      src,
	}
}

func (e ParseError) fmtLines(from, to int) string {
	re := ""
	maxLineIdx := len(e.srcLines) - 1
	for lineIdx := from; lineIdx <= to; lineIdx++ {
		if lineIdx < 0 || lineIdx > maxLineIdx {
			continue
		}
		line := e.srcLines[lineIdx]
		re += e.fmtLine(lineIdx+1, 4) + line + "\n"
	}
	return re
}

func (e ParseError) fmtLine(lineIdx, digitsLen int) string {
	return fmt.Sprintf(fmt.Sprintf("%%-%vv|", digitsLen), lineIdx)
}

func (e ParseError) lines() string {
	lineNum := e.wrapped.Position().Line - 1
	columnNum := e.wrapped.Position().Column
	if len(e.srcLines) <= lineNum {
		return ""
	}

	re := ""
	re += convert.ToCtx(e.wrapped.Position(), lexer2.Position{}).String() + "\n"

	line := e.srcLines[lineNum]
	if columnNum > columnNumLimit {
		columnNum = columnNumLimit
		line = "..." + line[len(line)-columnNumLimit:]
	}

	padding := strings.Repeat(" ", 4) + "|" + strings.Repeat(" ", columnNum)

	re += e.fmtLines(lineNum-offset-1, lineNum-1)
	re += padding + "↓\n"
	re += e.fmtLine(lineNum+1, 4) + line + "\n"
	re += padding + "↑\n"
	re += e.fmtLines(lineNum+1, lineNum+offset+1)
	return re
}

func (e ParseError) Error() string {
	var re string
	switch e.wrapped.(type) {
	case participle.UnexpectedTokenError:
		wrapped := e.wrapped.(participle.UnexpectedTokenError)
		re = convert.ToCtx(wrapped.Position(), lexer2.Position{}).Errorf(
			`unexpected token:
unexpected %s:
%s
expected:
%s `,
			lexer.TokenType(wrapped.Unexpected.Type),
			wrapped.Unexpected.String(),
			wrapped.Expected,
		).Error()
	default:
		re = e.wrapped.Error()
	}

	//re += "\n" + e.lines()
	return re[:len(re)-1]
}
