package astnext

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

type ExternalImplCallOnly struct {
	O  origin.Origin
	P  bool
	ID uuid.UUID
	F  func(*Runner, Call) (Node, error)
	T  CallType
}

func (e ExternalImplCallOnly) Origin() origin.Origin                        { return e.O }
func (e ExternalImplCallOnly) Hash() hash.Hash                              { return hash.Hash{Type: "ExternalCallImplOnly"} }
func (e ExternalImplCallOnly) Pure() bool                                   { return e.P }
func (e ExternalImplCallOnly) Valid() bool                                  { return true }
func (e ExternalImplCallOnly) Name() string                                 { return "" }
func (e ExternalImplCallOnly) CoaString() string                            { return External{ID: e.ID}.CoaString() }
func (e ExternalImplCallOnly) Clone() Node                                  { return e }
func (e ExternalImplCallOnly) Type() Type                                   { return e.T }
func (e ExternalImplCallOnly) Eval(*Runner) (Node, error)                   { return e, nil }
func (e ExternalImplCallOnly) Call(runner *Runner, call Call) (Node, error) { return e.F(runner, call) }
func (e ExternalImplCallOnly) Coercible(*Runner, Type) (bool, error)        { return false, nil }

func (e ExternalImplCallOnly) Coerce(*Runner, Node) (Node, error) {
	return nil, nil
}

var _ ExternalImpl = ExternalImplCallOnly{}
