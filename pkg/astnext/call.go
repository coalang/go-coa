package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
)

// Call represents a call to a Node with self and args
type Call struct {
	O        origin.Origin
	T        CallType
	Generics GenericsType
	Arg1     Node
	Arg2     Node
	Ee       Node
}

var _ Node = Call{}

func (c Call) Origin() origin.Origin { return c.O }

func (c Call) Hash() hash.Hash {
	return hash.Hash{
		Type: "Key",
		Digest: hash.Digest(hash.DigestToString(c.Generics.Hash().DigestAll()) +
			hash.DigestToString(hash.Of(c.Arg1).DigestAll()) +
			hash.DigestToString(hash.Of(c.Arg2).DigestAll()) +
			hash.DigestToString(hash.Of(c.Ee).DigestAll()) +
			hash.DigestToString(hash.Of(c.T).DigestAll()),
		),
	}
}

func (c Call) Pure() bool {
	return c.T.Pure() && c.Generics.Pure() && c.Arg1.Pure() && c.Arg2.Pure() && c.Ee.Pure()
}

func (c Call) Valid() bool {
	return c.T.Valid() && c.Generics.Valid() && c.Arg1.Valid() && c.Arg2.Valid() && c.Ee.Valid()
}

func (c Call) Name() string { return "" }

func (c Call) CoaString() string {
	return CoaString(c.Arg1) + " (" + CoaString(c.Ee) + ") (" + CoaString(c.Arg2) + ")"
}

func (c Call) Clone() Node {
	return Call{
		O:        c.O,
		T:        c.T.Clone().(CallType),
		Generics: c.Generics.Clone(),
		Arg1:     Clone(c.Arg1),
		Arg2:     Clone(c.Arg2),
		Ee:       Clone(c.Ee),
	}
}

func (c Call) Type() Type { return c.T }

func (c Call) Eval(runner *Runner) (Node, error) {
	if c.Arg1 == nil {
		c.Arg1 = Zero
	}
	if c.Arg2 == nil {
		c.Arg2 = Zero
	}

	ee, err := c.Ee.Eval(runner)
	if err != nil {
		return nil, err
	}
	c.Arg1, err = c.Arg1.Eval(runner)
	if err != nil {
		return nil, err
	}
	c.Arg2, err = c.Arg2.Eval(runner)
	if err != nil {
		return nil, err
	}

	switch ee := ee.(type) {
	case Callable:
		c.Ee = nil
		return ee.Call(runner, c)
	default:
		return nil, c.O.Errorf("not callable: %s", CoaString(ee.Type()))
	}
}
