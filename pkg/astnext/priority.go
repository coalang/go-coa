package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"log"
	"strconv"
)

type GeneratorMode uint8

const (
	ModePriority GeneratorMode = iota
	ModeStruct
	ModeStructType
	ModeEnum
	ModeInter
	ModeListOrMap
	ModeCallType
	ModeProgram
)

func (p GeneratorMode) String() string {
	switch p {
	case ModePriority:
		return "priority"
	case ModeStruct:
		return "struct"
	case ModeStructType:
		return "structtype"
	case ModeEnum:
		return "enum"
	case ModeInter:
		return "inter"
	case ModeListOrMap:
		return "list or map"
	case ModeCallType:
		return "calltype"
	case ModeProgram:
		return "program"
	default:
		return strconv.FormatUint(uint64(p), 10)
	}
}

func (p GeneratorMode) Valid() bool {
	return p <= ModeProgram
}

type Generator struct {
	O        origin.Origin
	Generics GenericsNode
	Content  []Node
	Mode     GeneratorMode
}

var _ Type = Generator{}

func (g Generator) Origin() origin.Origin { return g.O }

func (g Generator) Hash() hash.Hash {
	content := ""
	for _, node := range g.Content {
		content += hash.DigestToString(node.Hash().DigestAll())
	}
	return hash.Hash{
		Type:   "Generator",
		Digest: hash.Digest(string([]byte{byte(g.Mode)}) + hash.DigestToString(g.Generics.Hash().DigestAll()) + content),
	}
}

func (g Generator) Pure() bool {
	if !g.Generics.Pure() {
		return false
	}
	for _, node := range g.Content {
		if !node.Pure() {
			return false
		}
	}
	return true
}

func (g Generator) Valid() bool {
	if !g.Generics.Valid() {
		return false
	}
	for _, node := range g.Content {
		if !node.Valid() {
			return false
		}
	}
	return g.Mode.Valid()
}

func (g Generator) Name() string { return "" }

func (g Generator) CoaString() string {
	switch g.Mode {
	case ModePriority:
		return coaStringGroup("(", ")", g.Content)
	case ModeStruct:
		return coaStringGroupWithGenerics("[", lexer.SepStructSrc, "]", CoaString(g.Generics), g.Content)
	case ModeStructType:
		return coaStringGroupWithGenerics("[", lexer.SepStructTypeSrc, "]", CoaString(g.Generics), g.Content)
	case ModeEnum:
		return coaStringGroupWithGenerics("[", lexer.SepEnumSrc, "]", CoaString(g.Generics), g.Content)
	case ModeInter:
		return coaStringGroupWithGenerics("[", lexer.SepInterSrc, "]", CoaString(g.Generics), g.Content)
	case ModeListOrMap:
		return coaStringGroup("[", "]", g.Content)
	case ModeCallType:
		return coaStringGroupWithGenerics("[", lexer.SepCallTypeSrc, "]", CoaString(g.Generics), g.Content)
	case ModeProgram:
		re := ""
		for _, node := range g.Content {
			re += node.CoaString() + ",\n"
		}
		return re
	default:
		panic("Generator.Mode invalid")
	}
}

func (g Generator) Clone() Node {
	return Generator{
		O:        g.O,
		Generics: g.Generics.Clone(),
		Content:  cloneNodes(g.Content),
		Mode:     g.Mode,
	}
}

func (g Generator) Type() Type { return Zero }

func (g Generator) Eval(runner *Runner) (Node, error) {
	log.Println("generator eval", g.O, g.Generics, g.CoaString())
	inner := runner.inherit(Frame{
		O:      g.O,
		Reason: "generator " + g.Mode.String(),
		Lone:   false,
	})
	var returnNode Node
	switch g.Mode {
	case ModePriority:
		nodes, err := inner.Nodes(g.Content)
		if err != nil {
			return nil, err
		}
		returnNode = lastNonNilNode(nodes)
	case ModeStruct:
		genericsType, err := convertGenericsNodeToGenericsType(g.Generics)
		if err != nil {
			return nil, err
		}
		genericsType.Apply(inner)

		_, err = inner.Nodes(g.Content)
		if err != nil {
			return nil, err
		}
		pairs := make([]StructTypePair, 0, len(inner.Keys()))
		content := make([]Node, 0, len(inner.Keys()))
		var value Node
		for _, key := range inner.Keys() {
			value, _ = inner.Get(key)
			if value == nil {
				continue
			}
			pairs = append(pairs, StructTypePair{
				Key:     key,
				Value:   value.Type(),
				Default: value,
			})
			content = append(content, value)
		}
		returnNode = Struct{
			O:        g.O,
			Generics: g.Generics,
			T:        StructType{
				O:        g.O,
				Generics: genericsType,
				Pairs:    pairs,
			},
			Values:   content,
		}
	case ModeStructType, ModeEnum, ModeInter:
		generics, err := convertGenericsNodeToGenericsType(g.Generics)
		if err != nil {
			return nil, err
		}
		generics.Apply(inner)

		_, err = inner.Nodes(g.Content)
		if err != nil {
			return nil, err
		}
		pairs := make([]StructTypePair, 0, len(inner.Keys()))
		var value Node
		for _, key := range inner.Keys() {
			value, _ = inner.Get(key)
			if value == nil {
				continue
			}
			pairs = append(pairs, StructTypePair{
				Key:     key,
				Value:   value.Type(),
				Default: value,
			})
		}
		returnNode = StructType{
			O:        g.O,
			Generics: generics,
			Pairs:    pairs,
		}
		switch g.Mode {
		case ModeEnum:
			returnNode = Enum{StructType: returnNode.(StructType)}
		case ModeInter:
			returnNode = Inter{StructType: returnNode.(StructType)}
		}
	case ModeListOrMap:
		if g.Generics != nil {
			return nil, g.O.Errorf("list cannot have genericsType")
		}
		returnNode = List{
			O:       g.O,
			Content: g.Content,
		}
	case ModeCallType:
		generics, err := convertGenericsNodeToGenericsType(g.Generics)
		if err != nil {
			return nil, err
		}
		if l := len(g.Content); l != 3 {
			return nil, g.O.Errorf("length of calltype must be 3, not %d", l)
		}
		generics.Apply(inner)

		arg1 := g.Content[0]
		var arg1ID string
		if label, ok := arg1.(Label); ok {
			arg1ID = label.Er
			arg1 = label.Ee
		}
		arg2 := g.Content[1]
		var arg2ID string
		if label, ok := arg2.(Label); ok {
			arg2ID = label.Er
			arg2 = label.Ee
		}
		re := g.Content[2]
		if _, ok := re.(Label); ok {
			return nil, g.O.Errorf("return type cannot have label")
		}
		if c, ok := re.(Cast); ok {
			re = c.Er
		}
		if _, ok := re.(Type); !ok {
			return nil, g.O.Errorf("must be a type (generator calltype): %T", re)
		}
		log.Println("generator eval calltype", g.O, generics, g.Generics)
		log.Println(arg1.CoaString())
		returnNode = CallType{
			O:        g.O,
			Generics: generics,
			Arg1:     arg1.(Type),
			Arg1ID:   arg1ID,
			Return:   re.(Type),
			Arg2:     arg2.(Type),
			Arg2ID:   arg2ID,
		}
	case ModeProgram:
		var err error
		for i, node := range g.Content {
			g.Content[i], err = runner.Eval(node)
			if err != nil {
				return nil, err
			}
		}
		return g, nil // evaluating would result in infinite recursion
	default:
		panic("Generator.Mode invalid")
	}
	log.Println("generator", g.Mode, g.O, g.Generics)
	log.Println(inner.Frame)
	return returnNode.Eval(inner)
}

func (g Generator) Coercible(runner *Runner, from Type) (bool, error) {
	if g.Mode == ModeProgram {
		return false, g.O.Errorf("programs aren't coercible")
	}
	eval, err := g.Eval(runner)
	if err != nil {
		return false, err
	}
	if t, ok :=eval.(Type); ok {
		return t.Coercible(runner, from)
	}else{
		return false, g.O.Errorf("%w: coercible on %T", errs.ErrUnsupported, eval)
	}
}

func (g Generator) Coerce(runner *Runner, from Node) (Node, error) {
	if g.Mode == ModeProgram {
		return nil, g.O.Errorf("programs aren't coercible")
	}
	eval, err := g.Eval(runner)
	if err != nil {
		return nil, err
	}
	if t, ok :=eval.(Type); ok {
		return t.Coerce(runner, from)
	}else{
		return nil, g.O.Errorf("%w: coerce on %T", errs.ErrUnsupported, eval)
	}
}
