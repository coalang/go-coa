package astnext

func TypeOf(node Node) Type {
	if node == nil {
		return nil
	}
	return node.Type()
}

func Clone(node Node) Node {
	if node == nil {
		return nil
	}
	return node.Clone()
}

func CloneType(typ Type) Type {
	node := Clone(typ)
	if node == nil {
		return nil
	}
	return node.(Type)
}

func Pure(checkable PureCheckable) bool {
	if checkable != nil {
		return checkable.Pure()
	}
	return true
}

func Name(nameable Nameable) string {
	if nameable == nil {
		return ""
	}
	return nameable.Name()
}
