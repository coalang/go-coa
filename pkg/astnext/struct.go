package astnext

import (
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/astnext/source/lexer"
)

type Struct struct {
	O        origin.Origin
	Generics GenericsNode
	T        StructType
	Values   []Node
}

func (s Struct) Origin() origin.Origin { return s.O }

func (s Struct) Hash() hash.Hash {
	values := ""
	for _, value := range s.Values {
		values += hash.DigestToString(value.Hash().DigestAll())
	}
	return hash.Hash{
		Type: "Struct",
		Digest: hash.Digest(
			hash.DigestToString(s.Generics.Hash().DigestAll()) +
				hash.DigestToString(s.T.Hash().DigestAll()) +
				values,
		),
	}
}

func (s Struct) Pure() bool {
	if !s.Generics.Pure() {
		return false
	}
	if !s.T.Pure() {
		return false
	}
	for _, value := range s.Values {
		if !value.Pure() {
			return false
		}
	}
	return true
}

func (s Struct) Valid() bool {
	if !s.Generics.Valid() {
		return false
	}
	if !s.T.Valid() {
		return false
	}
	for _, value := range s.Values {
		if !value.Valid() {
			return false
		}
	}
	return true
}

func (s Struct) Name() string { return "" }

func (s Struct) CoaString() string {
	return Cast{
		Ee: rendered(coaStringGroupWithGenerics("[", lexer.SepStructSrc, "]", s.Generics.CoaString(), s.Values)),
		Er: s.T,
	}.CoaString()
}

func (s Struct) Clone() Node {
	return Struct{
		O:        s.O,
		Generics: s.Generics.Clone(),
		T:        s.T.Clone().(StructType),
		Values:   cloneNodes(s.Values),
	}
}

func (s Struct) Type() Type { return s.T }

func (s Struct) Eval(runner *Runner) (Node, error) {
	var err error
	s.Generics, err = s.Generics.Eval(runner)
	if err != nil {
		return nil, err
	}
	inner := runner.inherit(Frame{
		O:      s.O,
		Reason: "struct",
	})
	s.Generics.Apply(inner)

	t, err := s.T.Eval(runner)
	if err != nil {
		return nil, err
	}
	s.T = t.(StructType)
	for i, value := range s.Values {
		s.Values[i], err = inner.Eval(value)
		if err != nil {
			return nil, err
		}
	}
	return s, nil
}

var _ Node = Struct{}
