package astnext

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/astnext/hash"
	"gitlab.com/coalang/go-coa/pkg/astnext/origin"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type ExternalImplNodeOnly struct {
	O  origin.Origin
	P  bool
	ID uuid.UUID
	N  Node
}

func (e ExternalImplNodeOnly) Origin() origin.Origin                 { return e.O }
func (e ExternalImplNodeOnly) Hash() hash.Hash                       { return hash.Hash{Type: "ExternalCallImplOnly"} }
func (e ExternalImplNodeOnly) Pure() bool                            { return e.P }
func (e ExternalImplNodeOnly) Valid() bool                           { return true }
func (e ExternalImplNodeOnly) Name() string                          { return "" }
func (e ExternalImplNodeOnly) CoaString() string                     { return External{ID: e.ID}.CoaString() }
func (e ExternalImplNodeOnly) Clone() Node                           { return e }
func (e ExternalImplNodeOnly) Type() Type                            { return Zero }
func (e ExternalImplNodeOnly) Eval(*Runner) (Node, error)            { return e, nil }
func (e ExternalImplNodeOnly) Call(*Runner, Call) (Node, error)      { return nil, errs.ErrUnsupported }
func (e ExternalImplNodeOnly) Coercible(*Runner, Type) (bool, error) { return false, nil }

func (e ExternalImplNodeOnly) Coerce(runner *Runner, node Node) (Node, error) {
	if t, ok := e.N.(Type); ok {
		return t.Coerce(runner, node)
	}
	return nil, errs.ErrUnsupported
}

var _ ExternalImpl = ExternalImplNodeOnly{}
