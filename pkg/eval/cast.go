package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func (s *Scope) Cast(cast ast.Cast) (ast.Node, error) {
	ee, err := s.Node(cast.Ee)
	if err != nil {
		return ast.Cast{}, err
	}
	er, err := s.Type(cast.Er)
	if err != nil {
		return ast.Cast{}, err
	}

	if ast.EqualHash(er, ast.ToType(ee)) {
		return ee, nil // no need for a cast
	}
	return s.CoerceOld(ee, er)
}
