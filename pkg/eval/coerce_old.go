package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
)

func (s *Scope) CommonType(node ast.Node) (ast.Type, error) {
	switch node := node.(type) {
	case ast.Map:
		t := node.T.Clone().(ast.MapType)
		prev := ast.AnyType{}
		var err error
		for _, current := range node.Keys {
			t.Key, err = Merge(prev, ast.ToType(current))
			if err != nil {
				return nil, err
			}
		}
		prev = ast.AnyType{}
		for _, current := range node.Values {
			t.Value, err = Merge(prev, ast.ToType(current))
			if err != nil {
				return nil, err
			}
		}
		return t, nil
	default:
		return ast.AnyType{}, nil
	}
}

func Merge(a, b ast.Type) (ast.Type, error) {
	if ast.EqualHash(a, b) {
		return a, nil
	}
	if ast.IsAnyType(a) || ast.IsZero(a) {
		return b, nil
	}
	if ast.IsAnyType(b) || ast.IsZero(b) {
		return a, nil
	}
	switch a := a.(type) {
	case ast.MapType:
		switch b := b.(type) {
		case ast.MapType:
			key, err := Merge(a.Key, b.Key)
			if err != nil {
				return nil, err
			}
			value, err := Merge(a.Value, b.Value)
			if err != nil {
				return nil, err
			}
			return ast.MapType{
				C:     a.C,
				Key:   key,
				Value: value,
			}, nil
		}
	}
	return ast.AnyType{}, nil
}

func (s *Scope) coerceOk(from ast.Node, to ast.Type) (ok bool, err error) {
	_, ok, err = s.Coerce(from, to)
	if err != nil {
		return false, err
	}
	return ok, nil
}

func (s *Scope) CoerceOld(from ast.Node, to ast.Type) (ast.Node, error) {
	coerced, ok, err := s.Coerce(from, to)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, s.Errorf(ast.ToCtx(from), "cannot Coerce (Coerce) from %s to %s: %w", render.Type(ast.ToType(from)), render.Type(to), err)
	}
	return coerced, nil
}

func (s *Scope) init(t ast.Type) (ast.Node, error) {
	switch t := t.(type) {
	case ast.MapType:
		return ast.Map{T: t}, nil
	case ast.NumType:
		return ast.Num{}, nil
	case ast.RuneType:
		return ast.Rune{}, nil
	case ast.StructType:
		return ast.InitStructFromStructType(ast.Map{}, t)
	default:
		return nil, s.Errorf(ast.ToCtx(t), "cannot init %s", render.Type(t))
	}
}
