package eval

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/encoding"
	"gitlab.com/coalang/go-coa/pkg/errs"
	encoding2 "gitlab.com/coalang/go-coa/pkg/utils/encoding"
	path2 "gitlab.com/coalang/go-coa/pkg/utils/path"
	"io/fs"
	"path/filepath"
	"sync"
)

var podMemo = map[string]ast.Node{}
var podMemoLock sync.RWMutex

func podMemoSet(path string, node ast.Node) {
	podMemoLock.Lock()
	defer podMemoLock.Unlock()
	podMemo[filepath.Clean(path)] = node
}

func podMemoGet(path string) (ast.Node, bool) {
	podMemoLock.RLock()
	defer podMemoLock.RUnlock()
	node, ok := podMemo[filepath.Clean(path)]
	return node, ok
}

func (s *Scope) callPod(call ast.Call) (ast.Node, error) {
	call = call.Clone().(ast.Call)
	var err error
	call.Ee, err = s.Pod(call.Ee.(ast.Pod))
	if err != nil {
		return nil, err
	}
	return s.CallCall(call)
}

func (s *Scope) Pod(pod ast.Pod) (ast.Node, error) {
	if pod.Inside != nil {
		return pod.Inside, nil
	}

	var err error
	pod.Path, err = altPaths(s.fs, pod.Path)
	if err != nil {
		return nil, s.Errorf(pod.C, "finding alternative files: %w", err)
	}
	pod.C.Filepath = pod.Path

	memo, err := s.loadPodMemo(pod)
	if err != nil {
		return nil, s.Errorf(pod.C, "loading pod: %w", err)
	}
	if pod2, ok := memo.(ast.Pod); ok {
		return s.Pod(pod2)
	}
	return memo, nil
}

func (s *Scope) loadPodMemo(pod ast.Pod) (ast.Node, error) {
	if memo, ok := podMemoGet(pod.Path); ok {
		return memo, nil
	}
	return s.loadPod(pod)
}

func (s *Scope) loadPod(pod ast.Pod) (ast.Node, error) {
	inner, err := s.inherit(pod.C, "loading pod", "", false)
	if err != nil {
		return nil, s.Errorf(pod.C, "inherit: %w", err)
	}
	_, err = inner.Path(pod.Path)
	if err != nil {
		return nil, err
	}
	exported, ok := inner.Get("export")
	if !ok {
		return nil, s.Errorf(pod.C, "podFile: %w", errs.ErrNothingExported)
	}
	podMemoSet(pod.Path, exported)
	return exported, nil
}

func fileExists(fsys fs.FS, p string) (exists bool, err error) {
	_, err = fs.Stat(fsys, p)
	if errors.Is(err, fs.ErrNotExist) {
		err = nil
		return
	}
	if err != nil {
		return
	}
	exists = true
	return
}

func altEncs(fsys fs.FS, p string) (string, bool, error) {
	exists, err := fileExists(fsys, p+encoding.ExtJSON)
	if err != nil {
		return "", true, err
	}
	if exists {
		return p + encoding.ExtJSON, true, nil
	}
	exists, err = fileExists(fsys, p+encoding.ExtGob)
	if err != nil {
		return "", true, err
	}
	if exists {
		return p + encoding.ExtGob, true, nil
	}

	return "", false, nil
}

func altPaths(fsys fs.FS, p string) (string, error) {
	altPath, ok, err := altEncs(fsys, path2.ToCompiled(p))
	if err != nil {
		return "", err
	}
	if ok {
		return altPath, nil
	}
	altPath, ok, err = altEncs(fsys, path2.ToAny(p))
	if err != nil {
		return "", err
	}
	if ok {
		return altPath, nil
	}
	return p, nil
}

func (s *Scope) Path(path string) (program ast.Program, err error) {
	//start := time.Now()
	path = filepath.Clean(path)
	ctx := ast.Ctx{Filepath: path}

	data, err := fs.ReadFile(s.fs, path)
	if err != nil {
		return ast.Program{}, s.Wrap(ctx, err)
	}

	program, err = encoding2.BytesDecodeProgram(path, data)
	if err != nil {
		return ast.Program{}, err
	}

	_, err = s.Program(program)
	if err != nil {
		return ast.Program{}, err
	}

	//end := time.Now()
	//s.Logf("load\t%s:\t%s", end.Sub(start), path)
	return program, nil
}
