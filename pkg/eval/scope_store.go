package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"log"
	"sort"
)

// OwnKeys returns the keys it has in its internal map.
func (s *Scope) OwnKeys() []ast.ID {
	if s == nil {
		return nil
	}
	s.lock.RLock()
	defer s.lock.RUnlock()
	keys := make([]ast.ID, len(s.m))
	i := 0
	for key := range s.m {
		keys[i] = key
		i++
	}
	return keys
}

// AllKeys returns all keys it can access.
func (s *Scope) AllKeys() []ast.ID {
	s.lock.RLock()
	defer s.lock.RUnlock()
	var keys []ast.ID
	if s.outer != nil {
		keys = s.outer.AllKeys()
	} else if s.isRoot() && s.root != nil {
		keys = append(keys, s.root.AllKeys()...)
	}
	keys = append(s.OwnKeys(), keys...)
	sort.Strings(keys)
	return keys
}

// Get gets and returns an ast.Node.
// It first attempts to get it from itself, then tries scope.outer and if not self or arg, scope.root.
func (s *Scope) Get(key ast.ID) (ast.Node, bool) {
	if s == nil {
		return nil, false
	}
	s.lock.RLock()
	defer s.lock.RUnlock()
	node, ok := s.m[key]
	if ok {
		return node, true
	}
	if !s.current.Lone {
		node, ok = s.outer.Get(key)
		if ok {
			return node, true
		}
	}
	if key != ast.IDSelf && key != ast.IDArg {
		if s.root != nil {
			node, ok = s.root.Get(key)
			if ok {
				return node, true
			}
		}
	}
	if !s.isRoot() {
		log.Printf("Scope.Get %s %s %v %v", key, s.current, s.m[key], s.outer.GetSingle(key))
		log.Println(s.OwnKeys(), s.outer.OwnKeys())
		log.Println(s.outer.current)
	}
	return nil, false
}

// GetSingle is Scope.Get but returns only the ast.Node
func (s *Scope) GetSingle(key ast.ID) ast.Node {
	val, _ := s.Get(key)
	return val
}

// Set sets the internal map to the values.
// It doesn't affect outside scopes (scope.outer and scope.root).
func (s *Scope) Set(key ast.ID, val ast.Node) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	if s.root != nil {
		if _, ok := s.root.Get(key); ok {
			return s.Errorf(ast.CtxFromCaller(2), "%s %w", key, errs.ErrTaken)
		}
	}
	s.m[key] = val
	return nil
}
