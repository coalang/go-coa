//+build debug

package eval

// Logf logs to logs.Scope with the caller Ctx and Stack.
func (s *Scope) Logf(format string, v ...interface{}) {
	logs.Scope.Printf("%s: %s\n%s", ast.CtxFromCaller(2).String(), utils.Indent(fmt.Sprintf(format, v...)), s.Stack().String())
}
