package astx

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

type PlaceholderType struct {
	PlaceholderType struct{}     `json:"placeholder_type"`
	C               ast.Ctx      `json:"c"`
	ID              string       `json:"id"`
	P               ast.Pureness `json:"p"`
}

var _ ast.Type = PlaceholderType{}

func (p PlaceholderType) IsType() {}

func (p PlaceholderType) Ctx() ast.Ctx {
	return p.C
}

func (p PlaceholderType) Clone() ast.Node {
	return p
}

func (p PlaceholderType) Hash() ast.Hash {
	return ast.MakeHash("PlaceholderType", p.ID+fmt.Sprint(p.P))
}

func (p PlaceholderType) Type() ast.Type {
	return ast.RootType{}
}

func (p PlaceholderType) IsPure() ast.Pureness {
	return true
}

func (p PlaceholderType) IsZero() bool {
	return p.ID == "" && p.P == false && p.C.IsZero()
}

// Placeholder is a placeholder node.
// This is for testing only, and should not be used for real code.
type Placeholder struct {
	Placeholder struct{}     `json:"placeholder"`
	C           ast.Ctx      `json:"c"`
	ID          string       `json:"id"`
	P           ast.Pureness `json:"p"`
}

var _ ast.Node = Placeholder{}

func (p Placeholder) Ctx() ast.Ctx {
	return p.C
}

func (p Placeholder) Clone() ast.Node {
	return p
}

func (p Placeholder) Hash() ast.Hash {
	return ast.MakeHash("Placeholder", p.ID+fmt.Sprint(p.P))
}

func (p Placeholder) Type() ast.Type {
	return PlaceholderType{}
}

func (p Placeholder) IsPure() ast.Pureness {
	return p.P
}

func (p Placeholder) IsZero() bool {
	return p.P == false && p.ID == "" && p.C.IsZero()
}
