package astx

import "gitlab.com/coalang/go-coa/pkg/ast"

type Later struct {
	C              ast.Ctx  `json:"c"`
	UnderlyingType ast.Type `json:"underlying_type"`
}

var _ ast.Node = Later{}

func (l Later) Ctx() ast.Ctx {
	return l.C
}

func (l Later) Clone() ast.Node {
	return Later{
		C:              l.C,
		UnderlyingType: ast.CloneType(l.UnderlyingType),
	}
}

func (l Later) Hash() ast.Hash {
	return ast.MakeHash("Later", string(ast.ToHash(ast.ToType(l.UnderlyingType))))
}

func (l Later) Type() ast.Type {
	return l.UnderlyingType
}

func (l Later) IsPure() ast.Pureness {
	return false
}

func (l Later) IsZero() bool {
	return l.C.IsZero() && l.UnderlyingType == nil
}
