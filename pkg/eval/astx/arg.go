package astx

import "gitlab.com/coalang/go-coa/pkg/ast"

// Arg is used for arguments in BlockTypes (not all ast.Ref, just the self and args).
// Note that ordinary ast.Ref used in BlockTypes (e.g. type in [x\type]{}) should not become an Arg (only x in this example should become an Arg).
type Arg ast.Ref
