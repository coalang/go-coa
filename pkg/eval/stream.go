package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

type NativeStream interface {
	Close()
	IsClosed() bool
	Receive() ast.Node
	Send(ast.Node)
}

func (s *Scope) StreamType(streamType ast.StreamType) (ast.StreamType, error) {
	var err error
	streamType.Value, err = s.Type(streamType.Value)
	if err != nil {
		return ast.StreamType{}, err
	}
	return streamType, nil
}

func (s *Scope) Stream(stream ast.Stream) (ast.Stream, error) {
	var err error
	stream.T, err = s.StreamType(stream.T)
	if err != nil {
		return ast.Stream{}, err
	}
	return stream, nil
}
