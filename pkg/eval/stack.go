package eval

import "C"
import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"sort"
	"strconv"
)

type StackFrame struct {
	C    ast.Ctx  `json:"c"`
	Len  int      `json:"len"`
	Note string   `json:"note"`
	Call ast.Hash `json:"call"`
	Lone bool     `json:"isolated"`
}

func (s StackFrame) String() string {
	lone := ""
	if s.Lone {
		lone = "lone"
	}
	return fmt.Sprintf("%d\t%s\n%s", s.Len, s.C, lone+utils.IndentString+utils.Indent(s.Note)[1:])
}

func (s StackFrame) Hash() ast.Hash {
	lone := 'f'
	if s.Lone {
		lone = 't'
	}
	return ast.MakeHash("StackFrame", strconv.FormatInt(int64(s.Len), 10)+string(lone)+string(s.C.Hash()+s.Call))
}

type Stack []StackFrame

func (s Stack) String() string {
	re := "stack:\n"
	for _, frame := range s {
		re += frame.String() + "\n"
	}
	return re
}

func (s Stack) StringOnly(names map[int]string) string {
	re := "stack:\n"
	indexes := make([]int, 0, len(names)+1)
	lastIndex := len(s) - 1
	indexes = append(indexes, lastIndex)
	for index := range names {
		indexes = append(indexes, index)
	}
	sort.Ints(indexes)
	for i, index := range indexes {
		if index == lastIndex {
			re += "current:\n"
		} else {
			re += names[index] + ":\n"
		}
		re += utils.Indent(s[index].String()) + "\n"
		if i != len(indexes)-1 {
			framesHidden := indexes[i+1] - index - 1
			if framesHidden != 0 {
				re += fmt.Sprintf("%d hidden frame(s)\n", framesHidden)
			}
		}
	}
	return re
}

func (s Stack) Hash() ast.Hash {
	re := ""
	for _, frame := range s {
		re += string(frame.Hash())
	}
	return ast.MakeHash("StackString", re)
}
