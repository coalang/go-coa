package eval

import "gitlab.com/coalang/go-coa/pkg/ast"

func (s *Scope) Num(num ast.Num) (ast.Num, error) {
	return num.Fix()
}
