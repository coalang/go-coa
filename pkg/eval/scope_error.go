package eval

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"runtime"
)

// ScopeError is made by Scope.
// It wraps an error, ast.Ctx of its creation caller, ast.Ctx of the error, and Stack at the time of the error.
type ScopeError struct {
	MakeC   ast.Ctx
	MakeStack []byte
	C       ast.Ctx
	Stack   Stack
	Wrapped error
}

func (s ScopeError) Unwrap() error {
	return s.Wrapped
}

func (s ScopeError) Error() string {
	ctx := "(" + s.MakeC.String() + ")"
	if !s.C.IsZero() {
		ctx = s.C.String() + " " + ctx
	}
	return fmt.Sprintf("%s:\n%s\n\n%s\n\n%s", ctx, utils.Indent(s.Wrapped.Error()), s.Stack, s.MakeStack)
}

func (s ScopeError) String() string {
	return s.Error()
}

// Errorf makes a ScopeError with the necessary information.
func (s *Scope) Errorf(ctx ast.Ctx, format string, v ...interface{}) error {
	return s.wrap(3, ctx, fmt.Errorf(format, v...))
}

// Wrap wraps an error.
func (s *Scope) Wrap(ctx ast.Ctx, err error) error {
	return s.wrap(3, ctx, err)
}

func stack() []byte {
	buf := make([]byte, 1024)
	for {
		n := runtime.Stack(buf, true)
		if n < len(buf) {
			return buf[:n]
		}
		buf = make([]byte, 2*len(buf))
	}
}

func (s *Scope) wrap(skip uint, ctx ast.Ctx, err error) error {
	return ScopeError{
		MakeC:   ast.CtxFromCaller(skip),
		MakeStack: stack(),
		C:       ctx,
		Stack:   s.Stack(),
		Wrapped: err,
	}
}
