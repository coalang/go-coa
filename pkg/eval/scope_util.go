package eval

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/logs"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"sync"
)

// Logf logs to logs.Scope with the caller Ctx.
func (s *Scope) Logf(format string, v ...interface{}) {
	logs.Scope.Printf("%s:%s", ast.CtxFromCaller(2), utils.Indent(fmt.Sprintf(format, v...)))
}

// ContentInfo prints the contents of all keys accessible to this Scope.
func (s *Scope) ContentInfo() string {
	re := "scope content:\n"
	for _, key := range s.AllKeys() {
		node, _ := s.Get(key)
		re += fmt.Sprintf("%s\n%s$\n", key, utils.Indent(render.Node(node)))
	}
	return re
}

// RegisterExternal registers an external.
func (s *Scope) RegisterExternal(external ast.External, node ast.Node) {
	s.Externals.Set(external.ID, node)
}

// RegisterNative registers an ast.Native and its corresponding NativeImpl.
func (s *Scope) RegisterNative(native ast.Native, nativeImpl NativeImpl) {
	s.Natives.Set(native.ID, native, nativeImpl)
}

// AppendDisallowedRefs appends to disallowed refs.
func (s *Scope) AppendDisallowedRefs(ids []ast.ID) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.disallowedRefs = append(s.disallowedRefs, ids)
}

type NativeStreams struct {
	lock  sync.RWMutex
	m     map[uuid.UUID]NativeStream
	outer *NativeStreams
}

func (n *NativeStreams) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m == nil {
		n.m = map[uuid.UUID]NativeStream{}
	}
}

func (n *NativeStreams) Inherit() *NativeStreams {
	return &NativeStreams{
		outer: n,
	}
}

func (n *NativeStreams) Get(key uuid.UUID) (NativeStream, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val, ok := n.m[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val, ok
}

func (n *NativeStreams) GetSingle(key uuid.UUID) NativeStream {
	val, _ := n.Get(key)
	return val
}

func (n *NativeStreams) Set(key uuid.UUID, val NativeStream) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m[key] = val
}

type Natives struct {
	lock  sync.RWMutex
	m1    map[uuid.UUID]ast.Native
	m2    map[uuid.UUID]NativeImpl
	outer *Natives
}

func (n *Natives) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m1 == nil {
		n.m1 = map[uuid.UUID]ast.Native{}
	}
	if n.m2 == nil {
		n.m2 = map[uuid.UUID]NativeImpl{}
	}
}

func (n *Natives) Inherit() *Natives {
	return &Natives{
		outer: n,
	}
}

func (n *Natives) Keys() []uuid.UUID {
	n.InitIf()
	keys := make([]uuid.UUID, 0, len(n.m1))
	n.lock.RLock()
	defer n.lock.RUnlock()
	for key := range n.m1 {
		keys = append(keys, key)
	}
	return keys
}

func (n *Natives) Get(key uuid.UUID) (ast.Native, NativeImpl, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val1, ok := n.m1[key]
	val2, _ := n.m2[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val1, val2, ok
}

func (n *Natives) GetSingle(key uuid.UUID) (ast.Native, NativeImpl) {
	val1, val2, _ := n.Get(key)
	return val1, val2
}

func (n *Natives) Set(key uuid.UUID, val1 ast.Native, val2 NativeImpl) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m1[key] = val1
	n.m2[key] = val2
}

type Nodes struct {
	lock  sync.RWMutex
	m     map[uuid.UUID]ast.Node
	outer *Nodes
}

func (n *Nodes) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m == nil {
		n.m = map[uuid.UUID]ast.Node{}
	}
}

func (n *Nodes) Inherit() *Nodes {
	return &Nodes{
		outer: n,
	}
}

func (n *Nodes) Get(key uuid.UUID) (ast.Node, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val, ok := n.m[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val, ok
}

func (n *Nodes) GetSingle(key uuid.UUID) ast.Node {
	val, _ := n.Get(key)
	return val
}

func (n *Nodes) Set(key uuid.UUID, val ast.Node) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m[key] = val
}
