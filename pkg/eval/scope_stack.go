package eval

// StackLen returns the length of the stack.
func (s *Scope) StackLen() int {
	if s.outer != nil {
		return 1 + s.outer.StackLen()
	}
	return 0
}

// Stack returns the stack of the Scope.
func (s *Scope) Stack() Stack {
	s.lock.RLock()
	defer s.lock.RUnlock()
	if s.outer == nil {
		return []StackFrame{s.current}
	}
	return append(s.outer.Stack(), s.current)
}
