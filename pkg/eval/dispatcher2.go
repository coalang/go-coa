package eval

import (
	"errors"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
)

var (
	ErrNoCandidates    = errors.New("dispatch has no candidates")
	ErrNoMatches       = errors.New("dispatch has no match")
	ErrMultipleMatches = errors.New("dispatch has multiple matches")
)

func (s *Scope) Dispatcher2(dispatcher2 ast.Dispatcher2) (ast.Dispatcher2, error) {
	dispatcher2 = dispatcher2.Clone().(ast.Dispatcher2)
	var err error
	for i, key := range dispatcher2.Keys {
		dispatcher2.Keys[i], err = s.Call(key)
		if err != nil {
			return ast.Dispatcher2{}, err
		}
	}
	for i, val := range dispatcher2.Values {
		dispatcher2.Values[i], err = s.Callable(val)
		if err != nil {
			return ast.Dispatcher2{}, err
		}
	}
	return dispatcher2, nil
}

func (s *Scope) dispatcher2CallMatches(call ast.Call, limit int) ([]ast.Call, string, error) {
	dispatcher := call.Ee.(ast.Dispatcher2)

	all := fmt.Sprintf(
		"against\t%s _ %s → ?\n",
		render.Node(call.Self),
		render.Node(call.Arg),
	)

	var okSelf, okArg bool
	matches := make([]ast.Call, 0)
	for index, key := range dispatcher.Keys {
		call := call.Clone().(ast.Call)
		//s.Logf("dispatcher2 %d %s %s", index, render.Call(call), render.BlockType(key.T))
		okSelf = ast.EqualHash(call.Self, key.T.Self)
		okArg = ast.EqualHash(call.Arg, key.T.Arg)
		all += fmt.Sprintf(
			"%d\t%s _ %s → %s",
			index,
			render.Type(key.T.Self),
			render.Type(key.T.Arg),
			render.Type(key.T.Re),
		)
		all += "\n"
		if okSelf && okArg {
			matches = append(matches, call)
			continue
		}
	}

	return matches, all[:len(all)-1], nil
}

func (s *Scope) callDispatcher2(call ast.Call, limit int) (ast.Node, error) {
	call = ast.Clone(call).(ast.Call)
	matches, all, err := s.dispatcher2CallMatches(call, limit)
	if err != nil {
		if errors.Is(err, ErrNoCandidates) {
			return ast.Call{}, s.Errorf(call.C, "%w", err)
		}
		return nil, err
	}

	switch len(matches) {
	case 0:
		return ast.Call{}, s.Errorf(call.C, "%w\n%s", ErrNoMatches, all)
	case 1:
		return s.CallCall(matches[0])
	default:
		return ast.Call{}, s.Errorf(call.C, "%w\n%s", ErrMultipleMatches, all)
	}
}
