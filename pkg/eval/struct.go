package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func (s *Scope) Struct(s2 ast.Struct) (ast.Struct, error) {
	s2 = ast.Clone(s2).(ast.Struct)
	err := s.Nodes(s2.Values)
	if err != nil {
		return ast.Struct{}, err
	}
	s2.T, err = s.StructType(s2.T)
	if err != nil {
		return ast.Struct{}, err
	}
	return s2, nil
}

func (s *Scope) StructType(s2 ast.StructType) (ast.StructType, error) {
	s2 = ast.CloneType(s2).(ast.StructType)
	err := s.Nodes(s2.Defaults)
	if err != nil {
		return ast.StructType{}, err
	}
	err = s.Types(s2.Values)
	if err != nil {
		return ast.StructType{}, err
	}
	return s2, nil
}
