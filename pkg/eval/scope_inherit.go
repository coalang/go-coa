package eval

import "gitlab.com/coalang/go-coa/pkg/ast"

// isRoot returns whether it's the root Scope.
func (s *Scope) isRoot() bool {
	return s.current.Len == 0
}

// inherit makes a new child Scope.
// If the Scope is the root Scope, itself will be used instead of Scope.root for Scope.root of the new Scope.
func (s *Scope) inherit(c ast.Ctx, note string, call ast.Hash, lone bool) (*Scope, error) {
	//TODO: blockを孤独に実行するか検討
	lone = false
	s.lock.RLock()
	defer s.lock.RUnlock()
	root := s.root
	if s.isRoot() {
		root = s
	}

	s2 := &Scope{
		m:     map[ast.ID]ast.Node{},
		pure:  s.pure,
		outer: s,
		root:  root,
		disallowedRefs: s.disallowedRefs,
		current: StackFrame{
			Len:  s.StackLen() + 1,
			C:    c,
			Note: note,
			Call: call,
			Lone: lone,
		},
		fs:    s.fs,

		NativeStreams: s.NativeStreams.Inherit(),
		Natives:       s.Natives.Inherit(),
		Externals:     s.Externals.Inherit(),
		Promises:      s.Promises.Inherit(),
	}
	if s.StackLen()+1 == ScopeLenLimit {
		return nil, s2.Errorf(c, "stack overflow recursion limit of %d exceeded", ScopeLenLimit)
	}
	return s2, nil
}
