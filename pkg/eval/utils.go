package eval

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func (s *Scope) LoadBase(p string) error {
	pod, err := s.PodPath(p)
	if err != nil {
		return err
	}
	err = s.Set(ast.IDBase, pod)
	if err != nil {
		return err
	}
	node, err := s.Pod(pod)
	if err != nil {
		return err
	}
	s2 := node.(ast.Struct)
	for i, key := range s2.T.Keys {
		err = s.Set(key, s2.Values[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Scope) LoadCore(p string) error {
	pod, err := s.PodPath(p)
	if err != nil {
		return err
	}
	err = s.Set(ast.IDCore, pod)
	if err != nil {
		return err
	}
	return nil
}

func (s *Scope) LoadIndex(p string) error {
	pod, err := s.PodPath(p)
	if err != nil {
		return err
	}
	err = s.Set(ast.IDIndex, pod)
	if err != nil {
		return err
	}
	return nil
}

func (s *Scope) getCoerce() (ast.Dispatcher2, error) {
	convertBlock, ok := s.Get("coerce")
	if !ok {
		return ast.Dispatcher2{}, errors.New("coerce not found")
	}
	if _, ok := convertBlock.(ast.Dispatcher2); !ok {
		return ast.Dispatcher2{}, errors.New("coerce not Dispatcher2")
	}
	return convertBlock.(ast.Dispatcher2), nil
}

func (s *Scope) convertible(from ast.Node, to ast.Type) (ast.Call, error) {
	dispatcher2, err := s.getCoerce()
	if err != nil {
		return ast.Call{}, err
	}
	return ast.Call{
		C: ast.NewBuiltinCtx("convertible"),
		T: ast.BlockType{
			C:    ast.NewBuiltinCtx("convertible"),
			Self: ast.ToType(from),
			Re:   to,
		},
		Ee:   dispatcher2,
		Self: from,
		Arg:  nil,
	}, nil
}

func (s *Scope) FromProgram(p ast.Program, err error) (ast.Program, error) {
	if err != nil {
		return ast.Program{}, err
	}
	return s.Program(p)
}

func (s *Scope) Program(p ast.Program) (ast.Program, error) {
	var err error
	var re ast.Node
	p2 := ast.Program{
		C:       p.C,
		Content: make([]ast.Node, 0, len(p.Content)),
	}

	for _, node := range p.Content {
		re, err = s.Node(ast.Clone(node))
		if err != nil {
			return ast.Program{}, err
		}
		p2.Content = append(p2.Content, re)
	}
	return p2, nil
}
