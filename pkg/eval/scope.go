package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"io/fs"
	"sync"
)

const ScopeLenLimit = -1

// Scope stores the environment and lexical scope of a running state.
type Scope struct {
	m              map[ast.ID]ast.Node
	pure           bool
	outer          *Scope
	root           *Scope
	disallowedRefs [][]ast.ID // use comma refs (,core.io instead of ..io)
	current        StackFrame
	fs fs.FS

	NativeStreams *NativeStreams
	Natives       *Natives
	Externals     *Nodes
	Promises      *Nodes

	lock sync.RWMutex
}

// NewScope creates a new Scope.
// Use inherit to create child Scope.
func NewScope(c ast.Ctx, pure bool, disallowedRefs [][]ast.ID, fsys fs.FS) *Scope {
	return &Scope{
		m: map[ast.ID]ast.Node{},
		pure:           pure,
		disallowedRefs: disallowedRefs,
		current: StackFrame{
			Len:  0,
			C:    c,
			Note: "root",
		},
		fs:             fsys,

		NativeStreams: &NativeStreams{},
		Natives:       &Natives{},
		Externals:     &Nodes{},
		Promises:      &Nodes{},
	}
}

// Ctx returns the Ctx of the Scope.
func (s *Scope) Ctx() ast.Ctx {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return s.current.C
}

// Pure returns Scope's pureness mode.
func (s *Scope) Pure() bool {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return s.pure
}

// IsPure is an alias for Pure.
func (s *Scope) IsPure() bool {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return s.pure
}
