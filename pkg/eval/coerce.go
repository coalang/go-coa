package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
)

// Coerce coerces from to type to using the Coerce dispatched callable.
// If from's ast.Type and to is equal (hash), returns true.
// For coercing between the same AST type (e.g. ast.Map), use implementation-specific code. Otherwise, use natives and dispatch.
func (s *Scope) Coerce(from ast.Node, to ast.Type) (coerced ast.Node, ok bool, err error) {
	return s.coerceWithLimit(from, to, 10)
}

func (s *Scope) coerceWithLimit(from ast.Node, to ast.Type, limit int) (coerced ast.Node, ok bool, err error) {
	//s.Logf("Coerce %s %s %T %T", render.Node(from), render.Type(to), from, to)
	// TODO: move s.CoerceOld(ast.Node, ast.Type) (ast.Node, error) to s.Cast() (ast.Node, error)
	if to == nil {
		return from, true, nil
	}

	from = ast.Clone(from)
	to = ast.CloneType(to)
	if ast.EqualHash(ast.ToType(from), to) {
		return from, true, nil
	}

	switch to := to.(type) {
	case ast.AnyType:
		return from, true, nil
	case ast.StructType:
		if to.BlankKeysValuesDefaults() {
			// Coerce cast.Ee to a struct (initialize)
			initStruct, err := ast.InitStruct(from)
			if err != nil {
				return nil, false, s.Wrap(from.Ctx(), err)
			}
			return initStruct, true, nil
		} else {
			// Coerce cast.Ee to struct to
			structType, err := ast.InitStructFromStructType(from, to)
			if err != nil {
				return nil, false, s.Wrap(from.Ctx(), err)
			}
			return structType, true, nil
		}
	case ast.Inter:
		if to.T.BlankKeysValuesDefaults() {
			s, err := ast.InitStructType(from)
			if err != nil {
				return nil, false, err
			}
			return ast.Inter{T: s}, true, nil
		}
	}

	switch from := from.(type) {
	case ast.Stream:
		switch to := to.(type) {
		case ast.StreamType:
			if !from.T.Direction.CompatibleWith(to.Direction) {
				return nil, false, s.Errorf(to.C, "directions (%s and %s) non-compatible", from.T.Direction, to.Direction)
			}
			stream := from.Clone().(ast.Stream)
			value, err := s.CoerceOld(from.T.Value, to.Value)
			if err != nil {
				return nil, false, err
			}
			stream.T.Value = value.(ast.Type)
			return stream, true, nil
		}
	case ast.Map:
		switch to := to.(type) {
		case ast.MapType:
			fromC := from.T.Consecutive()
			toC := to.Consecutive()
			if !fromC.CompatibleWith(toC) {
				return nil, false, s.Errorf(to.C, "cannot Coerce map (%s and %s not compatible)", fromC, toC)
			}
			if from.T.Consecutive().CompatibleWith(ast.NonConsecutive) {
				if to.Consecutive().CompatibleWith(ast.Consecutive) {
				}
			}
			var err error
			for i, key := range from.Keys {
				from.Keys[i], err = s.CoerceOld(key, to.Key)
				if err != nil {
					return nil, false, err
				}
			}
			for i, value := range from.Values {
				from.Values[i], err = s.CoerceOld(value, to.Value)
				if err != nil {
					return nil, false, err
				}
			}
			return from, true, nil
		}
	case ast.Num:
		switch to := to.(type) {
		case ast.MapType:
			if !to.IsRuneMap() {
				return nil, false, s.Errorf(ast.ToCtx(from), "cannot Coerce (map) from %s to %s", render.Type(ast.ToType(from)), render.Type(to))
			}
			rat, err := from.Rat()
			if err != nil {
				return nil, false, err
			}
			return ast.NewRuneMap(from.C, rat.RatString()), true, nil
		}
	case ast.Symbol:
		switch to.(type) {
		case ast.NativeType:
			native, err := ast.NewNativeFromSymbol(from)
			if err != nil {
				return nil, false, err
			}
			re, err := s.Native(native)
			if err != nil {
				return nil, false, err
			}
			return re, true, nil
		case ast.ExternalType:
			external, err := ast.NewExternalFromSymbol(from)
			if err != nil {
				return nil, false, err
			}
			node, ok := s.Externals.Get(external.ID)
			if !ok {
				return nil, false, s.Errorf(from.C, "external %s not found", external.ID)
			}
			return node, true, nil
		}
	}
	if to == nil || to.IsZero() {
		return from, true, nil
	}
	if from == nil || from.IsZero() {
		var node ast.Node
		node, err = s.init(to)
		if err != nil {
			return nil, false, err
		}
		return node, true, nil
	}

	if ast.EqualHash(from.Type(), to) {
		return from, true, err
	}

	if limit <= 0 {
		return nil, false, nil
	}

	switch to := to.(type) {
	case ast.AnyType:
		return from, true, nil
	case ast.Inter:
		return s.coerceSpecialInter(from, to)
	case ast.StructType:
		if to.Enum {
			return s.coerceSpecialEnum(from, to)
		}
	}

	switch from := from.(type) {
	case ast.Map:
		if to, ok := to.(ast.MapType); ok {
			return s.coerceMap(from, to)
		}
	}

	coerceBlock, err := s.getCoerce()
	if err != nil {
		return nil, false, err
	}
	from, err = s.callDispatcher2(ast.Call{
		C:    ast.NewBuiltinCtx("coerce"),
		Ee:   coerceBlock,
		Self: from,
		T: ast.BlockType{
			C:    ast.NewBuiltinCtx("coerce/Type"),
			Self: ast.ToType(from),
			Re:   to,
		},
	}, limit-1)
	if err != nil {
		//if errors.Is(err, ErrNoMatches) {
		//	return nil, false, nil
		//}
		return nil, false, err
	}
	return from, true, nil
}

func (s *Scope) coerceSpecialEnum(from ast.Node, to ast.StructType) (ast.Node, bool, error) {
	for _, val := range to.Values {
		if ok, err := s.coerceOk(from, val); ok && err == nil {
			return from, true, nil
		} else if err != nil {
			return nil, false, err
		}
	}
	return nil, false, nil
}

func (s *Scope) coerceSpecialInter(from ast.Node, to ast.Inter) (ast.Node, bool, error) {
Main:
	for i, key := range to.T.Keys {
		callable, ok := s.Get(key)
		if !ok {
			return nil, false, nil
		}
		d, ok := callable.(ast.Dispatcher2)
		if !ok {
			return nil, false, s.Errorf(callable.Ctx(), "expected dispatcher")
		}
		for _, key2 := range d.Keys {
			_, ok, err := s.Coerce(to.T.Values[i], key2.Type())
			if err != nil {
				return nil, false, err
			}
			if ok {
				continue Main
			}
		}
		return nil, false, nil
	}
	return from, true, nil
}

func (s *Scope) coerceMap(from ast.Map, to ast.MapType) (ast.Node, bool, error) {
	var ok bool
	var err error
	if !from.T.Consecutive().CompatibleWith(to.Consecutive()) {
		return nil, false, nil
	}

	for i, key := range from.Keys {
		from.Keys[i], ok, err = s.Coerce(key, to.Key)
		if err != nil {
			return nil, false, err
		}
		if !ok {
			return nil, false, nil
		}
		from.Values[i], ok, err = s.Coerce(from.Values[i], to.Value)
		if err != nil {
			return nil, false, err
		}
		if !ok {
			return nil, false, nil
		}
	}
	return from, true, nil
}
