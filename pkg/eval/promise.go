package eval

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

type PromiseChan <-chan ast.Node

var PromiseChans = map[uuid.UUID]PromiseChan{}

func (s *Scope) PromiseType(promiseType ast.PromiseType) (ast.PromiseType, error) {
	var err error
	promiseType.Promised, err = s.Type(promiseType.Promised)
	if err != nil {
		return ast.PromiseType{}, err
	}
	return promiseType, nil
}

func (s *Scope) Promise(promise ast.Promise) (ast.Promise, error) {
	var err error
	promise.T, err = s.PromiseType(promise.T)
	if err != nil {
		return ast.Promise{}, err
	}
	return promise, nil
}

func (s *Scope) NewPromise(c ast.Ctx, p ast.Type) (ast.Promise, PromiseChan, error) {
	promise := ast.Promise{
		C: c,
		T: ast.PromiseType{
			C:        c,
			Promised: p,
		},
		ID: uuid.New(),
	}
	PromiseChans[promise.ID] = make(PromiseChan)
	return promise, PromiseChans[promise.ID], nil
}

func (s *Scope) PromiseWait(promise ast.Promise) (ast.Node, error) {
	ch, ok := PromiseChans[promise.ID]
	if !ok {
		return nil, s.Errorf(promise.C, "promise ID invalid")
	}
	return <-ch, nil
}
