package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

func (s *Scope) Types(types []ast.Type) error {
	var err error
	for i := range types {
		types[i], err = s.Type(types[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Scope) Type(typ ast.Type) (ast.Type, error) {
	switch typ := typ.(type) {
	case nil:
		return nil, nil

	case ast.MapType:
		return s.MapType(typ)
	case ast.StructType:
		return s.StructType(typ)
	case ast.Block:
		return s.Block(typ)
	case ast.BlockType:
		return s.BlockType(typ)
	case ast.Ref:
		return s.RefType(typ)
	case ast.RuneType, ast.NumType, ast.AnyType, ast.NativeType, ast.PodType, ast.ExternalType, ast.RefType, ast.StreamType:
		return typ, nil
	case ast.SymbolType:
		return s.SymbolType(typ)
	default:
		return nil, typ.Ctx().Errorf("%T %w for Type", typ, errs.ErrInvalid)
	}
}
