package eval

import (
	"context"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils/concurrency"
	"regexp"
	"runtime/pprof"
	"strings"
	"sync"
)

//goland:noinspection RegExpRedundantEscape
var pattern = regexp.MustCompile("\\([^\\(\\)\\[\\]\\{\\}\\s\\n'\",`:\\\\]+\\)")

func (s *Scope) runeMap(m ast.Map) (ast.Map, error) {
	rendered := render.Map(m)
	rendered = rendered[1 : len(rendered)-1]
	return ast.NewRuneMap(m.T.C, pattern.ReplaceAllStringFunc(rendered, func(match string) string {
		node, err := s.Ref(ast.Ref{IDs: strings.Split(match[1:len(match)-1], ".")})
		if err != nil {
			return match
		}
		node, err = s.Node(node)
		if err != nil {
			return match
		}
		switch node := node.(type) {
		case ast.Map:
			if node.T.IsRuneMap() {
				rendered, err := node.RuneMap()
				if err != nil {
					return render.Node(node)
				}
				return rendered
			}
			return render.Node(node)
		default:
			return render.Node(node)
		}
	})), nil
}

func (s *Scope) Map(m ast.Map) (ast.Map, error) {
	m = ast.Clone(m).(ast.Map)

	outer := s
	inner, err := outer.inherit(m.C, "map", "", false)
	if err != nil {
		return ast.Map{}, err
	}

	if m.T.IsRuneMap() {
		return inner.runeMap(m)
	}

	var wg sync.WaitGroup

	m.T, err = inner.makeMapType(m)
	if err != nil {
		return ast.Map{}, err
	}
	m.T, err = inner.MapType(m.T)
	if err != nil {
		return ast.Map{}, err
	}

	wg.Add(len(m.Keys) + len(m.Values))
	errsKey := make([]error, len(m.Keys))
	errsValue := make([]error, len(m.Values))
	go func() {
		for i, key := range m.Keys {
			i, key := i, key
			go concurrency.Do(pprof.Labels("ctx", ast.ToCtx(key).String()), func(_ context.Context) {
				m.Keys[i], errsKey[i] = inner.Node(key)
				wg.Done()
			})
		}
	}()
	go func() {
		for i, value := range m.Values {
			i, value := i, value
			go concurrency.Do(pprof.Labels("ctx", ast.ToCtx(value).String()), func(_ context.Context) {
				m.Values[i], errsValue[i] = inner.Node(value)
				wg.Done()
			})
		}
	}()

	wg.Wait()
	for _, err := range errsKey {
		if err != nil {
			return ast.Map{}, err
		}
	}
	for _, err := range errsValue {
		if err != nil {
			return ast.Map{}, err
		}
	}

	return m, nil
}

func (s *Scope) makeMapType(m ast.Map) (ast.MapType, error) {
	t := m.T.Clone().(ast.MapType)
	var err error
	t.Key = ast.AnyType{}
	t.Value = ast.AnyType{}
	for _, key := range m.Keys {
		t.Key, err = Merge(t.Key, ast.ToType(key))
		if err != nil {
			return ast.MapType{}, err
		}
	}
	for _, value := range m.Values {
		t.Value, err = Merge(t.Value, ast.ToType(value))
		if err != nil {
			return ast.MapType{}, err
		}
	}
	return t, nil
}

func (s *Scope) MapType(mapType ast.MapType) (ast.MapType, error) {
	mapType = ast.CloneType(mapType).(ast.MapType)
	var err error
	mapType.Key, err = s.Type(mapType.Key)
	if err != nil {
		return ast.MapType{}, err
	}
	mapType.Value, err = s.Type(mapType.Value)
	if err != nil {
		return ast.MapType{}, err
	}
	return mapType, nil
}

func (s *Scope) coercibleMapToMapType(from ast.Map, to ast.MapType) (bool, error) {
	if !from.T.Consecutive().CompatibleWith(to.Consecutive()) {
		return false, nil
	}
	if !ast.IsAnyType(to.Key) {
		for _, key := range from.Keys {
			ok, err := s.coerceOk(key, to.Key)
			if err != nil {
				return false, err
			}
			if !ok {
				return false, nil
			}
		}
	}
	if !ast.IsAnyType(to.Value) {
		for _, value := range from.Values {
			ok, err := s.coerceOk(value, to.Value)
			if err != nil {
				return false, err
			}
			if !ok {
				return false, nil
			}
		}
	}
	return true, nil
}
