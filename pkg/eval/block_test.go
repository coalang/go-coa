package eval

import (
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"testing"
)

func TestScope_Block(t *testing.T) {
	type test struct {
		scope *Scope
		block ast.Block
		want  ast.Block
	}
	tests := map[string]test{
		"sanity": {
			scope: NewScope(ast.NewBuiltinCtx("test"), true, nil, lib.StaticFS),
			block: ast.Block{
				C: ast.Ctx{
					Filepath: "test",
				},
				T: ast.BlockType{
					C: ast.Ctx{
						Filepath: "test2",
					},
					Self:     ast.NumType{},
					SelfName: "selfName",
				},
				Content:   []ast.Node{},
				DebugName: "name",
			},
			want: ast.Block{
				C: ast.Ctx{
					Filepath: "test",
				},
				T: ast.BlockType{
					C: ast.Ctx{
						Filepath: "test2",
					},
					Self:     ast.NumType{},
					SelfName: "selfName",
				},
				Content:   []ast.Node{},
				DebugName: "name",
			},
		},
	}
	for name, test := range tests {
		test := test
		t.Run(name, func(t *testing.T) {
			got, err := test.scope.Block(test.block)
			if err != nil {
				t.Error(err)
			}
			if !ast.EqualHash(test.want, got) {
				t.Errorf("mismatch: %v and %v", test.want, got)
			}
		})
	}
}

//func TestScope_CallBlock(t *testing.T) {
//	type test struct {
//		scope *scope
//		call  ast.Key
//		want  ast.Node
//	}
//	tests := map[string]test{
//		"sanity": {
//			scope: NewScopeWithMap(ast.NewBuiltinCtx("test"), map[ast.ID]ast.Node{
//				"Coerce": ast.NewDispatcher2(ast.Origin{}, []ast.Value{}),
//			}, true),
//			call: ast.Key{
//				O:         ast.Origin{
//					Filepath: "test",
//					Line: 1,
//				},
//				T:         ast.BlockType{
//					O:         ast.Origin{
//						Filepath: "test",
//						Line: 2,
//					},
//					Arg1:      astx.PlaceholderType{
//						ID:          "self",
//						P:           true,
//					},
//					Arg1ID:  "selfName",
//					Re:        astx.PlaceholderType{
//						ID:          "re",
//						P:           true,
//					},
//					Arg2: astx.PlaceholderType{
//						ID:          "argOrArgs",
//						P:           true,
//					},
//					Arg2ID:   "argName",
//				},
//				Ee:    ast.Value{
//					O:       ast.Origin{
//						Filepath: "test",
//						Line: 3,
//					},
//					T:       ast.BlockType{},
//					Content: []ast.Node{},
//					DebugName:    "callee",
//				},
//				Arg1:      astx.Placeholder{
//					ID:          "self",
//					P:           true,
//				},
//				Arg2: astx.Placeholder{
//					ID:          "arg",
//					P:           true,
//				},
//			},
//			want: nil,
//		},
//	}
//	for name, test := range tests {
//		test := test
//		t.Run(name, func(t *testing.T) {
//			got, err := test.scope.callBlock(test.call)
//			if err != nil {
//				t.Error(err)
//			}
//			if !ast.EqualHash(test.want, got) {
//				t.Errorf("mismatch: %s and %s", test.want, got)
//			}
//		})
//	}
//}
