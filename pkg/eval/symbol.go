package eval

import "gitlab.com/coalang/go-coa/pkg/ast"

func (s *Scope) Symbol(symbol ast.Symbol) (ast.Symbol, error) {
	return symbol, nil
}

func (s *Scope) SymbolType(symbolType ast.SymbolType) (ast.SymbolType, error) {
	return symbolType, nil
}
