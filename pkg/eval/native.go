package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type NativeImpl func(*Scope, ast.Call) (ast.Node, error)

func (s *Scope) Native(native ast.Native) (ast.Native, error) {
	_, _, ok := s.Natives.Get(native.ID)
	if !ok {
		return ast.Native{}, s.Errorf(native.C, "impl for %s native %s not found", native.P, native.ID)
	}
	return native, nil
}

func (s *Scope) callNative(call ast.Call) (ast.Node, error) {
	//s.Logf("callNative %s %s", call.C, render.Call(call))
	native := call.Ee.(ast.Native)
	native, nativeImpl := s.Natives.GetSingle(native.ID)
	if nativeImpl == nil {
		return nil, call.C.Errorf("native %s %w", native.ID, errs.ErrNotFound)
	}
	if s.Pure() && bool(!call.IsPure()) {
		return nil, errs.ErrNotImplemented
	}
	var err error
	var ok bool
	var self, arg ast.Node
	self, ok, err = s.Coerce(call.Self, native.T.Self)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, s.Errorf(call.C, "native self: %w", errs.NewErrTypeMismatch(native.T.Self, call.Self))
	}
	arg, ok, err = s.Coerce(call.Arg, native.T.Arg)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, s.Errorf(call.C, "native arg: %w", errs.NewErrTypeMismatch(native.T.Arg, call.Arg))
	}
	call.Self = self
	call.Arg = arg
	return nativeImpl(s, call)
}
