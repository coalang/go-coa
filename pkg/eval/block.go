package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

func (s *Scope) blockContent(block ast.Block) (ast.Node, error) {
	block = ast.Clone(block).(ast.Block)
	var blockRe, nodeRe ast.Node
	var err error
	for _, node := range block.Content {
		nodeRe, err = s.Node(node)
		if err != nil {
			return nil, err
		}
		if nodeRe != nil {
			blockRe = nodeRe
		}
	}
	return blockRe, nil
}

func (s *Scope) Callable(callable ast.Callable) (ast.Callable, error) {
	return ast.CallableNode(callable.Node())
}

func (s *Scope) Block(block ast.Block) (ast.Block, error) {
	block = ast.Clone(block).(ast.Block)
	var err error
	block.T, err = s.BlockType(block.T)
	if err != nil {
		return ast.Block{}, err
	}
	return block, nil
}

func (s *Scope) BlockType(blockType ast.BlockType) (ast.BlockType, error) {
	blockType = ast.Clone(blockType).(ast.BlockType)
	var err error
	blockType.Self, err = s.Type(blockType.Self)
	if err != nil {
		return ast.BlockType{}, err
	}
	blockType.Arg, err = s.Type(blockType.Arg)
	if err != nil {
		return ast.BlockType{}, err
	}
	blockType.Re, err = s.Type(blockType.Re)
	if err != nil {
		return ast.BlockType{}, err
	}
	return blockType, nil
}

func (s *Scope) BlockTypeCoercible(b ast.BlockType, t ast.BlockType) (bool, error) {
	self, err := s.coerceOk(b.Self, t.Self)
	if err != nil {
		return false, err
	}
	re, err := s.coerceOk(b.Re, t.Re)
	if err != nil {
		return false, err
	}
	arg, err := s.coerceOk(b.Arg, t.Arg)
	if err != nil {
		return false, err
	}
	return self && re && arg, nil
}

func (s *Scope) callBlock(call ast.Call) (ast.Node, error) {
	//TODO: blockを孤独に実行するか検討
	//return s.callBlockLone(call, true)
	return s.callBlockLone(call, false)
}

func (s *Scope) callBlockLone(call ast.Call, lone bool) (ast.Node, error) {
	if bool(call.Ee.IsPure()) && s.current.Lone {
		stack := s.Stack()
		for i, frame := range stack {
			if frame.Call == call.Hash() {
				return nil, s.Errorf(
					call.C,
					"%w: hash-equal call on stack:\n%s",
					errs.ErrLoop,
					stack.StringOnly(map[int]string{i: "first called"}),
				)
			}
		}
	}

	block := call.Ee.(ast.Block)
	outer := s
	inner, err := outer.inherit(call.C, block.DebugName, call.Hash(), lone)
	if err != nil {
		return nil, err
	}

	ok, err := inner.coerceOk(call.Self, block.T.Self)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, inner.Errorf(call.C, "block self: %w", errs.NewErrTypeMismatch(block.T.Self, ast.ToType(call.Self)))
	}

	ok, err = inner.coerceOk(call.Arg, block.T.Arg)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, inner.Errorf(call.C, "block arg: %w", errs.NewErrTypeMismatch(block.T.Arg, ast.ToType(call.Arg)))
	}

	if block.T.SelfName == "" {
		block.T.SelfName = ast.IDSelf
	}
	err = inner.Set(block.T.SelfName, call.Self)
	if err != nil {
		return nil, err
	}
	if block.T.ArgName == "" {
		block.T.ArgName = ast.IDArg
	}
	err = inner.Set(block.T.ArgName, call.Arg)
	if err != nil {
		return nil, err
	}

	result, err := inner.blockContent(block)
	if err != nil {
		return nil, err
	}
	return s.CoerceOld(result, block.T.Re)
}
