package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"io/fs"
	"path"
	"path/filepath"
	"strings"
)

// TODO: only make hidden pods available to pods in same podDir
//func isHidden(p string) bool {
//	ch := filepath.Base(p)[0]
//	return !(ch != '_' && ch != '.')
//}

// PodPath returns an appropriate ast.Pod from rootPath.
func (s *Scope) PodPath(rootPath string) (ast.Pod, error) {
	ctx := ast.Ctx{Filepath: rootPath}
	file, err := s.fs.Open(rootPath)
	if err != nil {
		return ast.Pod{}, ctx.Errorf("podFile: %w", err)
	}

	stat, err := file.Stat()
	if err != nil {
		return ast.Pod{}, ctx.Errorf("stat: %s", err)
	}

	if stat.IsDir() {
		return s.podDir(rootPath)
	} else {
		return s.podFile(rootPath)
	}
}

func (s *Scope) podDir(p string) (ast.Pod, error) {
	ctx := ast.Ctx{Filepath: p}
	dirEntries, err := fs.ReadDir(s.fs, p)
	if err != nil {
		return ast.Pod{}, ctx.Errorf("read podDir: %w", err)
	}
	subs := make([]ast.StructSub, 0)
	for _, entry := range dirEntries {
		if !entry.IsDir() && path.Ext(entry.Name()) != ".coa" {
			continue
		}

		key := strings.TrimSuffix(entry.Name(), path.Ext(entry.Name()))
		p2 := filepath.Join(p, entry.Name())
		value, err := s.PodPath(p2)
		if err != nil {
			return ast.Pod{}, err
		}
		subs = append(subs, ast.StructSub{
			StructTypeSub: ast.StructTypeSub{
				Key:     key,
				Default: value,
				Typ:     value.Type(),
			},
			Val: value,
		})
	}
	return ast.Pod{
		C:      ctx,
		Inside: ast.NewStruct(ctx, subs...),
	}, nil
}

func (s *Scope) podFile(p string) (ast.Pod, error) {
	return ast.Pod{
		C: ast.Ctx{
			Filepath: p,
		},
		Path: p,
	}, nil
}
