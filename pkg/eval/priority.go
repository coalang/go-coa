package eval

import "gitlab.com/coalang/go-coa/pkg/ast"

func (s *Scope) Priority(priority ast.Priority) (ast.Node, error) {
	return s.callBlockLone(ast.Call{
		C:  priority.Ctx(),
		Ee: priority.B,
	}, false)
}
