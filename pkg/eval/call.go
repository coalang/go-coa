package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

func (s *Scope) Call(call ast.Call) (ast.Call, error) {
	call = ast.Clone(call).(ast.Call)
	var err error
	call.T, err = s.BlockType(call.T)
	if err != nil {
		return ast.Call{}, err
	}
	call.Ee, err = s.Node(call.Ee)
	if err != nil {
		return ast.Call{}, err
	}
	call.Self, err = s.Node(call.Self)
	if err != nil {
		return ast.Call{}, err
	}
	call.Arg, err = s.Node(call.Arg)
	if err != nil {
		return ast.Call{}, err
	}
	return call, nil
}

func (s *Scope) CallCall(call ast.Call) (ast.Node, error) {
	call, err := s.Call(call)
	if err != nil {
		return nil, err
	}

	switch call.Ee.(type) {
	// these Node can become callable Node
	// TODO: check if there are any remaining
	case ast.Pod:
		return s.callPod(call)
	case ast.Ref:
		return s.callRef(call)
	case ast.Callable:
		call = call.Clone().(ast.Call)
		call.Ee = call.Ee.(ast.Callable).Node()
		return s.CallCall(call)

	case ast.Native:
		return s.callNative(call)
	case ast.Block:
		return s.callBlock(call)
	case ast.Dispatcher2:
		return s.callDispatcher2(call, -1)
	default:
		return nil, s.Errorf(call.C, "%T %w for CallCall", call.Ee, errs.ErrInvalid)
	}
}
