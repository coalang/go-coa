package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func (s *Scope) Label(label ast.Label) (ast.Node, error) {
	// TODO: 後でccc21j2がreassignment無しで出来るようにする
	//if _, ok := s.Get(label.Er); ok {
	//	return nil, label.C.Errorf("cannot reassign to %s", label.Er)
	//}
	ee, err := s.Node(label.Ee)
	if err != nil {
		return nil, err
	}

	err = s.Set(label.Er, ee)
	if err != nil {
		return nil, err
	}
	return ast.Label{
		C:  label.C,
		Er: label.Er,
		Ee: ee,
	}, nil
}
