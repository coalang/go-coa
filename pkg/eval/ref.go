package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

func (s *Scope) RefType(ref ast.Ref) (ast.Type, error) {
	node, err := s.ref(s.disallowedRefs, ref)
	if err != nil {
		return nil, err
	}
	switch node := node.(type) {
	case ast.Type:
		return node, nil
	default:
		return nil, ref.C.Errorf("ref %s was %T, not a type", render.Ref(ref), node)
	}
}

func (s *Scope) callRef(call ast.Call) (ast.Node, error) {
	call = call.Clone().(ast.Call)
	var err error
	call.Ee, err = s.Ref(call.Ee.(ast.Ref))
	if err != nil {
		return nil, err
	}
	return s.CallCall(call)
}

func (s *Scope) Ref(ref ast.Ref) (ast.Node, error) {
	node, err := s.ref(s.disallowedRefs, ref.ToComma())
	if err != nil {
		return nil, err
	}
	if pod, ok := node.(ast.Pod); ok {
		return s.Pod(pod)
	}
	return node, nil
}

func (s *Scope) ref(disallowedRefs [][]string, ref ast.Ref) (ast.Node, error) {
	// do the cheapest checks first
	if len(ref.IDs) == 0 {
		return nil, ref.C.Errorf("%w: ast.Ref.IDs len is 0", errs.ErrInvalid)
	}
	if ref.IsUnderscore() {
		return nil, nil
	}
	if ref.IsDisallowed(disallowedRefs) {
		return nil, s.Errorf(ref.C, "%w in scope (ref): %s", errs.ErrDisallowed, render.Ref(ref))
	}

	node, ok := s.Get(ref.IDs[0])
	if !ok {
		return nil, s.Errorf(ref.C, "%w in scope (ref): %s", errs.ErrNotFound, render.Ref(ref))
	}
	if len(ref.IDs) == 1 {
		return node, nil
	}

	re, err := s.refNode(disallowedRefs, node, ast.Ref{
		C:   ref.C,
		IDs: ref.IDs[1:],
	})
	if err != nil {
		return nil, s.Errorf(ref.C, "%s: %w", render.Ref(ref), err)
	}
	return re, nil
}

func (s *Scope) refNode(disallowedRefs [][]string, node ast.Node, ref ast.Ref) (ast.Node, error) {
	node2, err := s.Node(node)
	if err != nil {
		return nil, s.Wrap(node.Ctx(), err)
	}
	if len(ref.IDs) == 0 {
		return nil, s.Errorf(ref.C, "%w: ast.Ref.IDs len is 0", errs.ErrInvalid)
	}

	switch node2 := node2.(type) {
	case ast.Struct:
		val, ok := node2.Get(ref.IDs[0])
		if !ok {
			return nil, s.Errorf(ref.C, "%w: %s in struct", errs.ErrNotFound, ref.IDs[0])
		}
		if len(ref.IDs) == 1 {
			return val, nil
		}
		return s.refNode(disallowedRefs, val, ast.Ref{
			C:   ref.C,
			IDs: ref.IDs[1:],
		})
	case ast.NativeType:
		if len(ref.IDs) != 1 {
			return nil, s.Errorf(ref.C, "%w: ref must be singular", errs.ErrInvalid)
		}
		id, err := utils.UUIDFrom(ref.IDs[0])
		if err != nil {
			return nil, s.Errorf(ref.C, "invalid UUID: %w", err)
		}
		native, _, ok := s.Natives.Get(id)
		if !ok {
			return nil, s.Errorf(ref.C, "native %s not found", id)
		}
		return native, nil
	case ast.ExternalType:
		if len(ref.IDs) != 1 {
			return nil, s.Errorf(ref.C, "%w: ref must be singular", errs.ErrInvalid)
		}
		id, err := utils.UUIDFrom(ref.IDs[0])
		if err != nil {
			return nil, s.Errorf(ref.C, "invalid UUID: %w", err)
		}
		external, ok := s.Externals.Get(id)
		if !ok {
			return nil, s.Errorf(ref.C, "external %s not found", id)
		}
		return external, nil
	default:
		return nil, s.Errorf(ref.C, "%T which is invalid in refNode", node2)
	}
}
