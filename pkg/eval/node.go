package eval

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

func (s *Scope) Nodes(nodes []ast.Node) error {
	var err error
	for i := range nodes {
		nodes[i], err = s.Node(nodes[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Scope) Node(node ast.Node) (ast.Node, error) {
	switch node := node.(type) {
	case nil:
		return nil, nil
	case ast.Ref: // if ref is a node
		return s.Ref(node)
	case ast.Type:
		return s.Type(node)
	case ast.Call:
		return s.CallCall(node)
	case ast.Callable:
		return s.Node(node.Node())

	case ast.Block:
		return s.Block(node)
	case ast.Cast:
		return s.Cast(node)
	case ast.Dispatcher2:
		return s.Dispatcher2(node)
	case ast.Label:
		return s.Label(node)
	case ast.Struct:
		return s.Struct(node)
	case ast.Map:
		return s.Map(node)
	case ast.Rune:
		return node, nil
	case ast.Native:
		return s.Native(node)
	case ast.Num:
		return s.Num(node)
	case ast.Pod:
		return s.Pod(node)
	case ast.Priority:
		return s.Priority(node)
	case ast.Symbol:
		return s.Symbol(node)
	case ast.Stream:
		return node, nil
	default:
		return nil, node.Ctx().Errorf("%T %w for Node", node, errs.ErrInvalid)
	}
}
