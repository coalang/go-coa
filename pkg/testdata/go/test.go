package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	start := time.Now()
	buf := bufio.NewWriter(os.Stdout)
	for i := int64(1); i <= 100; i++ {
		line := ""
		if i%3 == 0 {
			line += "Fizz"
		}
		if i%5 == 0 {
			line += "Buzz"
		}
		if len(line) == 0 {
			line = strconv.FormatInt(i, 10)
		}
		_, _ = buf.WriteString(line + "\n")
	}
	_ = buf.Flush()
	end := time.Now()
	fmt.Println(end.Sub(start))
}
