import time

start = time.time()
print(*(str(i) if (line := 'Fizz' if i % 3 == 0 else '' + 'Buzz' if i % 5 == 0 else '') == '' else line
        for i in range(1, 101)), sep='\n')
end = time.time()
print((end-start)*1e6)
