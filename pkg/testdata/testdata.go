package testdata

import (
	"embed"
	_ "embed"
	"fmt"
	"io/fs"
)

//go:embed *
var files embed.FS
var Files = map[string]string{}

func init() {
	matches, err := fs.Glob(files, "*.coa")
	if err != nil {
		panic(err)
	}
	for _, match := range matches {
		if match[0] == '.' {
			continue
		}
		b, err := files.ReadFile(match)
		if err != nil {
			panic(fmt.Errorf("failed to open file: %w", err))
		}
		Files[match] = string(b)
	}
}
