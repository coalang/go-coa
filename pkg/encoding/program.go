package encoding

import "gitlab.com/coalang/go-coa/pkg/ast"

type Program struct {
	C       ast.Ctx `json:"c,omitempty"`
	Content []Node  `json:"content,omitempty"`
}

func EncodeProgram(program ast.Program) (Program, error) {
	program = program.Clone()
	content, err := EncodeNodes(program.Content)
	if err != nil {
		return Program{}, err
	}
	return Program{
		C:       program.C,
		Content: content,
	}, nil
}

func DecodeProgram(program Program) (ast.Program, error) {
	content, err := DecodeNodes(program.Content)
	if err != nil {
		return ast.Program{}, err
	}
	return ast.Program{
		C:       program.C,
		Content: content,
	}, nil
}
