package encoding

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
	"sync"
)

type Type struct {
	Block *Block `json:"block,omitempty"`

	AnyType         *ast.AnyType         `json:"any_type,omitempty"`
	BlockType       *BlockType           `json:"block_type,omitempty"`
	DispatcherType2 *ast.DispatcherType2 `json:"dispatcher_type_2,omitempty"`
	ExternalType    *ast.ExternalType    `json:"external_type,omitempty"`
	Inter           *Inter               `json:"inter,omitempty"`
	MapType         *MapType             `json:"map_type,omitempty"`
	NativeType      *NativeType          `json:"native_type,omitempty"`
	NumType         *ast.NumType         `json:"num_type,omitempty"`
	PodType         *ast.PodType         `json:"pod_type,omitempty"`
	PromiseType     *PromiseType         `json:"promise_type,omitempty"`
	RootType        *ast.RootType        `json:"root_type,omitempty"`
	RuneType        *ast.RuneType        `json:"rune_type,omitempty"`
	StreamType      *StreamType          `json:"stream_type,omitempty"`
	StructType      *StructType          `json:"struct_type,omitempty"`
	SymbolType      *ast.SymbolType      `json:"unique_type,omitempty"`
	Ref             *Ref                 `json:"ref,omitempty"`
}

func (t Type) Kind() NodeTypeKind {
	switch {
	case t.Block != nil:
		return BlockKind

	case t.AnyType != nil:
		return AnyTypeKind
	case t.BlockType != nil:
		return BlockTypeKind
	case t.DispatcherType2 != nil:
		return DispatcherType2Kind
	case t.ExternalType != nil:
		return ExternalTypeKind
	case t.Inter != nil:
		return InterKind
	case t.MapType != nil:
		return MapTypeKind
	case t.NativeType != nil:
		return NativeTypeKind
	case t.NumType != nil:
		return NumTypeKind
	case t.PodType != nil:
		return PodTypeKind
	case t.PromiseType != nil:
		return PromiseTypeKind
	case t.RootType != nil:
		return RootTypeKind
	case t.RuneType != nil:
		return RuneTypeKind
	case t.StreamType != nil:
		return StreamTypeKind
	case t.StructType != nil:
		return StructTypeKind
	case t.SymbolType != nil:
		return SymbolTypeKind
	case t.Ref != nil:
		return RefKind
	default:
		return NoKind
	}
}

func (t Type) IsZero() bool {
	return t.Block == nil &&
		t.AnyType == nil &&
		t.BlockType == nil &&
		t.DispatcherType2 == nil &&
		t.ExternalType == nil &&
		t.Inter == nil &&
		t.MapType == nil &&
		t.NativeType == nil &&
		t.NumType == nil &&
		t.PodType == nil &&
		t.PromiseType == nil &&
		t.RootType == nil &&
		t.RuneType == nil &&
		t.StreamType == nil &&
		t.StructType == nil &&
		t.SymbolType == nil &&
		t.Ref == nil
}

func EncodeTypes(types []ast.Type) ([]Type, error) {
	re := make([]Type, len(types))
	errs := make(chan error, len(types))
	var wg sync.WaitGroup
	wg.Add(len(types))
	for i, type_ := range types {
		go func(i int, type_ ast.Type) {
			p2, err := EncodeType(type_)
			if err != nil {
				errs <- err
			} else {
				re[i] = p2
			}
			wg.Done()
		}(i, type_)
	}
	wg.Wait()
	select {
	case err := <-errs:
		return nil, err
	default:
		return re, nil
	}
}

func DecodeTypes(types []Type) ([]ast.Type, error) {
	re := make([]ast.Type, len(types))
	errs := make(chan error, len(types))
	var wg sync.WaitGroup
	wg.Add(len(types))
	for i, type_ := range types {
		go func(i int, type_ Type) {
			p2, err := DecodeType(type_)
			if err != nil {
				errs <- err
			} else {
				re[i] = p2
			}
			wg.Done()
		}(i, type_)
	}
	wg.Wait()
	select {
	case err := <-errs:
		return nil, err
	default:
		return re, nil
	}
}

func EncodeType(p ast.Type) (Type, error) {
	switch p := p.(type) {
	case ast.Block:
		block, err := EncodeBlock(p)
		if err != nil {
			return Type{}, err
		}
		return Type{Block: &block}, nil

	case ast.AnyType:
		return Type{
			AnyType: &ast.AnyType{},
		}, nil
	case ast.BlockType:
		blockType, err := EncodeBlockType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			BlockType: &blockType,
		}, nil
	case ast.DispatcherType2:
		return Type{
			DispatcherType2: &ast.DispatcherType2{},
		}, nil
	case ast.ExternalType:
		return Type{
			ExternalType: &ast.ExternalType{},
		}, nil
	case ast.Inter:
		structType, err := EncodeStructType(p.T)
		if err != nil {
			return Type{}, err
		}
		return Type{Inter: &Inter{T: structType}}, nil
	case ast.MapType:
		mapType, err := EncodeMapType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			MapType: &mapType,
		}, nil
	case ast.NativeType:
		nativeType, err := EncodeNativeType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			NativeType: &nativeType,
		}, nil
	case ast.NumType:
		return Type{
			NumType: &p,
		}, nil
	case ast.PodType:
		return Type{
			PodType: &ast.PodType{},
		}, nil
	case ast.PromiseType:
		promiseType, err := EncodePromiseType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			PromiseType: &promiseType,
		}, nil
	case ast.RootType:
		return Type{
			RootType: &ast.RootType{},
		}, nil
	case ast.RuneType:
		return Type{
			RuneType: &ast.RuneType{},
		}, nil
	case ast.StreamType:
		streamType, err := EncodeStreamType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			StreamType: &streamType,
		}, nil
	case ast.StructType:
		structType, err := EncodeStructType(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			StructType: &structType,
		}, nil
	case ast.SymbolType:
		return Type{
			SymbolType: &ast.SymbolType{},
		}, nil
	case ast.Ref:
		ref, err := EncodeRef(p)
		if err != nil {
			return Type{}, err
		}
		return Type{
			Ref: &ref,
		}, nil
	case nil:
		return Type{}, nil
	default:
		return Type{}, p.Ctx().Errorf("invalid type %T %s", p, render.Type(p))
	}
}

func DecodeType(type_ Type) (ast.Type, error) {
	switch type_.Kind() {
	case BlockKind:
		return DecodeBlock(*type_.Block)

	case NoKind:
		return nil, nil
	case AnyTypeKind:
		return *type_.AnyType, nil
	case BlockTypeKind:
		return DecodeBlockType(*type_.BlockType)
	case DispatcherType2Kind:
		return *type_.DispatcherType2, nil
	case ExternalTypeKind:
		return *type_.ExternalType, nil
	case InterKind:
		structType, err := DecodeStructType(type_.Inter.T)
		if err != nil {
			return nil, err
		}
		return ast.Inter{T: structType}, nil
	case MapTypeKind:
		return DecodeMapType(*type_.MapType)
	case NativeTypeKind:
		return DecodeNativeType(*type_.NativeType)
	case NumTypeKind:
		return *type_.NumType, nil
	case PodTypeKind:
		return *type_.PodType, nil
	case PromiseTypeKind:
		return DecodePromiseType(*type_.PromiseType)
	case RootTypeKind:
		return *type_.RootType, nil
	case RuneTypeKind:
		return *type_.RuneType, nil
	case StreamTypeKind:
		return DecodeStreamType(*type_.StreamType)
	case StructTypeKind:
		return DecodeStructType(*type_.StructType)
	case SymbolTypeKind:
		return *type_.SymbolType, nil
	case RefKind:
		return DecodeRef(*type_.Ref)
	default:
		return nil, fmt.Errorf("invalid type kind %s", type_.Kind())
	}
}

type BlockType struct {
	C        ast.Ctx `json:"c,omitempty"`
	Self     Type    `json:"self,omitempty"`
	SelfName ast.ID  `json:"self_name,omitempty"`
	Re       Type    `json:"re,omitempty"`
	Arg      Type    `json:"arg,omitempty"`
	ArgName  ast.ID  `json:"arg_name,omitempty"`
}

func EncodeBlockType(blockType ast.BlockType) (BlockType, error) {
	self, err := EncodeType(blockType.Self)
	if err != nil {
		return BlockType{}, err
	}
	re, err := EncodeType(blockType.Re)
	if err != nil {
		return BlockType{}, err
	}
	arg, err := EncodeType(blockType.Arg)
	if err != nil {
		return BlockType{}, err
	}
	return BlockType{
		C:        blockType.C,
		Self:     self,
		SelfName: blockType.SelfName,
		Re:       re,
		Arg:      arg,
		ArgName:  blockType.ArgName,
	}, nil
}

func DecodeBlockType(blockType BlockType) (ast.BlockType, error) {
	self, err := DecodeType(blockType.Self)
	if err != nil {
		return ast.BlockType{}, err
	}
	re, err := DecodeType(blockType.Re)
	if err != nil {
		return ast.BlockType{}, err
	}
	arg, err := DecodeType(blockType.Arg)
	if err != nil {
		return ast.BlockType{}, err
	}
	return ast.BlockType{
		C:        blockType.C,
		Self:     self,
		SelfName: blockType.SelfName,
		Re:       re,
		Arg:      arg,
		ArgName:  blockType.ArgName,
	}, nil
}

type MapType struct {
	C     ast.Ctx `json:"c,omitempty"`
	Key   Type    `json:"key,omitempty"`
	Value Type    `json:"value,omitempty"`
}

func EncodeMapType(mapType ast.MapType) (MapType, error) {
	key, err := EncodeType(mapType.Key)
	if err != nil {
		return MapType{}, err
	}
	value, err := EncodeType(mapType.Value)
	if err != nil {
		return MapType{}, err
	}
	return MapType{
		C:     mapType.C,
		Key:   key,
		Value: value,
	}, nil
}

func DecodeMapType(mapType MapType) (ast.MapType, error) {
	key, err := DecodeType(mapType.Key)
	if err != nil {
		return ast.MapType{}, err
	}
	value, err := DecodeType(mapType.Value)
	if err != nil {
		return ast.MapType{}, err
	}
	return ast.MapType{
		C:     mapType.C,
		Key:   key,
		Value: value,
	}, nil
}

type NativeType struct {
	C     ast.Ctx   `json:"c,omitempty"`
	Inter BlockType `json:"type,omitempty"`
}

func EncodeNativeType(nativeType ast.NativeType) (NativeType, error) {
	blockType, err := EncodeBlockType(nativeType.Inter)
	if err != nil {
		return NativeType{}, err
	}
	return NativeType{
		C:     nativeType.C,
		Inter: blockType,
	}, nil
}

func DecodeNativeType(nativeType NativeType) (ast.NativeType, error) {
	blockType, err := DecodeBlockType(nativeType.Inter)
	if err != nil {
		return ast.NativeType{}, err
	}
	return ast.NativeType{
		C:     nativeType.C,
		Inter: blockType,
	}, nil
}

type Inter struct {
	T StructType `json:"t,omitempty"`
}

type PromiseType struct {
	C        ast.Ctx `json:"c,omitempty"`
	Promised Type    `json:"promised,omitempty"`
}

func EncodePromiseType(promiseType ast.PromiseType) (PromiseType, error) {
	promised, err := EncodeType(promiseType.Promised)
	if err != nil {
		return PromiseType{}, err
	}
	return PromiseType{
		C:        promiseType.C,
		Promised: promised,
	}, nil
}

func DecodePromiseType(promiseType PromiseType) (ast.PromiseType, error) {
	promised, err := DecodeType(promiseType.Promised)
	if err != nil {
		return ast.PromiseType{}, err
	}
	return ast.PromiseType{
		C:        promiseType.C,
		Promised: promised,
	}, nil
}

type StreamType struct {
	C         ast.Ctx             `json:"c,omitempty"`
	Value     Type                `json:"value,omitempty"`
	Direction ast.StreamDirection `json:"direction,omitempty"`
}

func EncodeStreamType(streamType ast.StreamType) (StreamType, error) {
	value, err := EncodeType(streamType.Value)
	if err != nil {
		return StreamType{}, err
	}
	return StreamType{
		C:         streamType.C,
		Value:     value,
		Direction: streamType.Direction,
	}, nil
}

func DecodeStreamType(streamType StreamType) (ast.StreamType, error) {
	value, err := DecodeType(streamType.Value)
	if err != nil {
		return ast.StreamType{}, err
	}
	return ast.StreamType{
		C:         streamType.C,
		Value:     value,
		Direction: streamType.Direction,
	}, nil
}

type StructType struct {
	C        ast.Ctx  `json:"c,omitempty"`
	Keys     []ast.ID `json:"keys,omitempty"`
	Values   []Type   `json:"values,omitempty"`
	Defaults []Node   `json:"defaults,omitempty"`
	Enum     bool     `json:"exclusive,omitempty"`
}

func EncodeStructType(structType ast.StructType) (StructType, error) {
	values, err := EncodeTypes(structType.Values)
	if err != nil {
		return StructType{}, err
	}
	defaults, err := EncodeNodes(structType.Defaults)
	if err != nil {
		return StructType{}, err
	}
	return StructType{
		C:        structType.C,
		Keys:     structType.Keys,
		Values:   values,
		Defaults: defaults,
		Enum:     structType.Enum,
	}, nil
}

func DecodeStructType(structType StructType) (ast.StructType, error) {
	values, err := DecodeTypes(structType.Values)
	if err != nil {
		return ast.StructType{}, err
	}
	defaults, err := DecodeNodes(structType.Defaults)
	if err != nil {
		return ast.StructType{}, err
	}
	return ast.StructType{
		C:        structType.C,
		Keys:     structType.Keys,
		Values:   values,
		Defaults: defaults,
		Enum:     structType.Enum,
	}, nil
}
