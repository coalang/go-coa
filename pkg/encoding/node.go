package encoding

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"sync"
)

type Node struct {
	Type *Type `json:"type,omitempty"`

	Call        *Call         `json:"call,omitempty"`
	Cast        *Cast         `json:"cast,omitempty"`
	Dispatcher2 *Dispatcher2  `json:"dispatcher_2,omitempty"`
	External    *ast.External `json:"external,omitempty"`
	Label       *Label        `json:"label,omitempty"`
	Map         *Map          `json:"map,omitempty"`
	Native      *Native       `json:"native,omitempty"`
	Num         *ast.Num      `json:"num,omitempty"`
	Pod         *Pod          `json:"pod,omitempty"`
	Priority    *Priority     `json:"priority,omitempty"`
	Promise     *Promise      `json:"promise,omitempty"`
	Rune        *ast.Rune     `json:"rune,omitempty"`
	Stream      *Stream       `json:"stream,omitempty"`
	Struct      *Struct       `json:"struct,omitempty"`
	Symbol      *ast.Symbol   `json:"unique,omitempty"`
}

func (n Node) Kind() NodeTypeKind {
	switch {
	case n.Type != nil:
		return n.Type.Kind()
	case n.Call != nil:
		return CallKind
	case n.Cast != nil:
		return CastKind
	case n.Dispatcher2 != nil:
		return Dispatcher2Kind
	case n.External != nil:
		return ExternalKind
	case n.Label != nil:
		return LabelKind
	case n.Map != nil:
		return MapKind
	case n.Native != nil:
		return NativeKind
	case n.Num != nil:
		return NumKind
	case n.Pod != nil:
		return PodKind
	case n.Priority != nil:
		return PriorityKind
	case n.Promise != nil:
		return PromiseKind
	case n.Rune != nil:
		return RuneKind
	case n.Stream != nil:
		return StreamKind
	case n.Struct != nil:
		return StructKind
	case n.Symbol != nil:
		return SymbolKind
	default:
		return NoKind
	}
}

func (n Node) IsZero() bool {
	return n.Type == nil &&
		n.Call == nil &&
		n.Cast == nil &&
		n.Dispatcher2 == nil &&
		n.External == nil &&
		n.Label == nil &&
		n.Map == nil &&
		n.Native == nil &&
		n.Num == nil &&
		n.Pod == nil &&
		n.Priority == nil &&
		n.Promise == nil &&
		n.Rune == nil &&
		n.Stream == nil &&
		n.Struct == nil &&
		n.Symbol == nil
}

func EncodeNodes(nodes []ast.Node) ([]Node, error) {
	reChan := make([]Node, len(nodes))
	errChan := make(chan error, len(nodes))
	var wg sync.WaitGroup
	wg.Add(len(nodes))
	for i, node := range nodes {
		go func(i int, node ast.Node) {
			p2, err := EncodeNode(node)
			if err != nil {
				errChan <- err
			} else {
				reChan[i] = p2
			}
			wg.Done()
		}(i, node)
	}
	wg.Wait()
	re2 := make([]Node, 0)
	for _, node := range reChan {
		if !node.IsZero() {
			re2 = append(re2, node)
		}
	}
	select {
	case err := <-errChan:
		return nil, err
	default:
		return re2, nil
	}
}

func EncodeNode(node ast.Node) (Node, error) {
	switch node := node.(type) {
	case ast.Type:
		encoded, err := EncodeType(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Type: &encoded}, nil

	case ast.Call:
		call, err := EncodeCall(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Call: &call}, nil
	case ast.Cast:
		cast, err := EncodeCast(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Cast: &cast}, nil
	case ast.Dispatcher2:
		dispatcher2, err := EncodeDispatcher2(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Dispatcher2: &dispatcher2}, nil
	case ast.External:
		return Node{External: &node}, nil
	case ast.Label:
		label, err := EncodeLabel(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Label: &label}, nil
	case ast.Map:
		map_, err := EncodeMap(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Map: &map_}, nil
	case ast.Native:
		native, err := EncodeNative(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Native: &native}, nil
	case ast.Num:
		return Node{Num: &node}, nil
	case ast.Pod:
		pod, err := EncodePod(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Pod: &pod}, nil
	case ast.Priority:
		priority, err := EncodePriority(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Priority: &priority}, nil
	case ast.Promise:
		promise, err := EncodePromise(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Promise: &promise}, nil
	case ast.Rune:
		return Node{Rune: &node}, nil
	case ast.Stream:
		stream, err := EncodeStream(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Stream: &stream}, nil
	case ast.Struct:
		p, err := EncodeStruct(node)
		if err != nil {
			return Node{}, err
		}
		return Node{Struct: &p}, nil
	case ast.Symbol:
		return Node{Symbol: &node}, nil
	case nil:
		return Node{}, nil
	default:
		return Node{}, node.Ctx().Errorf("%w node %T", errs.ErrASTInvalid, node)
	}
}

func DecodeNodes(nodes []Node) ([]ast.Node, error) {
	reChan := make([]ast.Node, len(nodes))
	errChan := make(chan error, len(nodes))
	var wg sync.WaitGroup
	wg.Add(len(nodes))
	for i, node := range nodes {
		go func(i int, node Node) {
			p2, err := DecodeNode(node)
			if err != nil {
				errChan <- err
			} else {
				reChan[i] = p2
			}
			wg.Done()
		}(i, node)
	}
	wg.Wait()
	re2 := make([]ast.Node, 0)
	for _, node := range reChan {
		if !ast.IsZero(node) {
			re2 = append(re2, node)
		}
	}
	select {
	case err := <-errChan:
		return nil, err
	default:
		return re2, nil
	}
}

func DecodeNode(node Node) (ast.Node, error) {
	switch node.Kind() {
	case NoKind:
		return nil, nil
	case CallKind:
		return DecodeCall(*node.Call)
	case CastKind:
		return DecodeCast(*node.Cast)
	case Dispatcher2Kind:
		return DecodeDispatcher2(*node.Dispatcher2)
	case ExternalKind:
		return *node.External, nil
	case LabelKind:
		return DecodeLabel(*node.Label)
	case MapKind:
		return DecodeMap(*node.Map)
	case NativeKind:
		return DecodeNative(*node.Native)
	case NumKind:
		return *node.Num, nil
	case PodKind:
		return DecodePod(*node.Pod)
	case PriorityKind:
		return DecodePriority(*node.Priority)
	case PromiseKind:
		return DecodePromise(*node.Promise)
	case RuneKind:
		return *node.Rune, nil
	case StreamKind:
		return DecodeStream(*node.Stream)
	case StructKind:
		return DecodeStruct(*node.Struct)
	case SymbolKind:
		return *node.Symbol, nil
	default:
		if node.Type != nil {
			return DecodeType(*node.Type)
		}
		return nil, fmt.Errorf("invalid node kind %s", node.Kind())
	}
}

type Block struct {
	C         ast.Ctx   `json:"c,omitempty"`
	T         BlockType `json:"type,omitempty"`
	Content   []Node    `json:"content,omitempty"`
	DebugName string    `json:"name,omitempty"`
}

func EncodeBlock(block ast.Block) (Block, error) {
	blockType, err := EncodeBlockType(block.T)
	if err != nil {
		return Block{}, err
	}
	nodes, err := EncodeNodes(block.Content)
	if err != nil {
		return Block{}, err
	}
	return Block{
		C:         block.C,
		T:         blockType,
		Content:   nodes,
		DebugName: block.DebugName,
	}, nil
}

func DecodeBlock(block Block) (ast.Block, error) {
	blockType, err := DecodeBlockType(block.T)
	if err != nil {
		return ast.Block{}, err
	}
	nodes, err := DecodeNodes(block.Content)
	if err != nil {
		return ast.Block{}, err
	}
	return ast.Block{
		C:         block.C,
		T:         blockType,
		Content:   nodes,
		DebugName: block.DebugName,
	}, nil
}

type Call struct {
	C    ast.Ctx   `json:"c,omitempty"`
	T    BlockType `json:"t,omitempty"`
	Self Node      `json:"self,omitempty"`
	Ee   Node      `json:"callee,omitempty"`
	Arg  Node      `json:"arg,omitempty"`
}

func EncodeCalls(calls []ast.Call) ([]Call, error) {
	re := make([]Call, len(calls))
	errors := make(chan error, len(calls))
	var wg sync.WaitGroup
	wg.Add(len(calls))
	for i, call := range calls {
		go func(i int, call ast.Call) {
			p2, err := EncodeCall(call)
			if err != nil {
				errors <- err
			} else {
				re[i] = p2
			}
			wg.Done()
		}(i, call)
	}
	wg.Wait()
	select {
	case err := <-errors:
		return nil, err
	default:
		return re, nil
	}
}

func EncodeCall(call ast.Call) (Call, error) {
	self, err := EncodeNode(call.Self)
	if err != nil {
		return Call{}, err
	}
	ee, err := EncodeNode(call.Ee)
	if err != nil {
		return Call{}, err
	}
	arg, err := EncodeNode(call.Arg)
	if err != nil {
		return Call{}, err
	}
	t, err := EncodeBlockType(call.T)
	if err != nil {
		return Call{}, err
	}
	return Call{
		C:    call.C,
		T:    t,
		Self: self,
		Ee:   ee,
		Arg:  arg,
	}, nil
}

func DecodeCalls(calls []Call) ([]ast.Call, error) {
	reChan := make([]ast.Call, len(calls))
	errChan := make(chan error, len(calls))
	var wg sync.WaitGroup
	wg.Add(len(calls))
	for i, call := range calls {
		go func(i int, call Call) {
			p2, err := DecodeCall(call)
			if err != nil {
				errChan <- err
			} else {
				reChan[i] = p2
			}
			wg.Done()
		}(i, call)
	}
	wg.Wait()
	select {
	case err := <-errChan:
		return nil, err
	default:
		return reChan, nil
	}
}

func DecodeCall(call Call) (ast.Call, error) {
	self, err := DecodeNode(call.Self)
	if err != nil {
		return ast.Call{}, err
	}
	ee, err := DecodeNode(call.Ee)
	if err != nil {
		return ast.Call{}, err
	}
	arg, err := DecodeNode(call.Arg)
	if err != nil {
		return ast.Call{}, err
	}
	t, err := DecodeBlockType(call.T)
	if err != nil {
		return ast.Call{}, err
	}
	return ast.Call{
		C:    call.C,
		T:    t,
		Self: self,
		Ee:   ee,
		Arg:  arg,
	}, nil
}

type Cast struct {
	C  ast.Ctx `json:"c,omitempty"`
	Er Type    `json:"caster,omitempty"`
	Ee Node    `json:"castee,omitempty"`
}

func EncodeCast(cast ast.Cast) (Cast, error) {
	er, err := EncodeType(cast.Er)
	if err != nil {
		return Cast{}, err
	}
	ee, err := EncodeNode(cast.Ee)
	if err != nil {
		return Cast{}, err
	}
	return Cast{
		C:  cast.C,
		Er: er,
		Ee: ee,
	}, nil
}

func DecodeCast(cast Cast) (ast.Cast, error) {
	er, err := DecodeType(cast.Er)
	if err != nil {
		return ast.Cast{}, err
	}
	ee, err := DecodeNode(cast.Ee)
	if err != nil {
		return ast.Cast{}, err
	}
	return ast.Cast{
		C:  cast.C,
		Er: er,
		Ee: ee,
	}, nil
}

type Label struct {
	C  ast.Ctx `json:"c,omitempty"`
	Er string  `json:"name,omitempty"`
	Ee Node    `json:"obj,omitempty"`
}

func EncodeLabel(label ast.Label) (Label, error) {
	ee, err := EncodeNode(label.Ee)
	if err != nil {
		return Label{}, err
	}
	return Label{
		C:  label.C,
		Er: label.Er,
		Ee: ee,
	}, nil
}

func DecodeLabel(label Label) (ast.Label, error) {
	ee, err := DecodeNode(label.Ee)
	if err != nil {
		return ast.Label{}, err
	}
	return ast.Label{
		C:  label.C,
		Er: label.Er,
		Ee: ee,
	}, nil
}

type Map struct {
	C       ast.Ctx `json:"c,omitempty"`
	T       MapType `json:"type,omitempty"`
	Keys    []Node  `json:"keys,omitempty"`
	Values  []Node  `json:"values,omitempty"`
	RuneMap string  `json:"rune_map,omitempty"`
}

func EncodeMap(p ast.Map) (Map, error) {
	t, err := EncodeMapType(p.T)
	if err != nil {
		return Map{}, err
	}

	if p.T.IsRuneMap() {
		text, err := p.RuneMap()
		if err != nil {
			return Map{}, err
		}
		return Map{
			C:       p.C,
			T:       t,
			RuneMap: text,
		}, nil
	}

	keys, err := EncodeNodes(p.Keys)
	if err != nil {
		return Map{}, err
	}
	values, err := EncodeNodes(p.Values)
	if err != nil {
		return Map{}, err
	}
	return Map{
		C:      p.C,
		T:      t,
		Keys:   keys,
		Values: values,
	}, nil
}

func DecodeMap(p Map) (ast.Map, error) {
	if p.RuneMap != "" {
		return ast.NewRuneMap(p.C, p.RuneMap), nil
	}
	t, err := DecodeMapType(p.T)
	if err != nil {
		return ast.Map{}, err
	}
	keys, err := DecodeNodes(p.Keys)
	if err != nil {
		return ast.Map{}, err
	}
	values, err := DecodeNodes(p.Values)
	if err != nil {
		return ast.Map{}, err
	}
	if len(keys) > len(values) {
		return ast.Map{}, p.C.Errorf("%w: more keys (%d) than values (%d)", errs.ErrInvalid, len(keys), len(values))
	} else if len(keys) < len(values) {
		// ensure length of keys and values are the same
		keys2 := make([]ast.Node, len(values))
		copy(keys2, keys)
		keys = keys2
	}
	return ast.Map{
		C:      p.C,
		T:      t,
		Keys:   keys,
		Values: values,
	}, nil
}

type Dispatcher2 struct {
	C      ast.Ctx `json:"c,omitempty"`
	Keys   []Call  `json:"keys,omitempty"`
	Values []Node  `json:"values,omitempty"`
}

func EncodeDispatcher2(d ast.Dispatcher2) (Dispatcher2, error) {
	keys, err := EncodeCalls(d.Keys)
	if err != nil {
		return Dispatcher2{}, err
	}
	values := make([]Node, len(d.Values))
	for i, value := range d.Values {
		node, err := EncodeNode(value)
		if err != nil {
			return Dispatcher2{}, err
		}
		values[i] = node
	}
	return Dispatcher2{
		C:      d.C,
		Keys:   keys,
		Values: values,
	}, nil
}

func DecodeDispatcher2(d Dispatcher2) (ast.Dispatcher2, error) {
	keys, err := DecodeCalls(d.Keys)
	if err != nil {
		return ast.Dispatcher2{}, err
	}
	values := make([]ast.Callable, len(d.Values))
	for i, value := range d.Values {
		node, err := DecodeNode(value)
		if err != nil {
			return ast.Dispatcher2{}, err
		}
		switch node := node.(type) {
		case ast.Block:
			values[i] = ast.CallableBlock(node)
		case ast.Native:
			values[i] = ast.CallableNative(node)
		default:
			panic("invalid value")
		}
	}
	return ast.Dispatcher2{
		C:      d.C,
		Keys:   keys,
		Values: values,
	}, nil
}

type Native struct {
	C  ast.Ctx      `json:"c,omitempty"`
	T  BlockType    `json:"t,omitempty"`
	P  ast.Pureness `json:"pure,omitempty"`
	ID uuid.UUID    `json:"id,omitempty"`
}

func EncodeNative(native ast.Native) (Native, error) {
	t, err := EncodeBlockType(native.T)
	if err != nil {
		return Native{}, err
	}
	return Native{
		C:  native.C,
		T:  t,
		P:  native.P,
		ID: native.ID,
	}, nil
}

func DecodeNative(native Native) (ast.Native, error) {
	t, err := DecodeBlockType(native.T)
	if err != nil {
		return ast.Native{}, err
	}
	return ast.Native{
		C:  native.C,
		T:  t,
		P:  native.P,
		ID: native.ID,
	}, nil
}

type Pod struct {
	C      ast.Ctx `json:"c,omitempty"`
	Inside Node    `json:"default,omitempty"`
	Path   string  `json:"path,omitempty"`
}

func EncodePod(pod ast.Pod) (Pod, error) {
	inside, err := EncodeNode(pod.Inside)
	if err != nil {
		return Pod{}, err
	}
	return Pod{
		C:      pod.C,
		Inside: inside,
		Path:   pod.Path,
	}, nil
}

func DecodePod(pod Pod) (ast.Pod, error) {
	inside, err := DecodeNode(pod.Inside)
	if err != nil {
		return ast.Pod{}, err
	}
	return ast.Pod{
		C:      pod.C,
		Inside: inside,
		Path:   pod.Path,
	}, nil
}

type Priority struct {
	B Block `json:"b,omitempty"`
}

func EncodePriority(priority ast.Priority) (Priority, error) {
	b, err := EncodeBlock(priority.B)
	if err != nil {
		return Priority{}, err
	}
	return Priority{B: b}, nil
}

func DecodePriority(priority Priority) (ast.Priority, error) {
	b, err := DecodeBlock(priority.B)
	if err != nil {
		return ast.Priority{}, err
	}
	return ast.Priority{B: b}, nil
}

type Promise struct {
	C  ast.Ctx     `json:"c,omitempty"`
	T  PromiseType `json:"t,omitempty"`
	ID uuid.UUID   `json:"id,omitempty"`
}

func EncodePromise(promise ast.Promise) (Promise, error) {
	t, err := EncodePromiseType(promise.T)
	if err != nil {
		return Promise{}, err
	}
	return Promise{
		C:  promise.C,
		T:  t,
		ID: promise.ID,
	}, nil
}

func DecodePromise(promise Promise) (ast.Promise, error) {
	t, err := DecodePromiseType(promise.T)
	if err != nil {
		return ast.Promise{}, err
	}
	return ast.Promise{
		C:  promise.C,
		T:  t,
		ID: promise.ID,
	}, nil
}

type Ref struct {
	C   ast.Ctx  `json:"c,omitempty"`
	IDs []ast.ID `json:"ids,omitempty"`
}

func EncodeRef(ref ast.Ref) (Ref, error) {
	return Ref{
		C:   ref.C,
		IDs: ref.IDs,
	}, nil
}

func DecodeRef(ref Ref) (ast.Ref, error) {
	return ast.Ref{
		C:   ref.C,
		IDs: ref.IDs,
	}, nil
}

type Stream struct {
	C  ast.Ctx      `json:"c,omitempty"`
	T  StreamType   `json:"t,omitempty"`
	ID uuid.UUID    `json:"id,omitempty"`
	P  ast.Pureness `json:"p,omitempty"`
}

func EncodeStream(stream ast.Stream) (Stream, error) {
	t, err := EncodeStreamType(stream.T)
	if err != nil {
		return Stream{}, err
	}
	return Stream{
		C:  stream.C,
		T:  t,
		ID: stream.ID,
		P:  stream.P,
	}, nil
}

func DecodeStream(stream Stream) (ast.Stream, error) {
	t, err := DecodeStreamType(stream.T)
	if err != nil {
		return ast.Stream{}, err
	}
	return ast.Stream{
		C:  stream.C,
		T:  t,
		ID: stream.ID,
		P:  stream.P,
	}, nil
}

type Struct struct {
	C      ast.Ctx    `json:"c,omitempty"`
	T      StructType `json:"type,omitempty"`
	Values []Node     `json:"values,omitempty"`
}

func EncodeStruct(p ast.Struct) (Struct, error) {
	t, err := EncodeStructType(p.T)
	if err != nil {
		return Struct{}, err
	}
	values, err := EncodeNodes(p.Values)
	if err != nil {
		return Struct{}, err
	}
	return Struct{
		C:      p.C,
		T:      t,
		Values: values,
	}, nil
}

func DecodeStruct(p Struct) (ast.Struct, error) {
	t, err := DecodeStructType(p.T)
	if err != nil {
		return ast.Struct{}, err
	}
	values, err := DecodeNodes(p.Values)
	if err != nil {
		return ast.Struct{}, err
	}
	return ast.Struct{
		C:      p.C,
		T:      t,
		Values: values,
	}, nil
}
