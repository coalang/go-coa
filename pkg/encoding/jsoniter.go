//+build jsoniter

package encoding

var Marshal = jsoniter.ConfigCompatibleWithStandardLibrary.Marshal
var Unmarshal = jsoniter.ConfigCompatibleWithStandardLibrary.Unmarshal
