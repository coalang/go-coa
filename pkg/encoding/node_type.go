package encoding

import "strconv"

type NodeTypeKind uint8

const (
	NoKind NodeTypeKind = iota
	AnyTypeKind
	BlockTypeKind
	DispatcherType2Kind
	ExternalTypeKind
	InterKind
	MapTypeKind
	NativeTypeKind
	NumTypeKind
	PodTypeKind
	PromiseTypeKind
	RootTypeKind
	RuneTypeKind
	StreamTypeKind
	StructTypeKind
	SymbolTypeKind

	BlockKind
	CallKind
	CastKind
	Dispatcher2Kind
	ExternalKind
	LabelKind
	MapKind
	NativeKind
	NumKind
	PodKind
	PriorityKind
	PromiseKind
	RefKind
	RuneKind
	StreamKind
	StructKind
	SymbolKind
)

func (n NodeTypeKind) String() string {
	switch n {
	case NoKind:
		return "NoKind"
	case AnyTypeKind:
		return "AnyType"
	case BlockTypeKind:
		return "BlockType"
	case DispatcherType2Kind:
		return "DispatcherType2"
	case ExternalTypeKind:
		return "ExternalType"
	case InterKind:
		return "Inter"
	case MapTypeKind:
		return "MapType"
	case NativeTypeKind:
		return "NativeType"
	case NumTypeKind:
		return "NumType"
	case PodTypeKind:
		return "PodType"
	case PromiseTypeKind:
		return "PromiseType"
	case RootTypeKind:
		return "RootType"
	case RuneTypeKind:
		return "RuneType"
	case StreamTypeKind:
		return "StreamType"
	case StructTypeKind:
		return "StructType"
	case SymbolTypeKind:
		return "SymbolType"
	case BlockKind:
		return "Value"
	case CallKind:
		return "Key"
	case CastKind:
		return "Cast"
	case Dispatcher2Kind:
		return "Dispatcher2"
	case ExternalKind:
		return "External"
	case LabelKind:
		return "Label"
	case MapKind:
		return "Map"
	case NativeKind:
		return "Native"
	case NumKind:
		return "Num"
	case PodKind:
		return "Pod"
	case PriorityKind:
		return "Priority"
	case PromiseKind:
		return "Promise"
	case RefKind:
		return "Ref"
	case RuneKind:
		return "Rune"
	case StreamKind:
		return "Stream"
	case StructKind:
		return "Struct"
	case SymbolKind:
		return "Symbol"
	default:
		return strconv.FormatInt(int64(n), 10)
	}
}
