package encoding

import (
	"bytes"
	"encoding/gob"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func GobEncodeProgram(program ast.Program) ([]byte, error) {
	buf := new(bytes.Buffer)
	encoded, err := EncodeProgram(program)
	if err != nil {
		return nil, err
	}
	err = gob.NewEncoder(buf).Encode(encoded)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func GobDecodeProgram(data []byte) (ast.Program, error) {
	buf := bytes.NewBuffer(data)
	program := Program{}
	err := gob.NewDecoder(buf).Decode(&program)
	if err != nil {
		return ast.Program{}, err
	}
	return DecodeProgram(program)
}
