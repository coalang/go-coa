package encoding

import (
	"encoding/json"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

var Marshal = json.Marshal
var Unmarshal = json.Unmarshal

func JSONEncodeProgram(program ast.Program) ([]byte, error) {
	data, err := EncodeProgram(program)
	if err != nil {
		return nil, err
	}
	return Marshal(data)
}

func JSONDecodeProgram(data []byte) (ast.Program, error) {
	program := Program{}
	err := Unmarshal(data, &program)
	if err != nil {
		return ast.Program{}, err
	}
	return DecodeProgram(program)
}
