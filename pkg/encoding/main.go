package encoding

const (
	ExtCoa  = ".coa"
	ExtJSON = ".json"
	ExtGob  = ".gob"

	ExtCompiled = ".c"
)
