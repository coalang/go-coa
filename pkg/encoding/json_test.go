package encoding

import (
	"bytes"
	jsonIter "github.com/json-iterator/go"
	"gitlab.com/coalang/go-coa/lib"
	"testing"
)

func TestJSONDecodeProgram(t *testing.T) {
	for name, file := range lib.JSONs {
		file := file
		t.Run(name, func(t *testing.T) {
			_, err := JSONDecodeProgram(file)
			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestJSONEncodeProgram(t *testing.T) {
	for name, file := range lib.JSONs {
		file := file
		t.Run(name, func(t *testing.T) {
			program, err := JSONDecodeProgram(file)
			if err != nil {
				t.Fatal(err)
			}
			encoded, err := JSONEncodeProgram(program)
			if err != nil {
				t.Fatal(err)
			}
			if !bytes.Equal(encoded, file) {
				t.Fatalf("input and encoded not equal")
			}
		})
	}
}

func BenchmarkJSONDecodeProgramTestCoaJsonIter(b *testing.B) {
	Marshal = jsonIter.ConfigCompatibleWithStandardLibrary.Marshal
	Unmarshal = jsonIter.ConfigCompatibleWithStandardLibrary.Unmarshal
	for n := 0; n < b.N; n++ {
		_, err := JSONDecodeProgram(lib.JSONs["test.coa.json"])
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkJSONDecodeProgramTestCoa(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := JSONDecodeProgram(lib.JSONs["test.coa.json"])
		if err != nil {
			b.Fatal(err)
		}
	}
}
