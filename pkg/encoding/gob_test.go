package encoding

import (
	"bytes"
	"gitlab.com/coalang/go-coa/lib"
	"testing"
)

func TestGobDecodeProgram(t *testing.T) {
	for name, file := range lib.Gobs {
		file := file
		t.Run(name, func(t *testing.T) {
			_, err := GobDecodeProgram(file)
			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestGobEncodeProgram(t *testing.T) {
	for name, file := range lib.Gobs {
		file := file
		t.Run(name, func(t *testing.T) {
			program, err := GobDecodeProgram(file)
			if err != nil {
				t.Fatal(err)
			}
			encoded, err := GobEncodeProgram(program)
			if err != nil {
				t.Fatal(err)
			}
			if !bytes.Equal(encoded, file) {
				t.Fatalf("input and encoded not equal")
			}
		})
	}
}

func BenchmarkGobDecodeProgramTestCoa(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := GobDecodeProgram(lib.Gobs["test.coa.gob"])
		if err != nil {
			b.Fatal(err)
		}
	}
}
