package render

import "gitlab.com/coalang/go-coa/pkg/astproto"

func Ref(ref *astproto.Ref) string {
	return ref.Id
}

func Symbol(symbol *astproto.Symbol) string {
	return "#" + symbol.Name
}

func Num(num *astproto.Num) string {
	switch content := num.GetContent().(type) {
	case *astproto.Num_Normal:
		return content.Normal
	case *astproto.Num_Inf:
		if content.Inf {
			return "inf.+"
		} else {
			return "inf.-"
		}
	case *astproto.Num_Nan:
		return "nan"
	default:
		panic("num.Content invalid")
	}
}
