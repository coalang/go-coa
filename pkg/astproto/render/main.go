package render

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astproto"
)

func Program(program *astproto.Program) string {
	if program == nil {
		return ""
	}
	return nodes(program.Content, "\n\n")
}

func nodes(nodes []*astproto.Node, delim string) string {
	re := ""
	for _, node := range nodes {
		re += Node(node) + delim
	}
	return re
}

func Node(node *astproto.Node) string {
	switch content := node.GetContent().(type) {
	case nil:
		return "_"
	case *astproto.Node_Type:
		return Type(content.Type)
	case *astproto.Node_Block:
	case *astproto.Node_Call:
	case *astproto.Node_Dispatcher:
	case *astproto.Node_External:
		id, err := astproto.ToUUID(content.External.Id)
		if err != nil {
			return ""
		}
		return Symbol(&astproto.Symbol{Name: "#" + id.String()})
	case *astproto.Node_Label:
	case *astproto.Node_Map:
	case *astproto.Node_Num:
		return Num(content.Num)
	case *astproto.Node_Pod:
	case *astproto.Node_Priority:
	case *astproto.Node_Ref:
		return Ref(content.Ref)
	case *astproto.Node_Struct:
	case *astproto.Node_Symbol:
		return Symbol(content.Symbol)
	default:
		panic("node.Content invalid")
	}
	return fmt.Sprintf("%T not implemented", node.GetContent())
}

func Type(type_ *astproto.Type) string {
	if type_ == nil {
		return "_"
	}
	return fmt.Sprintf("%T", type_.Content)
}
