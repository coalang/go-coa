package astproto

import (
	"fmt"
	"runtime"
)

func OriginFromCaller(skip uint) *Origin {
	_, filepath, line, ok := runtime.Caller(int(skip) + 1)
	if !ok {
		if skip != 1 {
			return OriginFromCaller(1)
		}
		return &Origin{
			StartLine: uint32(line),
			Name:      "from caller",
		}
	}
	return &Origin{
		Filepath:  filepath,
		StartLine: uint32(line),
		Name:      "from caller",
	}
}

func OriginString(o *Origin) string {
	if o == nil {
		return ""
	}
	re := o.Filepath
	if o.StartLine != 0 {
		re += fmt.Sprintf(":%d:%d", o.StartLine, o.StartColumn)
		if o.EndLine != 0 {
			re += fmt.Sprintf("→%d:%d", o.EndLine, o.EndColumn)
		}
	}
	if o.Name != "" {
		re += " (" + o.Name + ")"
	}
	return re
}

func OriginErrorf(o *Origin, format string, v ...interface{}) error {
	return fmt.Errorf(OriginString(o)+" "+format, v...)
}
