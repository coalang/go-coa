package astproto

import (
	"github.com/google/uuid"
)

func ToUUID(id *UUID) (uuid.UUID, error) {
	re := uuid.UUID{}
	err := re.UnmarshalBinary(id.Value)
	if err != nil {
		return [16]byte{}, err
	}
	return re, nil
}

func FromUUID(id uuid.UUID) *UUID {
	return &UUID{Value: id[:]}
}
