package run

import "gitlab.com/coalang/go-coa/pkg/astproto"

func (r *Runner) coerce(from *astproto.Node, to *astproto.Type) (*astproto.Node, bool, error) {
	coercible, err := r.coercible(from, to)
	if err != nil {
		return nil, false, err
	}
	if !coercible {
		return nil, false, nil
	}
	panic("not implemented")
}

func (r *Runner) coercible(from *astproto.Node, to *astproto.Type) (bool, error) {
	panic("not implemented")
}
