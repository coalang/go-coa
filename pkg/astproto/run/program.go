package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

func (r *Runner) Program(program *astproto.Program) (*astproto.Program, error) {
	if program == nil {
		return nil, errs.ErrUnexpectedNil
	}
	nodes, err := r.nodes(program.Content)
	if err != nil {
		return nil, err
	}
	program.Content = nodes
	return program, nil
}

func (r *Runner) nodes(nodes []*astproto.Node) ([]*astproto.Node, error) {
	// TODO: make concurrent
	var err error
	for i, node := range nodes {
		nodes[i], err = r.node(node)
		if err != nil {
			return nil, err
		}
	}
	return nodes, nil
}
