package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
)

func (r *Runner) call(call *astproto.Call) (*astproto.Call, error) {
	var err error
	call.T, err = r.callType(call.T)
	if err != nil {
		return nil, err
	}
	call.Self, err = r.node(call.Self)
	if err != nil {
		return nil, err
	}
	call.Ee, err = r.node(call.Ee)
	if err != nil {
		return nil, err
	}
	call.Arg, err = r.node(call.Arg)
	if err != nil {
		return nil, err
	}
	return call, nil
}

func (r *Runner) callType(callType *astproto.CallType) (*astproto.CallType, error) {
	if callType == nil {
		return nil, nil
	}
	var err error
	callType.Self, err = r.type_(callType.Self)
	if err != nil {
		return nil, err
	}
	callType.Re, err = r.type_(callType.Re)
	if err != nil {
		return nil, err
	}
	callType.Arg, err = r.type_(callType.Arg)
	if err != nil {
		return nil, err
	}
	return callType, nil
}
