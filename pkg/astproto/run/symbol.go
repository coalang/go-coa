package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

func (r *Runner) symbol(symbol *astproto.Symbol) (*astproto.Node, error) {
	if len(symbol.Name) != 0 && symbol.Name[0] == '#' {
		id, err := utils.UUIDFrom(symbol.Name[1:])
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_External{External: &astproto.External{
			O:  symbol.O,
			Id: astproto.FromUUID(id),
		}}}, nil
	}
	return &astproto.Node{Content: &astproto.Node_Symbol{Symbol: symbol}}, nil
}
