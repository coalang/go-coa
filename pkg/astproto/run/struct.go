package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
)

func (r *Runner) struct_(s *astproto.Struct) (*astproto.Struct, error) {
	var err error
	s.T, err = r.structType(s.T)
	if err != nil {
		return nil, err
	}
	for _, value := range s.Values {
		if value == nil {
			continue
		}
		value, err = r.node(value)
		if err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (r *Runner) structType(s *astproto.StructType) (*astproto.StructType, error) {
	var err error
	for _, pair := range s.Pairs {
		if pair == nil {
			continue
		}
		pair.Value, err = r.type_(pair.Value)
		if err != nil {
			return nil, err
		}
		pair.Default, err = r.node(pair.Default)
		if err != nil {
			return nil, err
		}
	}
	return s, nil
}

func (r *Runner) structGet(s *astproto.Struct, key string) (int, bool) {
	if s == nil {
		return 0, false
	}
	for i, pair := range s.T.Pairs {
		if pair == nil {
			continue
		}
		if pair.Key == key {
			return i, true
		}
	}
	return 0, false
}
