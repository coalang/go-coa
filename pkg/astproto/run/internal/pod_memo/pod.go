package pod_memo

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"sync"
)

var memo = map[string]*astproto.Node{}
var memoLock sync.RWMutex

func Get(p string) (*astproto.Node, bool) {
	memoLock.RLock()
	defer memoLock.RUnlock()
	node, ok := memo[p]
	return node, ok
}

func Set(p string, node *astproto.Node) {
	memoLock.Lock()
	defer memoLock.Unlock()
	memo[p] = node
}
