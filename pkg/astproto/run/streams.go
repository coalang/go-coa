package run

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"sync"
)

type StreamImpl interface {
	IsClosed() bool
	Close()
	Chan() chan ast.Node
}

type StreamImpls struct {
	lock  sync.RWMutex
	m     map[uuid.UUID]StreamImpl
	outer *StreamImpls
}

func (n *StreamImpls) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m == nil {
		n.m = map[uuid.UUID]StreamImpl{}
	}
}

func (n *StreamImpls) Inherit() *StreamImpls {
	return &StreamImpls{
		outer: n,
	}
}

func (n *StreamImpls) Get(key uuid.UUID) (StreamImpl, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val, ok := n.m[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val, ok
}

func (n *StreamImpls) GetSingle(key uuid.UUID) StreamImpl {
	val, _ := n.Get(key)
	return val
}

func (n *StreamImpls) Set(key uuid.UUID, val StreamImpl) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m[key] = val
}
