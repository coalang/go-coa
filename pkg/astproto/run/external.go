package run

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"log"
)

func (r *Runner) external(external *astproto.External) (*astproto.Node, error) {
	id, err := astproto.ToUUID(external.Id)
	if err != nil {
		return nil, err
	}
	impl, ok := r.ExternalImpls.Get(id)
	if !ok {
		log.Println(r.ExternalImpls.Keys())
		return nil, astproto.OriginErrorf(external.O, "%w: %s", errs.ErrExternalNotFound, id)
	}
	node, err := impl.Node()
	if err != nil {
		if errors.Is(err, errs.ErrNotImplemented) {
			return &astproto.Node{
				Immutable: true,
				Content:   &astproto.Node_External{External: external},
			}, nil
		}
		return nil, astproto.OriginErrorf(external.O, "%s: %w", id, err)
	}
	return node, nil
}
