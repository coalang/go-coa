package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"log"
)

func (r *Runner) node(node *astproto.Node) (*astproto.Node, error) {
	if node == nil {
		return nil, nil
	}
	log.Printf("node\t%T", node.Content)
	switch content := node.Content.(type) {
	case *astproto.Node_Type:
		type_, err := r.type_(content.Type)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Type{Type: type_}}, nil
	case *astproto.Node_Block:
		block, err := r.block(content.Block)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Block{Block: block}}, nil
	case *astproto.Node_Call:
		return r.callCall(content.Call)
	case *astproto.Node_Dispatcher:
	case *astproto.Node_External:
		return r.external(content.External)
	case *astproto.Node_Label:
		label, err := r.label(content.Label)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Label{Label: label}}, nil
	case *astproto.Node_Map:
	case *astproto.Node_Num:
		num, err := r.num(content.Num)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Num{Num: num}}, nil
	case *astproto.Node_Pod:
	case *astproto.Node_Priority:
	case *astproto.Node_Ref:
		return r.ref(content.Ref)
	case *astproto.Node_Struct:
		struct_, err := r.struct_(content.Struct)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Struct{Struct: struct_}}, nil
	case *astproto.Node_Symbol:
		return r.symbol(content.Symbol)
	default:
		panic("invalid node.Content")
	}
	panic("not implemented")
}

func (r *Runner) type_(type_ *astproto.Type) (*astproto.Type, error) {
	if type_ == nil {
		return nil, nil
	}
	log.Printf("type\t%T", type_.Content)
	switch content := type_.Content.(type) {
	case *astproto.Type_BlockAsType:
	case *astproto.Type_RefAsType:
		return r.refAsType(content.RefAsType)
	case *astproto.Type_CallType:
		callType, err := r.callType(content.CallType)
		if err != nil {
			return nil, err
		}
		return &astproto.Type{Content: &astproto.Type_CallType{CallType: callType}}, nil
	case *astproto.Type_DispatcherType:
	case *astproto.Type_ExternalType:
	case *astproto.Type_Inter:
	case *astproto.Type_MapType:
	case *astproto.Type_NumType, *astproto.Type_PodType, *astproto.Type_RootType:
		return type_, nil
	case *astproto.Type_StructType:
		structType, err := r.structType(content.StructType)
		if err != nil {
			return nil, err
		}
		return &astproto.Type{Content: &astproto.Type_StructType{StructType: structType}}, nil
	case *astproto.Type_SymbolType:
		return type_, nil
	default:
		panic("invalid node.Content")
	}
	panic("not implemented")
}
