package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"io/fs"
	"sort"
	"sync"
)

type Scope struct {
	m    map[string]*astproto.Node
	lock sync.RWMutex
}

func newScope() *Scope {
	return &Scope{
		m: map[string]*astproto.Node{},
	}
}

func (s *Scope) Keys() []string {
	keys := make([]string, len(s.m))
	i := 0
	for key := range s.m {
		keys[i] = key
		i++
	}
	sort.Strings(keys)
	return keys
}

func (s *Scope) Get(key string) (*astproto.Node, bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	value, ok := s.m[key]
	return value, ok
}

func (s *Scope) Set(key string, value *astproto.Node) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.m[key] = value
}

type Runner struct {
	scope *Scope
	root  *Scope
	outer *Runner
	*astproto.Frame
	fs fs.FS

	StreamImpls   *StreamImpls
	ExternalImpls *ExternalImpls

	lock sync.RWMutex
}

func NewRunner(fs fs.FS, pure bool) (*Runner, error) {
	r := newRunnerNoRoot(fs, pure)
	err := r.fileInPlace(BasePath)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func newRunnerNoRoot(fs fs.FS, pure bool) *Runner {
	return &Runner{
		scope: newScope(),
		root:  newScope(),
		Frame: &astproto.Frame{
			O:    astproto.OriginFromCaller(1),
			Note: "root",
			Pure: pure,
		},
		fs:            fs,
		StreamImpls:   &StreamImpls{},
		ExternalImpls: &ExternalImpls{},
	}
}

func (r *Runner) inherit(frame *astproto.Frame) *Runner {
	return &Runner{
		scope: newScope(),
		root:  r.root,
		outer: r,
		Frame: &astproto.Frame{
			O:        frame.O,
			Len:      r.Frame.Len + 1,
			Note:     frame.Note,
			CallHash: frame.CallHash,
			Lone:     frame.Lone,
			Pure:     r.Frame.Pure,
		},
		fs:            r.fs,
		StreamImpls:   r.StreamImpls.Inherit(),
		ExternalImpls: r.ExternalImpls.Inherit(),
	}
}

func (r *Runner) Keys() []string {
	return r.scope.Keys()
}

func (r *Runner) Get(key string) (*astproto.Node, bool) {
	if r == nil {
		return nil, false
	}
	node, ok := r.scope.Get(key)
	if ok {
		return node, true
	}
	if node, ok = r.root.Get(key); ok {
		return node, true
	}
	if r.outer != nil {
		return r.outer.Get(key)
	}
	return nil, false
}

func (r *Runner) Set(key string, value *astproto.Node) {
	r.scope.Set(key, value)
}
