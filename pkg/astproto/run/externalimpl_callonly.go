package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type ExternalImplCallOnly struct {
	F func(r *Runner, call *astproto.Call) (*astproto.Node, error)
	P bool
}

func (c ExternalImplCallOnly) Pure() (bool, error) {
	return c.P, nil
}

func (c ExternalImplCallOnly) Node() (*astproto.Node, error) {
	return nil, errs.NewErrNotImplemented("ExternalImplCallOnly.Node")
}

func (c ExternalImplCallOnly) Call(r *Runner, call *astproto.Call) (*astproto.Node, error) {
	return c.F(r, call)
}
