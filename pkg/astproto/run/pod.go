package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/encoding"
	"gitlab.com/coalang/go-coa/pkg/astproto/run/internal/pod_memo"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"io/fs"
	"log"
	"path/filepath"
	"strings"
)

const (
	BasePath  = `base.coa`
	CorePath  = `core`
	IndexPath = `index`
)

func (r *Runner) Pod(pod *astproto.Pod) (*astproto.Node, error) {
	if pod.Content != nil {
		return pod.Content, nil
	}
	return r.load(pod.Path)
}

func (r *Runner) load(p string) (*astproto.Node, error) {
	node, ok := pod_memo.Get(p)
	if ok {
		return node, nil
	}

	node = nil
	stat, err := fs.Stat(r.fs, p)
	if err != nil {
		return nil, err
	}
	if !stat.IsDir() {
		node, err = r.file(p)
	} else {
		node, err = r.dir(p)
	}
	if err != nil {
		return nil, err
	}
	node.Immutable = true
	pod_memo.Set(p, node)
	return node, nil
}

func (r *Runner) dir(p string) (*astproto.Node, error) {
	log.Printf("pod dir\t%s", p)
	var err error
	var entries []fs.DirEntry
	entries, err = fs.ReadDir(r.fs, p)
	if err != nil {
		return nil, err
	}
	s := &astproto.Struct{
		O: &astproto.Origin{
			Filepath: p,
			Name:     "pod entries",
		},
		T: &astproto.StructType{
			Pairs: make([]*astproto.StructTypePair, len(entries)),
		},
		Values: make([]*astproto.Node, len(entries)),
	}
	log.Printf("pod dir a\t%v", entries)
	for i, entry := range entries {
		name := entry.Name()
		s.Values[i], err = r.load(filepath.Join(p, name))
		if err != nil {
			return nil, err
		}
		s.T.Pairs[i] = &astproto.StructTypePair{
			Key:     strings.TrimSuffix(name, filepath.Ext(name)),
			Default: s.Values[i],
		}
	}
	return &astproto.Node{Content: &astproto.Node_Struct{Struct: s}}, nil
}

func (r *Runner) fileInPlace(p string) error {
	log.Printf("pod file\t%s", p)
	data, err := fs.ReadFile(r.fs, p)
	if err != nil {
		return err
	}
	program, err := encoding.Unmarshal(p, data)
	if err != nil {
		return err
	}
	_, err = r.Program(program)
	if err != nil {
		return err
	}
	return nil
}

func (r *Runner) file(p string) (*astproto.Node, error) {
	if filepath.Ext(p) != ".coa" {
		return nil, nil
	}
	inner := r.inherit(&astproto.Frame{
		O: &astproto.Origin{
			Filepath: p,
			Name:     "pod",
		},
		Note: "pod",
		Lone: true,
	})
	err := inner.fileInPlace(p)
	if err != nil {
		return nil, err
	}
	exported, ok := inner.Get("export")
	if !ok {
		return nil, astproto.OriginErrorf(inner.O, "%w", errs.ErrNothingExported)
	}
	return exported, nil
}
