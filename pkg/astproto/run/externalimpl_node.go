package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
)

type ExternalImplNode struct {
	N *astproto.Node
	P bool
}

var _ ExternalImpl = &ExternalImplNode{}

func (n *ExternalImplNode) Call(_ *Runner, _ *astproto.Call) (*astproto.Node, error) {
	return nil, errs.ErrNotImplemented
}

func (n *ExternalImplNode) Pure() (bool, error) {
	return n.P, nil
}

func (n *ExternalImplNode) Node() (*astproto.Node, error) {
	return n.N, nil
}
