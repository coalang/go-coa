package run

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"math/big"
)

func (r *Runner) num(num *astproto.Num) (*astproto.Num, error) {
	switch num.Content.(type) {
	case *astproto.Num_Normal:
		rat, ok := numRat(num)
		if !ok {
			return nil, errors.New("not a number")
		}
		num.Content.(*astproto.Num_Normal).Normal = rat.RatString()
		return num, nil
	case *astproto.Num_Inf, *astproto.Num_Nan:
		return num, nil
	default:
		panic("num.Content invalid")
	}
}

func numRat(num *astproto.Num) (*big.Rat, bool) {
	return new(big.Rat).SetString(num.GetNormal())
}
