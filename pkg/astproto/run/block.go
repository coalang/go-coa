package run

import "gitlab.com/coalang/go-coa/pkg/astproto"

func (r *Runner) block(block *astproto.Block) (*astproto.Block, error) {
	var err error
	block.T, err = r.callType(block.T)
	if err != nil {
		return nil, err
	}
	return block, nil
}
