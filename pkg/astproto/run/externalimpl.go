package run

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"sync"
)

type ExternalImpl interface {
	Call(r *Runner, call *astproto.Call) (*astproto.Node, error)
	Pure() (bool, error)
	Node() (*astproto.Node, error)
}

type ExternalImpls struct {
	lock  sync.RWMutex
	m     map[uuid.UUID]ExternalImpl
	outer *ExternalImpls
}

func (n *ExternalImpls) InitIf() {
	n.lock.Lock()
	defer n.lock.Unlock()
	if n.m == nil {
		n.m = map[uuid.UUID]ExternalImpl{}
	}
}

func (n *ExternalImpls) Inherit() *ExternalImpls {
	return &ExternalImpls{
		outer: n,
	}
}

func (n *ExternalImpls) Keys() []uuid.UUID {
	keys := make([]uuid.UUID, 0, len(n.m))
	for key := range n.m {
		keys = append(keys, key)
	}
	if n.outer != nil {
		keys = append(keys, n.outer.Keys()...)
	}
	return keys
}

func (n *ExternalImpls) Get(key uuid.UUID) (ExternalImpl, bool) {
	n.InitIf()
	n.lock.RLock()
	defer n.lock.RUnlock()
	val, ok := n.m[key]
	if !ok && n.outer != nil {
		return n.outer.Get(key)
	}
	return val, ok
}

func (n *ExternalImpls) GetSingle(key uuid.UUID) ExternalImpl {
	val, _ := n.Get(key)
	return val
}

func (n *ExternalImpls) Set(key uuid.UUID, val ExternalImpl) {
	n.InitIf()
	n.lock.Lock()
	defer n.lock.Unlock()
	n.m[key] = val
}
