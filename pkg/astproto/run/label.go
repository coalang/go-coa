package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"log"
)

func (r *Runner) label(label *astproto.Label) (*astproto.Label, error) {
	var err error
	label.Ee, err = r.node(label.Ee)
	if err != nil {
		return nil, err
	}
	log.Printf("label set\t%s", label.Er)
	r.Set(label.Er, label.Ee)
	return label, nil
}
