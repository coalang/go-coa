package run

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
)

func (r *Runner) callCall(call *astproto.Call) (*astproto.Node, error) {
	call, err := r.call(call)
	if err != nil {
		return nil, err
	}
	switch call.Ee.Content.(type) {
	case *astproto.Node_Block:
		return r.callBlock(call)
	case *astproto.Node_Dispatcher:
		return r.callDispatcher(call)
	case *astproto.Node_External:
		return r.callExternal(call)
	default:
		return nil, errs.ErrASTInvalid
	}
}

func (r *Runner) callBlock(call *astproto.Call) (*astproto.Node, error) {
	return r.callBlockLone(call, true, "block")
}

func (r *Runner) callBlockLone(call *astproto.Call, lone bool, note string) (*astproto.Node, error) {
	hash, err := astproto.Hash(call)
	if err != nil {
		return nil, err
	}
	inner := r.inherit(&astproto.Frame{
		O:        nil,
		Note:     note,
		CallHash: hash[:],
		Lone:     lone,
	})
	var re *astproto.Node
	ee := call.Ee.GetBlock()
	nodes, err := inner.nodes(ee.Content)
	if err != nil {
		return nil, err
	}
	for i := len(nodes) - 1; i > 0; i-- {
		if nodes[i] != nil {
			re = nodes[i]
			break
		}
	}
	return re, nil
}

func (r *Runner) callDispatcher(call *astproto.Call) (*astproto.Node, error) {
	match, err := r.callDispatcherGetMatches(call)
	if err != nil {
		return nil, err
	}
	call.Ee = match
	return r.callCall(call)
}

func (r *Runner) callDispatcherGetMatches(call *astproto.Call) (*astproto.Node, error) {
	ee := call.Ee.GetDispatcher()
	matches := make([]int, 0)
	for i, pair := range ee.Pairs {
		selfOk, err := r.coercible(call.Self, pair.Key.Self)
		if err != nil {
			return nil, err
		}
		argOk, err := r.coercible(call.Arg, pair.Key.Arg)
		if err != nil {
			return nil, err
		}
		if selfOk && argOk {
			matches = append(matches, i)
		}
	}
	switch len(matches) {
	case 0:
		return nil, eval.ErrNoMatches
	case 1:
		return ee.Pairs[matches[0]].Value, nil
	default:
		return nil, eval.ErrMultipleMatches
	}
}

func (r *Runner) callExternal(call *astproto.Call) (*astproto.Node, error) {
	ee := call.Ee.GetExternal()
	id, err := astproto.ToUUID(ee.Id)
	if err != nil {
		return nil, err
	}
	impl, ok := r.ExternalImpls.Get(id)
	if !ok {
		return nil, astproto.OriginErrorf(ee.O, "%w: %s", errs.ErrExternalNotFound, id)
	}
	return impl.Call(r, call)
}
