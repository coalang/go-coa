package run

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/render"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"strings"
)

func refToComma(ref *astproto.Ref) *astproto.Ref {
	if strings.HasPrefix(ref.Id, "...") {
		ref.Id = ast.IDIndex + ref.Id[2:]
	}
	if strings.HasPrefix(ref.Id, "..") {
		ref.Id = ast.IDCore + ref.Id[1:]
	}
	return ref
}

func (r *Runner) ref(ref *astproto.Ref) (*astproto.Node, error) {
	node, err := r.ref_(ref)
	if err != nil {
		return nil, err
	}
	return r.node(node)
}

func (r *Runner) ref_(ref *astproto.Ref) (*astproto.Node, error) {
	ref = refToComma(ref)
	// do the cheapest checks first
	if ref.Id == "" {
		return nil, fmt.Errorf("%w: ast.Ref.IDs len is 0", errs.ErrInvalid)
	}
	if ref.Id == "_" {
		return nil, nil
	}

	ids := strings.Split(ref.Id, ".")
	fmt.Println("ref", r.Keys())
	node, ok := r.Get(ids[0])
	if !ok {
		return nil, astproto.OriginErrorf(ref.O, "%w in scope (ref): %s", errs.ErrNotFound, render.Ref(ref))
	}
	if len(ids) == 1 {
		return node, nil
	}

	re, err := r.refNode(node, &astproto.Ref{
		O:  ref.O,
		Id: strings.Join(ids[1:], "."),
	})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", render.Ref(ref), err)
	}
	return re, nil
}

func (r *Runner) refNode(node *astproto.Node, ref *astproto.Ref) (*astproto.Node, error) {
	node2, err := r.node(node)
	if err != nil {
		return nil, err
	}
	if ref.Id == "" {
		return nil, fmt.Errorf("%w: ast.Ref.IDs len is 0", errs.ErrInvalid)

	}
	ids := strings.Split(ref.Id, ".")

	switch node2 := node2.Content.(type) {
	case *astproto.Node_Struct:
		i, ok := r.structGet(node2.Struct, ids[0])
		if !ok {
			return nil, fmt.Errorf("%w: %s in struct", errs.ErrNotFound, ids[0])
		}
		if len(ids) == 1 {
			return node2.Struct.Values[i], nil
		}
		return r.refNode(node2.Struct.Values[i], &astproto.Ref{
			O:  ref.O,
			Id: strings.Join(ids[1:], "."),
		})
	default:
		return nil, fmt.Errorf("%T which is invalid in refNode", node2)
	}
}

func (r *Runner) refAsType(ref *astproto.RefAsType) (*astproto.Type, error) {
	node, err := r.ref(ref.Ref)
	if err != nil {
		return nil, err
	}
	toType, err := astproto.ToType(node)
	if err != nil {
		return nil, astproto.OriginErrorf(ref.GetRef().GetO(), "%w", err)
	}
	return toType, nil
}
