// Package lib provides an embed.FS of the standard library and index, sample gob and JSON files for testing.
package lib

import (
	"embed"
)

//go:embed core index base.coa
var StaticFS embed.FS
