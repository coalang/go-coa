package builtin

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/run"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var IterID = utils.UUIDFromMust("fbc870eb-59b0-4611-b503-fe84478378b2")
var IterImpl = run.ExternalImplNode{
	N: iter,
	P: true,
}
var iter = &astproto.Node{
	Immutable: true,
	Content: &astproto.Node_Type{Type: &astproto.Type{Content: &astproto.Type_Inter{Inter: &astproto.Inter{
		T: &astproto.StructType{
			O: &astproto.Origin{
				Filepath: "builtin/iter",
			},
			Pairs: []*astproto.StructTypePair{
				{
					Key: "next",
					Value: &astproto.Type{Content: &astproto.Type_CallType{CallType: &astproto.CallType{
						O: &astproto.Origin{
							Filepath: "builtin/iter/next",
						},
						Self: &astproto.Type{Content: &astproto.Type_Inter{Inter: &astproto.Inter{}}},
						Re:   &astproto.Type{Content: &astproto.Type_Inter{Inter: &astproto.Inter{}}},
					}}},
				},
			},
		},
	}}}},
}
