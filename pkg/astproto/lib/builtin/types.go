package builtin

import (
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/run"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var numID = utils.UUIDFromMust("a2d8e271-4fa7-460c-8bac-b1c817e9ba81")
var numImpl = run.ExternalImplNode{
	N: &astproto.Node{Immutable: true, Content: &astproto.Node_Type{Type: &astproto.Type{Content: &astproto.Type_NumType{NumType: &astproto.NumType{}}}}},
	P: true,
}
