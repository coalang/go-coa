package builtin

import (
	"gitlab.com/coalang/go-coa/pkg/astproto/lib/builtin/arithmetic"
	"gitlab.com/coalang/go-coa/pkg/astproto/run"
)

func Apply(r *run.Runner) {
	arithmetic.Apply(r)
	r.ExternalImpls.Set(IterID, &IterImpl)
	r.ExternalImpls.Set(numID, &numImpl)
}
