package standalone

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/run"
	"gitlab.com/coalang/go-coa/pkg/utils/osfs"
	"google.golang.org/protobuf/proto"
	"os"
)

func load(data []byte) error {
	program := &astproto.Program{}
	err := proto.Unmarshal(data, program)
	if err != nil {
		return err
	}
	r := run.NewRunner(osfs.OSFS, false)
	_, err = r.Program(program)
	if err != nil {
		return err
	}
	return nil
}

func Main(data []byte) {
	err := load(data)
	if err != nil {
		_, err := fmt.Fprint(os.Stderr, err)
		if err != nil {
			panic(err)
		}
		os.Exit(1)
	}
}
