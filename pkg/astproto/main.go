// Package astproto has the generated AST from Protocol Buffer schema files.

package astproto

import (
	"golang.org/x/crypto/sha3"
	"google.golang.org/protobuf/proto"
)

//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative ast.proto
//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative general.proto
//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative services.proto

func Hash(m proto.Message) ([64]byte, error) {
	data, err := proto.Marshal(m)
	if err != nil {
		return [64]byte{}, err
	}
	return sha3.Sum512(data), nil
}

var RuneSymbol = &Symbol{
	O: &Origin{
		Filepath: "builtin/rune",
	},
	Name: "rune",
}
