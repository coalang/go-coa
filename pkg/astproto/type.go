package astproto

import (
	"fmt"
)

func ToType(node *Node) (*Type, error) {
	if node == nil {
		return nil, nil
	}
	if type_ := node.GetType(); type_ != nil {
		return type_, nil
	}
	if ref := node.GetRef(); ref != nil {
		return &Type{Content: &Type_RefAsType{RefAsType: &RefAsType{Ref: ref}}}, nil
	}
	if block := node.GetBlock(); block != nil {
		return &Type{Content: &Type_BlockAsType{BlockAsType: &BlockAsType{Content: block}}}, nil
	}
	if cast := node.GetCast(); cast != nil {
		return cast.Er, nil
	}
	return nil, fmt.Errorf("not a type: %T", node.Content)
}
