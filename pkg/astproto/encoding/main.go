package encoding

import (
	"bytes"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/astproto/convert"
	"gitlab.com/coalang/go-coa/pkg/astproto/render"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/parser"
	"gitlab.com/coalang/go-coa/pkg/utils/path"
	"google.golang.org/protobuf/proto"
)

func Unmarshal(p string, data []byte) (*astproto.Program, error) {
	switch path.GetEnc(p) {
	case path.Protobuf:
		program := &astproto.Program{}
		err := proto.Unmarshal(data, program)
		if err != nil {
			return nil, err
		}
		return program, nil
	case path.Source:
		parsed, err := parser.Parse(p, bytes.NewBuffer(data))
		if err != nil {
			return nil, err
		}
		return convert.RootBlockToProgram(*parsed)
	default:
		return nil, errs.ErrFormatUnsupported
	}
}

func Marshal(p string, program *astproto.Program) ([]byte, error) {
	switch path.GetEnc(p) {
	case path.Protobuf:
		return proto.Marshal(program)
	case path.Source:
		r := render.Program(program)
		return []byte(r), nil
	default:
		return nil, errs.ErrFormatUnsupported
	}
}
