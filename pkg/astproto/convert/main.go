package convert

import (
	"fmt"
	"github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/coalang/go-coa/pkg/astproto"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/ast"
	lexer2 "gitlab.com/coalang/go-coa/pkg/parser_lexer/lexer"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

func toOrigin(pos, endPos lexer.Position) *astproto.Origin {
	return &astproto.Origin{
		Filepath:    pos.Filename,
		StartLine:   uint32(pos.Line),
		StartColumn: uint32(pos.Column),
		EndLine:     uint32(endPos.Line),
		EndColumn:   uint32(endPos.Column),
		Name:        "parser",
	}
}

func RootBlockToProgram(block ast.RootBlock) (*astproto.Program, error) {
	var nodes []*astproto.Node
	if block.Exprs != nil {
		var err error
		nodes, err = exprsToNodes(*block.Exprs)
		if err != nil {
			return nil, err
		}
	}
	return &astproto.Program{
		O:       toOrigin(block.Pos, block.EndPos),
		Content: nodes,
	}, nil
}

func exprsToNodes(exprs ast.Exprs) ([]*astproto.Node, error) {
	nodes := make([]*astproto.Node, 1, 1+len(exprs.AfterExprs))
	var err error
	nodes[0], err = exprToNode(*exprs.FirstExpr)
	if err != nil {
		return nil, err
	}
	var node *astproto.Node
	for _, expr := range exprs.AfterExprs {
		if expr.Expr == nil {
			continue
		}
		node, err = exprToNode(*expr.Expr)
		if err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func exprToNode(expr ast.Expr) (*astproto.Node, error) {
	if expr.First == nil {
		return nil, fmt.Errorf("%w: expr.First is nil", errs.ErrInvalid)
	}
	if expr.Colon == "" {
		if expr.Second != nil {
			return nil, fmt.Errorf("%w: expr.Second is not nil (should not be label)", errs.ErrInvalid)
		}
		return exprInToNode(*expr.First)
	} else {
		if expr.Second == nil {
			return nil, fmt.Errorf("%w: expr.Second is nil (should be label)", errs.ErrInvalid)
		}

		o := toOrigin(expr.Pos, expr.EndPos)
		first, err := exprInToNode(*expr.First)
		if err != nil {
			return nil, err
		}
		ref := first.GetRef()
		if ref == nil {
			return nil, astproto.OriginErrorf(o, "%w: lhs of label is not ref", errs.ErrInvalid)
		}
		second, err := exprInToNode(*expr.Second)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Label{Label: &astproto.Label{
			O:  o,
			Er: ref.Id,
			Ee: second,
		}}}, nil
	}
}

func exprInToNode(exprIn ast.ExprIn) (*astproto.Node, error) {
	node, err := objToNode(exprIn.Obj)
	if err != nil {
		return nil, err
	}
	for _, suffix := range exprIn.Suffixes {
		arg, err := objToNode(suffix.Arg)
		if err != nil {
			return nil, err
		}
		callee, err := objToNode(suffix.Callee)
		if err != nil {
			return nil, err
		}
		node = &astproto.Node{Content: &astproto.Node_Call{Call: &astproto.Call{
			O:    toOrigin(suffix.Pos, suffix.EndPos),
			Self: node,
			Ee:   callee,
			Arg:  arg,
		}}}
	}
	return node, nil
}

func objToNode(obj ast.Obj) (*astproto.Node, error) {
	o := toOrigin(obj.Pos, obj.EndPos)
	if obj.BSlash != "" && obj.Second != nil {
		first, err := objRawToNode(o, *obj.First)
		if err != nil {
			return nil, err
		}
		second, err := objRawToNode(o, *obj.Second)
		if err != nil {
			return nil, err
		}
		firstT, err := astproto.ToType(first)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Cast{Cast: &astproto.Cast{
			O:  o,
			Er: firstT,
			Ee: second,
		}}}, nil
	}
	if obj.First == nil {
		return nil, astproto.OriginErrorf(o, "%w: first is nil", errs.ErrInvalid)
	}
	return objRawToNode(o, *obj.First)
}

func objRawToNode(o *astproto.Origin, raw ast.ObjRaw) (*astproto.Node, error) {
	switch {
	case raw.Text != "":
		toMap, err := textToMap(o, raw.Text)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Map{Map: toMap}}, nil
	case raw.Symbol != "":
		return &astproto.Node{Content: &astproto.Node_Symbol{Symbol: &astproto.Symbol{
			O:    o,
			Name: raw.Symbol[1:],
		}}}, nil
	case raw.Rune != "":
		unquoted, err := strconv.Unquote(raw.Rune)
		if err != nil {
			return nil, fmt.Errorf("unquote: %w", err)
		}
		if utf8.RuneCountInString(unquoted) != 1 {
			return nil, fmt.Errorf("%w: more than one rune in rune literal", errs.ErrInvalid)
		}
		r, _ := utf8.DecodeRuneInString(unquoted)
		return &astproto.Node{Content: &astproto.Node_Num{Num: runeToNum(o, r)}}, nil
	case raw.Ref != "":
		if isNum(raw.Ref) {
			return &astproto.Node{Content: &astproto.Node_Num{Num: &astproto.Num{
				O:       o,
				Content: &astproto.Num_Normal{Normal: raw.Ref},
			}}}, nil
		}
		if strings.ContainsAny(raw.Ref, lexer2.RefDisallowedChars) {
			return nil, fmt.Errorf("%w: ref contains disallowed chars", errs.ErrInvalid)
		}
		return &astproto.Node{Content: &astproto.Node_Ref{Ref: &astproto.Ref{
			O:  o,
			Id: raw.Ref,
		}}}, nil
	case raw.Bracked != nil:
		return brackedToNode(*raw.Bracked)
	case raw.Braced != nil:
		block, err := bracedToBlock(*raw.Braced)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Block{Block: block}}, nil
	case raw.Parened != nil:
		priority, err := parenedToPriority(*raw.Parened)
		if err != nil {
			return nil, err
		}
		return &astproto.Node{Content: &astproto.Node_Priority{Priority: priority}}, nil
	default:
		return nil, astproto.OriginErrorf(o, "%w: obj is blank for all possibilities", errs.ErrInvalid)
	}
}

func brackedToNode(bracked ast.Bracked) (*astproto.Node, error) {
	if bracked.Content == nil {
		return nil, nil
	}
	m, err := exprsToMap(toOrigin(bracked.Pos, bracked.EndPos), *bracked.Content)
	if err != nil {
		return nil, err
	}
	if bracked.Block != nil {
		block, err := bracedToBlock(*bracked.Block)
		if err != nil {
			return nil, err
		}
		block.T = &astproto.CallType{
			O: toOrigin(bracked.Pos, bracked.EndPos),
		}
		switch len(m.Pairs) {
		case 0, 1, 2:
			return nil, astproto.OriginErrorf(block.T.O, "block maps cannot have %d pairs", len(m.Pairs))
		case 3:
			{
				self := m.Pairs[0]
				if selfKey := self.Key.GetRef(); self.Key != nil && selfKey == nil {
					return nil, astproto.OriginErrorf(block.T.O, "block map key must be refs")
				} else {
					block.T.SelfID = selfKey.GetId()
				}
				block.T.Self, err = astproto.ToType(self.Value)
				if err != nil {
					return nil, astproto.OriginErrorf(block.T.O, "self: %w", err)
				}
			}
			{
				arg := m.Pairs[1]
				if argKey := arg.Key.GetRef(); arg.Key != nil && argKey == nil {
					return nil, astproto.OriginErrorf(block.T.O, "block map key must be refs")
				} else {
					block.T.ArgID = argKey.GetId()
				}
				block.T.Arg, err = astproto.ToType(arg.Value)
				if err != nil {
					return nil, astproto.OriginErrorf(block.T.O, "arg: %w", err)
				}
			}
			{
				re := m.Pairs[2]
				if re.Key != nil {
					return nil, astproto.OriginErrorf(block.T.O, "block map key must be blank for return")
				}
				block.T.Re, err = astproto.ToType(re.Value)
				if err != nil {
					return nil, astproto.OriginErrorf(block.T.O, "re: %w", err)
				}
			}
		default:
			return nil, astproto.OriginErrorf(block.T.O, "block maps cannot have more than 3 (%d) pairs", len(m.Pairs))
		}
		return &astproto.Node{Content: &astproto.Node_Block{Block: block}}, nil
	} else {
		return &astproto.Node{Content: &astproto.Node_Map{Map: m}}, nil
	}
}

func exprsToMap(o *astproto.Origin, exprs ast.Exprs) (*astproto.Map, error) {
	m := &astproto.Map{
		O: o,
		T: &astproto.MapType{
			O:     o,
			Key:   nil,
			Value: nil,
		},
		Pairs: make([]*astproto.MapPair, 0, 1+len(exprs.AfterExprs)),
	}
	var expr *ast.Expr
	var first, second *astproto.Node
	var err error
	for i := -1; i < len(exprs.AfterExprs); i++ {
		if i == -1 {
			expr = exprs.FirstExpr
		} else {
			expr = exprs.AfterExprs[i].Expr
		}
		if expr == nil {
			continue
		}
		if expr.First != nil {
			first, err = exprInToNode(*expr.First)
			if err != nil {
				return nil, err
			}
		}
		if expr.Second != nil {
			second, err = exprInToNode(*expr.Second)
			if err != nil {
				return nil, err
			}
		}
		if first != nil && second == nil {
			m.Pairs = append(m.Pairs, &astproto.MapPair{
				Value: first,
			})
		} else if first != nil && second != nil {
			m.Pairs = append(m.Pairs, &astproto.MapPair{
				Key:   first,
				Value: second,
			})
		} else {
			return nil, astproto.OriginErrorf(o, "invalid sequence: %t %t", first != nil, second != nil)
		}
	}
	return m, nil
}

func bracedToBlock(braced ast.Braced) (*astproto.Block, error) {
	if braced.Content == nil {
		return nil, nil
	}
	return exprsToBlock(toOrigin(braced.Pos, braced.EndPos), *braced.Content)
}

func parenedToPriority(parened ast.Parened) (*astproto.Priority, error) {
	o := toOrigin(parened.Pos, parened.EndPos)
	if parened.Content == nil {
		return nil, nil
	}
	block, err := exprsToBlock(o, *parened.Content)
	if err != nil {
		return nil, err
	}
	return &astproto.Priority{B: block}, nil
}

func exprsToBlock(o *astproto.Origin, exprs ast.Exprs) (*astproto.Block, error) {
	block := &astproto.Block{
		O:       o,
		Content: make([]*astproto.Node, 0, len(exprs.AfterExprs)+1),
	}
	var node *astproto.Node
	var err error
	if exprs.FirstExpr != nil {
		node, err = exprToNode(*exprs.FirstExpr)
		if err != nil {
			return nil, err
		}
		if node != nil {
			block.Content = append(block.Content, node)
		}
	}
	for _, expr := range exprs.AfterExprs {
		node, err = exprToNode(*expr.Expr)
		if err != nil {
			return nil, err
		}
		if node != nil {
			block.Content = append(block.Content, node)
		}
	}
	return block, nil
}

func isNum(ref string) bool {
	var first rune
	for _, r := range ref {
		first = r
		break
	}
	return unicode.IsDigit(first)
}

func textToMap(o *astproto.Origin, text string) (*astproto.Map, error) {
	text, err := strconv.Unquote(text)
	if err != nil {
		return nil, fmt.Errorf("unquote: %w", err)
	}
	pairs := make([]*astproto.MapPair, 0, utf8.RuneCountInString(text))
	for _, r := range text {
		pairs = append(pairs, &astproto.MapPair{
			Value: &astproto.Node{Content: &astproto.Node_Num{Num: runeToNum(o, r)}},
		})
	}
	return &astproto.Map{
		O:     o,
		Pairs: pairs,
	}, nil
}

func runeToNum(o *astproto.Origin, r rune) *astproto.Num {
	return &astproto.Num{
		O:       o,
		T:       &astproto.NumType{Unit: astproto.RuneSymbol},
		Content: &astproto.Num_Normal{Normal: strconv.FormatInt(int64(r), 10)},
	}
}
