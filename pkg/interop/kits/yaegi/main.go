package yaegi

import (
	"github.com/google/uuid"
	"github.com/traefik/yaegi/interp"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/interop/translate"
	"reflect"
	"sync"
)

// TODO: natives and externals added by pkg/kits/yaegi or pkg/interop/translate don't work

type Options struct {
	Yaegi interp.Options
}

func (o Options) NewKit(s *eval.Scope) *Kit {
	return &Kit{
		o: o,
		i: interp.New(o.Yaegi),
		s: s,
	}
}

type Kit struct {
	o Options
	i *interp.Interpreter
	s *eval.Scope

	lock sync.Mutex
}

func (k *Kit) NewNative(ctx ast.Ctx, id uuid.UUID, pure ast.Pureness, src string) (ast.Native, error) {
	_, err := k.i.Eval(src)
	if err != nil {
		return ast.Native{}, k.s.Errorf(ctx, "yaegi: %w", err)
	}
	value, err := k.i.Eval("entrypoint")
	if err != nil {
		return ast.Native{}, err
	}
	if kind := value.Kind(); kind != reflect.Func {
		return ast.Native{}, k.s.Errorf(ctx, "want func for kind of type of value, got %s", kind)
	}
	return k.newNative(value, id, pure)
}

func (k *Kit) newNative(value reflect.Value, id uuid.UUID, pure ast.Pureness) (ast.Native, error) {
	native, nativeImpl, err := translate.ToNative(k.s, id, value, pure)
	if err != nil {
		return ast.Native{}, err
	}
	k.s.RegisterNative(native, nativeImpl)
	return native, nil
}

func (k *Kit) NewNode(ctx ast.Ctx, src string) (ast.Node, error) {
	_, err := k.i.Eval(src)
	if err != nil {
		return ast.External{}, k.s.Errorf(ctx, "yaegi: %w", err)
	}
	value, err := k.i.Eval("entrypoint")
	if err != nil {
		return nil, err
	}
	if value.Kind() == reflect.Func {
		return nil, ctx.Errorf("funcs must be made using NewNative")
	}
	return translate.ToNode(k.s, value)
}
