package translate

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"reflect"
)

func FromStruct(p ast.Struct) (reflect.Value, error) {
	structType, err := FromStructType(p.T)
	if err != nil {
		return reflect.Value{}, err
	}
	struct_ := reflect.New(structType)
	for i, val := range p.T.Values {
		val_, err := FromNode(val)
		if err != nil {
			return reflect.Value{}, err
		}
		struct_.Field(i).Set(val_)
	}
	return struct_, nil
}

func FromStructType(structType ast.StructType) (reflect.Type, error) {
	if structType.Enum {
		return nil, fmt.Errorf("%w: enums", errs.ErrUnsupported)
	}
	fields := make([]reflect.StructField, len(structType.Keys))
	for i, key := range structType.Keys {
		valType, err := FromType(structType.Values[i])
		if err != nil {
			return nil, err
		}
		fields[i] = reflect.StructField{
			Name:    key,
			PkgPath: "gitlab.com/coalang/go-coa/interop",
			Type:    valType,
		}
	}
	return reflect.StructOf(fields), nil
}

func toStruct(s *eval.Scope, value reflect.Value) (ast.Struct, error) {
	structType, err := toStructType(value.Type())
	if err != nil {
		return ast.Struct{}, err
	}
	struct_ := ast.Struct{
		C:      ast.CtxFromCaller(2),
		T:      structType,
		Values: []ast.Node{},
	}
	for i := 0; i < value.NumField(); i++ {
		valGo, err := ToNode(s, value.Field(i))
		if err != nil {
			return ast.Struct{}, err
		}
		struct_.Values[i] = valGo
	}
	return struct_, nil
}

func toStructType(p reflect.Type) (ast.StructType, error) {
	s := ast.StructType{
		C:      toCtx(p),
		Keys:   []ast.ID{},
		Values: []ast.Type{},
	}
	for i := 0; i < p.NumField(); i++ {
		field := p.Field(i)
		s.Keys[i] = field.Name
		valGo, err := ToType(field.Type)
		if err != nil {
			return ast.StructType{}, err
		}
		s.Values[i] = valGo
	}
	return s, nil
}
