package translate

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"math/big"
	"reflect"
	"strconv"
)

func toNumFromSignedInt(value reflect.Value) (ast.Num, error) {
	return ast.Num{
		C:   ast.CtxFromCaller(2),
		Num: strconv.FormatInt(value.Int(), 10),
	}, nil
}

func toNumFromUnsignedInt(value reflect.Value) (ast.Num, error) {
	return ast.Num{
		C:   ast.CtxFromCaller(2),
		Num: strconv.FormatUint(value.Uint(), 10),
	}, nil
}

func toNumFromFloat(value reflect.Value) (ast.Num, error) {
	return ast.Num{
		C:   ast.CtxFromCaller(2),
		Num: new(big.Rat).SetFloat64(value.Float()).String(),
	}, nil
}

func FromNumTypeToFloat64Type(_ ast.NumType) (reflect.Type, error) {
	return reflect.TypeOf(0.0), nil
}

func FromNumToFloat64(num ast.Num) (reflect.Value, error) {
	rat, ok := new(big.Rat).SetString(num.Num)
	if !ok {
		return reflect.Value{}, fmt.Errorf("%w: num", errs.ErrInvalid)
	}
	float, _ := rat.Float64()
	return reflect.ValueOf(float), nil
}

func FromNumToInt64(num ast.Num) (reflect.Value, error) {
	rat, ok := new(big.Rat).SetString(num.Num)
	if !ok {
		return reflect.Value{}, fmt.Errorf("%w: num", errs.ErrInvalid)
	}
	float, _ := rat.Float64()
	return reflect.ValueOf(int64(float)), nil
}

func FromNumToUint64(num ast.Num) (reflect.Value, error) {
	rat, ok := new(big.Rat).SetString(num.Num)
	if !ok {
		return reflect.Value{}, fmt.Errorf("%w: num", errs.ErrInvalid)
	}
	float, _ := rat.Float64()
	return reflect.ValueOf(uint64(float)), nil
}
