package translate

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"reflect"
)

type StreamChan struct {
	value reflect.Value
	s     *eval.Scope
}

func (s StreamChan) Close() {
	s.value.Close()
}

func (s StreamChan) IsClosed() bool {
	return false // no way to check without actually receiving
}

func (s StreamChan) Send(node ast.Node) {
	value, err := FromNode(node)
	if err != nil {
		return
	}
	s.value.Send(value)
}

func (s StreamChan) Receive() ast.Node {
	value, ok := s.value.Recv()
	if !ok {
		return nil
	}
	node, err := ToNode(s.s, value)
	if err != nil {
		panic(err)
	}
	return node
}

func toStream(s *eval.Scope, value reflect.Value) (ast.Stream, error) {
	stream := ast.Stream{
		C:  newInteropCtx("chan"),
		ID: uuid.New(),
		P:  false,
	}
	var err error
	stream.T, err = toStreamType(value.Type())
	if err != nil {
		return ast.Stream{}, err
	}
	s.NativeStreams.Set(stream.ID, StreamChan{value: value})
	return stream, nil
}

func toStreamType(p reflect.Type) (ast.StreamType, error) {
	t := ast.StreamType{
		C: toCtx(p),
	}
	var err error
	t.Value, err = ToType(p.Elem())
	if err != nil {
		return ast.StreamType{}, err
	}
	switch p.ChanDir() {
	case reflect.SendDir:
		t.Direction = ast.StreamSource
	case reflect.RecvDir:
		t.Direction = ast.StreamSink
	case reflect.BothDir:
		t.Direction = ast.StreamBoth
	}
	return t, nil
}
