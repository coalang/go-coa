package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"reflect"
)

func FromRuneType(_ ast.RuneType) (reflect.Type, error) {
	return reflect.TypeOf('a'), nil
}

func FromRune(rune2 ast.Rune) (reflect.Value, error) {
	return reflect.ValueOf(rune2.R), nil
}
