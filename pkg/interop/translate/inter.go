package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"reflect"
)

func toInter(p reflect.Type) (ast.Inter, error) {
	return ast.Inter{
		T: ast.StructType{C: toCtx(p)},
	}, nil
}
