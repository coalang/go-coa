package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"reflect"
)

// To<coa type>From<go type>
// From<coa type>To<go type>

func toCtx(p reflect.Type) ast.Ctx {
	var pkg, name string
	if p.PkgPath() != "" {
		pkg = "/" + p.PkgPath()
	}
	if p.Name() != "" {
		name = "/" + p.Name()
	}

	if pkg == "" && name == "" {
		return ast.CtxFromCaller(2)
	}

	return newInteropCtx("reflect" + pkg + name)
}

func newInteropCtx(name string) ast.Ctx {
	return ast.Ctx{
		Filepath: "impl/interop/" + name,
	}
}
