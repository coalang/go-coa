package translate

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval/astx"
	"reflect"
)

func FromType(p ast.Type) (reflect.Type, error) {
	switch p := p.(type) {
	case ast.AnyType:
		return FromAnyTypeToAnyType(p)
	case ast.BlockType:
		return fromBlockType(p)
	case ast.MapType:
		return FromMapTypeToMapType(p)
	case ast.NumType:
		return FromNumTypeToFloat64Type(p)
	case ast.DispatcherType2, ast.NativeType, ast.PodType, ast.RootType, ast.SymbolType, astx.PlaceholderType:
		return nil, errs.ErrUnsupported
	case ast.RuneType:
		return FromRuneType(p)
	case ast.StructType:
		return FromStructType(p)
	default:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("FromType/%T", p))
	}
}

func ToType(p reflect.Type) (ast.Type, error) {
	switch p.Kind() {
	case reflect.Bool:
		return ast.Bool.T, nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		// TODO: implement limits (e.g. -129 to 127 for int8)
		return ast.NumType{}, nil
	case reflect.Array:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToType/%s", p.Kind()))
	case reflect.Chan:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToType/%s", p.Kind()))
	case reflect.Func:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToType/%s", p.Kind()))
	case reflect.Interface, reflect.Ptr:
		return toInter(p)
	case reflect.Map:
		return toMapTypeMap(p)
	case reflect.Slice:
		return toMapTypeFromSliceType(p)
	case reflect.Struct:
		return toStructType(p)
	case reflect.String:
		return toMapTypeString(p)
	case reflect.Complex64, reflect.Complex128:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToType/%s", p.Kind()))
	case reflect.UnsafePointer, reflect.Uintptr:
		return nil, fmt.Errorf("%w: %s", errs.ErrUnsupported, p.Kind())
	default:
		return nil, fmt.Errorf("%w: %s", errs.ErrInvalid, p.Kind())
	}
}
