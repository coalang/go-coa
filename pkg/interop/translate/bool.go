package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"reflect"
)

func toBool(value reflect.Value) (ast.Symbol, error) {
	if got := value.Kind(); got != reflect.Bool {
		return ast.Symbol{}, errs.NewErrTypeMismatch(reflect.Bool, got)
	}
	if value.Interface().(bool) {
		return ast.BoolTrue, nil
	} else {
		return ast.BoolFalse, nil
	}
}
