package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"reflect"
)

func FromSymbol(symbol ast.Symbol) (reflect.Value, error) {
	switch symbol.Name {
	case ast.BoolTrue.Name:
		return reflect.ValueOf(true), nil
	case ast.BoolFalse.Name:
		return reflect.ValueOf(false), nil
	default:
		return reflect.Value{}, symbol.C.Errorf("%w: only bool uniques are supported", errs.ErrUnsupported)
	}
}
