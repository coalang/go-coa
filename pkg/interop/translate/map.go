package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"reflect"
)

func FromMap(m ast.Map) (reflect.Value, error) {
	if m.T.Consecutive() {
		if m.T.Key != nil {
			return reflect.Value{}, errs.ErrUnsupported
		}
		valType, err := FromType(m.T.Value)
		if err != nil {
			return reflect.Value{}, err
		}
		slice := reflect.MakeSlice(reflect.SliceOf(valType), len(m.Values), len(m.Values))
		for i, val := range m.Values {
			valGo, err := FromNode(val)
			if err != nil {
				return reflect.Value{}, err
			}
			slice.Index(i).Set(valGo)
		}
		return slice, nil
	} else {
		if m.T.Key == nil {
			return reflect.Value{}, errs.ErrInvalid
		}
		keyType, err := FromType(m.T.Key)
		if err != nil {
			return reflect.Value{}, err
		}
		valType, err := FromType(m.T.Value)
		if err != nil {
			return reflect.Value{}, err
		}
		map_ := reflect.New(reflect.MapOf(keyType, valType))
		for i, key := range m.Keys {
			val := m.Values[i]
			keyGo, err := FromNode(key)
			if err != nil {
				return reflect.Value{}, err
			}
			valGo, err := FromNode(val)
			if err != nil {
				return reflect.Value{}, err
			}
			map_.SetMapIndex(keyGo, valGo)
		}
		return map_, nil
	}
}

func FromMapTypeToMapType(mapType ast.MapType) (reflect.Type, error) {
	key, err := FromType(mapType.Key)
	if err != nil {
		return nil, err
	}
	val, err := FromType(mapType.Value)
	if err != nil {
		return nil, err
	}
	return reflect.MapOf(key, val), nil
}

func toRuneMap(value reflect.Value) (ast.Map, error) {
	return ast.NewRuneMap(ast.CtxFromCaller(2), value.String()), nil
}

func toMapTypeString(p reflect.Type) (ast.MapType, error) {
	t := ast.NewRuneMapType()
	t.C = toCtx(p)
	return t, nil
}

func toMapMap(s *eval.Scope, value reflect.Value) (ast.Map, error) {
	typ, err := toMapTypeMap(value.Type())
	if err != nil {
		return ast.Map{}, err
	}
	keys, values := make([]ast.Node, value.Len()), make([]ast.Node, value.Len())
	for i, key := range value.MapKeys() {
		keys[i], err = ToNode(s, key)
		if err != nil {
			return ast.Map{}, err
		}
		values[i], err = ToNode(s, value.MapIndex(key))
		if err != nil {
			return ast.Map{}, err
		}
	}
	return ast.Map{
		C:      ast.CtxFromCaller(2),
		T:      typ,
		Keys:   keys,
		Values: values,
	}, nil
}

func toMapTypeMap(p reflect.Type) (ast.MapType, error) {
	key, err := ToType(p.Key())
	if err != nil {
		return ast.MapType{}, err
	}
	val, err := ToType(p.Elem())
	if err != nil {
		return ast.MapType{}, err
	}
	return ast.MapType{
		C:     toCtx(p),
		Key:   key,
		Value: val,
	}, nil
}

func toMapFromSlice(s *eval.Scope, value reflect.Value) (ast.Map, error) {
	typ, err := toMapTypeFromSliceType(value.Type())
	if err != nil {
		return ast.Map{}, err
	}
	values := make([]ast.Node, value.Len())
	for i := range values {
		values[i], err = ToNode(s, value.Index(i))
		if err != nil {
			return ast.Map{}, err
		}
	}
	return ast.Map{
		C:      ast.CtxFromCaller(2),
		T:      typ,
		Values: values,
	}, nil
}

func toMapTypeFromSliceType(p reflect.Type) (ast.MapType, error) {
	val, err := ToType(p.Elem())
	if err != nil {
		return ast.MapType{}, err
	}
	return ast.MapType{
		C:     toCtx(p),
		Value: val,
	}, nil
}
