package translate

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/eval/astx"
	"reflect"
)

func FromNode(node ast.Node) (reflect.Value, error) {
	switch node := node.(type) {
	case ast.Type:
		return reflect.Value{}, node.Ctx().Errorf("%w: cannot return type as value", errs.ErrUnsupported)

	case ast.Block:
		return FromBlock(node)
	case ast.Map:
		return FromMap(node)
	case ast.Num:
		return FromNumToFloat64(node)
	case ast.Rune:
		return FromRune(node)
	case ast.Struct:
		return FromStruct(node)
	case ast.Symbol:
		return FromSymbol(node)
	case ast.Call, ast.Cast, ast.Dispatcher2, ast.Native, ast.Label, ast.Pod, ast.Priority, ast.Ref, astx.Later, astx.Placeholder:
		return reflect.Value{}, errs.ErrUnsupported
	default:
		return reflect.Value{}, errs.ErrInvalid
	}
}

func ToNode(s *eval.Scope, value reflect.Value) (ast.Node, error) {
	switch value.Kind() {
	case reflect.Bool:
		return toBool(value)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return toNumFromSignedInt(value)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return toNumFromUnsignedInt(value)
	case reflect.Float32, reflect.Float64:
		return toNumFromFloat(value)
	case reflect.Array:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToNode/%s", value.Kind()))
	case reflect.Chan:
		return toStream(s, value)
	case reflect.Func:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToNode/%s", value.Kind()))
	case reflect.Interface:
		return toInter(value.Type())
	case reflect.Map:
		return toMapMap(s, value)
	case reflect.Slice:
		return toMapFromSlice(s, value)
	case reflect.Struct:
		return toStruct(s, value)
	case reflect.String:
		return toRuneMap(value)
	case reflect.Complex64, reflect.Complex128:
		return nil, errs.NewErrNotImplemented(fmt.Sprintf("ToNode/%s", value.Kind()))
	case reflect.UnsafePointer, reflect.Uintptr, reflect.Ptr:
		return nil, fmt.Errorf("%w: %s", errs.ErrUnsupported, value.Kind())
	default:
		return nil, fmt.Errorf("%w: %s", errs.ErrInvalid, value.Kind())
	}
}
