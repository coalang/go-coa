package translate

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"reflect"
)

func FromBlock(b ast.Block) (reflect.Value, error) {
	funcType, err := fromBlockType(b.T)
	if err != nil {
		return reflect.Value{}, err
	}
	return reflect.MakeFunc(funcType, func(args []reflect.Value) (results []reflect.Value) {
		if len(args) != 1 && len(args) != 2 {
			panic("must have one or two arguments")
		}
		var err error
		var self ast.Node
		var arg ast.Node
		var re ast.Node
		re, err = eval.NewScope(ast.CtxFromCaller(1), false, nil, lib.StaticFS).CallCall(ast.Call{
			C: ast.CtxFromCaller(2),
			T: ast.BlockType{
				C:    ast.CtxFromCaller(2),
				Self: ast.ToType(self),
				Arg:  ast.ToType(arg),
			},
			Self: self,
			Ee:   b,
			Arg:  arg,
		})
		if err != nil {
			panic(err)
		}
		reGo, err := FromNode(re)
		if err != nil {
			panic(err)
		}
		return []reflect.Value{reGo}
	}), nil
}

func ToNative(s *eval.Scope, id uuid.UUID, function reflect.Value, pure ast.Pureness) (ast.Native, eval.NativeImpl, error) {
	if got := function.Kind(); got != reflect.Func {
		return ast.Native{}, nil, errs.NewErrTypeMismatch(reflect.Func, got)
	}

	typ, err := ToBlockType(function.Type())
	if err != nil {
		return ast.Native{}, nil, err
	}
	n := ast.Native{
		T:  ast.NativeType{Inter: typ},
		P:  pure,
		ID: id,
	}
	var impl eval.NativeImpl

	var reImpl func([]reflect.Value) (ast.Node, error)

	switch function.Type().NumOut() {
	case 0:
		reImpl = func(_ []reflect.Value) (ast.Node, error) {
			return nil, nil
		}
	case 1:
		reImpl = func(values []reflect.Value) (ast.Node, error) {
			return ToNode(s, values[0])
		}
	default:
		reImpl = func(values []reflect.Value) (ast.Node, error) {
			nodes := make([]ast.Node, len(values))
			var err error
			for i, value := range values {
				nodes[i], err = ToNode(s, value)
				if err != nil {
					return nil, err
				}
			}
			return ast.Struct{
				T:      n.T.Inter.Re.(ast.StructType),
				Values: nodes,
			}, nil
		}
	}

	switch function.Type().NumIn() {
	case 0:
		impl = func(_ *eval.Scope, c ast.Call) (ast.Node, error) {
			// no arg
			return reImpl(function.Call([]reflect.Value{}))
		}
	case 1:
		impl = func(_ *eval.Scope, c ast.Call) (ast.Node, error) {
			// single arg
			value, err := FromNode(c.Arg)
			if err != nil {
				return nil, err
			}
			return reImpl(function.Call([]reflect.Value{value}))
		}
	default:
		impl = func(_ *eval.Scope, c ast.Call) (ast.Node, error) {
			// multiple (struct) arg
			args := c.Arg.(ast.Struct)
			values := make([]reflect.Value, len(args.Values))
			for i, node := range args.Values {
				var err error
				values[i], err = FromNode(node)
				if err != nil {
					return nil, err
				}
			}
			return reImpl(function.Call(values))
		}
	}
	return n, impl, nil
}

func fromBlockType(_ ast.BlockType) (reflect.Type, error) {
	return nil, errs.NewErrNotImplemented("fromBlockType")
}

func ToBlockType(functionType reflect.Type) (ast.BlockType, error) {
	if got := functionType.Kind(); got != reflect.Func {
		return ast.BlockType{}, errs.NewErrTypeMismatch(reflect.Func, got)
	}

	b := ast.BlockType{
		C: toCtx(functionType),
	}

	// Arg1 and Arg1ID are *never* used

	switch functionType.NumIn() {
	case 0:
	case 1:
		funcArg := functionType.In(0)
		b.ArgName = funcArg.Name()
		var err error
		b.Arg, err = ToType(funcArg)
		if err != nil {
			return ast.BlockType{}, err
		}
	default:
		var funcArg reflect.Type
		var typ ast.Type
		var err error
		subs := make([]ast.StructTypeSub, functionType.NumIn())
		for i := 0; i < functionType.NumIn(); i++ {
			funcArg = functionType.In(i)
			typ, err = ToType(funcArg)
			if err != nil {
				return ast.BlockType{}, err
			}
			subs[i] = ast.StructTypeSub{
				Key: funcArg.Name(),
				Typ: typ,
			}
		}
		b.ArgName = "arg"
		b.Arg = ast.NewStructType(toCtx(functionType), subs...)
	}

	switch functionType.NumOut() {
	case 0:
	case 1:
		funcRe := functionType.Out(0)
		var err error
		b.Re, err = ToType(funcRe)
		if err != nil {
			return ast.BlockType{}, err
		}
	default:
		var funcRe reflect.Type
		var typ ast.Type
		var err error
		subs := make([]ast.StructTypeSub, functionType.NumIn())
		for i := 0; i < functionType.NumIn(); i++ {
			funcRe = functionType.In(i)
			typ, err = ToType(funcRe)
			if err != nil {
				return ast.BlockType{}, err
			}
			subs[i] = ast.StructTypeSub{
				Key: funcRe.Name(),
				Typ: typ,
			}
		}
		b.Re = ast.NewStructType(toCtx(functionType), subs...)
	}

	return b, nil
}
