package translate

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"reflect"
)

func FromAnyTypeToAnyType(_ ast.AnyType) (reflect.Type, error) {
	return func(v interface{}) reflect.Type {
		return reflect.TypeOf(v)
	}(nil), nil
}
