package translate

import (
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
	scope2 "gitlab.com/coalang/go-coa/pkg/utils/scope"
	"reflect"
	"testing"
)

func TestAny(t *testing.T) {
	type test struct {
		v    interface{}
		want string
	}
	tests := map[string]test{
		"int": {
			v:    0,
			want: `0`,
		},
		"int8": {
			v:    int8(0),
			want: `0`,
		},
		"int16": {
			v:    int16(0),
			want: `0`,
		},
		"int32": {
			v:    int32(0),
			want: `0`,
		},
		"int64": {
			v:    int64(0),
			want: `0`,
		},
		"uint": {
			v:    uint(0),
			want: `0`,
		},
		"uint8": {
			v:    uint8(0),
			want: `0`,
		},
		"uint16": {
			v:    uint16(0),
			want: `0`,
		},
		"uint32": {
			v:    uint32(0),
			want: `0`,
		},
		"uint64": {
			v:    uint64(0),
			want: `0`,
		},
		"string": {
			v:    "",
			want: `""`,
		},
		"bool": {
			v:    true,
			want: `#true`,
		},
		"float32": {
			v:    float32(1.25),
			want: `5/4`,
		},
		"float64": {
			v:    1.25,
			want: `5/4`,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			s, err := scope2.NewScope(ast.CtxFromCaller(1), lib.StaticFS, false, [][]ast.ID{})
			if err != nil {
				t.Fatal(err)
			}

			node, err := ToNode(s, reflect.ValueOf(test.v))
			if err != nil {
				t.Errorf("%s: %s", name, err)
			}
			if got := render.Node(node); got != test.want {
				t.Errorf("%s: want %s, got %s", name, test.want, got)
			}
		})
	}
}
