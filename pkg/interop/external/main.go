package external

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"sync"
)

type Server interface {
	Natives() ([]uuid.UUID, error)
	Native(uuid.UUID, ast.Call) (ast.Node, bool, error)

	//ExternalImpls() ([]uuid.UUID, error)
	//External(uuid.UUID) (ast.Node, error)
	//
	//Promises() ([]uuid.UUID, error)
	//Promise(uuid.UUID) (ast.Node, error)
	//
	//StreamImpls() ([]uuid.UUID, error)
	//NativeStreamClose(uuid.UUID) error
	//NativeStreamIsClosed(uuid.UUID) (bool, error)
	//NativeStreamReceive(uuid.UUID) (ast.Node, error)
}

type ScopeWrapper struct {
	lock sync.RWMutex
	s    *eval.Scope
}

func (s *ScopeWrapper) Natives() ([]uuid.UUID, error) {
	return s.s.Natives.Keys(), nil
}

func (s *ScopeWrapper) Native(id uuid.UUID, call ast.Call) (ast.Node, bool, error) {
	native, _, ok := s.s.Natives.Get(id)
	if !ok {
		return nil, false, errs.ErrNotFound
	}
	call = call.Clone().(ast.Call)
	call.Ee = native
	re, err := s.s.CallCall(call)
	if err != nil {
		return nil, true, err
	}
	return re, true, nil
}
