# Coa-Go Interop

## Notes

- all `int`s will be converted to and treated as `int32`s for simplicity's sake
- all Go types are guaranteed to have Coa equivalents, but not the other way
    - `int8` → `{.\int, . < 128 && . > -126}`
    - `string` → `[rune]\map`
    - `chan rune` → `[rune]\chan]`
    - `interface{a()}` → `[a\{}.type]\inter`
    - `[a\int]\inter` → (interfaces in Go can only have functions)

## Modes

Note: In "implementation-dependent", "implementation" means Go Coa, etc, not Go.

- Internal Mode
    - implementation-dependent
    - low security
    - direct access to implementation resources (e.g. `(*eval.Scope).Get(ast.ID) (ast.Node, bool)`)
- AST Mode
    - implementation-independent
    - medium security (e.g. interrupting evaluation of code)
    - native functions return AST nodes (e.g. `RuneMap`, `Num`)
- Native Mode
    - implementation-independent
    - high security
    - native functions return Go types (e.g. `string`, `*big.Rat`)
