package errs

import (
	"errors"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/render"
)

type CoaStringer interface {
	CoaString() string
}

var (
	ErrInvalid           = errors.New("invalid") // problem with input
	ErrASTInvalid        = fmt.Errorf("ast %w", ErrInvalid)
	ErrUnsupported       = errors.New("unsupported") // problem with implementation
	ErrFormatUnsupported = fmt.Errorf("format %e", ErrUnsupported)
	ErrTypeMismatch      = errors.New("type mismatch")
	ErrNotImplemented    = errors.New("not implemented")
	ErrNotFound          = errors.New("not found")
	ErrDisallowed        = errors.New("disallowed")
	ErrNothingExported   = errors.New("nothing exported")
	ErrLoop              = errors.New("infinite loop")
	ErrTaken             = errors.New("already taken")
	ErrExternalNotFound  = fmt.Errorf("external %w", ErrNotFound)
	ErrUnexpectedNil     = errors.New("unexpected nil")
)

func NewErrTypeMismatch(want, got interface{}) error {
	switch want_ := want.(type) {
	case ast.Node:
		want = render.Node(want_)
	case CoaStringer:
		want = want_.CoaString()
	}
	switch got_ := got.(type) {
	case ast.Node:
		got = render.Node(got_)
	case CoaStringer:
		got = got_.CoaString()
	}
	return fmt.Errorf("%w: want %+v, got %+v", ErrTypeMismatch, want, got)
}

func NewErrNotImplemented(name string) error {
	return fmt.Errorf("%w: %s", ErrNotImplemented, name)
}
