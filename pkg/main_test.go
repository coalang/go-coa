package pkg

import (
	"bytes"
	"gitlab.com/coalang/go-coa/lib"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/convert"
	"gitlab.com/coalang/go-coa/pkg/logs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/parser"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/testdata"
	"gitlab.com/coalang/go-coa/pkg/utils/scope"
	"gitlab.com/coalang/go-coa/pkg/verify"
	"io"
	"log"
	"testing"
)

func TestAll(t *testing.T) {
	for name, src := range testdata.Files {
		t.Run(name, func(t *testing.T) {
			pureScope, err := scope.NewScopeWithStd(ast.CtxFromCaller(1), lib.StaticFS, ast.Pure, [][]ast.ID{})
			if err != nil {
				t.Error(err)
			}
			program, err := pureScope.FromProgram(verify.FromConverted(convert.FromParsed(parser.Parse(name, bytes.NewBufferString(src)))))
			if err != nil {
				t.Error(err)
			}
			t.Logf(render.Program(program))
		})
	}
}

func BenchmarkAll(b *testing.B) {
	for name, src := range testdata.Files {
		b.Run(name, func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				b.StopTimer()
				pureScope, err := scope.NewScopeWithStd(ast.CtxFromCaller(1), lib.StaticFS, ast.Pure, [][]ast.ID{})
				if err != nil {
					b.Error(err)
				}
				logs.Scope = log.New(io.Discard, "", 0)
				b.StartTimer()
				_, err = pureScope.FromProgram(verify.FromConverted(convert.FromParsed(parser.Parse(name, bytes.NewBufferString(src)))))
				if err != nil {
					b.Error(err)
				}
			}
		})
	}
}
