package convert

import (
	"github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func ToCtx(p, p2 lexer.Position) ast.Ctx {
	if p.Filename != p2.Filename {
		p.Filename += p2.Filename
	}
	return ast.Ctx{
		Filepath: p.Filename,
		Start: ast.CtxInFile{
			uint(p.Line),
			uint(p.Column),
		},
		End: ast.CtxInFile{
			uint(p2.Line),
			uint(p2.Column),
		},
	}
}
