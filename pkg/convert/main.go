package convert

import (
	ast2 "gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/ast"
	"log"
	"math/big"
	"strconv"
	"strings"
	"unicode"
)

func FromParsed(r *ast.RootBlock, err error) (ast2.Program, error) {
	if err != nil {
		return ast2.Program{}, err
	}
	return FromRootBlock(*r)
}

func FromRootBlock(r ast.RootBlock) (ast2.Program, error) {
	if r.Exprs == nil {
		return ast2.Program{}, nil
	}
	nodes, err := fromExprs(*r.Exprs)
	if err != nil {
		return ast2.Program{}, err
	}
	return ast2.Program{
		C:       ToCtx(r.Pos, r.EndPos),
		Content: nodes,
	}, nil
}

func fromExprs(exprs ast.Exprs) ([]ast2.Node, error) {
	nodes := make([]ast2.Node, 0)
	if exprs.FirstExpr != nil {
		node, err := fromExpr(*exprs.FirstExpr)
		if err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}
	for _, expr := range exprs.AfterExprs {
		if expr.Expr == nil {
			continue
		}
		node, err := fromExpr(*expr.Expr)
		if err != nil {
			return nil, err
		}
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func fromExpr(expr ast.Expr) (ast2.Node, error) {
	if expr.First == nil {
		return nil, ToCtx(expr.Pos, expr.EndPos).Errorf("expr.First must not be nil")
	}
	first, err := fromExprIn(*expr.First)
	if err != nil {
		return nil, err
	}
	if expr.Second == nil {
		return first, nil
	} else {
		second, err := fromExprIn(*expr.Second)
		if err != nil {
			return nil, err
		}
		switch first := first.(type) {
		case ast2.Ref:
			return ast2.Label{
				C:  ToCtx(expr.Pos, expr.EndPos),
				Er: first.Join(),
				Ee: second,
			}, nil
		case ast2.Call:
			switch ee := first.Ee.(type) {
			case ast2.Ref:
				switch second := second.(type) {
				case ast2.Block:
					second.T.Self = first.T.Self
					switch self := first.Self.(type) {
					case ast2.Ref:
						second.T.SelfName = self.Join()
					default:
						return nil, self.Ctx().Errorf("invalid labeler call callee self type %T", self)
					}
					second.T.Arg = first.T.Arg
					switch arg := first.Arg.(type) {
					case ast2.Ref:
						second.T.ArgName = arg.Join()
					default:
						return nil, arg.Ctx().Errorf("invalid labeler call callee arg type %T", arg)
					}

					return ast2.Label{
						C:  ast2.Ctx{},
						Er: ee.Join(),
						Ee: second,
					}, nil
				default:
					return nil, second.Ctx().Errorf("invalid labelee type %T", second)
				}
			default:
				return nil, first.Ee.Ctx().Errorf("invalid labeler call callee type %T", first.Ee)
			}
		default:
			return nil, ToCtx(expr.Pos, expr.EndPos).Errorf("invalid labeler type %T", first)
		}
	}
}

func fromExprIn(exprIn ast.ExprIn) (ast2.Node, error) {
	node, err := fromObj(exprIn.Obj)
	if err != nil {
		return nil, err
	}
	for _, suffix := range exprIn.Suffixes {
		callee, err := fromObj(suffix.Callee)
		if err != nil {
			return nil, err
		}
		arg, err := fromObj(suffix.Arg)
		if err != nil {
			return nil, err
		}
		node = ast2.Call{
			C:    ToCtx(suffix.Pos, suffix.EndPos),
			Self: node,
			Ee:   callee,
			Arg:  arg,
		}
	}
	return node, nil
}

func fromObj(obj ast.Obj) (ast2.Node, error) {
	first, err := fromObjRaw(*obj.First)
	if err != nil {
		return nil, err
	}
	if obj.BSlash == "" {
		return first, nil
	} else {
		caster := first
		if obj.Second == nil {
			switch caster := caster.(type) {
			case ast2.Type:
				return ast2.Cast{
					C:  ToCtx(obj.Pos, obj.EndPos),
					Ee: nil,
					Er: caster,
				}, nil
			default:
				return nil, ToCtx(obj.Pos, obj.EndPos).Errorf("cast caster must be type, not %T", caster)
			}
		}
		castee, err := fromObjRaw(*obj.Second)
		if err != nil {
			return nil, err
		}
		switch caster := caster.(type) {
		case ast2.Type:
			return ast2.NewCast(ToCtx(obj.Pos, obj.EndPos), caster, castee), nil
		default:
			return nil, ToCtx(obj.Pos, obj.EndPos).Errorf("cast caster must be type, not %T", caster)
		}
	}
}

func fromObjRaw(raw ast.ObjRaw) (ast2.Node, error) {
	ctx := ToCtx(raw.Pos, raw.EndPos)
	switch {
	case raw.Text != "":
		return fromText(ctx, raw.Text)
	case raw.Symbol != "":
		return fromSymbol(ctx, raw.Symbol)
	case raw.Rune != "":
		return fromRune(ctx, raw.Rune)
	case raw.Ref != "":
		return fromRef(ctx, raw.Ref)
	case raw.Bracked != nil:
		return fromBracked(*raw.Bracked)
	case raw.Braced != nil:
		return fromBraced(*raw.Braced)
	case raw.Parened != nil:
		return fromParened(*raw.Parened)
	default:
		return nil, ctx.Errorf("%w: pkg/parser_lexer/ast.ObjRaw (didn't match anything)", errs.ErrInvalid)
	}
}

func fromText(ctx ast2.Ctx, src string) (ast2.Map, error) {
	// use strconv.Unquote
	dest := ""
	for i, line := range strings.Split(src[1:len(src)-1], "\n") {
		line, err := strconv.Unquote(`"` + line + `"`)
		if err != nil {
			return ast2.Map{}, ctx.Errorf("line %d: string is invalid: %w", i, err)
		}
		dest += line + "\n"
	}
	return ast2.NewRuneMap(ctx, dest[:len(dest)-1]), nil
}

func fromSymbol(ctx ast2.Ctx, src string) (ast2.Symbol, error) {
	if src == "" {
		return ast2.Symbol{}, ctx.Errorf("%w: src is blank", errs.ErrASTInvalid)
	}
	return ast2.Symbol{
		C:    ctx,
		Name: src[1:],
	}, nil
}

func fromRune(ctx ast2.Ctx, src string) (ast2.Rune, error) {
	unquoted, err := strconv.Unquote(src)
	if err != nil {
		return ast2.Rune{}, ctx.Errorf("unquoting: %w", err)
	}
	var first rune
	for _, r := range unquoted {
		first = r
		break
	}
	return ast2.Rune{
		C: ctx,
		R: first,
	}, nil
}

func fromRef(ctx ast2.Ctx, src string) (ast2.Node, error) {
	var first rune
	for _, r := range src {
		first = r
		break
	}
	if unicode.IsDigit(first) {
		rat, ok := new(big.Rat).SetString(src)
		if !ok {
			return ast2.Ref{}, ctx.Errorf("first rune is a digit but not a number")
		}
		return ast2.Num{
			C:   ctx,
			Num: rat.RatString(),
		}, nil
	}

	return ast2.Ref{
		C:   ctx,
		IDs: strings.Split(src, "."),
	}, nil
}

func fromBracked(bracked ast.Bracked) (ast2.Node, error) {
	if bracked.Block != nil {
		return fromBrackedToBlock(bracked)
	} else {
		return fromBrackedToMap(bracked)
	}
}

func fromBrackedToMap(bracked ast.Bracked) (ast2.Map, error) {
	map_ := ast2.Map{
		C: ToCtx(bracked.Pos, bracked.EndPos),
		T: ast2.MapType{
			C:     ToCtx(bracked.Pos, bracked.EndPos),
			Key:   nil,
			Value: nil,
		},
		Values: []ast2.Node{},
		Keys:   []ast2.Node{},
	}
	if bracked.Content == nil {
		// no content
		return map_, nil
	}
	exprs := make([]*ast.Expr, len(bracked.Content.AfterExprs))
	for i, afterExpr := range bracked.Content.AfterExprs {
		exprs[i] = afterExpr.Expr
	}

	for _, expr := range append([]*ast.Expr{bracked.Content.FirstExpr}, exprs...) {
		if expr == nil {
			continue
		}

		if expr.Second == nil {
			node, err := fromExpr(*expr)
			if err != nil {
				return ast2.Map{}, err
			}

			map_.Keys = append(map_.Keys, nil)
			map_.Values = append(map_.Values, node)
		} else {
			lhs, err := fromExprIn(*expr.First)
			if err != nil {
				return ast2.Map{}, err
			}
			rhs, err := fromExprIn(*expr.Second)
			if err != nil {
				return ast2.Map{}, err
			}

			map_.Keys = append(map_.Keys, lhs)
			map_.Values = append(map_.Values, rhs)
		}
	}
	return map_, nil
}

func idFromNode(node ast2.Node) (ast2.ID, ast2.Type, error) {
	switch node := node.(type) {
	case ast2.Cast:
		id, _, err := idFromNode(node.Ee)
		if err != nil {
			return "", nil, err
		}
		return id, node.Er, nil
	case ast2.Symbol:
		return node.Name, nil, nil
	case ast2.Ref:
		if node.IsUnderscore() {
			return "", nil, nil
		}
	case nil:
		return "", nil, nil
	}
	return "", nil, node.Ctx().Errorf("key of block description map must be a symbol, not %T", node)
}

func idFromMapIndex(p ast2.Map, i int) (ast2.ID, ast2.Type, error) {
	if p.Keys[i] == nil {
		return idFromNode(p.Values[i])
	} else {
		if p.Values[i] != nil {
			log.Printf("%s: block description map cannot have default values", p.Values[i].Ctx())
		}
		return idFromNode(p.Keys[i])
	}
}

func fromBrackedToBlock(bracked ast.Bracked) (ast2.Block, error) {
	block, err := fromBraced(*bracked.Block)
	if err != nil {
		return ast2.Block{}, err
	}
	map_, err := fromBrackedToMap(bracked)
	if err != nil {
		return ast2.Block{}, err
	}
	switch len(map_.Keys) {
	case 0:
		// ignore blank map
		return block, nil
	case 1:
		// self

		block.T.SelfName, block.T.Self, err = idFromMapIndex(map_, 0)
		if err != nil {
			return ast2.Block{}, err
		}
		return block, nil
	case 2:
		// self, arg

		block.T.SelfName, block.T.Self, err = idFromMapIndex(map_, 0)
		if err != nil {
			return ast2.Block{}, err
		}
		block.T.ArgName, block.T.Arg, err = idFromMapIndex(map_, 1)
		if err != nil {
			return ast2.Block{}, err
		}
		return block, nil
	case 3:
		// self, arg, re
		if map_.Keys[2] != nil {
			// invalid since return type should not have keys
			return ast2.Block{}, map_.C.Errorf(
				"return type for block should not have a key",
			)
		}

		block.T.SelfName, block.T.Self, err = idFromMapIndex(map_, 0)
		if err != nil {
			return ast2.Block{}, err
		}
		block.T.ArgName, block.T.Arg, err = idFromMapIndex(map_, 1)
		if err != nil {
			return ast2.Block{}, err
		}
		_, block.T.Re, err = idFromMapIndex(map_, 2)
		if err != nil {
			return ast2.Block{}, err
		}
		return block, nil
	default:
		return ast2.Block{}, map_.C.Errorf("block description map cannot have length of 4 or more (generics coming soon)")
	}
}

func fromBraced(braced ast.Braced) (ast2.Block, error) {
	var nodes []ast2.Node
	var err error
	if braced.Content != nil {
		nodes, err = fromExprs(*braced.Content)
		if err != nil {
			return ast2.Block{}, err
		}
	}

	block := ast2.Block{
		C:         ToCtx(braced.Pos, braced.EndPos),
		T:         ast2.AnyBlockType(),
		Content:   nodes,
		DebugName: "unlabeled block",
	}
	return block, nil
}

func fromParened(parened ast.Parened) (ast2.Priority, error) {
	block, err := fromBraced(ast.Braced{
		Pos:     parened.Pos,
		Content: parened.Content,
	})
	if err != nil {
		return ast2.Priority{}, err
	}
	return ast2.Priority{B: block}, nil
}
