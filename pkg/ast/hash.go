package ast

import (
	"encoding/hex"
	"golang.org/x/crypto/sha3"
	"sync"
)

var toHashMemo = map[string]map[string]Hash{}
var toHashMemoLock sync.RWMutex

func IsAnyType(node Node) bool {
	switch node := node.(type) {
	case AnyType:
		return true
	case Inter:
		return node.T.BlankKeysValuesDefaults()
	default:
		return false
	}
}

func EqualHash(n1, n2 Node) bool {
	return ToHash(n1) == ToHash(n2)
}

var HashZero = MakeHash("", "")

func ToHash(hashable Hashable) Hash {
	if hashable == nil {
		return HashZero
	}
	return hashable.Hash()
}

// MakeHash makes a hash with typ and src.
// typ must not contain any spaces (\x20)
func MakeHash(typ, src string) Hash {
	hash, ok := toHashMemoGet(typ, src)
	if ok {
		return hash
	} else {
		digestTyp := sha3.Sum512([]byte(typ))
		digestSrc := sha3.Sum512([]byte(src))
		hash = Hash(hex.EncodeToString(digestTyp[:]) + hex.EncodeToString(digestSrc[:]))
		go toHashMemoSet(typ, src, hash)
		return hash
	}
}

func toHashMemoGet(typ, src string) (Hash, bool) {
	toHashMemoLock.RLock()
	defer toHashMemoLock.RUnlock()
	hash, ok := toHashMemo[typ][src]
	return hash, ok
}

func toHashMemoSet(typ, src string, hash Hash) {
	toHashMemoLock.Lock()
	defer toHashMemoLock.Unlock()
	if toHashMemo[typ] == nil {
		toHashMemo[typ] = map[string]Hash{}
	}
	toHashMemo[typ][src] = hash
}
