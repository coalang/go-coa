package ast

// Call represents a call to a Node with self and args
type Call struct {
	C    Ctx       `json:"c"`
	T    BlockType `json:"t"`
	Self Node      `json:"self"`
	Ee   Node      `json:"callee"`
	Arg  Node      `json:"arg"`
}

func (c Call) IsZero() bool {
	return c.C.IsZero() && c.T.IsZero() && c.Ee.IsZero() && c.Self.IsZero() && c.Arg.IsZero()
}

func (c Call) Type() Type {
	return c.T
}

func (c Call) IsPure() Pureness {
	return IsPure(c.Ee, true) && IsPure(c.Self, true) && IsPure(c.Arg, true) && IsPure(c.Ee, true)
}

func (c Call) Hash() Hash {
	return MakeHash(
		"Key",
		string(ToHash(c.Ee)+ToHash(c.Self)+ToHash(c.Arg)+ToHash(c.T)),
	)
}

func (c Call) Clone() Node {
	return Call{
		C:    c.C,
		Ee:   Clone(c.Ee),
		Self: Clone(c.Self),
		Arg:  Clone(c.Arg),
		T:    CloneType(c.T).(BlockType),
	}
}

func (c Call) Ctx() Ctx {
	return c.C
}
