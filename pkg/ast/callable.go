package ast

import (
	"fmt"
)

type Callable struct {
	Native *Native
	Block  *Block
}

var _ Node = Callable{}

func CallableNode(node Node) (Callable, error) {
	switch node := node.(type) {
	case Native:
		return CallableNative(node), nil
	case Block:
		return CallableBlock(node), nil
	default:
		return Callable{}, fmt.Errorf("%T unsupported for Callable", node)
	}
}

func CallableBlock(block Block)Callable {
	return Callable{Block: &block}
}

func CallableNative(native Native) Callable {
	return Callable{Native: &native}
}

func (c Callable) Node() Node {
	if c.Native != nil {
		return *c.Native
	}
	return *c.Block
}

func (c Callable) Ctx() Ctx {
	return c.Node().Ctx()
}

func (c Callable) Hash() Hash {
	return c.Node().Hash()
}

func (c Callable) IsPure() Pureness {
	return c.Node().IsPure()
}

func (c Callable) IsZero() bool {
	return c.Node().IsZero()
}

func (c Callable) Clone() Node {
	if c.Native != nil {
		return CallableNative(c.Native.Clone().(Native))
	}
	return CallableBlock(c.Block.Clone().(Block))
}

func (c Callable) Type() Type {
	return c.Node().Type()
}
