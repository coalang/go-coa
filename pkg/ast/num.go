package ast

import (
	"errors"
	"math/big"
	"strconv"
)

type NumType struct {
	Unit Symbol `json:"unit"`
}

var _ Type = NumType{}

var NumTypeGlobal = NumType{}

func (n NumType) IsZero() bool {
	return true
}

func (n NumType) Type() Type {
	return RootType{}
}

func (n NumType) IsPure() Pureness {
	return true
}

func (n NumType) Clone() Node {
	return n
}

func (n NumType) IsType() {}

func (n NumType) Ctx() Ctx {
	return NewBuiltinCtx("NumType")
}

func (n NumType) Hash() Hash {
	return MakeHash("NumType", string(n.Unit.Hash()))
}

type NumSub uint8

const (
	Normal NumSub = iota
	NaN
	PInf
	NInf
)

func (n NumSub) String() string {
	switch n {
	case Normal:
		return "normal"
	case NaN:
		return "nan"
	case PInf:
		return "+inf"
	case NInf:
		return "-inf"
	default:
		return strconv.FormatUint(uint64(n), 10)
	}
}

type Num struct {
	C   Ctx
	T   NumType
	Num string
	Sub NumSub
}

var _ Node = Num{}

func NewNumNaN(c Ctx) Num {
	return Num{C: c, Sub: NaN}
}

func NewNum(c Ctx, v string) Num {
	n, err := Num{
		C:   c,
		Num: v,
	}.Fix()
	if err != nil {
		panic(err)
	}
	return n
}

func (n Num) IsZero() bool {
	return n.C.IsZero() && n.Num == "" && n.Sub == 0
}

func (n Num) Type() Type {
	return n.T
}

func (n Num) IsPure() Pureness {
	return true
}

// Fix tries to fix incorrect things in Num.
// If it can't it returns with an error.
func (n Num) Fix() (dst Num, err error) {
	dst.C = n.C
	if n.Sub != Normal {
		dst.Sub = n.Sub
	} else {
		if n.Num == "" {
			n.Num = "0"
		}
		real_, ok := big.NewRat(1, 1).SetString(n.Num)
		if !ok {
			err = errors.New("Num.Num invalid")
			return
		}
		dst.Num = real_.RatString()
	}
	return
}

func (n Num) Rat() (*big.Rat, error) {
	rat, ok := big.NewRat(1, 1).SetString(n.Num)
	if !ok {
		return nil, errors.New("Num.Num invalid")
	}
	return rat, nil
}

func (n Num) Hash() Hash {
	// realString and imagString are the string forms of real_ and imag_ in proper format
	realString := ""

	if n.Sub != Normal {
		realString = n.Sub.String()
	} else {
		if n.Num == "" {
			n.Num = "0"
		}
		real_, ok := big.NewRat(1, 1).SetString(n.Num)
		if !ok {
			panic("Num.Num invalid")
		}

		if real_.Cmp(big.NewRat(0, 1)) != 0 {
			realString = real_.RatString()
		}
	}
	return MakeHash("Num", realString)
}

func (n Num) Clone() Node {
	return n
}

func (n Num) Ctx() Ctx {
	return n.C
}
