package ast

// BlockType represents the type of a Block.
type BlockType struct {
	C        Ctx  `json:"c"`
	Self     Type `json:"self"`      // type of self
	SelfName ID   `json:"self_name"` // name of self (not included when coerciblility-checking)
	Re       Type `json:"re"`        // type of return value
	Arg      Type `json:"arg"`       // type of arg(s)
	ArgName  ID   `json:"arg_name"`  // name of arg(s) (not included when coerciblility-checking)
}

func AnyBlockType() BlockType {
	return BlockType{
		Self: AnyType{},
		Re:   AnyType{},
		Arg:  AnyType{},
	}
}

var _ Type = BlockType{}

func (b BlockType) IsZero() bool {
	return b.Arg == nil && b.Self == nil && b.Re == nil && b.SelfName == "" && b.ArgName == ""
}

func (b BlockType) Type() Type {
	return RootType{}
}

func (b BlockType) IsPure() Pureness {
	return Pureness(IsPure(b.Self, true) &&
		IsPure(b.Re, true) &&
		IsPure(b.Arg, true))
}

func (b BlockType) IsType() {}

func (b BlockType) Hash() Hash {
	return MakeHash("BlockType", string(ToHash(b.Self)+ToHash(b.Re)+ToHash(b.Arg)))
}

func (b BlockType) Clone() Node {
	return BlockType{
		C:        b.C,
		Self:     CloneType(b.Self),
		SelfName: b.SelfName,
		Re:       CloneType(b.Re),
		Arg:      CloneType(b.Arg),
		ArgName:  b.ArgName,
	}
}

func (b BlockType) Ctx() Ctx {
	return b.C
}

type Block struct {
	C         Ctx
	T         BlockType
	Content   []Node
	DebugName string
}

var _ Type = Block{}

func (b Block) Name() string {
	return b.DebugName
}

func (b Block) IsType() {}

func (b Block) IsZero() bool {
	return b.C.IsZero() && b.T.IsZero() && len(b.Content) == 0 && b.DebugName == ""
}

func (b Block) Type() Type {
	return b.T
}

func (b Block) IsPure() Pureness {
	if !b.T.IsPure() {
		return false
	}
	for _, n := range b.Content {
		if !IsPure(n, true) {
			return false
		}
	}
	return true
}

func (b Block) Hash() Hash {
	var re Hash
	for _, node := range b.Content {
		re += ToHash(node)
	}
	return MakeHash("Value", string(b.T.Hash()+re))
}

func (b Block) Clone() Node {
	content := make([]Node, len(b.Content))
	for i, obj := range b.Content {
		if obj == nil {
			continue
		}
		content[i] = obj.Clone()
	}
	return Block{
		C:         b.C,
		T:         b.T.Clone().(BlockType),
		Content:   content,
		DebugName: b.DebugName,
	}
}

func (b Block) Ctx() Ctx {
	return b.C
}
