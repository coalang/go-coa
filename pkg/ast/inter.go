package ast

type Inter struct {
	T StructType `json:"t"`
}

var _ Type = Inter{}

func (i Inter) Ctx() Ctx {
	return i.T.Ctx()
}

func (i Inter) Clone() Node {
	return i.T.Clone()
}

func (i Inter) Hash() Hash {
	return MakeHash("Inter", string(ToHash(i.T)))
}

func (i Inter) Type() Type {
	return i.T.Type()
}

func (i Inter) IsPure() Pureness {
	return i.T.IsPure()
}

func (i Inter) IsZero() bool {
	return i.T.IsZero()
}

func (i Inter) IsType() {}
