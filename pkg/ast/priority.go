package ast

type Priority struct {
	B Block `json:"b"`
}

var _ Node = Priority{}

func (p Priority) Ctx() Ctx {
	return p.B.Ctx()
}

func (p Priority) Clone() Node {
	return Priority{
		B: Clone(p.B).(Block),
	}
}

func (p Priority) Hash() Hash {
	return MakeHash("Priority", string(ToHash(p.B)))
}

func (p Priority) Type() Type {
	return p.B.Type()
}

func (p Priority) IsPure() Pureness {
	return p.B.IsPure()
}

func (p Priority) IsZero() bool {
	return IsZero(p.B)
}
