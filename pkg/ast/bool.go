package ast

var (
	BoolTrue = Symbol{
		C:    NewBuiltinCtx("Bool/true"),
		Name: "true",
	}

	BoolFalse = Symbol{
		C:    NewBuiltinCtx("Bool/false"),
		Name: "false",
	}

	Bool = Struct{
		C: NewBuiltinCtx("bool"),
		T: StructType{
			C: NewBuiltinCtx("bool/type"),
			Keys: []ID{
				"true",
				"false",
			},
			Values: []Type{
				SymbolType{},
				SymbolType{},
			},
			Defaults: []Node{
				BoolTrue,
				BoolFalse,
			},
			Enum: true,
		},
		Values: []Node{
			BoolTrue,
			BoolFalse,
		},
	}
)

func SymbolToBool(unique Symbol) (bool, bool) {
	switch unique.Name {
	case BoolTrue.Name:
		return true, true
	case BoolFalse.Name:
		return false, true
	default:
		return false, false
	}
}
