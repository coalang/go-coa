package ast

type PodType struct{}

var _ Type = PodType{}

func (p PodType) Ctx() Ctx {
	return NewBuiltinCtx("PodType")
}

func (p PodType) Clone() Node {
	return p
}

func (p PodType) Hash() Hash {
	return MakeHash("PodType", "")
}

func (p PodType) Type() Type {
	return RootType{}
}

func (p PodType) IsPure() Pureness {
	return true
}

func (p PodType) IsZero() bool {
	return true
}

func (p PodType) IsType() {}
