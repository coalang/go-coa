package ast

type Cast struct {
	C  Ctx  `json:"c"`
	Er Type `json:"caster"`
	Ee Node `json:"castee"`
}

var _ Node = Cast{}

func NewCast(c Ctx, er Type, ee Node) Node {
	switch ee := ee.(type) {
	case Ref:
		return Ref{
			C:   c,
			IDs: ee.IDs,
		}
	default:
		return Cast{
			C:  c,
			Ee: ee,
			Er: er,
		}
	}
}

func (c Cast) Ctx() Ctx {
	return c.C
}

func (c Cast) Clone() Node {
	return Cast{
		C:  c.C,
		Ee: Clone(c.Ee),
		Er: CloneType(c.Er),
	}
}

func (c Cast) Hash() Hash {
	return MakeHash("Cast", string(ToHash(c.Er)+ToHash(c.Ee)))
}

func (c Cast) Type() Type {
	return c.Er
}

func (c Cast) IsPure() Pureness {
	return Pure
}

func (c Cast) IsZero() bool {
	return c.C.IsZero() && c.Er == nil && c.Ee == nil
}
