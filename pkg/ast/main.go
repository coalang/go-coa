package ast

import (
	"encoding/json"
	"fmt"
	"github.com/alecthomas/participle/v2/lexer"
	"go/token"
	"runtime"
	"strconv"
	"strings"
)

func NewBuiltinCtx(name string) Ctx {
	return Ctx{Filepath: "builtin/" + name}
}

// ID represents a valid Coa identifier.
type ID = string

// Hash represents a hash from Node.Hash()
type Hash string

type CtxInFile [2]uint

var _ json.Marshaler = CtxInFile{}

func (c CtxInFile) MarshalJSON() ([]byte, error) {
	if c.IsZero() {
		return []byte("null"), nil
	} else {
		return json.Marshal([2]uint(c))
	}
}

func (c CtxInFile) Hash() Hash {
	return MakeHash("CtxInFile", fmt.Sprintf("%d:%d", c[0], c[1]))
}

func (c CtxInFile) LexerPosition(filepath string) lexer.Position {
	return lexer.Position{
		Filename: filepath,
		Line:     int(c[0]),
		Column:   int(c[1]),
	}
}

func (c CtxInFile) TokenPosition(filepath string) token.Position {
	return token.Position{
		Filename: filepath,
		Line:     int(c[0]),
		Column:   int(c[1]),
	}
}

func (c CtxInFile) IsZero() bool {
	return c[0] == 0 && c[1] == 0
}

func (c CtxInFile) String() string {
	re := strconv.FormatUint(uint64(c[0]), 10)
	if c[1] != 0 {
		re += ":" + strconv.FormatUint(uint64(c[1]), 10)
	}
	return re
}

// Ctx holds the original context a node was in (path, line).
type Ctx struct {
	Filepath string    `json:"path,omitempty"`
	Start    CtxInFile `json:"start,omitempty"`
	End      CtxInFile `json:"end,omitempty"`
}

var _ json.Marshaler = Ctx{}

func (c Ctx) MarshalJSON() ([]byte, error) {
	p, err := json.Marshal(c.Filepath)
	if err != nil {
		return nil, err
	}
	var start, end string
	if !c.Start.IsZero() {
		start_, err := c.Start.MarshalJSON()
		if err != nil {
			return nil, err
		}
		start = fmt.Sprintf(`,"start":%s`, start_)
	}
	if !c.End.IsZero() {
		end_, err := c.End.MarshalJSON()
		if err != nil {
			return nil, err
		}
		end = fmt.Sprintf(`,"end":%s`, end_)
	}
	return []byte(fmt.Sprintf(`{"p":%s%s%s}`, p, start, end)), nil
}

func CtxFromCaller(skip uint) Ctx {
	_, filepath, line, ok := runtime.Caller(int(skip))
	if !ok {
		if skip != 1 {
			return CtxFromCaller(1)
		}
		return Ctx{
			Filepath: "failed to get caller",
			Start:    CtxInFile{uint(line)},
		}
	}
	return Ctx{
		Filepath: strings.TrimPrefix(filepath, `/home/sotai/Documents/go-coa/`),
		Start:    CtxInFile{uint(line)},
	}
}

func (c Ctx) Hash() Hash {
	return MakeHash("Origin", string(c.Start.Hash()+c.End.Hash())+c.Filepath)
}

func (c Ctx) Errorf(format string, v ...interface{}) error {
	return fmt.Errorf(c.String()+":\n"+format, v...)
}

func (c Ctx) IsZero() bool {
	return c.Filepath == "" && c.Start.IsZero() && c.End.IsZero()
}

func (c Ctx) String() string {
	re := c.Filepath
	if !c.Start.IsZero() {
		re += ":" + c.Start.String()
	}
	if !c.End.IsZero() {
		re += " → " + c.Filepath + ":" + c.End.String()
	}
	return re
}

type Pureness = bool

const (
	Pure   Pureness = true
	Impure Pureness = false
)

// Node is an AST node, and may or may not be an object.
type Node interface {
	HasCtx
	Hashable
	IsPureCheckable
	IsZeroCheckable
	Clone() Node // must return same as its own type
	Type() Type
}

type HasCtx interface {
	Ctx() Ctx // return source context
}

type Hashable interface {
	Hash() Hash // must be SHA3_512 hash (must not take its own Ctx into account when calculating hash)
}

type IsZeroCheckable interface {
	IsZero() bool
}

type IsPureCheckable interface {
	IsPure() Pureness
}

type IsValidCheckable interface {
	Valid() bool
}

type Nameable interface {
	Name() string
}

// Type is an AST node of a Coa type.
// if Type.Type() is not RootType, type matching changes to value matching
type Type interface {
	Node
	IsType()
}
