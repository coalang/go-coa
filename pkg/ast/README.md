# `pkg/ast` AST

New AST node: `New`*type* (e.g. `NewStruct`)

New AST node from AST node: `Init`*type* (e.g. `InitStruct`)
