package ast

type Label struct {
	C  Ctx    `json:"c"`
	Er string `json:"name"`
	// labeler
	// using Node, not ID to support this:
	// fib[0]: 0
	// fib[1]: 1
	// fib[x]: { fib[x + 1] + fib[x + 2] }
	Ee Node `json:"obj"`
	// labelee
}

var _ Node = Label{}

func (l Label) IsZero() bool {
	return l.C.IsZero() && l.Er == "" && l.Ee.IsZero()
}

func (l Label) Type() Type {
	return l.Ee.Type()
}

func (l Label) IsPure() Pureness {
	return l.Ee.IsPure()
}

func (l Label) Hash() Hash {
	// l.DebugName should have no space
	return MakeHash("Label", string(ToHash(l.Ee))+l.Er)
}

func (l Label) Ctx() Ctx {
	return l.C
}

func (l Label) Clone() Node {
	return Label{
		Er: l.Er,
		Ee: l.Ee.Clone(),
	}
}
