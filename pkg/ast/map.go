package ast

import (
	"fmt"
	"sort"
)

// MapType represents the type of a Map.
// If Key is nil, this represents an unkeyed Map.
// If Key is not nil, this represents a keyed Map.
// If Consecutive is true, this represents a Map with consecutive integer keys (i.e. traditional array).
// If Consecutive is false, this represents a Map without consecutive integer keys and Key must not be nil (i.e. traditional map).
type MapType struct {
	C     Ctx  `json:"c"`
	Key   Type `json:"key"`
	Value Type `json:"value"`
}

func AnyMapType() MapType {
	return MapType{
		Key:   AnyType{},
		Value: AnyType{},
	}
}

var _ Type = MapType{}

type MapTypeConsecutive uint8

const (
	Both MapTypeConsecutive = iota + 1
	Consecutive
	NonConsecutive
)

func (m MapTypeConsecutive) CompatibleWith(m2 MapTypeConsecutive) bool {
	if m == Both || m2 == Both {
		return true
	}
	return m == m2
}

func (m MapTypeConsecutive) String() string {
	switch m {
	case Both:
		return "both"
	case Consecutive:
		return "consecutive"
	case NonConsecutive:
		return "non-consecutive"
	default:
		panic(fmt.Sprintf("invalid MapTypeConsecutive value %v", uint8(m)))
	}
}

func (m MapType) Consecutive() MapTypeConsecutive {
	if IsAnyType(m.Key) {
		return Both
	}
	if m.Key == nil {
		return Consecutive
	}
	return NonConsecutive
}

func (m MapType) ZeroKeyVal() bool {
	return m.Key == nil && m.Value == nil
}

func (m MapType) IsZero() bool {
	return m.C.IsZero() && m.Key == nil && m.Value == nil
}

func (m MapType) Type() Type {
	return RootType{}
}

func (m MapType) IsPure() Pureness {
	return IsPure(m.Key, true) && IsPure(m.Value, true)
}

func (m MapType) Hash() Hash {
	return MakeHash(
		"MapType",
		string(ToHash(m.Key))+string(m.Consecutive().String()[0])+string(ToHash(m.Value)),
	)
}

func (m MapType) IsType() {}

func (m MapType) Clone() Node {
	return MapType{
		C:     m.C,
		Key:   CloneType(m.Key),
		Value: CloneType(m.Value),
	}
}

func (m MapType) Ctx() Ctx {
	return m.C
}

func (m MapType) IsRuneMap() bool {
	_, ok := m.Value.(RuneType)
	return m.Consecutive().CompatibleWith(Consecutive) && ok
}

// Map represents a list of nodes with or without keys.
// Note: the length of Keys may be smaller than the length of Values.
type Map struct {
	C      Ctx     `json:"c"`
	T      MapType `json:"type"`
	Keys   []Node  `json:"keys"`
	Values []Node  `json:"values"`
}

func NewRuneMap(ctx Ctx, src string) Map {
	values := make([]Node, len(src))

	for _, r := range src {
		values = append(values, Rune{
			R: r,
		})
	}

	return Map{
		C:      ctx,
		T:      NewRuneMapType(),
		Values: values,
	}
}

var _ Node = Map{}

func (m Map) RuneMap() (string, error) {
	text := ""
	for _, r := range m.Values {
		if r == nil {
			continue
		}
		switch r := r.(type) {
		case Rune:
			text += string(r.R)
		default:
			return "", r.Ctx().Errorf("wanted rune")
		}
	}
	return text, nil
}

func (m Map) IsZero() bool {
	return m.C.IsZero() && m.T.IsZero() && len(m.Keys) == 0 && len(m.Values) == 0
}

func (m Map) Len() int {
	return len(m.Values)
}

func (m *Map) Set(key, value Node) {
	m.Keys = append(m.Keys, key)
	m.Values = append(m.Values, value)
}

func (m Map) GetKey(i int) Node {
	if i >= len(m.Keys) {
		return nil
	}
	return m.Keys[i]
}

func (m Map) Get(key Node) (Node, bool) {
	keyhash := key.Hash()
	idx := -1
	for i, maybeKey := range m.Keys {
		if maybeKey.Hash() == keyhash {
			idx = i
			break
		}
	}
	if idx == -1 {
		return nil, false
	}
	return m.Values[idx], true
}

func (m Map) Type() Type {
	return m.T
}

func (m Map) IsPure() Pureness {
	if !m.T.IsPure() {
		return false
	}
	for _, val := range m.Values {
		if !IsPure(val, true) {
			return false
		}
	}
	return true
}

func (m Map) Hash() Hash {
	keys := make([]string, len(m.Keys))
	for i, key := range m.Keys {
		keys[i] = string(ToHash(key))
	}
	sort.Strings(keys)
	keys_ := ""
	for _, key := range keys {
		keys_ += key + " "
	}

	var values Hash
	for _, value := range m.Values {
		values += ToHash(value)
	}
	return MakeHash("Map", string(m.T.Hash()+values)+keys_)
}

func (m Map) Ctx() Ctx {
	return m.C
}

func (m Map) Clone() Node {
	values := make([]Node, len(m.Values))
	for i, value := range m.Values {
		if value != nil {
			values[i] = value.Clone()
		}
	}
	keys := make([]Node, len(m.Keys))
	for key, value := range m.Keys {
		if value != nil {
			keys[key] = value.Clone()
		}
	}
	return Map{
		C:      m.C,
		T:      m.T.Clone().(MapType),
		Values: values,
		Keys:   keys,
	}
}
