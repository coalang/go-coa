package ast

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

type ExternalType struct{}

func (e ExternalType) IsType() {}

var _ Type = ExternalType{}

func (e ExternalType) Ctx() Ctx {
	return NewBuiltinCtx("external")
}

func (e ExternalType) Clone() Node {
	return e
}

func (e ExternalType) Hash() Hash {
	return MakeHash("ExternalType", "")
}

func (e ExternalType) Type() Type {
	return RootType{}
}

func (e ExternalType) IsPure() Pureness {
	return true
}

func (e ExternalType) IsZero() bool {
	return true
}

type External struct {
	C  Ctx       `json:"c"`
	ID uuid.UUID `json:"id"`
}

var _ Node = External{}

func NewExternalFromSymbol(symbol Symbol) (External, error) {
	id, err := utils.UUIDFrom(symbol.Name)
	if err != nil {
		return External{}, err
	}
	return External{
		ID: id,
	}, nil
}

func (e External) Ctx() Ctx {
	return e.C
}

func (e External) Clone() Node {
	return e
}

func (e External) Hash() Hash {
	return MakeHash("External", e.ID.String())
}

func (e External) Type() Type {
	return ExternalType{}
}

func (e External) IsPure() Pureness {
	return true
}

func (e External) IsZero() bool {
	return e.C.IsZero() && e.ID == [16]byte{}
}
