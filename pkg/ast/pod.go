package ast

type Pod struct {
	C         Ctx    `json:"c"`
	Inside    Node   `json:"inside"`
	Path      string `json:"path"`
}

var _ Node = Pod{}

func (p Pod) IsZero() bool {
	return p.C.IsZero() && IsZero(p.Inside) && p.Path == ""
}

func (p Pod) Ctx() Ctx {
	return p.C
}

func (p Pod) Clone() Node {
	return Pod{
		C:      p.C,
		Inside: Clone(p.Inside),
		Path:   p.Path,
	}
}

func (p Pod) Hash() Hash {
	return MakeHash("Pod", string(ToHash(p.Inside))+p.Path)
}

func (p Pod) Type() Type {
	return PodType{}
}

func (p Pod) IsPure() Pureness {
	return true
}
