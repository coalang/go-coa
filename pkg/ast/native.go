package ast

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

type NativeType struct {
	C     Ctx       `json:"c"`
	Inter BlockType `json:"type"`
}

var _ Type = NativeType{}

func (n NativeType) IsZero() bool {
	return n.C.IsZero() && n.Inter.IsZero()
}

func (n NativeType) Ctx() Ctx {
	return n.C
}

func (n NativeType) Clone() Node {
	return NativeType{
		C:     n.C,
		Inter: n.Inter.Clone().(BlockType),
	}
}

func (n NativeType) Hash() Hash {
	return MakeHash("NativeType", string(n.Inter.Hash()))
}

func (n NativeType) Type() Type {
	return RootType{}
}

func (n NativeType) IsPure() Pureness {
	return n.Inter.IsPure()
}

func (n NativeType) IsType() {}

type Native struct {
	C  Ctx
	T  BlockType
	P  Pureness
	ID uuid.UUID
}

var _ Node = Native{}

func NewNativeFromSymbol(symbol Symbol) (Native, error) {
	id, err := utils.UUIDFrom(symbol.Name)
	if err != nil {
		return Native{}, err
	}
	return Native{
		ID: id,
	}, nil
}

func NewNative(id uuid.UUID, pure Pureness, ctx Ctx, self, arg, re Type) Native {
	return Native{
		C: ctx,
		T: BlockType{
			C:    ctx,
			Self: self,
			Re:   re,
			Arg:  arg,
		},
		P:  pure,
		ID: id,
	}
}

func (n Native) IsZero() bool {
	return n.C.IsZero() && n.P == false && n.ID == [16]byte{} && n.T.IsZero()
}

func (n Native) Type() Type {
	return n.T
}

func (n Native) IsPure() Pureness {
	return n.P
}

func (n Native) Hash() Hash {
	return MakeHash("callNative", string(n.T.Hash())+n.ID.String())
}

func (n Native) Ctx() Ctx {
	return n.C
}

func (n Native) Clone() Node {
	return n
}
