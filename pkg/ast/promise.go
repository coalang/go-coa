package ast

import "github.com/google/uuid"

type PromiseType struct {
	C        Ctx  `json:"c"`
	Promised Type `json:"promised"`
}

var _ Type = PromiseType{}

func (p PromiseType) Ctx() Ctx {
	return p.C
}

func (p PromiseType) Clone() Node {
	return PromiseType{
		C:        p.C,
		Promised: CloneType(p.Promised),
	}
}

func (p PromiseType) Hash() Hash {
	return MakeHash("PromiseType", string(ToHash(p.Promised)))
}

func (p PromiseType) Type() Type {
	return RootType{}
}

func (p PromiseType) IsPure() Pureness {
	return true
}

func (p PromiseType) IsZero() bool {
	return p.C.IsZero() && IsZero(p.Promised)
}

func (p PromiseType) IsType() {}

type Promise struct {
	C  Ctx
	T  PromiseType
	ID uuid.UUID
}

var _ Node = Promise{}

func (p Promise) Ctx() Ctx {
	return p.C
}

func (p Promise) Clone() Node {
	return Promise{
		C:  p.C,
		T:  p.T.Clone().(PromiseType),
		ID: p.ID,
	}
}

func (p Promise) Hash() Hash {
	return MakeHash("Promise", string(p.T.Hash())+p.ID.String())
}

func (p Promise) Type() Type {
	return p.T
}

func (p Promise) IsPure() Pureness {
	return true
}

func (p Promise) IsZero() bool {
	return p.C.IsZero() && p.T.IsZero() && p.ID == [16]byte{}
}
