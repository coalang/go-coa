package ast

var AnyTypeGlobal = AnyType{}
//var AnyTypeGlobal = Inter{T: StructType{C: NewBuiltinCtx("AnyTypeGlobal")}}

type AnyType struct{}

var _ Type = AnyType{}

func (a AnyType) Ctx() Ctx {
	return Ctx{}
}

func (a AnyType) Clone() Node {
	return a
}

func (a AnyType) Hash() Hash {
	return MakeHash("AnyType", "")
}

func (a AnyType) Type() Type {
	return RootType{}
}

func (a AnyType) IsPure() Pureness {
	return Pure
}

func (a AnyType) IsZero() bool {
	return true
}

func (a AnyType) IsType() {}
