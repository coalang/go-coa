package ast

import (
	"encoding/json"
)

var RootTypeGlobal RootType

// RootType is the type of types.
type RootType struct{}

var _ Type = RootType{}

func (r RootType) IsZero() bool {
	return true
}

func (r RootType) Type() Type {
	return r
}

func (r RootType) IsPure() Pureness {
	return true
}

// Hash returns the same hash
func (r RootType) Hash() Hash {
	return MakeHash("RootType", "")
}

// Ctx returns a blank Ctx
func (r RootType) Ctx() Ctx {
	return Ctx{}
}

// Clone returns itself since RootType is a "singleton" (RootType doesn't have any data itself)
func (r RootType) Clone() Node {
	return r
}

// IsType is used to satisfy the Type interface
func (r RootType) IsType() {}

type Meta struct{}

type Program struct {
	C       Ctx
	Content Nodes `json:"content"`
}

func (p Program) Ctx() Ctx {
	return p.C
}

func (p Program) Clone() Program {
	p2 := Program{
		C:       p.C,
		Content: make([]Node, len(p.Content)),
	}
	for i, node := range p.Content {
		p2.Content[i] = Clone(node)
	}
	return p2
}

func (p Program) SerializeMust(serializer func(interface{}) ([]byte, error)) []byte {
	b, err := serializer(p)
	if err != nil {
		panic(err)
	}
	return b
}

func (p Program) JSON() []byte {
	return p.SerializeMust(json.Marshal)
}

func (p Program) JSONIndent() []byte {
	return p.SerializeMust(func(i interface{}) ([]byte, error) {
		return json.MarshalIndent(i, "", "\t")
	})
}
