package ast

import (
	"encoding/json"
	"fmt"
)

type nodeJSON struct {
	AnyType         AnyType         `json:"any_type"`
	BlockType       BlockType       `json:"block_type"`
	DispatcherType2 DispatcherType2 `json:"dispatcher_type_2"`
	ExternalType    ExternalType    `json:"external_type"`
	Inter           Inter           `json:"inter"`
	MapType         MapType         `json:"map_type"`
	NativeType      NativeType      `json:"native_type"`
	NumType         NumType         `json:"num_type"`
	PodType         PodType         `json:"pod_type"`
	PromiseType     PromiseType     `json:"promise_type"`
	RootType        RootType        `json:"root_type"`
	RuneType        RuneType        `json:"rune_type"`
	StreamType      StreamType      `json:"stream_type"`
	StructType      StructType      `json:"struct_type"`
	SymbolType      SymbolType      `json:"unique_type"`
	Block           Block           `json:"block"`
	Call            Call            `json:"call"`
	Cast            Cast            `json:"cast"`
	Dispatcher2     Dispatcher2     `json:"dispatcher_2"`
	External        External        `json:"external"`
	Label           Label           `json:"label"`
	Map             Map             `json:"map"`
	Native          Native          `json:"native"`
	Num             Num             `json:"num"`
	Pod             Pod             `json:"pod"`
	Priority        Priority        `json:"priority"`
	Promise         Promise         `json:"promise"`
	Ref             Ref             `json:"ref"`
	Rune            Rune            `json:"rune"`
	Stream          Stream          `json:"stream"`
	Struct          Struct          `json:"struct"`
	Symbol          Symbol          `json:"unique"`
}

type Nodes []Node

var _ json.Unmarshaler = &Nodes{}
var _ json.Marshaler = &Nodes{}

func (n *Nodes) MarshalJSON() ([]byte, error) {
	result := make([]nodeJSON, len(*n))
	for i, node := range *n {
		switch node := node.(type) {
		case AnyType:
			result[i] = nodeJSON{AnyType: node}
		case BlockType:
			result[i] = nodeJSON{BlockType: node}
		case DispatcherType2:
			result[i] = nodeJSON{DispatcherType2: node}
		case ExternalType:
			result[i] = nodeJSON{ExternalType: node}
		case Inter:
			result[i] = nodeJSON{Inter: node}
		case MapType:
			result[i] = nodeJSON{MapType: node}
		case NativeType:
			result[i] = nodeJSON{NativeType: node}
		case NumType:
			result[i] = nodeJSON{NumType: node}
		case PodType:
			result[i] = nodeJSON{PodType: node}
		case PromiseType:
			result[i] = nodeJSON{PromiseType: node}
		case RootType:
			result[i] = nodeJSON{RootType: node}
		case RuneType:
			result[i] = nodeJSON{RuneType: node}
		case StreamType:
			result[i] = nodeJSON{StreamType: node}
		case StructType:
			result[i] = nodeJSON{StructType: node}
		case SymbolType:
			result[i] = nodeJSON{SymbolType: node}
		case Block:
			result[i] = nodeJSON{Block: node}
		case Call:
			result[i] = nodeJSON{Call: node}
		case Cast:
			result[i] = nodeJSON{Cast: node}
		case Dispatcher2:
			result[i] = nodeJSON{Dispatcher2: node}
		case External:
			result[i] = nodeJSON{External: node}
		case Label:
			result[i] = nodeJSON{Label: node}
		case Map:
			result[i] = nodeJSON{Map: node}
		case Native:
			result[i] = nodeJSON{Native: node}
		case Num:
			result[i] = nodeJSON{Num: node}
		case Pod:
			result[i] = nodeJSON{Pod: node}
		case Priority:
			result[i] = nodeJSON{Priority: node}
		case Promise:
			result[i] = nodeJSON{Promise: node}
		case Ref:
			result[i] = nodeJSON{Ref: node}
		case Rune:
			result[i] = nodeJSON{Rune: node}
		case Stream:
			result[i] = nodeJSON{Stream: node}
		case Struct:
			result[i] = nodeJSON{Struct: node}
		case Symbol:
			result[i] = nodeJSON{Symbol: node}
		default:
			return nil, fmt.Errorf("index %d: invalid node %T", i, node)
		}
	}
	return json.Marshal(result)
}

func (n *Nodes) UnmarshalJSON(data []byte) error {
	nodeJSONs := make([]nodeJSON, 0)
	err := json.Unmarshal(data, &nodeJSONs)
	if err != nil {
		return err
	}
	*n = make(Nodes, 0)
	for i, nodeJSON := range nodeJSONs {
		switch {
		case !IsZero(nodeJSON.AnyType):
			(*n)[i] = nodeJSON.AnyType
		case !IsZero(nodeJSON.BlockType):
			(*n)[i] = nodeJSON.BlockType
		case !IsZero(nodeJSON.DispatcherType2):
			(*n)[i] = nodeJSON.DispatcherType2
		case !IsZero(nodeJSON.ExternalType):
			(*n)[i] = nodeJSON.ExternalType
		case !IsZero(nodeJSON.Inter):
			(*n)[i] = nodeJSON.Inter
		case !IsZero(nodeJSON.MapType):
			(*n)[i] = nodeJSON.MapType
		case !IsZero(nodeJSON.NativeType):
			(*n)[i] = nodeJSON.NativeType
		case !IsZero(nodeJSON.NumType):
			(*n)[i] = nodeJSON.NumType
		case !IsZero(nodeJSON.PodType):
			(*n)[i] = nodeJSON.PodType
		case !IsZero(nodeJSON.PromiseType):
			(*n)[i] = nodeJSON.PromiseType
		case !IsZero(nodeJSON.RootType):
			(*n)[i] = nodeJSON.RootType
		case !IsZero(nodeJSON.RuneType):
			(*n)[i] = nodeJSON.RuneType
		case !IsZero(nodeJSON.StreamType):
			(*n)[i] = nodeJSON.StreamType
		case !IsZero(nodeJSON.StructType):
			(*n)[i] = nodeJSON.StructType
		case !IsZero(nodeJSON.SymbolType):
			(*n)[i] = nodeJSON.SymbolType
		case !IsZero(nodeJSON.Block):
			(*n)[i] = nodeJSON.Block
		case !IsZero(nodeJSON.Call):
			(*n)[i] = nodeJSON.Call
		case !IsZero(nodeJSON.Cast):
			(*n)[i] = nodeJSON.Cast
		case !IsZero(nodeJSON.Dispatcher2):
			(*n)[i] = nodeJSON.Dispatcher2
		case !IsZero(nodeJSON.External):
			(*n)[i] = nodeJSON.External
		case !IsZero(nodeJSON.Label):
			(*n)[i] = nodeJSON.Label
		case !IsZero(nodeJSON.Map):
			(*n)[i] = nodeJSON.Map
		case !IsZero(nodeJSON.Native):
			(*n)[i] = nodeJSON.Native
		case !IsZero(nodeJSON.Num):
			(*n)[i] = nodeJSON.Num
		case !IsZero(nodeJSON.Pod):
			(*n)[i] = nodeJSON.Pod
		case !IsZero(nodeJSON.Priority):
			(*n)[i] = nodeJSON.Priority
		case !IsZero(nodeJSON.Promise):
			(*n)[i] = nodeJSON.Promise
		case !IsZero(nodeJSON.Ref):
			(*n)[i] = nodeJSON.Ref
		case !IsZero(nodeJSON.Rune):
			(*n)[i] = nodeJSON.Rune
		case !IsZero(nodeJSON.Stream):
			(*n)[i] = nodeJSON.Stream
		case !IsZero(nodeJSON.Struct):
			(*n)[i] = nodeJSON.Struct
		case !IsZero(nodeJSON.Symbol):
			(*n)[i] = nodeJSON.Symbol
		default:
			return fmt.Errorf("index %d: invalid node", i)
		}
	}
	return nil
}
