package ast

import (
	"log"
)

type StructType struct {
	C        Ctx    `json:"c"`
	Keys     []ID   `json:"keys"`
	Values   []Type `json:"values"`
	Defaults []Node `json:"defaults"`
	Enum     bool   `json:"exclusive"`
}

var _ Type = StructType{}

type StructTypeSub struct {
	Key     ID
	Default Node
	Typ     Type
}

func NewStructType(c Ctx, subs ...StructTypeSub) StructType {
	keys, types, defaults :=
		make([]ID, len(subs)),
		make([]Type, len(subs)),
		make([]Node, len(subs))
	for i, sub := range subs {
		keys[i] = sub.Key
		types[i] = sub.Typ
		defaults[i] = sub.Default
	}
	return StructType{
		C:        c,
		Keys:     keys,
		Values:   types,
		Defaults: defaults,
		Enum:     false,
	}
}

// InitStructType makes a new StructType from a Map.
func InitStructType(node Node) (StructType, error) {
	switch node := node.(type) {
	case Map:
		subs := make([]StructTypeSub, node.Len())
		for i, key := range node.Keys {
			keyString := ""
			value := node.Values[i]
			var typ Type

			switch key := key.(type) {
			case Symbol:
				if key.Name == "" {
					return StructType{}, key.Ctx().Errorf("symbol not be blank")
				}
				keyString = key.Name
				typ = ToType(value)
			case Cast:
				switch castee := key.Ee.(type) {
				case Symbol:
					if castee.Name == "" {
						return StructType{}, key.Ctx().Errorf("symbol not be blank")
					}
					keyString = castee.Name
				default:
					return StructType{}, key.Ctx().Errorf("castee must be symbol")
				}
				typ = key.Er
			default:
				return StructType{}, key.Ctx().Errorf("map key must be symbol or cast of symbol")
			}

			subs[i] = StructTypeSub{
				Key:     keyString,
				Default: value,
				Typ:     typ,
			}
		}
		return NewStructType(node.Ctx(), subs...), nil
	default:
		return StructType{}, node.Ctx().Errorf("cannot coerce %T to structtype (from node)", node)
	}
}

func (s StructType) BlankKeysValuesDefaults() bool {
	return len(s.Keys) == 0 && len(s.Values) == 0 && len(s.Defaults) == 0
}

func (s StructType) IsZero() bool {
	return len(s.Keys) == 0 && len(s.Values) == 0 && len(s.Defaults) == 0 && s.Enum == false && s.C.IsZero()
}

func (s StructType) Type() Type {
	return RootType{}
}

func (s StructType) IsPure() Pureness {
	for _, typ := range s.Values {
		if !typ.IsPure() {
			return false
		}
	}
	return true
}

func (s StructType) Hash() Hash {
	re := ""
	for i := range s.Keys {
		re += string(ToHash(s.Values[i])) + s.Keys[i]
	}
	return MakeHash("T", re)
}

func (s StructType) IsType() {}

func (s StructType) Ctx() Ctx {
	return s.C
}

func (s StructType) Clone() Node {
	keys, values, defaults := make([]ID, len(s.Keys)), make([]Type, len(s.Values)), make([]Node, len(s.Defaults))
	for i := range s.Keys {
		keys[i] = s.Keys[i]
	}
	for i := range s.Values {
		values[i] = CloneType(s.Values[i])
	}
	for i := range s.Defaults {
		defaults[i] = CloneType(s.Values[i])
	}
	return StructType{
		C:        s.C,
		Keys:     keys,
		Values:   values,
		Defaults: defaults,
		Enum:     s.Enum,
	}
}

func (s StructType) Get(key ID) (int, Type, Node, bool) {
	for i, key_ := range s.Keys {
		if key_ == key {
			return i, s.Values[i], s.Defaults[i], true
		}
	}
	return 0, nil, nil, false
}

func (s StructType) GetSingle(key ID) Type {
	for i, key_ := range s.Keys {
		if key_ == key {
			return s.Values[i]
		}
	}
	return nil
}

func (s StructType) HasDefault() bool {
	for _, default_ := range s.Defaults {
		if default_ != nil || !IsZero(default_) {
			return true
		}
	}
	return false
}

func (s StructType) Default() Struct {
	return Struct{
		C: s.C,
		T: s,
		//Keys:   s.Keys,
		Values: s.Defaults,
	}
}

func (s StructType) Len() int {
	return len(s.Values)
}

type Struct struct {
	C Ctx        `json:"c"`
	T StructType `json:"type"`
	//Keys   []ID       `json:"keys"`
	Values []Node `json:"values"`
}

var _ Node = Struct{}

func InitStruct(node Node) (Struct, error) {
	structType, err := InitStructType(node)
	if err != nil {
		return Struct{}, err
	}
	return structType.Default(), nil
}

func InitStructFromStructType(node Node, structType StructType) (Struct, error) {
	log.Printf("InitStructFromStructType %T %T", node, structType)
	log.Println(node.(Map).Values)
	log.Printf("%T", structType.Values[0])
	log.Println(structType.C)
	switch node := node.(type) {
	case Map:
		s := structType.Default()
		for i, key := range node.Keys {
			keyString := ""
			value := node.Values[i]
			var typ Type

			switch key := key.(type) {
			case Symbol:
				if key.Name == "" {
					return Struct{}, key.Ctx().Errorf("symbol not be blank")
				}
				keyString = key.Name
				typ = ToType(value)
			case Cast:
				switch castee := key.Ee.(type) {
				case Symbol:
					if castee.Name == "" {
						return Struct{}, key.Ctx().Errorf("symbol not be blank")
					}
					keyString = castee.Name
				default:
					return Struct{}, key.Ctx().Errorf("castee must be symbol")
				}
				typ = key.Er
			case nil:
			default:
				return Struct{}, node.Keys[i].Ctx().Errorf("map key must be symbol or cast of symbol, not %T", node.Keys[i])
			}

			if typ == nil {
				typ = ToType(value)
			}

			index, _, def, ok := structType.Get(keyString)
			if !ok {
				return Struct{}, ToCtx(key).Errorf("key %s not found in type", keyString)
			}

			if IsZero(value) {
				value = def
			}

			s.Values[index] = value
		}
		return s, nil
	default:
		return Struct{}, node.Ctx().Errorf("cannot coerce %T to struct (from structtype)", node)
	}
}

type StructSub struct {
	StructTypeSub
	Val Node
}

func NewStruct(c Ctx, subs ...StructSub) Struct {
	values := make([]Node, len(subs))
	typeSubs := make([]StructTypeSub, len(subs))
	for i, sub := range subs {
		typeSubs[i] = sub.StructTypeSub
		values[i] = sub.Val
	}
	return Struct{
		C:      c,
		T:      NewStructType(c, typeSubs...),
		Values: values,
	}
}

func (s Struct) IsZero() bool {
	return s.C.IsZero() && s.T.IsZero() &&
		//len(s.Keys) == 0 &&
		len(s.Values) == 0
}

func (s Struct) Type() Type {
	return s.T
}

func (s Struct) IsPure() Pureness {
	for _, val := range s.Values {
		if !val.IsPure() {
			return false
		}
	}
	return s.T.IsPure()
}

func (s Struct) Hash() Hash {
	re := ""
	for i := range s.Values {
		re += string(ToHash(s.Values[i]))
	}
	return MakeHash("Struct", re+" "+string(s.T.Hash()))
}

func (s Struct) Ctx() Ctx {
	return s.C
}

func (s Struct) Clone() Node {
	values := make([]Node, len(s.Values))
	for i := range s.Values {
		values[i] = Clone(s.Values[i])
	}
	return Struct{
		C:      s.C,
		T:      s.T.Clone().(StructType),
		Values: values,
	}
}

func (s Struct) UnsafeSet(sub StructSub) {
	s.T.Keys = append(s.T.Keys, sub.Key)
	s.T.Values = append(s.T.Values, sub.Typ)
	s.T.Defaults = append(s.T.Defaults, sub.Default)
	s.Values = append(s.Values, sub.Val)
}

func (s Struct) Get(key ID) (Node, bool) {
	for i, key_ := range s.T.Keys {
		if key_ == key {
			return s.Values[i], true
		}
	}
	return nil, false
}

func (s Struct) GetSingle(key ID) Node {
	node, _ := s.Get(key)
	return node
}
