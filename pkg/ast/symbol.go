package ast

type SymbolType struct {
	C Ctx `json:"c"`
}

var _ Type = SymbolType{}

func (u SymbolType) Ctx() Ctx {
	return u.C
}

func (u SymbolType) Clone() Node {
	return u
}

func (u SymbolType) Hash() Hash {
	return MakeHash("SymbolType", "")
}

func (u SymbolType) Type() Type {
	return RootType{}
}

func (u SymbolType) IsPure() Pureness {
	return true
}

func (u SymbolType) IsZero() bool {
	return u.C.IsZero()
}

func (u SymbolType) IsType() {}

type Symbol struct {
	C    Ctx `json:"c"`
	Name ID  `json:"name"`
}

var _ Node = Symbol{}

func (u Symbol) Ctx() Ctx {
	return u.C
}

func (u Symbol) Clone() Node {
	return u
}

func (u Symbol) Hash() Hash {
	return MakeHash("Symbol", u.Name)
}

func (u Symbol) Type() Type {
	return SymbolType{}
}

func (u Symbol) IsPure() Pureness {
	return true
}

func (u Symbol) IsZero() bool {
	return u.C.IsZero() && u.Name == ""
}
