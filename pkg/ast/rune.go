package ast

type RuneType struct{}

var _ Type = RuneType{}

func (r RuneType) IsZero() bool {
	return true
}

func (r RuneType) Type() Type {
	return RootType{}
}

func (r RuneType) IsPure() Pureness {
	return true
}

func (r RuneType) Ctx() Ctx {
	return Ctx{}
}

func (r RuneType) Clone() Node {
	return r
}

func (r RuneType) Hash() Hash {
	return MakeHash("RuneType", "")
}

func (r RuneType) IsType() {}

type Rune struct {
	C Ctx  `json:"c"`
	R rune `json:"r"`
}

var _ Node = Rune{}

func (r Rune) IsZero() bool {
	return r.C.IsZero() && r.R == 0
}

func (r Rune) Type() Type {
	return RuneType{}
}

func (r Rune) IsPure() Pureness {
	return true
}

func (r Rune) Ctx() Ctx {
	return r.C
}

func (r Rune) Clone() Node {
	return r
}

func (r Rune) Hash() Hash {
	return MakeHash("Rune", string(r.R))
}
