package ast

import (
	"strings"
)

const (
	IDBase  = ",b"
	IDCore  = ",c"
	IDIndex = ",i"
	IDSelf  = ",s"
	IDArg   = ",a"
)

var RefTypeGlobal RefType

type RefType struct{}

var _ Type = RefType{}

var refTypeCtx = NewBuiltinCtx("RefType")

func (r RefType) Ctx() Ctx {
	return refTypeCtx
}

var refTypeHash = MakeHash("RefType", "")

func (r RefType) Hash() Hash {
	return refTypeHash
}

func (r RefType) IsPure() Pureness {
	return Pure
}

func (r RefType) IsZero() bool {
	return true
}

func (r RefType) Clone() Node {
	return r
}

func (r RefType) Type() Type {
	return RootTypeGlobal
}

func (r RefType) IsType() {}

// Ref represents a reference to a variable in the current scope.
// If an ID in Ref.IDs starts with a comma (`,`),
// then it references a special object, such as:
// - block self fallback (`,self`)
// - block arg fallback (`,arg`)
// - core pod (`,core`)
// - central index (`,index`)
type Ref struct {
	C   Ctx  `json:"c"`
	IDs []ID `json:"ids"`
}

func NewRefSingleID(id ID) Ref {
	return Ref{IDs: []ID{id}}
}

var _ Type = Ref{}

func (r Ref) IsZero() bool {
	return r.C.IsZero() && len(r.IDs) == 0
}

func (r Ref) Dot() bool {
	// does r.IDs represent "."?
	return len(r.IDs) == 2 && r.IDs[0] == "" && r.IDs[1] == ""
}

func (r Ref) Type() Type {
	return RefTypeGlobal
}

func (r Ref) IsPure() Pureness {
	//if len(r.IDs) == 1 && r.IDs[0] == "_" {
	//	return Pure // guaranteed nil
	//}
	return Pure
}

func (r Ref) Hash() Hash {
	re := ""
	for _, id := range r.IDs {
		re += id + " "
	}
	return MakeHash("Ref", re)
}

func (r Ref) Clone() Node {
	ids := make([]ID, len(r.IDs))
	copy(ids, r.IDs)
	return Ref{
		C:   r.C,
		IDs: ids,
	}
}

func (r Ref) Ctx() Ctx {
	return r.C
}

func (r Ref) IsType() {}

func (r Ref) Join() string {
	return strings.Join(r.IDs, ".")
}

func (r Ref) IsUnderscore() bool {
	return len(r.IDs) == 1 && r.IDs[0] == "_"
}

func (r Ref) ToComma() Ref {
	switch {
	case len(r.IDs) >= 2:
		if len(r.IDs) == 2 &&
			r.IDs[0] == "" &&
			r.IDs[1] == "" {
			// block self fallback
			r.IDs = []ID{IDSelf}
			return r
		}
		if r.IDs[0] == "" &&
			r.IDs[1] != "" {
			// block self fallback
			r.IDs = append([]ID{IDSelf}, r.IDs[2:]...)
			return r
		}
		fallthrough
	case len(r.IDs) >= 3:
		if len(r.IDs) >= 4 &&
			r.IDs[0] == "" &&
			r.IDs[1] == "" &&
			r.IDs[2] == "" {
			r.IDs = append([]ID{IDIndex}, r.IDs[3:]...)
			return r
		}
		if len(r.IDs) >= 3 &&
			r.IDs[0] == "" &&
			r.IDs[1] == "" {
			r.IDs = append([]ID{IDCore}, r.IDs[2:]...)
			return r
		}
	}
	return r
}

func (r Ref) IsDisallowed(disallowedRefs [][]ID) bool {
	for _, disallowedRef := range disallowedRefs {
		if len(disallowedRef) == 0 {
			continue
		}
		for i, id := range disallowedRef {
			if i >= len(r.IDs) && r.IDs[i] != id {
				return false
			}
		}
		return true
	}
	return false
}
