package ast

func externalRefsForNodes(nodes []Node) []string {
	re := make([]string, 0)
	for _, node := range nodes {
		re = append(re, ExternalRefs(node)...)
	}
	return re
}

func ExternalRefs(node Node) []string {
	switch node := node.(type) {
	case Block:
		return append(externalRefsForNodes(node.Content), ExternalRefs(node.T)...)
	case Call:
		return externalRefsForNodes([]Node{node.T, node.Self, node.Ee, node.Arg})
	case Cast:
		return append(ExternalRefs(node.Ee), ExternalRefs(node.Er)...)
	case Dispatcher2:
		re := make([]string, 0, len(node.Keys)+len(node.Values))
		for _, key := range node.Keys {
			re = append(re, ExternalRefs(key)...)
		}
		for _, value := range node.Values {
			re = append(re, ExternalRefs(value)...)
		}
		return re
	case External:
		return []string{}
	case Label:
		return ExternalRefs(node.Ee)
	case Map:
		re := make([]string, 0, len(node.Keys)+len(node.Values))
		for _, key := range node.Keys {
			re = append(re, ExternalRefs(key)...)
		}
		for _, value := range node.Values {
			re = append(re, ExternalRefs(value)...)
		}
		return append(re, ExternalRefs(node.T)...)
	case Native:
		return ExternalRefs(node.T)
	case Num:
		return []string{}
	case Pod:
		return ExternalRefs(node.Inside)
	case Priority:
		return ExternalRefs(node.B)
	case Promise:
		return ExternalRefs(node.T)
	case Ref:
		return node.IDs
	case Rune:
		return []string{}
	case Stream:
		return ExternalRefs(node.T)
	case Struct:
		re := make([]string, 0, len(node.Values))
		for _, value := range node.Values {
			re = append(re, ExternalRefs(value)...)
		}
		return append(re, ExternalRefs(node.T)...)
	case Symbol:
		return []string{}
	case AnyType:
		return []string{}
	case BlockType:
		return externalRefsForNodes([]Node{node.Self, node.Re, node.Arg})
	case DispatcherType2:
		return []string{}
	case ExternalType:
		return []string{}
	case Inter:
		return ExternalRefs(node.T)
	case MapType:
		return externalRefsForNodes([]Node{node.Key, node.Value})
	case NativeType:
		return ExternalRefs(node.Inter)
	case NumType:
		return []string{}
	case PodType:
		return []string{}
	case PromiseType:
		return ExternalRefs(node.Promised)
	case RootType:
		return []string{}
	case RuneType:
		return []string{}
	case StreamType:
		return ExternalRefs(node.Value)
	case StructType:
		re := make([]string, 0, len(node.Defaults)+len(node.Values))
		for _, default_ := range node.Defaults {
			re = append(re, ExternalRefs(default_)...)
		}
		for _, value := range node.Values {
			re = append(re, ExternalRefs(value)...)
		}
		return re
	case SymbolType:
		return []string{}
	}
	return nil
}
