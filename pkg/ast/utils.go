package ast

func NewRuneMapType() MapType {
	return MapType{
		C:     NewBuiltinCtx("NewRuneMapType"),
		Key:   nil,
		Value: RuneType{},
	}
}

func ToType(node Node) Type {
	if node == nil {
		return nil
	}
	return node.Type()
}

func Clone(node Node) Node {
	if node == nil {
		return nil
	}
	return node.Clone()
}

func CloneType(typ Type) Type {
	node := Clone(typ)
	if node == nil {
		return nil
	}
	return node.(Type)
}

func IsPure(isPureable IsPureCheckable, zero Pureness) Pureness {
	if isPureable == nil {
		return zero
	} else {
		return isPureable.IsPure()
	}
}

func IsZero(isZeroer IsZeroCheckable) bool {
	if isZeroer == nil {
		return true
	}
	return isZeroer.IsZero()
}

func Name(nameable Nameable) string {
	if nameable == nil {
		return ""
	}
	return nameable.Name()
}

func ToCtx(hasCtx HasCtx) Ctx {
	if hasCtx != nil {
		return hasCtx.Ctx()
	}
	return Ctx{}
}
