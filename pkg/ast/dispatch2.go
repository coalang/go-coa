package ast

type DispatcherType2 struct{}

func (d DispatcherType2) IsZero() bool {
	return true
}

func (d DispatcherType2) Ctx() Ctx {
	return Ctx{}
}

func (d DispatcherType2) Clone() Node {
	return d
}

func (d DispatcherType2) Hash() Hash {
	return MakeHash("DispatcherType2", "")
}

func (d DispatcherType2) Type() Type {
	return RootType{}
}

func (d DispatcherType2) IsPure() Pureness {
	return true
}

func (d DispatcherType2) IsType() {}

var _ Type = DispatcherType2{}

type Dispatcher2 struct {
	C      Ctx     `json:"c"`    // first occurrence of dispatchable data (e.g. first dispatchable block declaration)
	Keys   []Call  `json:"keys"` // Call.Ee should be nil
	Values []Callable `json:"vals"`
}

var _ Node = Dispatcher2{}

func NewDispatcher2(ctx Ctx, blocks []Callable) Dispatcher2 {
	keys := make([]Call, len(blocks))
	for i, block := range blocks {
		keys[i] = Call{
			C: block.Ctx(),
			T: block.Type().(BlockType),
		}
	}
	return Dispatcher2{
		C:      ctx,
		Keys:   keys,
		Values: blocks,
	}
}

func (d Dispatcher2) IsZero() bool {
	return d.C.IsZero() && len(d.Keys) == 0 && len(d.Values) == 0
}

func (d Dispatcher2) Type() Type {
	return DispatcherType2{}
}

func (d Dispatcher2) IsPure() Pureness {
	for _, key := range d.Keys {
		if !key.IsPure() {
			return false
		}
	}
	for _, val := range d.Values {
		if !val.IsPure() {
			return false
		}
	}
	return true
}

func (d Dispatcher2) Ctx() Ctx {
	return d.C
}

func (d Dispatcher2) Clone() Node {
	keys := make([]Call, len(d.Keys))
	for i, val := range d.Keys {
		keys[i] = val.Clone().(Call)
	}
	values := make([]Callable, len(d.Values))
	for i, val := range d.Values {
		values[i] = val.Clone().(Callable)
	}
	d2 := Dispatcher2{
		C:      d.C,
		Keys:   keys,
		Values: values,
	}
	return d2
}

func (d Dispatcher2) Hash() Hash {
	re := ""
	for _, key := range d.Keys {
		re += string(ToHash(key)) + "\n"
	}
	for _, val := range d.Values {
		re += string(ToHash(val)) + "\n"
	}
	return MakeHash("Dispatcher2", re)
}
