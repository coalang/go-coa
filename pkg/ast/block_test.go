package ast

import (
	"testing"
)

func TestBlock_Hash(t *testing.T) {
	type test struct {
		a, b Block
	}
	var tests = map[string]test{
		"blank": {
			a: Block{
				T: BlockType{
					Self: NumType{},
					Re:   NumType{},
					Arg:  StructType{},
				},
				Content: []Node{},
			},
			b: Block{
				T: BlockType{
					Self: NumType{},
					Re:   NumType{},
					Arg:  StructType{},
				},
				Content: []Node{},
			},
		},
	}
	for name, test := range tests {
		if test.a.Hash() != test.b.Hash() {
			t.Fatalf("%s: mismatch", name)
		}
	}
}
