package ast

import (
	"github.com/google/uuid"
	"strconv"
)

type StreamDirection uint8

const (
	StreamBoth StreamDirection = iota
	StreamSink
	StreamSource
)

func (s StreamDirection) CompatibleWith(s2 StreamDirection) bool {
	return s == StreamBoth || s2 == StreamBoth || s+s2 == 3
}

func (s StreamDirection) IsZero() bool {
	return s == 0
}

func (s StreamDirection) Rune() rune {
	switch s {
	case StreamSource:
		return 's'
	case StreamSink:
		return 'r'
	case StreamBoth:
		return 'b'
	default:
		return 0
	}
}

func (s StreamDirection) String() string {
	switch s {
	case StreamSource:
		return "send"
	case StreamSink:
		return "receive"
	case StreamBoth:
		return "both"
	default:
		return strconv.FormatInt(int64(s), 10)
	}
}

type StreamType struct {
	C         Ctx             `json:"c"`
	Value     Type            `json:"value"`
	Direction StreamDirection `json:"direction"`
}

var _ Type = StreamType{}

func AnyStreamType() StreamType {
	return StreamType{
		Value: AnyType{},
	}
}

func (s StreamType) Ctx() Ctx {
	return s.C
}

func (s StreamType) Clone() Node {
	return StreamType{
		C:         s.C,
		Value:     CloneType(s.Value),
		Direction: s.Direction,
	}
}

func (s StreamType) Hash() Hash {
	return MakeHash("StreamType", string(ToHash(s.Value))+s.Direction.String())
}

func (s StreamType) Type() Type {
	return RootType{}
}

func (s StreamType) IsPure() Pureness {
	return true
}

func (s StreamType) IsZero() bool {
	return s.C.IsZero() && IsZero(s.Value) && s.Direction.IsZero()
}

func (s StreamType) IsType() {}

type Stream struct {
	// TODO: replace with inter of send, receive, close, closed, value
	C  Ctx        `json:"c"`
	T  StreamType `json:"t"`
	ID uuid.UUID  `json:"id"`
	P  Pureness   `json:"p"`
}

var _ Node = Stream{}

func AnyStream() Stream {
	return Stream{
		T: AnyStreamType(),
	}
}

func (s Stream) Ctx() Ctx {
	return s.C
}

func (s Stream) Clone() Node {
	return Stream{
		C:  s.C,
		T:  s.T,
		ID: s.ID,
		P:  s.P,
	}
}

func (s Stream) Hash() Hash {
	return MakeHash("Stream", string(s.T.Hash()))
}

func (s Stream) Type() Type {
	return s.T
}

func (s Stream) IsPure() Pureness {
	return s.P
}

func (s Stream) IsZero() bool {
	return s.C.IsZero() && s.T.IsZero() && s.ID == [16]byte{} && s.P == false
}
