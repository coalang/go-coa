package logs

import (
	"log"
	"os"
)

var Scope = log.New(os.Stderr, "", 0)
