package parser_lexer

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/convert"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/parser"
	"io"
)

func ParseToProgram(filename string, src io.Reader) (program ast.Program, err error) {
	parsed, err := parser.Parse(filename, src)
	if err != nil {
		return ast.Program{}, err
	}
	return convert.FromRootBlock(*parsed)
}
