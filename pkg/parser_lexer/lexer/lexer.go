package lexer

import (
	"fmt"
	"github.com/alecthomas/participle/v2/lexer/stateful"
	"regexp"
)

var (
	SymbolPatternSrc      = "#[^\\.\\(\\)\\[\\]\\{\\}\\s\\n'\",`:\\\\]+"
	RefPatternSrc         = "[^#\\(\\)\\[\\]\\{\\}\\s\\n'\",`:\\\\]+"
	RefDisallowedChars    = "#()[]{} \n'\",`:\\"
	SymbolDisallowedChars = "()[]{} \n'\",`:\\"
	SymbolPatternFullSrc  = `^` + SymbolPatternSrc + `$`
	SymbolPatternFull     = regexp.MustCompile(SymbolPatternFullSrc)
)

var Maker = stateful.MustSimple([]stateful.Rule{
	{Name: `Sep`, Pattern: `(,)|\n(\s*)\n`},
	{Name: `Space`, Pattern: `[\s]+`},
	{Name: `Text`, Pattern: `"[^"]*"`},
	{Name: `Symbol`, Pattern: SymbolPatternSrc},
	{Name: `Rune`, Pattern: `'[^']*'`},
	{Name: `Ref`, Pattern: RefPatternSrc},
	{Name: `Colon`, Pattern: `:`},
	{Name: `BSlash`, Pattern: `\\`},
	{Name: `OParen`, Pattern: `\(`},
	{Name: `CParen`, Pattern: `\)`},
	{Name: `OBrack`, Pattern: `\[`},
	{Name: `CBrack`, Pattern: `\]`},
	{Name: `OBrace`, Pattern: `\{`},
	{Name: `CBrace`, Pattern: `\}`},
})

type TokenType rune

const (
	_ TokenType = -iota
	EOF

	Sep
	Space

	Text
	Symbol
	Rune
	Ref

	Colon
	BSlash

	OParen
	CParen
	OBrack
	CBrack
	OBrace
	CBrace
)

func (t TokenType) String() string {
	switch t {
	case EOF:
		return "EOF"
	case Sep:
		return "separator"
	case Space:
		return "inline space"
	case Text:
		return "text literal"
	case Symbol:
		return "symbol literal"
	case Rune:
		return "rune literal"
	case Ref:
		return "ref literal"
	case Colon:
		return "colon"
	case BSlash:
		return "backslash"
	case OParen:
		return "open parentheses"
	case CParen:
		return "close parentheses"
	case OBrack:
		return "open bracket"
	case CBrack:
		return "close bracket"
	case OBrace:
		return "open brace"
	case CBrace:
		return "close brace"
	default:
		return fmt.Sprint(rune(t))
	}
}
