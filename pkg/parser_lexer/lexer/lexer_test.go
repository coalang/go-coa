package lexer

import (
	"fmt"
	"testing"

	lexer2 "github.com/alecthomas/participle/v2/lexer"
)

func TestLexer(t *testing.T) {
	testOk := func(t *testing.T, filename, s string, eTokens []lexer2.Token) {
		lexed, err := Maker.LexString(filename, s)
		if err != nil {
			t.Error(err)
		}
		// eToken: expected token
		// aTok: actual token
		for i, eToken := range eTokens {
			if aToken, err := lexed.Next(); err != nil {
				t.Error(err)
			} else if aToken.Value != eToken.Value || aToken.Type != eToken.Type {
				eTokS := fmt.Sprintf("%s (type %v)", eToken.Value, eToken.Type)
				aTokS := fmt.Sprintf("%s (type %v)", aToken.Value, aToken.Type)
				t.Errorf("%d: want %s, got %s", i, eTokS, aTokS)
			}
		}
	}

	t.Run("none", func(t *testing.T) {
		testOk(t, "none.coa", "", nil)
	})
	t.Run("sep/newlines", func(t *testing.T) {
		testOk(t, ".coa", "\n\n", []lexer2.Token{
			{Value: "\n\n", Type: rune(Sep)},
		})
	})
	t.Run("sep/comma", func(t *testing.T) {
		testOk(t, ".coa", `,`, []lexer2.Token{
			{Value: `,`, Type: rune(Sep)},
		})
	})
	t.Run("space", func(t *testing.T) {
		testOk(t, "none.coa", " ", []lexer2.Token{
			{Value: " ", Type: rune(Space)},
		})
	})
	t.Run("paren", func(t *testing.T) {
		// \u0061: lowercase a
		testOk(t, ".coa", `()`, []lexer2.Token{
			{Value: `(`, Type: rune(OParen)},
			{Value: `)`, Type: rune(CParen)},
		})
	})
	t.Run("brack", func(t *testing.T) {
		// \u0061: lowercase a
		testOk(t, ".coa", `[]`, []lexer2.Token{
			{Value: `[`, Type: rune(OBrack)},
			{Value: `]`, Type: rune(CBrack)},
		})
	})
	t.Run("brace", func(t *testing.T) {
		// \u0061: lowercase a
		testOk(t, ".coa", `{}`, []lexer2.Token{
			{Value: `{`, Type: rune(OBrace)},
			{Value: `}`, Type: rune(CBrace)},
		})
	})
	t.Run("bSlash", func(t *testing.T) {
		testOk(t, ".coa", `a\b`, []lexer2.Token{
			{Value: `a`, Type: rune(Ref)},
			{Value: `\`, Type: rune(BSlash)},
			{Value: `b`, Type: rune(Ref)},
		})
	})
	t.Run("colon", func(t *testing.T) {
		testOk(t, ".coa", `a:b`, []lexer2.Token{
			{Value: `a`, Type: rune(Ref)},
			{Value: `:`, Type: rune(Colon)},
			{Value: `b`, Type: rune(Ref)},
		})
	})
	t.Run("text", func(t *testing.T) {
		testOk(t, ".coa", `"abc"`, []lexer2.Token{
			{Value: `"abc"`, Type: rune(Text)},
		})
	})
	t.Run("rune/char", func(t *testing.T) {
		testOk(t, ".coa", `'a'`, []lexer2.Token{
			{Value: `'a'`, Type: rune(Rune)},
		})
	})
	t.Run("rune/unicode", func(t *testing.T) {
		testOk(t, ".coa", `'\u0061'`, []lexer2.Token{
			{Value: `'\u0061'`, Type: rune(Rune)},
		})
	})
	t.Run("ref", func(t *testing.T) {
		testOk(t, ".coa", `a`, []lexer2.Token{
			{Value: `a`, Type: rune(Ref)},
		})
	})
}
