package ast

import (
	"github.com/alecthomas/participle/v2/lexer"
)

// RootBlock is the root block
type RootBlock struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Exprs  *Exprs `parser:"@@?"`
}

type Exprs struct {
	FirstExpr  *Expr       `parser:"@@"`
	AfterExprs []AfterExpr `parser:"@@*"`
}

func (e Exprs) Len() int {
	count := len(e.AfterExprs)
	if e.FirstExpr != nil {
		count += 1
	}
	return count
}

type AfterExpr struct {
	Sep  string `parser:"@Sep"`
	Expr *Expr  `parser:"@@?"`
}

type Expr struct {
	Pos    lexer.Position
	EndPos lexer.Position
	First  *ExprIn `parser:"@@"`
	Colon  string  `parser:"(@Colon"`
	Second *ExprIn `parser:" @@)?"`
}

type ExprIn struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	Obj      Obj          `parser:"@@"`
	Suffixes []ExprSuffix `parser:"@@*"`
}

type Obj struct {
	Pos    lexer.Position
	EndPos lexer.Position
	First  *ObjRaw `parser:"@@"`
	BSlash string  `parser:"(@BSlash"`
	Second *ObjRaw `parser:" @@?)?"`
}

type ObjRaw struct {
	Pos     lexer.Position
	EndPos  lexer.Position
	Text    string   `parser:" @Text"`
	Symbol  string   `parser:"|@Symbol"`
	Rune    string   `parser:"|@Rune"`
	Ref     string   `parser:"|@Ref"`
	Bracked *Bracked `parser:"|@@"`
	Braced  *Braced  `parser:"|@@"`
	Parened *Parened `parser:"|@@"`
}

type ExprSuffix struct {
	Pos    lexer.Position
	EndPos lexer.Position
	Callee Obj `parser:"@@"`
	Arg    Obj `parser:"@@"`
}
