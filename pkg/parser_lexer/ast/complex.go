package ast

import "github.com/alecthomas/participle/v2/lexer"

type Bracked struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string  `parser:"@OBrack"`
	Content  *Exprs  `parser:"@@?"`
	ContentC string  `parser:"@CBrack"`
	Block    *Braced `parser:"@@?"`
}

type Braced struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string `parser:"@OBrace"`
	Content  *Exprs `parser:"@@?"`
	ContentC string `parser:"@CBrace"`
}

type Parened struct {
	Pos      lexer.Position
	EndPos   lexer.Position
	ContentO string `parser:"@OParen"`
	Content  *Exprs `parser:"@@?"`
	ContentC string `parser:"@CParen"`
}
