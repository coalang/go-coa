package verify

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"strings"
)

func Type(t ast.Type) (ast.Type, error) {
	if t == nil {
		return nil, nil
	}
	switch t := t.(type) {
	case ast.NumType, ast.RuneType, ast.RootType, ast.AnyType, nil:
		return t, nil
	case ast.BlockType:
		return BlockType(t)
	case ast.MapType:
		return MapType(t)
	case ast.StructType:
		return StructType(t)
	default:
		if t.Type() != (ast.RootType{}) {
			// value as type
			return t, nil
		}
		return nil, t.Ctx().Errorf("%T invalid or not implemented for pkg/compile.Type", t)
	}
}

func BlockType(b ast.BlockType) (ast.BlockType, error) {
	if !b.IsPure() {
		return b, nil
	}
	var err error
	b.Re, err = Type(b.Re)
	if err != nil {
		return ast.BlockType{}, err
	}
	b.Self, err = Type(b.Self)
	if err != nil {
		return ast.BlockType{}, err
	}
	b.Arg, err = Type(b.Arg)
	if err != nil {
		return ast.BlockType{}, err
	}
	return b, nil
}

func MapType(m ast.MapType) (ast.MapType, error) {
	if !m.IsPure() {
		return m, nil
	}
	var err error
	m.Key, err = Type(m.Key)
	if err != nil {
		return ast.MapType{}, err
	}
	m.Value, err = Type(m.Value)
	if err != nil {
		return ast.MapType{}, err
	}
	return m, nil
}

func StructType(s ast.StructType) (ast.StructType, error) {
	if !s.IsPure() {
		return s, nil
	}
	var err error
	for i := range s.Values {
		if strings.ContainsRune(s.Keys[i], '.') {
			return ast.StructType{}, s.C.Errorf(
				"%w: key %d contains invalid character (`.`)",
				errs.ErrASTInvalid,
				i,
			)
		}
		s.Values[i], err = Type(s.Values[i])
		if err != nil {
			return ast.StructType{}, err
		}
	}
	return s, nil
}
