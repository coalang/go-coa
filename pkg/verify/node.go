package verify

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/lexer"
	"strconv"
	"unicode/utf8"
)

func Node(n ast.Node) (ast.Node, error) {
	switch node := n.(type) {
	case ast.Type:
		return Type(node)

	case ast.Block:
		return Block(node)
	case ast.Call:
		return Call(node)
	case ast.Cast:
		return Cast(node)
	case ast.Dispatcher2:
		return Dispatcher2(node)
	case ast.Label:
		return Label(node)
	case ast.Map:
		return Map(node)
	case ast.Num:
		return node.Fix()
	case ast.Priority:
		return Priority(node)
	case ast.Ref:
		return Ref(node)
	case ast.Rune:
		return node, nil
	case ast.Struct:
		return Struct(node)
	case ast.Symbol:
		return Symbol(node)
	case nil:
		return nil, nil
	default:
		return nil, node.Ctx().Errorf("%T %w (pkg/compile.Node)", node, errs.ErrInvalid)
	}
}

func Callable(c ast.Callable) (ast.Callable, error) {
	if c.Native != nil {
		return c, nil
	}
	block, err := Block(*c.Block)
	if err != nil {
		return ast.Callable{}, err
	}
	return ast.CallableBlock(block), nil
}

func Block(b ast.Block) (ast.Block, error) {
	if !b.IsPure() {
		return b, nil
	}
	var err error
	b.T, err = BlockType(b.T)
	if err != nil {
		return ast.Block{}, err
	}
	for i, node := range b.Content {
		b.Content[i], err = Node(node)
		if err != nil {
			return ast.Block{}, err
		}
	}
	return b, nil
}

func Call(c ast.Call) (ast.Call, error) {
	var err error
	c.Ee, err = Node(c.Ee)
	if err != nil {
		return ast.Call{}, err
	}
	c.Self, err = Node(c.Self)
	if err != nil {
		return ast.Call{}, err
	}
	c.Arg, err = Node(c.Arg)
	if err != nil {
		return ast.Call{}, err
	}
	return c, nil
}

func Cast(cast ast.Cast) (ast.Node, error) {
	var err error
	// special case for:
	// - `external\#00000000-0000-0000-0000-000000000000`
	// - `native\#00000000-0000-0000-0000-000000000000`
	if er, ok := cast.Er.(ast.Ref); ok {
		if len(er.IDs) == 1 {
			switch er.IDs[0] {
			case "native":
				if ee, ok := cast.Ee.(ast.Symbol); ok {
					return ast.NewNativeFromSymbol(ee)
				}
			case "external":
				if ee, ok := cast.Ee.(ast.Symbol); ok {
					return ast.NewExternalFromSymbol(ee)
				}

			}
		}
	}

	cast.Ee, err = Node(cast.Ee)
	if err != nil {
		return ast.Cast{}, err
	}
	cast.Er, err = Type(cast.Er)
	if err != nil {
		return ast.Cast{}, err
	}
	return cast, nil
}

func Dispatcher2(dispatcher2 ast.Dispatcher2) (ast.Dispatcher2, error) {
	var err error
	for i, val := range dispatcher2.Values {
		dispatcher2.Values[i], err = Callable(val)
		if err != nil {
			return ast.Dispatcher2{}, err
		}
	}
	for i, key := range dispatcher2.Keys {
		dispatcher2.Keys[i], err = Call(key)
		if err != nil {
			return ast.Dispatcher2{}, err
		}
	}
	return dispatcher2, nil
}

func Label(l ast.Label) (ast.Label, error) {
	if !l.IsPure() {
		return l, nil
	}
	var err error
	l.Ee, err = Node(l.Ee)
	if err != nil {
		return ast.Label{}, err
	}
	return l, nil
}

func Map(m ast.Map) (ast.Map, error) {
	if !m.IsPure() {
		return m, nil
	}
	var err error
	for i, val := range m.Values {
		m.Values[i], err = Node(val)
		if err != nil {
			return ast.Map{}, err
		}
	}
	m.T, err = MapType(m.T)
	if err != nil {
		return ast.Map{}, err
	}
	return m, nil
}

func Priority(priority ast.Priority) (ast.Priority, error) {
	b, err := Block(priority.B)
	if err != nil {
		return ast.Priority{}, err
	}
	return ast.Priority{B: b}, nil
}

func Ref(ref ast.Ref) (ast.Node, error) {
	switch len(ref.IDs) {
	case 0:
		return nil, ref.C.Errorf("length of ref cannot be 0")
	case 1:
		if ref.IDs[0] == "_" {
			return nil, nil
		}
	default:
		for i, id := range ref.IDs {
			if !utf8.ValidString(id) {
				return nil, ref.C.Errorf("id %d: invalid UTF-8", i)
			}
		}
	}
	return ref, nil
}

func Struct(s ast.Struct) (ast.Struct, error) {
	if !s.IsPure() {
		return s, nil
	}
	var err error
	for i, val := range s.Values {
		s.Values[i], err = Node(val)
		if err != nil {
			return ast.Struct{}, err
		}
	}
	return s, nil
}

func Symbol(symbol ast.Symbol) (ast.Symbol, error) {
	if symbol.Name == "" {
		return ast.Symbol{}, symbol.C.Errorf("%w: .N is blank", errs.ErrASTInvalid)
	}
	if !lexer.SymbolPatternFull.MatchString("#" + symbol.Name) {
		return ast.Symbol{}, symbol.C.Errorf(
			"%w: .N contains invalid characters: %s",
			errs.ErrASTInvalid,
			strconv.Quote(symbol.Name),
		)
	}
	return symbol, nil
}
