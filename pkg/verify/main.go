package verify

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func FromConverted(p ast.Program, err error) (ast.Program, error) {
	if err != nil {
		return ast.Program{}, err
	}
	return Program(p)
}

func Program(p ast.Program) (ast.Program, error) {
	var err error
	p2 := ast.Program{
		C:       p.C,
		Content: []ast.Node{},
	}
	for _, node := range p.Content {
		node, err = Node(node)
		if err != nil {
			return ast.Program{}, err
		}
		p2.Content = append(p2.Content, node)
	}
	return p2, nil
}
