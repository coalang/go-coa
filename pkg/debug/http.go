package debug

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
	"html/template"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type HTTP struct {
	scope  *eval.Scope
	ctx    ast.Ctx
	Router *gin.Engine
	label  string
}

func NewHTTP(scope *eval.Scope, ctx ast.Ctx, label string, quit chan struct{}) *HTTP {
	s := &HTTP{scope: scope, ctx: ctx, label: label}
	s.Router = gin.Default()
	tmpl := template.Must(template.New("1").Funcs(map[string]interface{}{
		"check": func(x bool, names ...string) template.HTML {
			nameYes, nameNo := "Yes", "No"
			switch len(names) {
			case 0:
			case 1:
				nameYes = names[0]
			case 2:
				nameYes = names[0]
				nameNo = names[1]
			}
			if x {
				return template.HTML(`<span class="w3-text-green">✓ ` + nameYes + `</span>`)
			} else {
				return template.HTML(`<span class="w3-text-red">✗ ` + nameNo + `</span>`)
			}
		},
		"renderCoa": render.Node,
		//"renderHtml": html.Node,
		"escape": url.PathEscape,
		"time": func() string {
			return time.Now().UTC().String()
		},
		"subNodes":       s.subNodes,
		"subnodes":       s.subNodes,
		"renderRefComma": s.refComma,
		"renderRefIDs": func(ref ast.Ref) []string {
			//return []string{s.refComma(ref)}
			return strings.Split(s.refComma(ast.Ref{IDs: append(ref.IDs, "")}), ".")
		},
		"nn": func(x interface{}) bool {
			return x != nil
		},
		"sub1": func(x int) int {
			return x - 1
		},
	}).ParseGlob("tmpl/*.html"))
	s.Router.SetHTMLTemplate(tmpl)
	s.Router.Static("/static", "static")
	s.Router.GET("/resume", func(c *gin.Context) {
		quit <- struct{}{}
		c.Redirect(http.StatusTemporaryRedirect, "/ref")
	})
	s.Router.GET("/ref/:ref", s.getIndex)
	s.Router.GET("/ref", s.getIndex)
	s.Router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusPermanentRedirect, "/ref")
	})
	return s
}

func newHTTPCtx(c *gin.Context) ast.Ctx {
	return ast.Ctx{
		Filepath: c.Request.Method + " " + c.Request.URL.String() + "_debug",
	}
}

func (s *HTTP) getIndex(c *gin.Context) {
	var node ast.Node
	var err error

	if strings.HasSuffix(c.Param("ref"), ".") {
		c.Redirect(http.StatusPermanentRedirect, c.Param("ref")[:len(c.Param("ref"))-1])
	}

	status := http.StatusOK
	ref := ast.Ref{
		C:   newHTTPCtx(c),
		IDs: strings.Split(c.Param("ref"), "."),
	}
	root := c.Param("ref") == ""

	if !root {
		node, err = s.scope.Ref(ref)
		if err != nil {
			status = http.StatusInternalServerError
			if errors.Is(err, errs.ErrNotFound) {
				status = http.StatusNotFound
			}
		}
	} else {
		ref.IDs = []ast.ID{}
	}

	c.HTML(status, "ref.html", gin.H{
		"status":    status,
		"ref":       ref,
		"prefixRef": ast.Ref{IDs: append(ref.IDs, "")},
		"node":      node,
		"err":       err,
		"root":      root,
		"scope":     s.scope,
		"ctx":       s.ctx,
		"label":     s.label,
	})
}

func (*HTTP) refComma(ref ast.Ref) string {
	re := strings.Join(ref.IDs, ".")
	if strings.HasPrefix(re, "...") {
		return ast.IDIndex + re[2:]
	}
	if strings.HasPrefix(re, "..") {
		return ast.IDCore + re[1:]
	}
	if len(re) > 1 && strings.HasPrefix(re, ".") {
		return ast.IDSelf + re
	}
	return re
}

func (s *HTTP) subNodes(node_ interface{}) map[string]ast.Node {
	subNodes := map[ast.ID]ast.Node{}
	if node_ == nil {
		for _, key := range s.scope.AllKeys() {
			val, ok := s.scope.Get(key)
			if !ok {
				panic("s.scope.Get(...) contradicted s.scope.OwnKeys()")
			}
			subNodes[key] = val
		}
		return subNodes
	} else {
		node := node_.(ast.Node)
		switch node := node.(type) {
		case ast.Pod:
			switch inside := node.Inside.(type) {
			case ast.Struct:
				for i, key := range inside.T.Keys {
					subNodes[key] = inside.Values[i]
				}
			}
		case ast.Struct:
			for i, key := range node.T.Keys {
				subNodes[key] = ast.Cast{
					C:  node.Values[i].Ctx(),
					Ee: node.Values[i],
					Er: node.T.Values[i],
				}
			}
		case ast.StructType:
			for i, key := range node.Keys {
				subNodes[key] = ast.Cast{
					C:  node.Defaults[i].Ctx(),
					Ee: node.Defaults[i],
					Er: node.Values[i],
				}
			}
		}
		return subNodes
	}
}
