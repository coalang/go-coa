package scope

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/lib"
	"io/fs"
)

// NewScope makes a new scope.
func NewScope(c ast.Ctx, fs fs.FS, pure ast.Pureness, disallowedRefs [][]ast.ID) (*eval.Scope, error) {
	s := eval.NewScope(c, bool(pure), disallowedRefs, fs)
	lib.ApplyNatives(s)
	return s, nil
}

// NewScopeWithStd makes a new scope with builtins, core and index.
func NewScopeWithStd(c ast.Ctx, fs fs.FS, pure ast.Pureness, disallowedRefs [][]ast.ID) (*eval.Scope, error) {
	s, err := NewScope(c, fs, pure, disallowedRefs)
	if err != nil {
		return nil, err
	}
	err = lib.ApplyBase(s)
	if err != nil {
		return nil, err
	}
	err = s.LoadBase(BasePath)
	if err != nil {
		return nil, err
	}
	err = s.LoadCore(CorePath)
	if err != nil {
		return nil, err
	}
	err = s.LoadIndex(IndexPath)
	if err != nil {
		return nil, err
	}
	return s, nil
}
