package osfs

import (
	"fmt"
	"io/fs"
	"os"
)

//goland:noinspection GoUnusedGlobalVariable
var OSFS osFS

type osFS struct{}

var _ fs.ReadDirFS = osFS{}
var _ fs.ReadFileFS = osFS{}
var _ fmt.Stringer = osFS{}

func (r osFS) String() string {
	return "osFS"
}

func (r osFS) Open(name string) (fs.File, error) {
	return os.Open(name)
}

func (r osFS) ReadDir(name string) ([]fs.DirEntry, error) {
	return os.ReadDir(name)
}

func (r osFS) ReadFile(name string) ([]byte, error) {
	return os.ReadFile(name)
}
