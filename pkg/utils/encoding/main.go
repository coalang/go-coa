package encoding

import (
	"bytes"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/convert"
	"gitlab.com/coalang/go-coa/pkg/encoding"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer/parser"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils/path"
	"gitlab.com/coalang/go-coa/pkg/verify"
)

func BytesEncodeProgram(enc path.Enc, program ast.Program) ([]byte, error) {
	switch enc {
	case path.Source:
		return []byte(render.Program(program)), nil
	case path.Gob:
		return encoding.GobEncodeProgram(program)
	case path.JSON:
		return encoding.JSONEncodeProgram(program)
	default:
		return nil, program.C.Errorf("enc %w", errs.ErrInvalid)
	}
}

func BytesDecodeProgram(p string, data []byte) (ast.Program, error) {
	switch path.GetEnc(p) {
	case path.Source:
		return verify.FromConverted(convert.FromParsed(parser.Parse(p, bytes.NewBuffer(data))))
	case path.Gob:
		return encoding.GobDecodeProgram(data)
	case path.JSON:
		return encoding.JSONDecodeProgram(data)
	default:
		return ast.Program{}, fmt.Errorf("enc %w", errs.ErrInvalid)
	}
}
