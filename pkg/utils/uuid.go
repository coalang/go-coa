package utils

import (
	"encoding/hex"
	"github.com/google/uuid"
	"strings"
)

func UUIDFromMust(id string) uuid.UUID {
	re, err := UUIDFrom(id)
	if err != nil {
		panic(err)
	}
	return re
}

func UUIDFrom(id string) (uuid.UUID, error) {
	decoded, err := hex.DecodeString(strings.ReplaceAll(id, "-", ""))
	if err != nil {
		return [16]byte{}, err
	}
	uuid_, err := uuid.FromBytes(decoded)
	if err != nil {
		return [16]byte{}, err
	}
	return uuid_, nil
}
