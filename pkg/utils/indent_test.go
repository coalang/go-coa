package utils

import (
	"fmt"
	"testing"
)

func TestIndent(t *testing.T) {
	type test struct {
		Input, Want string
	}
	tests := map[string]test{
		"sanity":       {"abc", fmt.Sprintf("%sabc", IndentString)},
		"newline/lf":   {"abc\ndef", fmt.Sprintf("%sabc\n%sdef", IndentString, IndentString)},
		"newline/crlf": {"abc\r\ndef", fmt.Sprintf("%sabc\n%sdef", IndentString, IndentString)},
		"newline/cr":   {"abc\rdef", fmt.Sprintf("%sabc\n%sdef", IndentString, IndentString)},
	}
	for key, test := range tests {
		t.Run(key, func(t *testing.T) {
			if got := Indent(test.Input); got != test.Want {
				t.Errorf(
					`want %s got %s`,
					test.Want,
					got,
				)
			}
		})
	}
}
