package utils

import "strings"

const IndentString = "\t"

func Indent(s string) string {
	onlyLf := strings.ReplaceAll(strings.ReplaceAll(s, "\r\n", "\n"), "\r", "\n")
	return IndentString + strings.ReplaceAll(onlyLf, "\n", "\n"+IndentString)
}
