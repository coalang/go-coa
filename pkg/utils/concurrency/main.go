//+build !debug

package concurrency

import (
	"context"
	"runtime/pprof"
)

func Do(_ pprof.LabelSet, f func(context.Context)) {
	f(context.Background())
}
