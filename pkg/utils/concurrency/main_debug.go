//+build debug

package concurrency

import (
	"context"
	"runtime/pprof"
)

func Do(labels pprof.LabelSet, f func(context.Context)) {
	pprof.Do(context.Background(), labels, f)
}
