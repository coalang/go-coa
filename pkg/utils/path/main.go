package path

import (
	"gitlab.com/coalang/go-coa/pkg/encoding"
	"path/filepath"
	"regexp"
	"strconv"
)

type Type struct {
	Enc
	Status
}

func (t Type) String() string {
	return t.Status.String() + " " + t.Enc.String()
}

type Enc uint8

func (s Enc) String() string {
	switch s {
	case Source:
		return "source"
	case Gob:
		return "gob"
	case JSON:
		return "JSON"
	default:
		return strconv.FormatUint(uint64(s), 10)
	}
}

type Status uint8

func (s Status) String() string {
	switch s {
	case Unknown:
		return "unknown"
	case Any:
		return "any"
	case ImpureOnly:
		return "impure only"
	default:
		return strconv.FormatUint(uint64(s), 10)
	}
}

const (
	Source Enc = iota
	Gob
	JSON
	Protobuf

	Unknown Status = iota
	Any
	ImpureOnly
)

func GetEnc(p string) Enc {
	switch filepath.Ext(p) {
	case encoding.ExtCoa:
		return Source
	case encoding.ExtGob:
		return Gob
	case encoding.ExtJSON:
		return JSON
	default:
		return Source
	}
}

var compiledPattern = regexp.MustCompile(`^(.*)\.c\.coa.*$`)
var anyPattern = regexp.MustCompile(`^(.*)\.coa.*$`)

func GetStatus(p string) Status {
	switch {
	case compiledPattern.MatchString(p):
		return ImpureOnly
	case anyPattern.MatchString(p):
		return Any
	default:
		return Unknown
	}
}

func ToAny(p string) string {
	if GetStatus(p) == ImpureOnly {
		return compiledPattern.ReplaceAllString(p, "$1.coa")
	}
	return p
}

func ToCompiled(p string) string {
	if GetStatus(p) != ImpureOnly {
		return anyPattern.ReplaceAllString(p, "$1.c.coa")
	}
	return p
}

func GetType(p string) Type {
	return Type{
		Enc:    GetEnc(p),
		Status: GetStatus(p),
	}
}
