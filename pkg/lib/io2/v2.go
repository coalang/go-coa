package io2

import (
	"bufio"
	"context"
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"gitlab.com/coalang/go-coa/pkg/utils/concurrency"
	"os"
	"runtime/pprof"
	"sync"
)

type fileStream struct {
	file   *os.File
	reader *bufio.Reader
	lock   sync.Mutex
	ch     chan rune
}

var _ eval.NativeStream = &fileStream{}

func newFileStream(file *os.File) *fileStream {
	f := &fileStream{
		file:   file,
		reader: bufio.NewReader(file),
		ch:     make(chan rune, 1024),
	}
	go concurrency.Do(pprof.Labels("ctx", ast.CtxFromCaller(1).String()), func(_ context.Context) {
		f.handleCh()
	})
	return f
}

func (s *fileStream) Close() {
	s.lock.Lock()
	defer s.lock.Unlock()
	err := s.file.Close()
	if err != nil {
		panic(err)
	}
}

func (s *fileStream) IsClosed() bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	return false
}

func (s *fileStream) Send(node ast.Node) {
	s.ch <- node.(ast.Rune).R
}

func (s *fileStream) handleCh() {
	for r := range s.ch {
		_, err := s.file.WriteString(string(r))
		if err != nil {
			panic(err)
		}
	}
}

var receiveCtx ast.Ctx

func (s *fileStream) Receive() ast.Node {
	if receiveCtx.IsZero() {
		receiveCtx = ast.CtxFromCaller(1)
	}
	if s.reader == nil {
		panic("cannot receive")
	}
	r, _, err := s.reader.ReadRune()
	if err != nil {
		panic(err)
	}
	return ast.Rune{
		C: receiveCtx,
		R: r,
	}
}

var (
	StdoutID = utils.UUIDFromMust("e94ffdd4-c554-4cd4-8869-cf927a47a3d7")
	StderrID = utils.UUIDFromMust("699d533d-d79e-4d49-83cf-3a6f71ec2eaf")
	StdinID  = utils.UUIDFromMust("a3a33294-d8fb-4ef1-9df8-72c23fef381e")

	stdout = newFileStream(os.Stdout)
	stderr = newFileStream(os.Stderr)
	stdin  = newFileStream(os.Stdin)

	Stdout = newStreamFromFile(StdoutID, ast.NewBuiltinCtx("stdout"), ast.StreamSink)
	Stderr = newStreamFromFile(StderrID, ast.NewBuiltinCtx("stderr"), ast.StreamSink)
	Stdin  = newStreamFromFile(StdinID, ast.NewBuiltinCtx("stdin"), ast.StreamSource)
)

func Apply(s *eval.Scope) {
	s.NativeStreams.Set(StdoutID, stdout)
	s.NativeStreams.Set(StderrID, stderr)
	s.NativeStreams.Set(StdinID, stdin)
	s.Externals.Set(StdoutID, Stdout)
	s.Externals.Set(StderrID, Stderr)
	s.Externals.Set(StdinID, Stdin)
}

func newStreamFromFile(id uuid.UUID, c ast.Ctx, dir ast.StreamDirection) ast.Stream {
	return ast.Stream{
		C: c,
		T: ast.StreamType{
			C:         c,
			Value:     ast.RuneType{},
			Direction: dir,
		},
		ID: id,
		P:  false,
	}
}
