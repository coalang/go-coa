package io

import (
	"bufio"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"io"
	"os"
)

var (
	OutID   = utils.UUIDFromMust("d01efd39-b9b0-403b-a28b-511ba060c596")
	OutlnID = utils.UUIDFromMust("75c298f2-0b12-4e1e-afda-c73fede8efe6")
	InlnID  = utils.UUIDFromMust("ebf7b271-a712-48c5-b3cb-66996e9a1cd7")
)

// OutImpl provides the skeleton implementation for out (root scope).
func OutImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	var err error
	switch self := c.Self.(type) {
	case ast.Map:
		if !self.T.IsRuneMap() {
			// NOTE: workaround until implicit conversion
			return OutImpl(nil, ast.Call{
				C:    c.C,
				Self: ast.NewRuneMap(ast.Ctx{}, render.Node(self)),
			})
		}

		text, err := self.RuneMap()
		if err != nil {
			return nil, err
		}
		_, err = os.Stdout.WriteString(text)
		return nil, err
	case nil:
		_, err := os.Stdout.WriteString("nil")
		return nil, err
	default:
		_, err = os.Stdout.WriteString(render.Node(self))
		return nil, err
	}
}

// OutlnImpl provides the skeleton implementation for out (root scope).
func OutlnImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	re, err := OutImpl(nil, c)
	if err != nil {
		return nil, err
	}
	_, err = os.Stdout.WriteString("\n")
	return re, err
}

// InlnImpl provides the skeleton implementation for inln (root scope).
func InlnImpl(_ *eval.Scope, _ ast.Call) (ast.Node, error) {
	//log.Println("waiting for input")
	//debug.PrintStack()
	reader := bufio.NewReader(os.Stdin)
	line, _, err := reader.ReadLine()
	if err != nil && err != io.EOF {
		return nil, err
	}
	if err != nil && err == io.EOF {
		line = []byte("testing mode")
	}

	return ast.NewRuneMap(ast.Ctx{}, string(line)), nil
}
