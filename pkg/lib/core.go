package lib

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
)

func TypeImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	return ast.ToType(c.Self), nil
}

// MatchImpl implements the "match" builtin function/block.
func MatchImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	cases := c.Arg.(ast.Map)

	all := fmt.Sprintf("%d option(s)\nagainst\t%s\n", len(cases.Keys), render.Node(c.Self))
	for i, key := range cases.Keys {
		all += fmt.Sprintf("%d\t%s", i, render.Node(key))
		if i != len(cases.Keys)-1 {
			all += "\n"
		}
		switch key := key.(type) {
		case nil:
			return cases.Values[i], nil
		case ast.Block:
			re, err := s.CallCall(ast.Call{
				C:    ast.NewBuiltinCtx("MatchImpl/Value"),
				Ee:   key,
				Self: c.Self,
				T: ast.BlockType{
					Self: c.Self.Type(),
					Arg:  ast.AnyType{},
					Re:   ast.AnyType{},
				},
			})
			if err != nil {
				return nil, err
			}

			if ast.EqualHash(re, ast.BoolTrue) {
				return cases.Values[i], nil
			}
		default:
			if ast.EqualHash(c.Self, key) {
				return cases.Values[i], nil
			}
		}
	}
	return nil, c.C.Errorf("match not found:\n%s", all)
}
