package lib

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/arithmetic"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/coerce"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/each"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/fold"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/insert"
	int2 "gitlab.com/coalang/go-coa/pkg/lib/builtin/int"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/next"
	select_ "gitlab.com/coalang/go-coa/pkg/lib/builtin/select"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/split"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/stream"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/until"
)

func nativeFromUUID(id uuid.UUID, pure ast.Pureness) ast.Native {
	return ast.Native{
		P:  pure,
		ID: id,
	}
}

// TODO: make base a pod (base/...)

var base = map[ast.ID]ast.Node{
	"+":  arithmetic.NewAddImpl(),
	"-":  nativeFromUUID(arithmetic.SubNumID, true),
	"*":  nativeFromUUID(arithmetic.MulNumID, true),
	"/":  nativeFromUUID(arithmetic.DivNumID, true),
	"%":  nativeFromUUID(arithmetic.ModNumID, true),
	"**": nativeFromUUID(arithmetic.PowNumID, true),
	"//": nativeFromUUID(arithmetic.RootNumID, true),

	">": nativeFromUUID(GTNumID, true),
	"<": nativeFromUUID(LTNumID, true),

	"&&": nativeFromUUID(AndID, true),
	"||": nativeFromUUID(OrID, true),

	"=": nativeFromUUID(EqID, true),

	"型":      ast.Ref{IDs: []ast.ID{"type"}},
	"type":   nativeFromUUID(TypeID, true),
	"なら":     ast.Ref{IDs: []ast.ID{"match"}},
	"match":  nativeFromUUID(MatchID, true),
	"一時停止":   ast.Ref{IDs: []ast.ID{"debug"}},
	"debug":  nativeFromUUID(DebugID, true),
	"べき":     ast.Ref{IDs: []ast.ID{"assert"}},
	"assert": nativeFromUUID(AssertID, true),

	"それぞれ":  ast.Ref{IDs: []ast.ID{"each"}},
	"each":  nativeFromUUID(each.ID, true),
	"から":    ast.Ref{IDs: []ast.ID{"range"}},
	"range": nativeFromUUID(RangeID, true),
	"畳む":    ast.Ref{IDs: []ast.ID{"fold"}},
	"fold":  nativeFromUUID(fold.CommutativeID, true),
	//"畳む":    ast.Ref{IDs: []ast.ID{"foldr"}},
	"foldr": nativeFromUUID(fold.CommutativeID, true),
	//"畳む":    ast.Ref{IDs: []ast.ID{"foldl"}},
	"foldl": nativeFromUUID(fold.CommutativeID, true),

	//"→": nativeFromUUID(stream.ArrowID, true),
	"→": ast.NewDispatcher2(ast.NewBuiltinCtx("→"), []ast.Callable{
		ast.CallableNative(ast.Native{
			C: ast.NewBuiltinCtx("arrow"),
			T: ast.BlockType{
				C:    ast.NewBuiltinCtx("arrow"),
				Self: ast.StreamType{Value: ast.AnyTypeGlobal, Direction: ast.StreamSource},
				Re:   ast.StreamType{Value: ast.AnyTypeGlobal, Direction: ast.StreamSink},
				Arg:  ast.StreamType{Value: ast.AnyTypeGlobal, Direction: ast.StreamSource},
			},
			P:  true,
			ID: stream.ArrowID,
		}),
	}),

	"until":  ast.CallableNative(until.Native),
	"split":  ast.CallableNative(split.Native),
	"select": ast.CallableNative(select_.Native),
	"insert": ast.CallableNative(insert.Native),

	"int": ast.CallableNative(int2.Native),

	"next": ast.NewDispatcher2(ast.NewBuiltinCtx("next"), []ast.Callable{
		ast.CallableNative(next.StreamNative),
		ast.CallableNative(next.MapNative),
	}),

	"nan": ast.NewNumNaN(ast.NewBuiltinCtx("NaN")),

	"変換": ast.Ref{IDs: []ast.ID{"coerce"}},
	"coerce": ast.NewDispatcher2(ast.NewBuiltinCtx("coerce"), []ast.Callable{
		ast.CallableNative(coerce.MapStreamNative),
	}),

	"any":         ast.AnyType{},
	"block":       ast.AnyBlockType(),
	"bool":        ast.Bool,
	"true":        ast.BoolTrue,
	"false":       ast.BoolFalse,
	"dispatcher2": ast.DispatcherType2{},
	"inter":       ast.Inter{},
	"map":         ast.AnyMapType(),
	"text":        ast.NewRuneMapType(),
	"native":      ast.NativeType{},
	"external":    ast.ExternalType{},
	"num":         ast.NumType{},
	"pod":         ast.PodType{},
	"root":        ast.RootType{},
	"rune":        ast.MapType{},
	"struct":      ast.StructType{},
	"stream": ast.Inter{T: ast.NewStructType(ast.NewBuiltinCtx("stream"), ast.StructTypeSub{
		Key: "receive",
		Typ: ast.BlockType{
			C:  ast.NewBuiltinCtx("stream/receive"),
			Re: ast.AnyTypeGlobal,
		},
	}, ast.StructTypeSub{
		Key: "send",
		Typ: ast.BlockType{
			C:    ast.NewBuiltinCtx("stream/send"),
			Self: ast.AnyTypeGlobal,
		},
	})},
	"unique": ast.SymbolType{},
}

func ApplyBase(s *eval.Scope) error {

	for key, val := range base {
		err := s.Set(key, val)
		if err != nil {
			return err
		}
	}
	return nil
}
