package lib

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/debug"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/arithmetic"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/coerce"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/each"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/fold"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/insert"
	int2 "gitlab.com/coalang/go-coa/pkg/lib/builtin/int"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/next"
	select_ "gitlab.com/coalang/go-coa/pkg/lib/builtin/select"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/split"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/stream"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/until"
	"gitlab.com/coalang/go-coa/pkg/lib/io"
	"gitlab.com/coalang/go-coa/pkg/lib/io2"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"log"
	"math/big"
	"net/http"
	"os"
	"time"

	"gitlab.com/coalang/go-coa/pkg/ast"
)

var (
	GTNumID = utils.UUIDFromMust("e74609be-94e1-4f8a-ab5d-b95dea6bdffc")
	LTNumID = utils.UUIDFromMust("24c644c7-a628-4512-bd2e-6dd6c79b7881")
	AndID   = utils.UUIDFromMust("1cabe4c0-57f9-4cec-b545-891643c691c2")
	OrID    = utils.UUIDFromMust("4a1277bb-5bf9-44a7-bd0b-c9583089097f")
	EqID    = utils.UUIDFromMust("9ae44c09-9096-4ee6-99cc-cd4c03ab0962")

	TypeID   = utils.UUIDFromMust("37255a68-7f69-491f-866b-e9fb7ae72ced")
	MatchID  = utils.UUIDFromMust("840214d5-d45c-41aa-b58c-164f4f75dd2e")
	DebugID  = utils.UUIDFromMust("26c975f1-8ba9-4e02-b820-1539e13e15d9")
	AssertID = utils.UUIDFromMust("3ddbc603-e19f-42ea-88dd-1daa41b1d5a6")

	RangeID = utils.UUIDFromMust("2f7b976e-c866-47d4-a8f4-a97bdb17a189")
)

func NewNative(id uuid.UUID, pure ast.Pureness, ctx ast.Ctx, self, arg, re ast.Type) ast.Native {
	return ast.Native{
		C: ctx,
		T: ast.BlockType{
			C:    ctx,
			Self: self,
			Re:   re,
			Arg:  arg,
		},
		P:  pure,
		ID: id,
	}
}

func applyNative(s *eval.Scope, n ast.Native, impl eval.NativeImpl) {
	s.Natives.Set(n.ID, n, impl)
}

func ApplyNatives(s *eval.Scope) {
	arithmetic.Apply(s)

	applyNative(s, NewNative(GTNumID, true, ast.NewBuiltinCtx("GTNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.Bool.T), GTNumImpl)
	applyNative(s, NewNative(LTNumID, true, ast.NewBuiltinCtx("LTNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.Bool.T), LTNumImpl)
	applyNative(s, NewNative(AndID, true, ast.NewBuiltinCtx("And"), ast.Bool.T, ast.Bool.T, ast.Bool.T), AndImpl)
	applyNative(s, NewNative(OrID, true, ast.NewBuiltinCtx("Or"), ast.Bool.T, ast.Bool.T, ast.Bool.T), OrImpl)
	applyNative(s, NewNative(OrID, true, ast.NewBuiltinCtx("Or"), ast.AnyTypeGlobal, ast.AnyTypeGlobal, ast.Bool.T), EqImpl)

	applyNative(s, NewNative(TypeID, true, ast.NewBuiltinCtx("Type"), ast.AnyTypeGlobal, ast.AnyTypeGlobal, ast.Bool.T), TypeImpl)
	applyNative(s, NewNative(MatchID, true, ast.NewBuiltinCtx("Match"), ast.AnyTypeGlobal, ast.AnyMapType(), ast.AnyTypeGlobal), MatchImpl)
	applyNative(s, NewNative(AssertID, true, ast.NewBuiltinCtx("Assert"), ast.Bool.T, nil, nil), AssertImpl)
	applyNative(s, NewNative(DebugID, true, ast.NewBuiltinCtx("Debug"), ast.NewRuneMapType(), nil, nil), DebugImpl)

	//applyNative(s, NewNative(each.MapID, true, ast.NewBuiltinCtx("EachMap"), ast.AnyMapType(), ast.AnyTypeGlobal, ast.AnyMapType()), each.MapImpl)
	applyNative(s, each.MapNative, each.MapImpl)
	applyNative(s, each.Native, each.Impl)
	applyNative(s, next.StreamNative, next.StreamImpl)
	applyNative(s, next.MapNative, next.MapImpl)
	applyNative(s, coerce.MapStreamNative, coerce.MapStreamImpl)
	applyNative(s, NewNative(RangeID, true, ast.NewBuiltinCtx("Range"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.MapType{
		Key:   ast.AnyType{},
		Value: ast.NumType{},
	}), RangeImpl)

	applyNative(s, until.Native, until.Impl)
	applyNative(s, split.Native, split.Impl)
	applyNative(s, select_.Native, select_.Impl)
	applyNative(s, insert.Native, insert.Impl)

	applyNative(s, int2.Native, int2.Impl)

	// pkg/lib/builtin/stream
	applyNative(s, NewNative(stream.ArrowID, true, ast.NewBuiltinCtx("Arrow"), ast.AnyStreamType(), ast.AnyStreamType(), ast.AnyStreamType()), stream.ArrowImpl)
	applyNative(s, NewNative(stream.ArrowMapID, true, ast.NewBuiltinCtx("ArrowMap"), ast.AnyMapType(), ast.AnyStreamType(), ast.AnyStreamType()), stream.ArrowMapImpl)

	// pkg/lib/builtin/fold
	applyNative(s, NewNative(fold.CommutativeID, true, ast.NewBuiltinCtx("fold"), ast.AnyMapType(), ast.AnyTypeGlobal, ast.AnyMapType()), fold.CommutativeImpl)
	applyNative(s, NewNative(fold.RightID, true, ast.NewBuiltinCtx("foldRight"), ast.AnyMapType(), ast.AnyTypeGlobal, ast.AnyMapType()), fold.RightImpl)
	applyNative(s, NewNative(fold.LeftID, true, ast.NewBuiltinCtx("foldLeft"), ast.AnyMapType(), ast.AnyTypeGlobal, ast.AnyMapType()), fold.LeftImpl)

	// pkg/io
	applyNative(s, NewNative(io.OutlnID, false, ast.NewBuiltinCtx("io.OutlnImpl"), ast.AnyType{}, nil, nil), io.OutlnImpl)
	applyNative(s, NewNative(io.OutID, false, ast.NewBuiltinCtx("io.Out"), ast.AnyType{}, nil, nil), io.OutImpl)
	applyNative(s, NewNative(io.InlnID, false, ast.NewBuiltinCtx("io.InlnImpl"), nil, nil, ast.NewRuneMapType()), io.InlnImpl)

	io2.Apply(s)
}

var (
	GTNumImpl = makeNumNumBoolOp(func(a *big.Rat, b *big.Rat) (bool, error) {
		return a.Cmp(b) == 1, nil
	})
	LTNumImpl = makeNumNumBoolOp(func(a *big.Rat, b *big.Rat) (bool, error) {
		return a.Cmp(b) == -1, nil
	})
	AndImpl = makeBinaryOp(func(a bool, b bool) (bool, error) {
		return a && b, nil
	})
	OrImpl = makeBinaryOp(func(a bool, b bool) (bool, error) {
		return a || b, nil
	})
)

func RangeImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	start, err := c.Self.(ast.Num).Rat()
	if err != nil {
		return nil, err
	}
	end, err := c.Arg.(ast.Num).Rat()
	if err != nil {
		return nil, err
	}
	step := big.NewRat(1, 1)
	re := ast.Map{
		C: c.C,
		T: ast.MapType{
			C:     c.Self.Ctx(),
			Value: ast.NumType{},
		},
		Values: []ast.Node{},
	}

	for i := start; i.Cmp(end) == -1; i.Add(i, step) {
		re.Values = append(re.Values, ast.NewNum(c.C, i.RatString()))
	}
	return re, nil
}

func EqImpl(_ *eval.Scope, call ast.Call) (ast.Node, error) {
	if ast.EqualHash(call.Self, call.Arg) {
		return ast.BoolTrue, nil
	} else {
		return ast.BoolFalse, nil
	}
}

func AssertImpl(_ *eval.Scope, call ast.Call) (ast.Node, error) {
	a, ok := ast.SymbolToBool(call.Self.(ast.Symbol))
	if !ok {
		return nil, errors.New("not a bool")
	}
	if a {
		return call.Self, nil
	} else {
		return nil, errors.New("assertion failed")
	}
}

var DebugWeb = true

func DebugImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	//goland:noinspection GoBoolExpressions
	if DebugWeb {
		return DebugWebImpl(s, c)
	} else {
		return DebugTtyImpl(s, c)
	}
}

func DebugWebImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	quit := make(chan struct{})
	httpServer := debug.NewHTTP(s, c.C, render.RuneMap(c.Self.(ast.Map)), quit)
	srv := &http.Server{
		Addr:    ":8080",
		Handler: httpServer.Router,
	}

	// Initializing the httpServer in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()

	<-quit

	// The context is used to inform the httpServer it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		return nil, err
	}
	return nil, nil
}

func DebugTtyImpl(s *eval.Scope, _ ast.Call) (ast.Node, error) {
	s.Logf("===DEBUG (level: %d)", s.StackLen())
	s.Logf(s.Stack().String())
	s.Logf(s.ContentInfo())
	fmt.Println("press enter to continue")
	reader := bufio.NewReader(os.Stdin)
	_, _, err := reader.ReadLine()
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func makeBinaryOp(opFunc func(bool, bool) (bool, error)) eval.NativeImpl {
	return func(s *eval.Scope, call ast.Call) (ast.Node, error) {
		a, ok := ast.SymbolToBool(call.Self.(ast.Symbol))
		if !ok {
			return nil, errors.New("not a bool")
		}
		b, ok := ast.SymbolToBool(call.Arg.(ast.Symbol))
		if !ok {
			return nil, errors.New("not a bool")
		}

		c, err := opFunc(a, b)
		if err != nil {
			return nil, err
		}
		if c {
			return ast.BoolTrue, nil
		} else {
			return ast.BoolFalse, nil
		}
	}
}

func makeNumNumBoolOp(opFunc func(*big.Rat, *big.Rat) (bool, error)) func(s *eval.Scope, c ast.Call) (ast.Node, error) {
	return func(s *eval.Scope, c ast.Call) (ast.Node, error) {
		a := c.Self.(ast.Num)
		b := c.Arg.(ast.Num)
		if a.Sub + b.Sub != ast.Normal {
			return ast.NewNumNaN(c.C), nil
		}

		aR, err := numToRat(a)
		if err != nil {
			return nil, err
		}
		bR, err := numToRat(b)
		if err != nil {
			return nil, err
		}
		re, err := opFunc(aR, bR)
		if err != nil {
			return nil, err
		}
		if re {
			return ast.BoolTrue, nil
		} else {
			return ast.BoolFalse, nil
		}
	}
}

func numToRat(num ast.Num) (real_ *big.Rat, err error) {
	var ok bool
	if num.Sub == ast.NaN {
		return nil, errors.New("NaN")
	}
	if num.Num == "" {
		num.Num = "0"
	}
	real_, ok = new(big.Rat).SetString(num.Num)
	if !ok {
		return nil, errors.New("Num.Num invalid")
	}
	return real_, nil
}
