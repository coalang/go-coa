package arithmetic

import (
	"errors"
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"math/big"
)

var (
	AddNumID  = utils.UUIDFromMust("4f92eb67-cd73-4726-9a68-dd564f7b9c4e")
	AddMapID  = utils.UUIDFromMust("d4f69919-e492-4a5b-8be4-0c2261a9cf3c")
	SubNumID  = utils.UUIDFromMust("d998c33f-93ae-4eed-894b-9024689c3895")
	MulNumID  = utils.UUIDFromMust("5a867598-f67d-4d86-9b02-832074d87118")
	DivNumID  = utils.UUIDFromMust("a5f028f8-5f95-4b8b-b195-6879fafed4b7")
	ModNumID  = utils.UUIDFromMust("cb5dc99e-887b-4a60-95be-d5d462cfc0d3")
	PowNumID  = utils.UUIDFromMust("2ce4c662-e679-47dc-b92b-2eb13004a3d7")
	RootNumID = utils.UUIDFromMust("e78161c6-941d-4193-8537-47b1e90d0811")
)

func Apply(s *eval.Scope) {
	s.RegisterNative(ast.NewNative(AddNumID, true, ast.NewBuiltinCtx("AddNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), AddNumImpl)
	s.RegisterNative(ast.NewNative(AddMapID, true, ast.NewBuiltinCtx("AddMap"), ast.AnyMapType(), ast.AnyMapType(), ast.AnyMapType()), addMapImpl)
	s.RegisterNative(ast.NewNative(SubNumID, true, ast.NewBuiltinCtx("SubNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), subNumImpl)
	s.RegisterNative(ast.NewNative(MulNumID, true, ast.NewBuiltinCtx("MulNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), mulNumImpl)
	s.RegisterNative(ast.NewNative(DivNumID, true, ast.NewBuiltinCtx("DivNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), divNumImpl)
	s.RegisterNative(ast.NewNative(ModNumID, true, ast.NewBuiltinCtx("ModNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), modNumImpl)
	s.RegisterNative(ast.NewNative(PowNumID, true, ast.NewBuiltinCtx("PowNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), powNumImpl)
	s.RegisterNative(ast.NewNative(RootNumID, true, ast.NewBuiltinCtx("RootNum"), ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), rootNumImpl)
}

func newBlock(t ast.BlockType, native ast.Native) ast.Block {
	return ast.Block{
		T:         t,
		DebugName: native.ID.String(),
		Content: []ast.Node{
			ast.Call{
				Ee:   native,
				Self: ast.NewRefSingleID(t.SelfName),
				Arg: ast.Ref{
					C:   ast.NewBuiltinCtx("newBlock/Arg2"),
					IDs: []ast.ID{t.ArgName},
				},
				T: ast.BlockType{
					Self: t.Self,
					Arg:  t.Arg,
					Re:   t.Re,
				},
			},
		},
	}
}

func newBlockType(self, re, arg ast.Type) ast.BlockType {
	return ast.BlockType{
		C:        ast.NewBuiltinCtx(fmt.Sprintf("newBlockType(%v, %v, %v)", self, re, arg)),
		Self:     self,
		SelfName: "x",
		Re:       re,
		Arg:      arg,
		ArgName:  "y",
	}
}

func NewAddImpl() ast.Dispatcher2 {
	return ast.NewDispatcher2(ast.NewBuiltinCtx("NewAddImpl"), []ast.Callable{
		ast.CallableBlock(newBlock(newBlockType(ast.NumTypeGlobal, ast.NumTypeGlobal, ast.NumTypeGlobal), ast.Native{ID: AddNumID, P: ast.Pure})),
		ast.CallableBlock(newBlock(newBlockType(ast.AnyMapType(), ast.AnyMapType(), ast.AnyMapType()), ast.Native{ID: AddMapID, P: ast.Pure})),
	})
}

func AddNumImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	// self + args
	a := c.Self.(ast.Num)
	b := c.Arg.(ast.Num)
	if a.Sub == ast.NaN || b.Sub == ast.NaN || a.Sub == ast.PInf || a.Sub == ast.NInf || b.Sub == ast.PInf || b.Sub == ast.NInf {
		return ast.NewNumNaN(c.C), nil
	}

	aR, err := numToRat(a)
	if err != nil {
		return nil, err
	}
	bR, err := numToRat(b)
	if err != nil {
		return nil, err
	}
	aR.Add(aR, bR)
	return ast.Num{
		Num: aR.RatString(),
	}, nil
}

func addMapImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	// self + args
	a := c.Self.(ast.Map)
	b := c.Arg.(ast.Map)

	if !a.T.Consecutive().CompatibleWith(b.T.Consecutive()) {
		// special case for better error messages
		return nil, s.Errorf(
			c.C,
			"cannot add maps %s %s",
			a.T.Consecutive(),
			b.T.Consecutive(),
		)
	}

	values := make([]ast.Node, len(a.Values)+len(b.Values))
	keys := make([]ast.Node, len(a.Values)+len(b.Values))

	for i, value := range a.Values {
		values[i] = value
	}
	for i, value := range b.Values {
		values[len(a.Values)+i] = value
	}

	// if consecutive, keys don't matter
	// if not consecutive, keys do matter
	if a.T.Consecutive().CompatibleWith(ast.NonConsecutive) {
		for i, key := range a.Values {
			keys[i] = key
		}
		for i, key := range b.Values {
			keys[len(a.Values)+i] = key
		}
	}

	merged, err := eval.Merge(a.T, b.T)
	if err != nil {
		return nil, err
	}
	return ast.Map{
		C:      c.C,
		T:      merged.(ast.MapType),
		Values: values,
		Keys:   keys,
	}, nil
}

var (
	subNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return a.Sub(a, b), false, nil
	})
	mulNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return a.Mul(a, b), false, nil
	})
	divNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		if b.Cmp(big.NewRat(0, 1)) == 0 {
			return nil, true, nil
		} else {
			return a.Mul(a, b.Inv(b)), false, nil
		}
	})
	modNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		if !a.IsInt() || !b.IsInt() {
			return nil, true, nil
		}
		{
			a, b := a.Num(), b.Num()
			r := new(big.Int)
			a.QuoRem(a, b, r)
			// FIXME: use QuoRem or DivMod?
			return new(big.Rat).SetInt(r), false, nil
		}
	})
	powNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return nil, false, errs.NewErrNotImplemented("pow")
	})
	rootNumImpl = makeOperators(func(a *big.Rat, b *big.Rat) (*big.Rat, bool, error) {
		return nil, false, errs.NewErrNotImplemented("root")
	})
)

func makeOperators(opFunc func(*big.Rat, *big.Rat) (*big.Rat, bool, error)) func(s *eval.Scope, c ast.Call) (ast.Node, error) {
	return func(s *eval.Scope, c ast.Call) (ast.Node, error) {
		a := c.Self.(ast.Num)
		b := c.Arg.(ast.Num)
		if a.Sub == ast.NaN || b.Sub == ast.NaN {
			return ast.NewNumNaN(c.C), nil
		}
		if a.Sub != ast.Normal || b.Sub != ast.Normal {
			panic("+inf and -inf not implemented")
		}

		aR, err := numToRat(a)
		if err != nil {
			return nil, err
		}
		bR, err := numToRat(b)
		if err != nil {
			return nil, err
		}
		cR, nan, err := opFunc(aR, bR)
		if err != nil {
			return nil, err
		}
		if nan {
			return ast.NewNumNaN(c.C), nil
		}
		return ast.Num{
			C:   c.C,
			Num: cR.RatString(),
		}, nil
	}
}

func numToRat(num ast.Num) (real_ *big.Rat, _ error) {
	var ok bool
	if num.Sub != ast.NaN {
		if num.Num == "" {
			num.Num = "0"
		}
		real_, ok = big.NewRat(1, 1).SetString(num.Num)
		if !ok {
			return nil, errors.New("Num.Num invalid")
		}
	}
	return real_, nil
}
