package until

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var Native = ast.Native{
	C: ast.NewBuiltinCtx("until"),
	T: ast.BlockType{
		C:    ast.NewBuiltinCtx("until"),
		Self: ast.AnyMapType(),
		Re:   ast.AnyMapType(),
		Arg:  ast.AnyTypeGlobal,
	},
	P:  true,
	ID: utils.UUIDFromMust("49298435-3a41-4b12-93fa-b6a0b7990dbb"),
}

func Impl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	self := c.Self.(ast.Map)
	until := c.Arg
	untilIndex := -1
	for i, node := range self.Values {
		if ast.EqualHash(node, until) {
			untilIndex = i
		}
	}
	if untilIndex == -1 {
		return self, nil
	}
	re := self.Clone().(ast.Map)
	re.Values = re.Values[:untilIndex]
	if len(re.Keys) > untilIndex {
		re.Keys = re.Keys[:untilIndex]
	}
	return re, nil
}
