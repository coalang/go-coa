package insert

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var Native = ast.Native{
	C: ast.NewBuiltinCtx("insert"),
	T: ast.BlockType{
		C:    ast.NewBuiltinCtx("insert"),
		Self: ast.AnyMapType(),
		Re:   ast.AnyMapType(),
		Arg:  ast.AnyTypeGlobal,
	},
	P:  true,
	ID: utils.UUIDFromMust("261dddf6-e3bd-4ef3-9a5d-002b9950fd07"),
}

func Impl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	self := c.Self.(ast.Map)
	insert := c.Arg
	re := self.Clone().(ast.Map)
	re.Keys = make([]ast.Node, 0, len(re.Keys)*2)
	for _, key := range self.Keys {
		re.Keys = append(re.Keys, key.Clone(), insert.Clone())
	}
	re.Values = make([]ast.Node, 0, len(re.Values)*2)
	for _, value := range self.Values {
		re.Values = append(re.Values, value.Clone(), insert.Clone())
	}
	return re, nil
}
