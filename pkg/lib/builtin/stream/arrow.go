package stream

import (
	"errors"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var ArrowID = utils.UUIDFromMust("22dc755f-5256-4e4d-9574-ea173b95c8c1")
var ArrowMapID = utils.UUIDFromMust("9e63bb44-0702-4a33-b0ad-c1daf2d90be3")

func ArrowImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	src := c.Self.(ast.Stream)
	dst := c.Arg.(ast.Stream)
	if src.T.Direction == ast.StreamSink {
		return nil, src.C.Errorf("src stream must be able to send")
	}
	if dst.T.Direction == ast.StreamSource {
		return nil, dst.C.Errorf("dst stream must be able to receive")
	}
	srcNS, ok := s.NativeStreams.Get(src.ID)
	if !ok {
		return nil, c.C.Errorf("src NS doesn't exist")
	}
	dstNS, ok := s.NativeStreams.Get(dst.ID)
	if !ok {
		return nil, c.C.Errorf("dst NS doesn't exist")
	}
	for !srcNS.IsClosed() || !dstNS.IsClosed() {
		dstNS.Send(srcNS.Receive())
	}
	return src, nil
}

func ArrowMapImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	src := c.Self.(ast.Map)
	dst := c.Arg.(ast.Stream)
	if dst.T.Direction == ast.StreamSource {
		return nil, errors.New("dst stream must be receive")
	}
	for _, value := range src.Values {
		s.NativeStreams.GetSingle(dst.ID).Send(value)
	}
	return src, nil
}
