package each

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
	concurrency2 "gitlab.com/coalang/go-coa/pkg/utils/concurrency"
	"runtime/pprof"
	"strconv"
	"sync"
)

var Iterable = ast.Inter{T: ast.NewStructType(ast.NewBuiltinCtx("iterable"), ast.StructTypeSub{
	Key: "next",
	Typ: ast.BlockType{
		Re: ast.AnyTypeGlobal,
	},
})}

var (
	MapID     = utils.UUIDFromMust("e69a13bb-7e32-4768-8e99-a0e70d76df9a")
	MapNative = ast.Native{
		C: ast.NewBuiltinCtx("each/map"),
		T: ast.BlockType{
			C: ast.NewBuiltinCtx("each/map"),
		},
		P:  true,
		ID: MapID,
	}

	ID     = utils.UUIDFromMust("c2b689af-2ae0-4eb0-98f8-e78cd2cc6f71")
	Native = ast.Native{
		C: ast.NewBuiltinCtx("each"),
		T: ast.BlockType{
			C:    ast.NewBuiltinCtx("each"),
			Self: ast.AnyTypeGlobal,
			Re:   ast.AnyMapType(),
			Arg:  ast.AnyBlockType(),
		},
		P:  true,
		ID: ID,
	}
)

func Impl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	switch c.Self.(type) {
	case ast.Map:
		return MapImpl(s, c)
	case ast.Stream:
		return StreamImpl(s, c)
	default:
		return nil, s.Errorf(c.C, "%w type %T %s", errs.ErrUnsupported, c.Self, render.Node(c.Self))
	}
}

type ChanStream struct {
	Ch     chan ast.Node
	closed bool
	Dir    ast.StreamDirection
	lock   sync.Mutex
}

func (c *ChanStream) Close() {
	close(c.Ch)
	c.lock.Lock()
	c.closed = true
	defer c.lock.Unlock()
}

func (c *ChanStream) IsClosed() bool {
	c.lock.Lock()
	defer c.lock.Unlock()
	return c.closed
}

func (c *ChanStream) Receive() ast.Node {
	if c.Dir == ast.StreamSink {
		panic("cannot receive from sink")
	}
	return <-c.Ch
}

func (c *ChanStream) Send(node ast.Node) {
	if c.Dir == ast.StreamSource {
		panic("cannot send to source")
	}
	c.Ch <- node
}

func StreamImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	src := c.Self.(ast.Stream)
	applier := c.Arg.(ast.Block)
	var err error

	concurrent := false
	if applier.IsPure() {
		concurrent = true
	}

	// get src
	srcNS, ok := s.NativeStreams.Get(src.ID)
	if !ok {
		return nil, s.Errorf(c.C, "src doesn't exist")
	}

	// make dst
	var dstID = uuid.New()
	dstNS := &ChanStream{
		Ch:  make(chan ast.Node),
		Dir: ast.StreamBoth,
	}
	s.NativeStreams.Set(dstID, dstNS)

	// stream
	var value ast.Node
	applierType := ast.BlockType{
		C:    ast.NewBuiltinCtx("each"),
		Self: src.T.Value,
		Re:   src.T.Value,
		Arg:  ast.NumType{},
	}

	f := func() {
		for i := int64(0); !srcNS.IsClosed() || !dstNS.IsClosed(); i++ {
			value, err = s.CallCall(ast.Call{
				C:    c.C,
				T:    applierType,
				Self: srcNS.Receive(),
				Ee:   applier,
				Arg:  ast.NewNum(c.C, strconv.FormatInt(i, 10)),
			})
			if err != nil {
				panic(err)
			}
			go dstNS.Send(value)
		}
	}

	if concurrent {
		go f()
	} else {
		f()
	}

	return ast.Stream{
		C:  c.C,
		T:  src.T,
		ID: dstID,
		P:  src.P,
	}, nil
}

func MapImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	appliees := c.Self.(ast.Map)
	applier := c.Arg.(ast.Block)
	var err error

	concurrent := false
	// TODO: workaround for ccc21j2
	//if applier.IsPure() {
	//	concurrent = true
	//}

	applierType := ast.BlockType{
		C:    ast.NewBuiltinCtx("each"),
		Self: appliees.T.Key,
		Re:   appliees.T.Value,
		Arg:  appliees.T.Value,
	}

	var wg sync.WaitGroup
	wg.Add(len(appliees.Values))
	errors := make([]error, len(appliees.Values))

	for i, val := range appliees.Values {
		func(i int, val ast.Node) {
			f := func(_ context.Context) {
				appliees.Values[i], err = s.CallCall(ast.Call{
					C:    c.C,
					T:    applierType,
					Self: val,
					Ee:   applier,
					Arg:  appliees.GetKey(i),
				})
				if err != nil {
					errors[i] = err
				}
				wg.Done()
			}
			if concurrent {
				go concurrency2.Do(pprof.Labels("index", strconv.FormatInt(int64(i), 10), "ctx", val.Ctx().String()), f)
			} else {
				f(nil)
			}
		}(i, val)
	}
	wg.Wait()
	for _, err := range errors {
		if err != nil {
			return nil, err
		}
	}
	return appliees, nil
}
