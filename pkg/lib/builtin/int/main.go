package int

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/errs"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"math/big"
)

var Native = ast.Native{
	C: ast.NewBuiltinCtx("int"),
	T: ast.BlockType{
		C:    ast.NewBuiltinCtx("int"),
		Self: ast.AnyTypeGlobal,
		Re:   ast.NumTypeGlobal,
	},
	P:  true,
	ID: utils.UUIDFromMust("fda04eee-c4d1-4442-a456-2e830662891f"),
}

func Impl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	var err error
	c.Self, err = s.Node(c.Self)
	if err != nil {
		return nil, err
	}
	switch self := c.Self.(type) {
	case ast.Map:
		if self.T.IsRuneMap() {
			runeMap, err := self.RuneMap()
			if err != nil {
				return nil, err
			}
			r, ok := new(big.Rat).SetString(runeMap)
			if !ok {
				return nil, fmt.Errorf("%w: not a number: %s", errs.ErrInvalid, runeMap)
			}
			return ast.Num{
				C:   c.C,
				Num: r.String(),
			}, nil
		}
	case ast.Num:
		return c.Self, nil
	}
	return nil, fmt.Errorf("unable to convert %s (%T) from %s to num", render.Type(ast.ToType(c.Self)), c.Self, c.Self.Ctx())
}
