package next

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/render"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var StreamID = utils.UUIDFromMust("ca4ce0d2-3ccc-49a5-8a04-351eb2a885dc")
var StreamNative = ast.Native{
	C: ast.NewBuiltinCtx("stream"),
	T: ast.BlockType{
		Self: ast.AnyStreamType(),
		Re:   ast.AnyTypeGlobal,
	},
	P:  true,
	ID: StreamID,
}

func StreamImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	src := c.Self.(ast.Stream)
	srcNS, ok := s.NativeStreams.Get(src.ID)
	if !ok {
		return nil, s.Errorf(c.C, "src NativeStream doesn't exist")
	}
	received := srcNS.Receive()
	coerced, ok, err := s.Coerce(received, src.T.Value)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, s.Errorf(c.C, "received unexpected node %s (wanted %s)", render.Node(received), render.Type(src.T.Value))
	}
	return coerced, nil
}
