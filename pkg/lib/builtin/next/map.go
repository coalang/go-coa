package next

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var MapID = utils.UUIDFromMust("ca4ce0d2-3ccc-49a5-8a04-351eb2a885dc")
var MapNative = ast.Native{
	C: ast.NewBuiltinCtx("map"),
	T: ast.BlockType{
		Self: ast.AnyMapType(),
		Re:   ast.AnyTypeGlobal,
	},
	P:  true,
	ID: MapID,
}

func MapImpl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	arg := c.Arg.(ast.Map)
	value := arg.Values[0]
	arg.Values = arg.Values[1:]
	arg.Keys = arg.Keys[1:]
	return value, nil
}
