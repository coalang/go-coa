package fold

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var (
	CommutativeID = utils.UUIDFromMust("14a9f07d-c2ff-4153-8e37-1aa10d1dd44b")
	LeftID        = utils.UUIDFromMust("f90b7e30-0a63-4c2f-ba31-f7f635bf595e")
	RightID       = utils.UUIDFromMust("dcc23f35-4652-40d3-9adf-6b450a14f143")

	CommutativeImpl = makeFoldImpl(commutative)
	LeftImpl        = makeFoldImpl(left)
	RightImpl       = makeFoldImpl(right)
)

type mode int

const (
	commutative mode = iota
	left
	right
)

type er func(*eval.Scope, ast.Ctx, ast.Map, ast.Node, ast.BlockType) (re ast.Node, err error)

type foldTwo func(ast.Node, ast.Node) (ast.Node, error)

func makeFoldTwo(s *eval.Scope, ctx ast.Ctx, applier ast.Node, applierType ast.BlockType) foldTwo {
	return func(a ast.Node, b ast.Node) (ast.Node, error) {
		return s.CallCall(ast.Call{
			C:    ctx,
			T:    applierType,
			Self: a,
			Ee:   applier,
			Arg:  b,
		})
	}
}

func erLeft(s *eval.Scope, ctx ast.Ctx, appliees ast.Map, applier ast.Node, applierType ast.BlockType) (re ast.Node, err error) {
	// ((a + b) + c) + d
	foldTwo := makeFoldTwo(s, ctx, applier, applierType)
	for i := range appliees.Values {
		val := appliees.Values[i]
		if i == 0 {
			re = val
		} else {
			re, err = foldTwo(re, val)
			if err != nil {
				return
			}
		}
	}
	return
}

func erRight(s *eval.Scope, ctx ast.Ctx, appliees ast.Map, applier ast.Node, applierType ast.BlockType) (re ast.Node, err error) {
	// a + (b + (c + d))
	foldTwo := makeFoldTwo(s, ctx, applier, applierType)
	lastIndex := len(appliees.Values) - 1
	for i := lastIndex; i > 0; i-- {
		val := appliees.Values[i]
		if i == lastIndex {
			re = val
		} else {
			re, err = foldTwo(val, re)
			if err != nil {
				return
			}
		}
	}
	return
}

func erCommutativeFold(foldTwo foldTwo, values []ast.Node, a, b int) (err error) {
	values[a], err = foldTwo(values[a], values[b])
	if err != nil {
		return
	}
	values[b] = nil
	return
}

func erCommutative(s *eval.Scope, ctx ast.Ctx, appliees ast.Map, applier ast.Node, applierType ast.BlockType) (value ast.Node, err error) {
	// (a + b) + (c + d)
	foldTwo := makeFoldTwo(s, ctx, applier, applierType)
	values := appliees.Values
	var valuesNew []ast.Node
	var indexes []int
	var i int
	// reduce like a tree
	// "a" "b" "c" "d" "e"
	// "a" "b" "c" "de"
	// "ab" nil "cde" nil
	// "ab" "cde"
	// "abcde" _
	// "abcde"
	if len(values)%2 == 1 && len(values) > 1 {
		err = erCommutativeFold(foldTwo, values, len(values)-2, len(values)-1)
		if err != nil {
			return
		}
		values = values[:len(values)-1]
	}

	for len(values) > 1 {
		for i = 0; i < len(values)-1; i += 2 {
			err = erCommutativeFold(foldTwo, values, i, i+1)
			if err != nil {
				return
			}
		}
		valuesNew = make([]ast.Node, 0, len(indexes))
		for i = 0; i < len(values)-1; i += 2 {
			valuesNew = append(valuesNew, values[i])
		}
		values = valuesNew
	}
	value = values[0]
	return
}

func makeFoldImpl(mode mode) eval.NativeImpl {
	var fold er
	switch mode {
	case commutative:
		fold = erCommutative
	case left:
		fold = erLeft
	case right:
		fold = erRight
	default:
		panic("makeFoldImpl mode is invalid")
	}

	return func(s *eval.Scope, c ast.Call) (ast.Node, error) {
		appliees := c.Self.(ast.Map)
		t, err := s.CommonType(appliees)
		if err != nil {
			return nil, err
		}
		appliees.T = t.(ast.MapType)
		applier, ok := c.Arg.(ast.Node)
		if !ok {
			return nil, s.Errorf(c.Arg.Ctx(), "not callable")
		}
		applierType := ast.BlockType{
			C:    c.C,
			Self: appliees.T.Value,
			Re:   appliees.T.Value,
			Arg:  appliees.T.Value,
		}

		re, err := fold(s, c.C, appliees, applier, applierType)
		if err != nil {
			return nil, err
		}

		return re, nil
	}
}
