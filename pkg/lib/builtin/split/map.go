package split

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var Native = ast.Native{
	C: ast.NewBuiltinCtx("split"),
	T: ast.BlockType{
		C:    ast.NewBuiltinCtx("split"),
		Self: ast.AnyMapType(),
		Re:   ast.AnyMapType(),
		Arg:  ast.AnyTypeGlobal,
	},
	P:  true,
	ID: utils.UUIDFromMust("266f5a4e-b6fd-4c04-b8ff-69f08157931c"),
}

func Impl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	self := c.Self.(ast.Map)
	split := c.Arg
	splitIndex := 0
	re := ast.Map{
		C: c.C,
		T: ast.MapType{
			C:     c.C,
			Value: ast.CloneType(self.T),
		},
		Values: []ast.Node{},
	}
	var keys, values []ast.Node
	var key, value ast.Node
	var j int
	for i, node := range self.Values {
		if ast.EqualHash(node, split) {
			if splitIndex < len(self.Keys) || i < len(self.Keys) {
				keys = self.Keys[splitIndex:i]
				for j, key = range keys {
					keys[j] = ast.Clone(key)
				}
			} else {
				keys = nil
			}
			values = self.Values[splitIndex:i]
			for j, value = range values {
				values[j] = ast.Clone(value)
			}
			re.Values = append(re.Values, ast.Map{
				C:      c.C,
				T:      ast.CloneType(self.T).(ast.MapType),
				Keys:   keys,
				Values: values,
			})
			splitIndex = i + 1
		}
	}
	if splitIndex == 0 {
		return self, nil
	} else {
		i := len(self.Values)
		if splitIndex < len(self.Keys) || i < len(self.Keys) {
			keys = self.Keys[splitIndex:i]
			for i, key = range keys {
				keys[i] = ast.Clone(key)
			}
		} else {
			keys = nil
		}
		values = self.Values[splitIndex:i]
		for i, value = range values {
			values[i] = ast.Clone(value)
		}
		re.Values = append(re.Values, ast.Map{
			C:      c.C,
			T:      ast.CloneType(self.T).(ast.MapType),
			Keys:   keys,
			Values: values,
		})
	}
	return re, nil
}
