package coerce

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/lib/builtin/each"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var MapStreamID = utils.UUIDFromMust("2065d4fd-ece3-49ff-9097-b14e546acf71")
var MapStreamNative = ast.Native{
	C: ast.NewBuiltinCtx("coerce/map_stream"),
	T: ast.BlockType{
		Self: ast.AnyMapType(),
		Re:   ast.AnyStreamType(),
	},
	P:  true,
	ID: MapStreamID,
}

func MapStreamImpl(s *eval.Scope, c ast.Call) (ast.Node, error) {
	self := c.Self.(ast.Map)
	dstNS := &each.ChanStream{
		Ch:  make(chan ast.Node, len(self.Values)),
		Dir: ast.StreamSource,
	}
	dstID := uuid.New()
	s.NativeStreams.Set(dstID, dstNS)
	for _, node := range self.Values {
		dstNS.Ch <- node
	}
	return ast.Stream{
		C: c.C,
		T: ast.StreamType{
			C:         c.C,
			Value:     self.T.Value,
			Direction: ast.StreamSource,
		},
		ID: dstID,
		P:  true,
	}, nil
}
