package select_

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

var Native = ast.Native{
	C: ast.NewBuiltinCtx("select_"),
	T: ast.BlockType{
		C:    ast.NewBuiltinCtx("select_"),
		Self: ast.AnyMapType(),
		Re:   ast.AnyMapType(),
		Arg:  ast.NumTypeGlobal,
	},
	P:  true,
	ID: utils.UUIDFromMust("c477de87-42da-4141-a70f-ab26ea47b0bf"),
}

func Impl(_ *eval.Scope, c ast.Call) (ast.Node, error) {
	self := c.Self.(ast.Map)
	index, err := c.Arg.(ast.Num).Rat()
	if err != nil {
		return nil, err
	}
	if !index.IsInt() {
		return nil, fmt.Errorf("select index must be integer, not %s", index)
	}
	i := index.Num().Int64()
	if l := len(self.Values); int(i) >= l {
		return nil, fmt.Errorf("index %d out of range for length of %d", i, l)
	}
	return self.Values[i], nil
}
