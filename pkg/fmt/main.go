package fmt

import (
	"bytes"
	"gitlab.com/coalang/go-coa/pkg/parser_lexer"
	"gitlab.com/coalang/go-coa/pkg/render"
	"io"
)

func Reader(filename string, src io.Reader) (formatted string, err error) {
	program, err := parser_lexer.ParseToProgram(filename, src)
	return render.Program(program), nil
}

func String(filename, src string) (formatted string, err error) {
	return Reader(filename, bytes.NewBufferString(src))
}
