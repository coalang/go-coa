package render

import "gitlab.com/coalang/go-coa/pkg/eval/astx"

func Placeholder(_ astx.Placeholder) string {
	return "..placeholder"
}

func PlaceholderType(_ astx.PlaceholderType) string {
	return "..placeholder.type"
}
