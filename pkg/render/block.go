package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func Block(block ast.Block) string {
	return blockTypeRaw(block.Type().(ast.BlockType)) + group(block.Content, "{", "}", SepTwo)
}

func BlockType(blockType ast.BlockType) string {
	return Call(ast.Call{
		C:    ast.Ctx{},
		T:    ast.BlockType{},
		Self: ast.Ref{IDs: []ast.ID{blockTypeRaw(blockType)}},
		Ee:   ast.Ref{IDs: []ast.ID{"block"}},
	})
}

func blockTypeRaw(blockType ast.BlockType) string {
	if blockType.IsZero() {
		return Map(ast.Map{})
	}

	if blockType.Self == (ast.AnyType{}) &&
		blockType.SelfName == "" &&
		blockType.Arg == (ast.AnyType{}) &&
		blockType.ArgName == "" {
		if blockType.Re == (ast.AnyType{}) {
			return ""
		} else {
			return Map(ast.Map{
				Keys: []ast.Node{
					nil,
				},
				Values: []ast.Node{
					ast.Cast{
						Ee: nil,
						Er: blockType.Re,
					},
				},
			})
		}
	}

	// fill in default for arg is exists
	var argDefault ast.Node
	arg := blockType.Arg
	switch arg := arg.(type) {
	case ast.StructType:
		if arg.HasDefault() {
			argDefault = arg.Default()
		}
	}

	return Map(ast.Map{
		Keys: []ast.Node{
			ast.Cast{
				Ee: ast.Symbol{Name: blockType.SelfName},
				Er: blockType.Self,
			},
			ast.Cast{
				Ee: ast.Symbol{Name: blockType.ArgName},
				Er: blockType.Arg,
			},
			ast.Cast{
				Ee: ast.NewRefSingleID(""),
				Er: blockType.Re,
			},
		},
		Values: []ast.Node{
			nil,
			argDefault,
			nil,
		},
	})
}
