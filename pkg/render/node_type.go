package render

import (
	"fmt"
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval/astx"
)

func NodeParen(node ast.Node) string {
	switch node := node.(type) {
	case ast.Call, ast.BlockType:
		return "(" + Node(node) + ")"
	default:
		return Node(node)
	}
}

func Node(node ast.Node) string {
	switch node := node.(type) {
	case ast.Type:
		return Type(node)
	case nil:
		return "_"
	case ast.Call:
		return Call(node)
	case ast.Cast:
		return Cast(node)
	case ast.Dispatcher2:
		return Dispatcher2(node)
	case ast.Label:
		return Label(node)
	case astx.Later:
		return Later(node)
	case ast.Map:
		return Map(node)
	case ast.Native:
		return Native(node)
	case ast.Num:
		return Num(node)
	case astx.Placeholder:
		return Placeholder(node)
	case ast.Pod:
		return Pod(node)
	case ast.Priority:
		return Priority(node)
	case ast.Symbol:
		return Symbol(node)
	case ast.Ref:
		return Ref(node)
	case ast.Rune:
		return Rune(node)
	case ast.Struct:
		return Struct(node)
	case ast.Stream:
		return Stream(node)
	case ast.Callable:
		if node.Native != nil {
			return Native(*node.Native)
		}
		return Block(*node.Block)
	default:
		return fmt.Sprintf(`"invalid node %T"`, node)
	}
}

func Type(typ ast.Type) string {
	switch typ := typ.(type) {
	case nil:
		return "_"
	case ast.Block:
		return Block(typ)
	case ast.BlockType:
		return BlockType(typ)
	case ast.DispatcherType2:
		return DispatcherType2(typ)
	case ast.Inter:
		return Inter(typ)
	case ast.MapType:
		return MapType(typ)
	case ast.NativeType:
		return NativeType(typ)
	case ast.NumType:
		return NumType(typ)
	case ast.Ref:
		return Ref(typ)
	case ast.RootType:
		return RootType(typ)
	case ast.RuneType:
		return "rune"
	case ast.StructType:
		return StructType(typ)
	case astx.PlaceholderType:
		return PlaceholderType(typ)
	case ast.PodType:
		return "pod"
	case ast.SymbolType:
		return "unique"
	case ast.AnyType:
		return "any"
	case ast.StreamType:
		return StreamType(typ)
	default:
		return fmt.Sprintf(`"invalid type %T"`, typ)
	}
}
