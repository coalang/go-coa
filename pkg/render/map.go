package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"strconv"
	"strings"
)

func Map(p ast.Map) string {
	typ := p.Type().(ast.MapType)
	if typ.IsRuneMap() {
		return RuneMap(p)
	}

	nodes := make([]ast.Node, len(p.Values))
	for i, val := range p.Values {
		if len(p.Keys) > i && p.Keys[i] != nil {
			// keyed
			nodes[i] = ast.Label{
				Er: Node(p.Keys[i]),
				Ee: val,
			}
		} else {
			// unkeyed
			nodes[i] = val
		}
	}

	src := group(nodes, "[", "]", SepOne)
	if p.T.ZeroKeyVal() {
		return src
	}
	return Cast(ast.Cast{
		Ee: ast.Ref{IDs: []ast.ID{src}},
		Er: p.T,
	})
}

func RuneMap(p ast.Map) string {
	text := ""
	for _, val := range p.Values {
		if val == nil {
			continue
		}
		text += string(val.(ast.Rune).R)
	}

	re := ""
	for _, line := range strings.Split(text, "\n") {
		quoted := strconv.Quote(line)
		re += quoted[1:len(quoted)-1] + "\n"
	}
	//             ↓ remove last \n
	return `"` + re[:len(re)-1] + `"`
}

func MapType(mapType ast.MapType) string {
	if mapType.IsRuneMap() {
		return "text"
	}
	callee := ast.Ref{IDs: []ast.ID{"map"}}
	if ast.IsAnyType(mapType.Key) && ast.IsAnyType(mapType.Value) {
		return Ref(callee)
	}
	return "(" + Call(ast.Call{
		Ee:   callee,
		Self: mapType.Key,
		Arg:  mapType.Value,
	}) + ")"
}
