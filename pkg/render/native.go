package render

import (
	"github.com/google/uuid"
	"gitlab.com/coalang/go-coa/pkg/ast"
)

var Natives = map[uuid.UUID]ast.Native{}

func Native(native ast.Native) string {
	if native_, ok := Natives[native.ID]; ok {
		native = native_
	}

	c := ast.Cast{
		Er: ast.Ref{IDs: []ast.ID{"native"}},
		Ee: ast.Symbol{Name: native.ID.String()},
	}
	if native.T.IsZero() {
		return Cast(c)
	}
	return Cast(ast.Cast{
		Ee: c,
		Er: native.T,
	})
}

func NativeType(_ ast.NativeType) string {
	return "native"
}
