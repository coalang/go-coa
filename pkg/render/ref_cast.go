package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"strings"
)

const BSlash = `\`

func BSlashRaw(lhs, rhs ast.Node) string {
	return NodeParen(lhs) + BSlash + NodeParen(rhs)
}

func Ref(ref ast.Ref) string {
	for i, refID := range ref.IDs {
		switch refID {
		case ast.IDCore:
			ref.IDs[i] = "."
		case ast.IDIndex:
			ref.IDs[i] = ".."
		case ast.IDSelf:
			ref.IDs[i] = ""
		case ast.IDArg:
			// as is
		}
	}
	re := strings.Join(ref.IDs, ".")
	return re
}

func Cast(cast ast.Cast) string {
	if cast.Ee == nil {
		if cast.Er == nil {
			return `_`
		}
		return BSlashRaw(cast.Er, nil)
	}
	if cast.Er == nil {
		return Node(cast.Ee)
	}
	return BSlashRaw(cast.Er, cast.Ee)
}
