package render

import "gitlab.com/coalang/go-coa/pkg/eval/astx"

func Later(_ astx.Later) string {
	return "..later"
}
