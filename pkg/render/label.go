package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

func LabelRaw(lhs, rhs string) string {
	if len(lhs)+2+len(rhs) > WrapThreshold {
		return lhs + ":\n" + utils.Indent(rhs)
	}
	return lhs + ": " + rhs
}

func Label(label ast.Label) string {
	switch label.Ee.(type) {
	case ast.Block:
		return LabelBlock(label.Er, label.Ee.(ast.Block))
	}
	return LabelRaw(label.Er, Node(label.Ee))
}

func LabelBlock(name string, block ast.Block) string {
	block = ast.Clone(block).(ast.Block)
	if block.T.ArgName == "" {
		block.T.ArgName = "_"
	}
	if block.T.SelfName == "" {
		block.T.SelfName = "_"
	}

	return LabelRaw(Call(ast.Call{
		T: block.T,
		Self: ast.Cast{
			Er: ast.Ref{IDs: []ast.ID{block.T.SelfName}},
			Ee: block.T.Self,
		},
		Ee: ast.Ref{IDs: []ast.ID{name}},
		Arg: ast.Cast{
			Er: ast.Ref{IDs: []ast.ID{block.T.ArgName}},
			Ee: block.T.Arg,
		},
	}), Block(ast.Block{
		T: ast.BlockType{
			Re: block.T.Re,
		},
		Content: block.Content,
	}))
}
