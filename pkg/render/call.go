package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"strings"
)

func Call(call ast.Call) string {
	self, callee, arg := call.Self, call.Ee, call.Arg
	if call.T.Self != nil && ast.EqualHash(ast.ToType(call.Self), call.T.Self) {
		self = ast.Cast{
			Ee: call.Self,
			Er: call.T.Self,
		}
	}
	if call.T.Arg != nil && EqualRender(ast.ToType(call.Arg), call.T.Arg) {
		arg = ast.Cast{
			Ee: call.Arg,
			Er: call.T.Arg,
		}
	}

	selfRendered := Node(self)
	rhsRendered := NodeParen(callee) + " " + NodeParen(arg)
	if len(strings.Split(selfRendered, "\n")[0]) >= WrapThreshold {
		return selfRendered + "\n" + utils.Indent(
			rhsRendered,
		)
	}
	return selfRendered + " " + rhsRendered
}
