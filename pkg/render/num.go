package render

import "gitlab.com/coalang/go-coa/pkg/ast"

func Num(num ast.Num) string {
	if num.Sub != ast.Normal {
		return num.Sub.String()
	}
	rat, err := num.Rat()
	if err != nil {
		return "nan"
	}
	return rat.RatString()
}

func NumType(numType ast.NumType) string {
	if numType.Unit.IsZero() {
		return "num"
	}
	return "num.unit." + numType.Unit.Name
}
