package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func Priority(priority ast.Priority) string {
	return group(priority.B.Content, "(", ")", SepTwo)
}
