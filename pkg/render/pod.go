package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func Pod(p ast.Pod) string {
	return Node(p.Inside)
}
