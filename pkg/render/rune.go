package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"strconv"
)

func Rune(rune2 ast.Rune) string {
	return strconv.QuoteRune(rune2.R)
}
