package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

func Inter(inter ast.Inter) string {
	return Cast(ast.Cast{
		Ee: StructTypeRaw(inter.T),
		Er: ast.Ref{IDs: []ast.ID{"inter"}},
	})
}

func Struct(s ast.Struct) string {
	keys, values := make([]ast.Node, len(s.T.Keys)), make([]ast.Node, len(s.Values))
	for i, key := range s.T.Keys {
		keys[i] = ast.Symbol{Name: key}
	}
	for i, val := range s.Values {
		values[i] = val
	}
	return Call(ast.Call{
		Ee:   ast.Ref{IDs: []ast.ID{"struct"}},
		Self: StructTypeRaw(s.Type().(ast.StructType)),
		Arg: ast.Map{
			Keys:   keys,
			Values: values,
		},
	})
}

func StructType(structType2 ast.StructType) string {
	return Cast(ast.Cast{
		Ee: StructTypeRaw(structType2),
		Er: ast.Ref{IDs: []ast.ID{"struct.type"}},
	})
}

func StructTypeRaw(structType ast.StructType) ast.Map {
	keys, values := make([]ast.Node, len(structType.Keys)), make([]ast.Node, len(structType.Values))
	for i, key := range structType.Keys {
		keys[i] = ast.Symbol{Name: key}
	}
	for i, val := range structType.Defaults {
		values[i] = val
	}
	return ast.Map{
		Keys:   keys,
		Values: values,
	}
}
