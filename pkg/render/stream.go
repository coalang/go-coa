package render

import "gitlab.com/coalang/go-coa/pkg/ast"

func StreamType(typ ast.StreamType) string {
	return Call(ast.Call{
		Self: ast.Ref{IDs: []ast.ID{typ.Direction.String()}},
		Ee:   ast.Ref{IDs: []ast.ID{"stream"}},
		Arg:  typ.Value,
	})
}

func Stream(stream ast.Stream) string {
	return Cast(ast.Cast{
		Er: stream.T,
		Ee: ast.Ref{IDs: []ast.ID{stream.ID.String()}},
	})
}
