package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/utils"
)

type Sep = string

const (
	SepZero Sep = ", "
	SepOne  Sep = ",\n"
	SepTwo  Sep = "\n\n"
)

func Program(program ast.Program) string {
	re := ""
	for _, node := range program.Content {
		if node == nil {
			continue
		}
		if nodeString := Node(node); nodeString != "" {
			re += nodeString + SepTwo
		}
	}
	return re
}

func group(nodes []ast.Node, open, close string, sep Sep) string {
	if len(nodes) == 0 {
		return open + close
	}
	re := open
	lastIndex := len(nodes) - 1
	for i, node := range nodes {
		re += Node(node)
		if i != lastIndex {
			re += SepZero
		}
	}
	re += close
	//goland:noinspection GoBoolExpressions
	if (MultilineThreshold > 0 && len(nodes) > MultilineThreshold) || len(re) > WrapThreshold {
		re = open + "\n"
		for _, node := range nodes {
			re += utils.Indent(Node(node)) + sep
		}
		re += close
	}
	return re
}
