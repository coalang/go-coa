package render

import "gitlab.com/coalang/go-coa/pkg/ast"

func RootType(_ ast.RootType) string {
	return "type"
}
