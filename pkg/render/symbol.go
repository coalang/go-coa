package render

import "gitlab.com/coalang/go-coa/pkg/ast"

func Symbol(symbol ast.Symbol) string {
	return "#" + symbol.Name
}
