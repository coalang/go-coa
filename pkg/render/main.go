package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
)

var (
	WrapThreshold      = 60
	MultilineThreshold = -1
)

func EqualRender(a, b ast.Node) bool {
	return Node(a) == Node(b)
}
