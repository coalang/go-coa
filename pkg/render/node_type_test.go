package render

import (
	"gitlab.com/coalang/go-coa/pkg/ast"
	"gitlab.com/coalang/go-coa/pkg/eval/astx"
	"gitlab.com/coalang/go-coa/pkg/utils"
	"testing"
)

func TestNode(t *testing.T) {
	WrapThreshold = 123456
	type test struct {
		Node ast.Node
		Want string
	}
	tests := map[string]test{
		"block/any": {
			Node: ast.Block{
				T: ast.AnyBlockType(),
			},
			Want: "{}",
		},
		"block": {
			Node: ast.Block{
				T: ast.AnyBlockType(),
				Content: []ast.Node{
					astx.Placeholder{},
					astx.Placeholder{},
				},
			},
			Want: "{..placeholder, ..placeholder}",
		},
		"block/type": {
			Node: ast.Block{
				T: ast.BlockType{
					Self:     astx.PlaceholderType{},
					SelfName: "a",
					Re:       astx.PlaceholderType{},
					Arg:      astx.PlaceholderType{},
					ArgName:  "b",
				},
			},
			Want: `[..placeholder.type\a: _, ..placeholder.type\b: _, ..placeholder.type\: _]{}`,
		},
		"blocktype/zero": {
			Node: ast.BlockType{},
			Want: "[] block _",
		},
		"blocktype": {
			Node: ast.BlockType{
				Self:     astx.PlaceholderType{},
				SelfName: "a",
				Re:       astx.PlaceholderType{},
				Arg:      astx.PlaceholderType{},
				ArgName:  "b",
			},
			Want: `[..placeholder.type\a: _, ..placeholder.type\b: _, ..placeholder.type\: _] block _`,
		},
		"call": {
			Node: ast.Call{
				Ee: ast.Ref{
					IDs: []ast.ID{"callee"},
				},
				Self: ast.Ref{
					IDs: []ast.ID{"self"},
				},
				Arg: ast.Ref{
					IDs: []ast.ID{"arg"},
				},
			},
			Want: "self callee arg",
		},
		"cast": {
			Node: ast.Cast{
				Er: ast.Ref{IDs: []ast.ID{"type"}},
				Ee: ast.Ref{IDs: []ast.ID{"node"}},
			},
			Want: `type\node`,
		},
		"dispatcher2/zero": {
			Node: ast.Dispatcher2{},
			Want: "_ dispatcher []",
		},
		"dispatchertype2/zero": {
			Node: ast.DispatcherType2{},
			Want: "dispatcher",
		},
		"label": {
			Node: ast.Label{
				Er: "name",
				Ee: ast.Ref{IDs: []ast.ID{"obj"}},
			},
			Want: "name: obj",
		},
		"map/zero": {
			Node: ast.Map{},
			Want: "[]",
		},
		"map/rune": {
			Node: ast.Map{
				T: ast.MapType{
					Value: ast.RuneType{},
				},
				Values: []ast.Node{
					ast.Rune{R: 'a'},
					ast.Rune{R: '\x30'}, // \x30 is digit 0
					ast.Rune{R: 'c'},
				},
			},
			Want: `"a0c"`,
		},
		"map/unkeyed": {
			Node: ast.Map{
				C:    ast.Ctx{},
				T:    ast.MapType{},
				Keys: nil,
				Values: []ast.Node{
					ast.Ref{IDs: []ast.ID{"val"}},
				},
			},
			Want: "[val]",
		},
		"map/keyed": {
			Node: ast.Map{
				C:    ast.Ctx{},
				T:    ast.MapType{},
				Keys: nil,
				Values: []ast.Node{
					ast.Label{
						Er: "key",
						Ee: ast.Ref{IDs: []ast.ID{"val"}},
					},
				},
			},
			Want: "[key: val]",
		},
		"maptype/zero": {
			Node: ast.MapType{},
			Want: "(_ map _)",
		},
		"native": {
			Node: ast.Native{
				ID: utils.UUIDFromMust("356bd6d5-daa6-454a-884f-236f7ccd32ff"),
			},
			Want: `native.356bd6d5-daa6-454a-884f-236f7ccd32ff`,
		},
		"nativetype/zero": {
			Node: ast.NativeType{},
			Want: "native",
		},
		"nil": {
			Node: nil,
			Want: "_",
		},
		"num/invalid": {
			Node: ast.Num{
				Num:   "invalid",
			},
			Want: "nan",
		},
		"num/int": {
			Node: ast.Num{
				Num:   "1",
			},
			Want: "1",
		},
		"num/nan": {
			Node: ast.Num{
				Sub: ast.NaN,
			},
			Want: "nan",
		},
		"priority/zero": {
			Node: ast.Priority{
				B: ast.Block{},
			},
			Want: "()",
		},
		"ref": {
			Node: ast.Ref{
				C:   ast.Ctx{},
				IDs: []ast.ID{"a", "b", "c"},
			},
			Want: "a.b.c",
		},
		"roottype": {
			Node: ast.RootType{},
			Want: "type",
		},
		"rune": {
			Node: ast.Rune{
				C: ast.Ctx{},
				R: 'a',
			},
			Want: `'a'`,
		},
		"runetype": {
			Node: ast.RuneType{},
			Want: "rune",
		},
		"struct/zero": {
			Node: ast.Struct{},
			Want: "[] struct []",
		},
		"structtype/zero": {
			Node: ast.StructType{},
			Want: `struct.type\[]`,
		},
		"later/zero": {
			Node: astx.Later{},
			Want: "..later",
		},
		"placeholder/zero": {
			Node: astx.Placeholder{},
			Want: "..placeholder",
		},
		"placeholdertype": {
			Node: astx.PlaceholderType{},
			Want: "..placeholder.type",
		},
		"pod/zero": {
			Node: ast.Pod{},
			Want: "_",
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			if got := Node(test.Node); got != test.Want {
				t.Errorf("want %s, got %s", test.Want, got)
			}
		})
	}
}
