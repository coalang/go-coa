package render

import "gitlab.com/coalang/go-coa/pkg/ast"

func Dispatcher2(dispatcher2 ast.Dispatcher2) string {
	arg := ast.Map{
		Keys:   make([]ast.Node, len(dispatcher2.Keys)),
		Values: make([]ast.Node, len(dispatcher2.Values)),
	}
	for i, value := range dispatcher2.Values {
		arg.Values[i] = value
	}
	return Cast(ast.Cast{
		Er: ast.Ref{IDs: []ast.ID{"dispatcher"}},
		Ee: arg,
	})
}

func DispatcherType2(_ ast.DispatcherType2) string {
	return "dispatcher"
}
