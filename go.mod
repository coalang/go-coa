module gitlab.com/coalang/go-coa

go 1.16

require (
	github.com/alecthomas/participle/v2 v2.0.0-alpha3
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1
	github.com/gin-gonic/gin v1.7.2
	github.com/google/uuid v1.2.0
	github.com/json-iterator/go v1.1.11
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/traefik/yaegi v0.9.19
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
)
