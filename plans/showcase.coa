"
# Showcase
"

(
    "
    ## Print to stdout and stderr

    stdout and stderr are both streams. To send data, use the `→` block.
    "

    "Hello!\n" → ..io.stdout

    "some error message"\text.ln → ..io.stderr
)

(
    "
    ## Read from stdin

    stdin is a stream. To receive all data, use the `→` block. To get only the data you want to, use a block like `→until`.
    "

    ..io.stdin →until '\n'
)

(
    "
    ## Tee
    "

    args: [
        output_path: [
            name: _,
            required: true,
        ]
    ]\..lang.posix.args parse ..os.args

    ..io.stdin → ..io.stdout → (args.output_path open _)
)

(
    "
    ## Misc.
    "

    "### Initialize stuff"

    book: [
        name\text,
        isbn\..lang.isbn,
    ]\struct.type

    a_book: ["Death's End", "978-0765377104"]\book

    "A book: (a_book)\n" → ..io.stdout

    "### Groups"

    "Structs: A group of data ordered by index and key."

    vertex: [x\rat, y\rat]\struct.type

    "Maps: Like structs, but keys and their types aren't part of the type definition."

    vertexes: [vertex]\map

    "Block: A list of code that can be evaluated."

    hello: {"Hello, world!" ln→ ..io.stdout}

    "Priority: Like blocks, but the code inside is immediately evaluated."

    result: ("Question: " ln→ ..io.stdout, ..io.stdin →until '\n')

    "Stream: Like maps, but data cannot be indexed."

    ..io.stdout each {"I got (.)" ln→ ..io.stdout}
)
