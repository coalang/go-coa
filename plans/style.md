# Code Style

## Go

- Method names must be verbs (e.g. `Validate`) if the method has side effects,
  and must not be verbs (e.g. `Valid`) if not.
- If a variable fulfils an interface (e.g. `pkg/ast.Node`)
  and the code is certain that it is of a type,
  use the interface method instead of the concrete type's method or field
  (e.g. `pkg/ast.Node.Ctx()` returns the same value as `pkg/ast.Native.C`).
- Don't use `copy()`, I am traumatized by it (shallow copy)
- use `.Errorf` to make nested errors (eventually make own equivalent)
- use the format `<what the code was doing>: <message or {nested error}>` for error messages
- [use "filepath" or "file path" depending on context](https://english.stackexchange.com/a/428231)
  ([archived](https://web.archive.org/web/20210506180609/https://english.stackexchange.com/questions/428215/filepath-or-file-path))
