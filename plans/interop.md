# Interop

|          | Internal  | External     |
|----------|-----------|--------------|
| Between  | itself    | two runners  |
| Encoding | pkg/ast   | pkg/encoding |
| IPC      | N/A       | gRPC         |
| Platform | dependent | independent  |

## Internal

### Example

```go
translate.ToNode(s, reflect.ValueOf(json.Marshal))
```

## External

### Example

```mermaid
sequenceDiagram
    participant r1 as Go Coa
    participant r2 as Qt Wrapper
    r1->>r2: construct QApplication
    r2->>r1: external of QApplication
    r1->>r2: construct QMainWindow
    r2->>r1: external of QMainWindow
    r1-)r2: destruct QMainWindow
    r1-)r2: destruct QApplication
```
