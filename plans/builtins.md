# Builtins

`ee`, `er`, etc means `<subject>ee` e.g. `callee`

- `source → sink` pipe
    - `..io.stdin → ..io.stdout`
- Arithmetic
    - `\num + \num`
    - `\num - \num`
    - `\num * \num`
    - `\num / \num`
    - `\num % \num` modulo
    - `\num ** \num` power
    - `\num // \num` root
    - `\num ! _` factorial
    - `\num abs _` absolute value
- Comparison
    - `\any = \any`
    - `\any ≠ \any`
    - `\any > \any`
    - `\any ≥ \any`
    - `\any < \any`
    - `\any ≤ \any`
- Map/list
    - `in: [item: any, \item, \(any map item), bool]`
    - `index: [key: any, val: any, \(key map val), \key, \val]` get object at index `y` in stream `x`
    - `x index y: z` set object at index `y` in stream `x` to `z`
- Stream
    - `accrue: [item: any, \(item stream _), \([\item, \item, \item] block _), item]`
    - `start range stop`
- Boolean
    - `x ∧ y` and
    - `x ∨ y` or
    - `x ⊻ y` xor
