# Pods

Pods are the equivalent of packages (e.g. the `coreutils` package in `ubuntu-groovy-main`) in Coa.

## File Embedding

Coa code can embed files if they can access the pod that the embedded files are in.

- `csv` *cannot embed `locales/` since it's hidden*
- `i18n/`
    - `export.coa`
    - `locales.coa` *can embed `_locales/`*
    - `_locales/`
        - `en-ca.json`

## Naming

Hidden names (starts with an `_` underscore) can only be accessed by pods in the same pod or sub-pods thereof.

- `csv/`
    - `_format/` *can only be accessed by pods in `csv/` or sub-pods thereof*
    - `formats/` *can be accessed by others*
    - `export.coa`

## Structure

Pods have two different file structures for single-file and multi-file pods. Multi-file pods can embed files, while
single-file pods cannot.

### Multi-File Pod

- `i18n/`
    - `export.coa`
    - `locales/`
        - `en-ca.json`

All non-hidden (doesn't start with an `_` underscore) variables of `i18n/export.coa` are exported.

### Single-File Pod

- `regex.coa`

`regex.coa` should have a variable named `export` that has the values that should be exported.
