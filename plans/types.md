Note: only lists types used in Coa, not in implementations of Coa.

- `block` A function.
- `dispatcher` Provides multiple dispatch.
- `native` An object that runs functions on or under the level of the implementation.
- `num` A rational number (sorry no irrationals yet)
    - `unit` Units for numbers; e.g.
        - `metre`
        - `millimetre` / `metre * 1e-3`
        - `metre3` / `metre ** 3`
- `rune` (will be deprecated) A single 
- `stream` Generalisation for list of objects
    - `map` Immutable list of objects in memory
    - `chan` List of objects in a channel
- `struct` Object organised by order and name
