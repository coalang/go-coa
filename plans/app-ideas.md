
# App Ideas for Coa

## Paper Summary

- Scan paper DOI or smth
- Get paper abstract, conclusion, and diagram captions
- Optionally translate

## Audio/Video Editor

- Use Coa code to add effects
    - `transition[duration]: [pre, post]{(pre fade.out duration) (duration +.overlap _) (post fade.in duration)}, video1 ((+ 5000 ms) transition _) video2`
    - `piano: "piano.sf2" path _ soundfont.load _, notes: "notes.txt" path _ notes.load _, notes piano.play _`
- GUI (probably PyQt/PySide) for choosing when to play audio, etc
- Server (Coa) receives commands and returns audio
- Client (GUI, Python?) sends commands and provides interface

## Writing Software (like LibreOffice Write) (nevermind)

- Text is not English text, but rather only the ideas
- The ideas are automatically put together
- Can easily move around groups of words

I like pancakes because:
- fluffiness
- tastiness

I like pancakes because they're fluffy and tasty.

```python3
import math

save(volume(0.1, math.sin), 10)
```

```coa
math.sin
  volume (+ 10 %)
  → "output.flac"
```
