# Node Types

## Types

- `AnyType` anything is coercible to this *deprecated*, use `Inter` with no requirements
- `BlockType` the type of blocks
- `DispatcherType2`
- `ExternalType`
- `Inter`
- `MapType`
- `NativeType` the type of "native" functions
- `NumType`
- `PlaceholderType`
- `PodType`
- `PromiseType`
- `RootType` the type of types
- `RuneType` *deprecated*, use `NumType` with unit
- `StreamType`
- `StructType`
- `UniqueType`

## Non-types

- `Block`
- `Call`
- `Cast`
- `Dispatcher2`
- `External`
- `Label`
- `Later`
- `Map`
- `Native` "native" (Go) functions
- `Num`
- `Placeholder`
- `Pod`
- `Priority`
- `Promise`
- `Ref`
- `Rune`
- `Stream`
- `Struct`
- `Unique`
