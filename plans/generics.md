```
email: ...re.pattern_type\"<elided>"

phone: [num\]{it >= 0 && it <= 1e31}

contact: [~
    text\name,
    ( email || phone )\method,
]

"(...)  is evaluated immediately, returns last non-nil value"
"[~...] is evaluated immediately, returns struct of scope"
"[...]  is evaluated immediately, returns map of content"
"{...}  is evaluated on-demand,   returns block of content, lone scope"

contacts: _ map contact

stream: inter\[~
    type\_value,
    block\[_value\, _, _]\send,
    block\[_, _, _value\]\receive,
    block\[]\close,
    bool\closed,
]
```
