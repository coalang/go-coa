package lib

import (
	"io"
	"strconv"
)

type Value byte

func Split(ch chan Value, sep Value) chan []Value {
	re := make(chan []Value)
	go func() {
		cache := make([]Value, 0)
		for v := range ch {
			cache = append(cache, v)
			if v == sep {
				re <- cache
				cache = make([]Value, 0)
			}
		}
	}()
	return re
}

func Until(ch chan Value, sep Value) chan Value {
	re := make(chan Value)
	go func() {
		for v := range ch {
			if v == sep {
				close(re)
				break
			}
			re <- v
		}
	}()
	return re
}

func Lit(source []Value) chan Value {
	re := make(chan Value)
	go func() {
		for _, v := range source {
			re <- v
		}
	}()
	return re
}

func Writer(ch chan Value, writer io.Writer) {
	var err error
	for v := range ch {
		_, err = writer.Write([]byte{byte(v)})
		if err != nil {
			panic(err)
		}
	}
}

func Join(ch chan []Value, sep Value) chan Value {
	re := make(chan Value)
	go func() {
		for vs := range ch {
			for _, v := range vs {
				re <- v
			}
			re <- sep
		}
	}()
	return re
}

func String(ch chan Value) chan []Value {
	re := make(chan []Value)
	go func() {
		for v := range ch {
			re <- []Value(strconv.FormatInt(int64(v), 10))
		}
	}()
	return re
}

func Range(start, stop int) chan Value {
	re := make(chan Value)
	go func() {
		for i := start; i < stop; i++ {
			re <- Value(i)
		}
		close(re)
	}()
	return re
}
