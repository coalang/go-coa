package ast

import (
	"gitlab.com/coalang/go-coa/next/ast/internal/
	"gitlab.com/coalang/go-coa/next/ast/internal/gen"
)

func RunNodes(r *Runner, nodes *Nodes) (*Nodes, *Error) {
	var err error
	for i, node := range nodes.Content {
		nodes.Content[i], err = RunNode(r, node)
		if err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

func RunNode(r *Runner, n *Node) (*Node, *Error) {
	switch content :=n.Content.(type) {
	case *Node_Type:
		t, err := RunType(r, content.Type)
		if err != nil {
			return nil, err
		}
		return &Node{Content: &Node_Type{Type: t}}, nil
	case *Node_Name:
	case *Node_Struct:
	case *Node_Func:
	case *Node_List:
	case *Node_Map:
	default:
		panic("Node.Content invalid")
	}
}

func RunType(r *Runner, t *Type) (*Type, *Error) {
	switch content := t.Content.(type) {
	case *Type_Struct:
	case *Type_Macro:
	case *Type_List:
	case *Type_Map:
	default:
		panic("Type.Content invalid")
	}
}

func RunName(r *Runner, n *Name) (*Node, *Error) {
	if len(n.Ids) == 0 {
		return nil, nil
	}
	node, ok := r.Get(n.Ids[0])
	if !ok {
		return nil, ErrorCaller(n, (*Error)(&gen.Error{
			Content: &gen.Error_NotFound{NotFound: &gen.ErrorNotFound{
				Name:  n,
				Index: 0,
			}},
		}))
	}
	return node, nil
}

func RunNameOnTarget(r *Runner, n *Name, target *Node) (*Node, *Error) {
}
