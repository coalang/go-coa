syntax = "proto3";
option go_package = "gitlab.com/coalang/go-coa/next/ast";
package ast;

service Runner {
  rpc Run(RunArg) returns (RunRe);
}

message RunArg {
  Env env = 1;
  Node node = 2;
}

message RunRe {
  Env env = 1;
  Node node = 2;
  Error error = 3;
}

message Error {
  Origin o = 1;
  Origin oInternal = 2;
  oneof content {
    ErrorNotFound notFound = 3;
    string other = 4;
  }
}

message ErrorNotFound {
  Name name = 1;
  int32 index = 2;
}

message Frame {
  Origin o = 1;
  bool lone = 2;
  bool pure = 3;
  string reason = 4;
  uint32 length = 5;
  Line line = 6;
}

message Scope {
  map<string, Node> content = 1;
}

message Env {
  Frame frame = 1;
  Scope root = 2;
  Scope local = 3;
  Env outer = 4;
}

message Origin {
  string filename = 1;
  uint32 startLine = 2;
  uint32 startCol = 3;
  uint32 startOffset = 4;
  uint32 endLine = 5;
  uint32 endCol = 6;
  uint32 endOffset = 7;
}

message Line {
  Origin o = 1;
  string name = 2;
  Node source = 3;
  repeated Node joints = 4;
  Node sink = 5;
}

message LineType {
  Origin o = 1;
  Type source = 2;
  Type sink = 3;
}

message Node {
  oneof content {
    Type type = 1;
    Name name = 2;
    Struct struct = 3;
    Macro func = 4;
    List list = 5;
    Map map = 6;
  }
}

message Type {
  // all types are immutable
  oneof content {
    StructType struct = 1;
    MacroType macro = 2;
    ListType list = 3;
    MapType map = 4;
  }
}

message Name {
  Origin o = 1;
  repeated string ids = 2;
}

message Macro {
  Origin o = 1;
  MacroType T = 2;
  oneof content {
    Nodes nodes = 3;
    string external = 4;
  }
}

message Nodes {
  repeated Node content = 1;
}

message MacroType {
  Origin o = 1;
  Type source = 2;
  StructType args = 3;
  Type sink = 4;
}

message Struct {
  Origin o = 1 ;
  StructType t = 2;
  bool mutable = 3;
  repeated Node fields = 4;
}

message StructType {
  Origin o = 1;
  repeated string fieldNames = 2;
  repeated Type fields = 3;
}

message List {
  Origin o = 1;
  ListType t = 2;
  bool mutable = 3;
  repeated Node values = 4;
}

message ListType {
  Origin o = 1;
  Type value = 2;
}

message Map {
  Origin o = 1;
  MapType t = 2;
  bool mutable = 3;
  repeated Node keys = 4;
  repeated Node values = 5;
}

message MapType {
  Origin o = 1;
  Type key = 2;
  Type value = 3;
}
