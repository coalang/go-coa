package ast

import (
	"fmt"
	"gitlab.com/coalang/go-coa/next/ast/internal/gen"
	"runtime"
	"sync"
)

type Env = gen.Env
type ErrorNotFound = gen.ErrorNotFound
type Error_NotFound = gen.Error_NotFound
type Error_Other = gen.Error_Other
type Frame = gen.Frame
type Line = gen.Line
type LineType = gen.LineType
type List = gen.List
type ListType = gen.ListType
type Macro = gen.Macro
type MacroType = gen.MacroType
type Macro_External = gen.Macro_External
type Macro_Nodes = gen.Macro_Nodes
type Map = gen.Map
type MapType = gen.MapType
type Name = gen.Name
type Node_Func = gen.Node_Func
type Node_List = gen.Node_List
type Node_Map = gen.Node_Map
type Node_Name = gen.Node_Name
type Node_Struct = gen.Node_Struct
type Node_Type = gen.Node_Type
type Nodes = gen.Nodes
type RunArg = gen.RunArg
type RunRe = gen.RunRe
type Scope = gen.Scope
type Struct = gen.Struct
type StructType = gen.StructType
type Type = gen.Type
type Type_List = gen.Type_List
type Type_Macro = gen.Type_Macro
type Type_Map = gen.Type_Map
type Type_Struct = gen.Type_Struct

type Node = gen.Node

type Origin gen.Origin

func OriginCaller(skip int) *Origin{
	_, filename, line, _ := runtime.Caller(skip+1)
	return (*Origin)(&gen.Origin{
		Filename:  filename,
		StartLine: uint32(line),
	})
}

func (o *Origin) String() string {
	re := o.Filename
	if o.StartLine != 0 {
		re += fmt.Sprintf(":%d:%d", o.StartLine, o.StartCol)
	}
	if o.EndLine != 0 {
		re += fmt.Sprintf("→%d:%d", o.EndLine, o.EndCol)
	}
	return re
}

type Error gen.Error

type HasOrigin interface {
	GetO() *gen.Origin
}

func ErrorCaller(hasOrigin HasOrigin,e *Error) *Error {
	e.O = hasOrigin.GetO()
	e.OInternal = (*gen.Origin)(OriginCaller(1))
	return e
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s (%s)\n%s", (*Origin)(e.O), (*Origin)(e.OInternal), e.Content)
}

type Runner struct {
	r *gen.Env
	lock sync.RWMutex
}

func (r *Runner) Get(key string) (*Node, bool) {
	r.lock.RLock()
	defer r.lock.RUnlock()
	node, ok := r.r.Root.Content[key]
	if ok {
		return node, true
	}
	node, ok = r.r.Local.Content[key]
	if !ok {
		return nil, false
	}
	return node, true
}

func (r *Runner) Set(key string, value *Node) {
	r.lock.Lock()
	defer r.lock.Unlock()
	r.r.Local.Content[key] = value
}
