package main

import (
	. "gitlab.com/coalang/go-coa/next/lib"
	"os"
)

func main() {
	Writer(Join(String(Range(1, 100)), '\n'), os.Stdout)
}
