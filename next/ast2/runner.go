package ast2

import "sync"

type Runner struct {
	Frame Frame
	Root Scope
	Local Scope
	Outer *Runner
	lock sync.RWMutex
}

type Frame struct {
	O Origin
	Lone bool
	Pure bool
	Reason string
	Len uint
	Line *Line
}
