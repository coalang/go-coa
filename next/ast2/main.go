package ast2

type Node interface {
	CoaString() string
	Eval(*Runner) (Node, error)
}

type Callable interface {
	Node
	Call(*Runner, *Line) (Node, error)
}

type Type interface {
	Node
	Convert(Node) (Node, error)
}

type Origin struct {
	Filename string
	Start    [3]uint
	End      [3]uint
}

type Line struct {
	O      Origin
	Name   string
	Source Node
	Calls  []Callable
	Sink   Node
}
